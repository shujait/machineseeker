<?php
if (!defined('BASEPATH'))
 exit('No direct script access allowed');


    function currentDate()
	{
		return date('Y-m-d H:i:s');
	}

    function special_characterDe($val) {
        $searchkeywords = array("ü", "ä", "ö", "ß", "Ü", "Ä", "Ö");
        $replacekeywords = array("ue", "ae", "ou", "ss", "Ue", "Ae", "Oe");
        return str_replace($searchkeywords, $replacekeywords,$val );
    }

    function slug($str){
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);
        return $str;
    }

   function print_rm($data)
	{
            echo '<pre>';
            print_r($data);exit;
	}
	function userAgent()
	{
		   $CI =& get_instance();
	    $CI->load->library('user_agent');
	    return $CI->agent->is_mobile();
	}
	function dump($data)
    {
        echo '<pre>';
        print_r($data);exit;
    }
	

    function checkRightAccess($module_id,$role_id,$can){
        $CI = & get_Instance();
        $CI->load->model('Module_rights_model');
        
        $fetch_by = array();
        $fetch_by['ModuleID'] = $module_id;
        $fetch_by['RoleID']   = $role_id;
        $fetch_by[$can]       = 1;
        
        $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
        if($result){
            return true;
        }else{
            return false;
        }
    }
    
    
    function NullToEmpty($data)
    {
        $returnArr = array();
        if (isset($data[0])) // checking if array is a multi-dimensional one, if so then checking for each row
        {
            $i = 0;
            foreach ($data as $row)
            {
                foreach ($row as $key => $value) {
                    if (null === $value) {
                        $returnArr[$i][$key] = "";
                    }else{
                        $returnArr[$i][$key] = $value;
                    }
                }
                $i++;
            }
        }else{
            foreach ($data as $key => $value) {
                if (null === $value) {
                    $returnArr[$key] = "";
                }else{
                    $returnArr[$key] = $value;
                }
            }
        }
        return $returnArr;
    }

    function checkUserRightAccess($module_id,$user_id,$can){
        $CI = & get_Instance();
        //$CI->load->model('Modules_users_rights_model');
        $CI->load->model('Module_rights_model');
        $fetch_by = array();
        $fetch_by['ModuleID'] = $module_id;
        $fetch_by['RoleID']   = $CI->session->userdata['admin']['RoleID'];
        //$fetch_by['UserID']   = $user_id;
        $fetch_by[$can]       = 1;

        $result = $CI->Module_rights_model->getWithMultipleFields($fetch_by);
        //$result = $CI->Modules_users_rights_model->getWithMultipleFields($fetch_by);
        if($result){
            return true;
        }else{
            return false;
        }
    }
	
   function getSystemLanguagesBySession(){
       $CI = & get_Instance();
       $CI->load->model('System_language_model');
        $fetch_by = array();
		$session_lang = $CI->session->userdata('lang');
		if($session_lang){
		$fetch_by['ShortCode'] = $session_lang;
		}else{
                    $fetch_by['ShortCode'] = 'de';
		}
        $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
        $languages = $CI->System_language_model->getAllLanguages($result->SystemLanguageID);
        return $languages;
   }
   function getSystemLanguages($system_language_id = 1){
       $CI = & get_Instance();
       $CI->load->model('System_language_model');
       $languages = $CI->System_language_model->getAllLanguages($system_language_id);
       return $languages;
   }
   
    function getDefaultLanguage()
   {
        
        $CI = & get_Instance();
        $CI->load->Model('System_language_model');
        $fetch_by = array();
		$session_lang = $CI->session->userdata('lang');
		if($session_lang){
		$fetch_by['ShortCode'] = $session_lang;
		}else{
        $fetch_by['IsDefault'] = 1;
		}
        $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
        return $result;
   }

   function getLanguageByCode($code = 'de')
   {
        
        $CI = & get_Instance();
        $CI->load->Model('System_language_model');
        $fetch_by = array();
		$fetch_by['ShortCode'] = $code;
        $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
        return $result;
   }

    function getDefaultLanguageByDefault()
   {
        
        $CI = & get_Instance();
        $CI->load->Model('System_language_model');
        $fetch_by = array();
		$session_lang = $CI->session->userdata('lang');
		
        $fetch_by['IsDefault'] = 1;
		
        $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
        return $result;
   }



   function getDefaultAdvertisement($id){
   	$CI = & get_Instance();
    $CI->load->Model('Advertise_model');
    $fetch_by = array();
    $lng  = getDefaultLanguageByDefault();
	
    $result = $CI->Advertise_model->getJoinedData(false,'AdvertiseID','advertises_text.SystemLanguageID = '.$lng->SystemLanguageID.' AND advertises.AdvertiseID = '.$id);
    //echo $CI->db->last_query();exit;
   
    if($result){
    	return $result[0];
    }else{
    	return false;
    }

   }

	function getDefaultLogo()
   {
        $CI = & get_Instance();
        $CI->load->Model('System_language_model');
        $fetch_by = array();
		$rec = getDefaultLanguage();
		if($rec->Logo){
		$logo = $rec->Logo;
		}else if(empty($logo)){
        $fetch_by['IsDefault'] = 1;
        $result = $CI->System_language_model->getWithMultipleFields($fetch_by);
        $logo = $result->Logo;	
		if(empty($logo)){
		$logo = 'assets/frontend/images/logo-de.png';
		}
	}
		return base_url($logo);
   }
   
   
   function getAllActiveModules($role_id,$system_language_id,$where){
        $CI = & get_Instance();
        $CI->load->Model('Module_rights_model');
        $result = $CI->Module_rights_model->getModulesWithRights($role_id,$system_language_id,$where);
        return $result;
   }
   
   
   function getActiveUserModule($user_id,$system_language_id,$where){
        $CI = & get_Instance();
        $CI->load->Model('Modules_users_rights_model');
        $result = $CI->Modules_users_rights_model->getModulesWithRights($user_id,$system_language_id,$where);
        return $result;
   }
	
   function checkAdminSession()
   {
	   $CI = & get_Instance();
	   if($CI->session->userdata('admin'))
	   {
		  $rec = get_user_info(array('a.UserID' => $CI->session->userdata['admin']['UserID'],'a.IsActive' => 1));
		   if($rec){		   
		   return true;
		   }else{
			redirect(base_url('cms/account/logout'));   
		   }
		   
	   }else
	   {
		   redirect($CI->config->item('base_url'));
	   }
   }
   
   
   function sendEmail($data= array(),$files = false)
	{
	
		 $options = [
             'protocol' => 'smtp',
             'smtp_host' => 'xb7.serverdomain.org',
             'smtp_port' => '587',
             'smtp_user' => 'xb7191p1',
             'smtp_pass' => 'K98%BgPuZ',
             'charset' => 'utf-8',
             'newline' => "\r\n",
             'mailtype' => 'html',
             'validation' => 'true',
		 	'smtp_crypto' => 'tls',
		 ];
		
		 $CI = & get_Instance();
		 $CI->load->library('email');
		 $CI->email->initialize($options);

//		$CI = & get_Instance();
//		$CI->load->library('email');
		$CI->email->from($data['from'],$data['from_name']);
		$CI->email->to($data['to']);
		$CI->email->subject($data['subject']);
		$CI->email->message($data['body']);
		$CI->email->set_mailtype('html');
		if($files){
			foreach($files as $file){
				$CI->email->attach(FCPATH.'/'.$file);
			}
		}

		if($CI->email->send()){
			return true;

		}else
		{
			return false;
		}
	}

/**
   * get_user_info()
   */
if (!function_exists('get_user_info')) {
    function get_user_info(array $where)
    {
        $CI = get_instance();
        $CI->db->select('*');
        $CI->db->from('users AS a');
		$CI->db->join('users_text AS b','a.UserID = b.UserID' );  
		  if(is_array($where)){
			   foreach($where as $k => $v){
				 $CI->db->where($k,$v);
			   }
		   }
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }
}

 /**
   * dateformat()
   */

if (!function_exists('dateformat')) {

    function dateformat ($date,$type = NULL) {

			if ($date == '0000-00-00 00:00:00'){				

             return NULL;

			}else{

			 $datetime = strtotime($date);
			
			 if($type == 'full'){

			return date("d-M-Y H:i A",$datetime);

			}else if ($type == 'shortdate'){

			return date("d.m", $datetime);

			}else if ($type == 'datenumeric'){

			return date("d-m-y", $datetime);

			}
			else if ($type == 'date'){

			return date("d-M-Y", $datetime);

			}
			else if ($type == 'slashdate'){

			return date("d/M/Y", $datetime);

			}	
			else if ($type == 'datedesc'){

			return date("Y-m-d", $datetime);

			}		
		
			else if ($type == 'time'){

			return date("g:i A", $datetime);			

			}else if($type == "order"){
				
			return date("d-M-Y H:i A", $datetime);
				
			}else if($type == "de"){
			
			return date("d.m.Y", $datetime);
			
			}else if ($type == 'year'){

			return date("Y", $datetime);

			}else if ($type == 'day'){

			return date("d", $datetime);

			}else if ($type == 'month'){

			return date("m", $datetime);

			}
			
		  }
  }

}


/**
  *getCouByIP()
  */
   if (!function_exists('getCountryBYIP')) {   
   function getCountryBYIP($ip){
	    $CI = & get_Instance();
	    if($_SERVER['HTTP_HOST'] != 'localhost'){
        $CI->geoip->setIp($ip);
	    echo strtolower($CI->geoip->getCountry());
		}
    }
   }
  /**
   * getValue()
   */
   if (!function_exists('getValue')) {
  function getValue($table, array $where,$rec = 'row')
  {
       $CI = & get_Instance();
	   $CI->db->select('*');
	   $CI->db->from($table);
	   foreach($where as $k => $vl){
	   $CI->db->where($k,$vl);
		}
	   $result = $CI->db->get();
	  
	if ($result->num_rows() > 0) {
		return $result->$rec();
	}else
	{
		return $result->num_rows();
	}
  }  
   }


if (!function_exists('UserRole')) {

function UserRole($where,$rec = 'row') {
	
	
	   $CI = & get_Instance();
	   $default_lang = getDefaultLanguage();
	   $CI->db->select('*');
	   $CI->db->from('roles AS a');
	   $CI->db->join('roles_text AS b','a.RoleID = b.RoleID' );           
	   $CI->db->where('a.Hide',0);
	   $CI->db->where('a.IsActive',1);
	   $CI->db->where($where); 
	   $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
	   $result = $CI->db->get();
	if ($result->num_rows() > 0) {
		$data = $result->$rec();
			if($rec != 'row_array' && $rec != 'row' && $rec != 'num_rows'){

				foreach ($data as $row) {
                if($rec == 'result_array') {
                    if($row['Title'] == ''){

                        $row['Title'] = GetDefaultLangDataH('RoleID' , $row['RoleID'],'Title','roles_text');
                    }
                } else {

                    if($row['Title'] == ''){
                        $row['Title'] = GetDefaultLangDataH('RoleID' , $row->RoleID,'Title','roles_text');
                    }
                }
            
              }
			}
		return $data;
	}else
	{
		 $data['Title'] = GetDefaultLangDataH('RoleID' , $where['b.RoleID'],'Title','roles_text');
		return $data;
	}	

	}
}


/**
   *check_user_package_active
   */
if(! function_exists('check_user_package_active')){
function check_user_package_active(){ 

	    $CI = get_instance();
	    $CI->load->Model('Purchase_model');
	    if(in_array($CI->session->userdata['admin']['RoleID'],array(3,4))){
            $status = "(2,4,5)";
            $purchase =  $CI->Purchase_model->getPurchaseData(array('b.Status IN '.$status.'' , 'a.UserID' => $CI->session->userdata['admin']['UserID']),'row');
            $rec  = getValue('machines',array('UserID' => $CI->session->userdata['admin']['UserID'],'DATE_FORMAT(`CreatedAt`,\'%Y-%m\')' => date('Y-m')),'result');
            $total_amount = $added_machine = 0;
            if($purchase){
                if($rec){
                    foreach($rec as $val){
                        if(is_numeric($val->Price))
                        {
                            $added_machine++;
                            $total_amount += $val->Price;
                        }
                    }
                }
                if(date('Y-m-d') <= $purchase->ExpiryDate){
                    if($added_machine <= $purchase->Qty and $total_amount <= $purchase->PerMonth){
                        $response = true;
                    }else{
                        $response = false;
                    }
                }else{
                    $response = false;
                }
            }else{
                $response = false;
            }
        }else{
		    $response = false;
		}
		return $response;	
	
    }
}
if(! function_exists('DateDiff')){
function DateDiff($lastlogin){
$update_date = new DateTime($lastlogin.' 00:00:00');
$today = new DateTime(date('Y-m-d h:i:s'));
$rec = $update_date->diff($today);
 if($rec->days == 0){
	 return lang('today');
 }else if($rec->days == 1){
	 return lang('last-day');
 }else if($rec->days <= 7){
	 return lang('last-week');
  }else if($rec->m != 0){
	 return lang('last-month');
  }else if($rec->y != 0){
	 return lang('last-year');
 }
 }
}
/**
   * apply_class()
   */
if (!function_exists('apply_class')) {
  function apply_class($string, $match)
  {
	
      if (strcmp($string, $match) == 0) {
          return "active";
      }
  }
}
/**
   * getChecked()
   */
if (!function_exists('getChecked')) {
  function getChecked($row, $status)
  {
      if ($row == $status) {
          return "checked";
      }
  }
}
  /**
   * getArrayChecked()
   */
if (!function_exists('getArrayChecked')) {
  function getArrayChecked(array $array, $status)
  {
	  if(!empty($array)){
      if (in_array($status,$array)) {
          return "checked=\"checked\"";
      }
	}
  }
}
  /**
   * getSelected()
   */
if (!function_exists('getSelected')) {
  function getSelected($row, $status)
  {
      if ($row == $status) {
          return 'selected="selected"';
      }
  }
}
/**

* getArraySelected()

*/
if (!function_exists('getArraySelected')) {
    function getArraySelected(array $array, $status)
    {
		if(!empty($array)){
        if (in_array($status, $array)) {
            return 'selected="selected"';
        }
	  }
    }
}

if (!function_exists('getGenderTitle')) {

function getGenderTitle($id = null) {
	$current_language = getDefaultLanguage();
	if($current_language->SystemLanguageID == 2){
		$arr = array("Mr", "Mrs" );
	}elseif($current_language->SystemLanguageID == 1){
		$arr = array("Frau", "Herr" );
	}
 	
	if($id){
	 return $arr[$id];
	}else{
	 return $arr;
      }
	}
}
/**
   * cleanOut()
   */
   
  function cleanOut($text) {
	 $text =  strtr($text, array('\r\n' => "", '\r' => "", '\n' => ""));
	 $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
	 $text = str_replace('<br>', '<br />', $text);
	 return stripslashes($text);
  }
if (!function_exists('configPagination')) {
    function configPagination($url, $total_rows, $segment, $per_page = 10)
    {
        $CI = get_instance();
        $CI->load->library('pagination');
        $config['base_url']       = site_url($url);
		$config['use_page_numbers'] = true;
		$config['uri_segment']    = $segment;
		$config['per_page']       = $per_page;
        $config['total_rows']     = $total_rows;
		$config['num_links']      = $per_page;
        $config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
        $config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
        $CI->pagination->initialize($config);
        return $CI->pagination->create_links();
    }
}

if (!function_exists('CountryList')) {

function CountryList($id = null) {
	
	
	   $CI = & get_Instance();
	   $default_lang = getDefaultLanguage();
	   $CI->db->select('a.CountryID,b.Title');
	   $CI->db->from('countries AS a');
	   $CI->db->join('countries_text AS b','a.CountryID = b.CountryID' );           
	   $CI->db->where('a.Hide',0);
	   $CI->db->where('a.IsActive',1);
		if($id){
			$CI->db->where('a.CountryID',$id);
		}
	   $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
	   $result = $CI->db->get();
	if ($result->num_rows() > 0) {
		if($id){
		return $result->row();
		}else{
			return $result->result();
		}
	}else
	{
		return false;
	}	
	}
}
function CountryList2($language,$where){

	   $CI = & get_Instance();
	   $CI->load->Model('Country_model');
 	   $result = $CI->Country_model->getAllJoinedData(false,'CountryID',$language,$where,'ASC','SortOrder','Title');
	   return $result;	
}

if (!function_exists('getFeature')) {

function getFeature()
	{
		  
		   $default_lang = getDefaultLanguage();
		   $language = $default_lang->ShortCode;
		   $where = 'features.Hide = 0 AND features.IsActive = 1';
		   $CI = & get_Instance();
		   $CI->load->Model('Feature_model');
 		   $result = $CI->Feature_model->getAllJoinedData(false,'FeatureID',$language,$where,'ASC','SortOrder','Title');
	       
          /* $CI->db->select('a.FeatureID,b.Title');
	       $CI->db->from('features AS a');
           $CI->db->join('features_text AS b','a.FeatureID = b.FeatureID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
	       $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			return $result->result();
		}else
		{
			return false;
		}*/

		return $result;
 	}
}
if (!function_exists('getPackage')) {

function getPackage($id)
	{
	
		   $CI = & get_Instance();
	       $default_lang = getDefaultLanguage();
           $CI->db->select('*');
	       $CI->db->from('packages AS a');
           $CI->db->join('packages_text AS b','a.PackageID = b.PackageID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
	       $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
	       $CI->db->where('a.PackageID',$id); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			return $result->row();
		}else
		{
			return false;
		}
 	}
}
if (!function_exists('getOldQty')) {

function getOldQty($id)
	{
        $CI = & get_Instance();
        $default_lang = getDefaultLanguage();
        $CI->db->select('a.*,b.*,c.Lang,c.IsVerified,c.IsPaid');
        $CI->db->from('user_package_purchase AS a');
        $CI->db->join('user_payment AS b','a.PurchaseID = b.PurchaseID');
        $CI->db->join('users AS c','c.UserID = a.UserID','left');
        $CI->db->where_in('a.UserID',$id);
        $CI->db->order_by('a.PurchaseID','DESC');
        return $CI->db->get()->result();
 	}
}
if (!function_exists('chekifMultiplePkg')) {

function chekifMultiplePkg($id)
	{
        $CI = & get_Instance();
        $default_lang = getDefaultLanguage();
        $CI->db->select('a.*,b.*,c.Lang,c.IsVerified,c.IsPaid');
        $CI->db->from('user_package_purchase AS a');
        $CI->db->join('user_payment AS b','a.PurchaseID = b.PurchaseID');
        $CI->db->join('users AS c','c.UserID = a.UserID','left');
        $CI->db->where_in('a.UserID',$id);
        $CI->db->order_by('a.PurchaseID','DESC');
        $result = $CI->db->get();
        if ($result->num_rows() > 1) {
            return $result->row();
        } else {
            return false;
        }
 	}
}



function getPackageDefault($id)
	{
	
		   $CI = & get_Instance();
	       $default_lang = getDefaultLanguageByDefault();
           $CI->db->select('*');
	       $CI->db->from('packages AS a');
           $CI->db->join('packages_text AS b','a.PackageID = b.PackageID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
	       $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
	       $CI->db->where('a.PackageID',$id); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			return $result->row();
		}else
		{
			return false;
		}
 	}
if (!function_exists('getPages')) {

function getPages($where = array(),$rec = 'result',$title_key = 'Title',$join_field = 'PageID')
	{
		   $CI = & get_Instance();
	       $default_lang = getDefaultLanguage();
           $CI->db->select('a.PageID,a.Url,b.*');
	       $CI->db->from('pages AS a');
           $CI->db->join('pages_text AS b','a.PageID = b.PageID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
			if(is_array($where)){
			   foreach($where as $k => $v){
				 $CI->db->where($k,$v);
			   }
		   }
		   $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID);
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			$data =  $result->$rec();
			if($rec != 'row' && $rec != 'num_rows'){

				foreach ($data as $row) {
                if($rec == 'result_array') {
                    if($row[$title_key] == ''){

                        $row[$title_key] = GetDefaultLangDataH($join_field , $row[$join_field],$title_key,'pages_text');
                    }
                } else {

                    if($row->$title_key == ''){
                        $row->$title_key = GetDefaultLangDataH($join_field , $row->$join_field,$title_key,'pages_text');
                    }
                }
            
            }
			}else if($rec == 'row' && $data->$title_key == ''){

				$data  = getDataForRow($where);
			}

			return $data;
		}else
		{
			return false;
		}
 	}
}

function getDataForRow($where = array()){

	$CI = & get_Instance();
	       $default_lang = getDefaultLanguageByDefault();

           $CI->db->select('a.PageID,a.Url,b.*');
	       $CI->db->from('pages AS a');
           $CI->db->join('pages_text AS b','a.PageID = b.PageID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
			if(is_array($where)){
			   foreach($where as $k => $v){
				 $CI->db->where($k,$v);
			   }
		   }
		   $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID);
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			return $result->row();
		}else
		{
			return false;
		}
 	}

 function getCategories($language,$where = false,$limit = false,$start = 0,$categorytype=false,$active_category=false){
 	 
	$CI = & get_Instance();
	$CI->load->Model('Category_model');
	$result = $CI->Category_model->getAllJoinedData(false,'CategoryID',$language,$where,'ASC','categories.SortOrder','Title',$limit,$start,true,false,$categorytype,$active_category);
	//echo $CI->db->last_query();exit;
	  
	return $result;
 }

 function getsideCategories(){
	 	$result = $CI->db->query('SELECT
									categories.CategoryID,
									categories.Slug,
									categories.SortOrder,
									categories_text.Title,
									categories_text.ShortTitle,
									count(machines.MachineID) as counter
								FROM
									categories
								INNER JOIN categories_text ON categories.CategoryID = categories_text.CategoryID
								INNER JOIN system_languages ON categories_text.SystemLanguageID = system_languages.SystemLanguageID
								INNER JOIN machines on categories.CategoryID = machines.CategoryID
								WHERE
									system_languages.ShortCode = "'.$CI->session->userdata('lang').'" AND 
									categories.IsActive = 1 AND 
									categories.ParentID != 0
								GROUP BY categories.ParentID
								ORDER BY categories.ParentID ASC')->result_array();

	return $result;
 }

 function getHomeCategories($language,$where = false,$limit = false,$start = 0){
 	 
 	 $CI = & get_Instance();
 	 $CI->load->Model('Category_model');
 	 $result = $CI->Category_model->getAllJoinedData(false,'CategoryID',$language,$where,'ASC','categories.SortOrder','Title',$limit,$start,true);
 	 //echo $CI->db->last_query();exit;
 	 return $result;
 }


if (!function_exists('getCategory')) {

function getCategory($where,$rec = 'result',$start = 0, $limit = '',$language = false,$title_key = 'Title',$join_field = 'CategoryID')
	{
	
		   $CI = & get_Instance();
	       $default_lang = getDefaultLanguage();
           $CI->db->select('*');
	       $CI->db->from('categories AS a');
           $CI->db->join('categories_text AS b','a.CategoryID = b.CategoryID' );           
           $CI->db->join('system_languages','system_languages.SystemLanguageID
            = b.SystemLanguageID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           if($language != 'no'){
	           	if($language){
	           		$CI->db->where('system_languages.ShortCode',$language);
	           }else{
	           		$CI->db->where('system_languages.SystemLanguageID',$default_lang->SystemLanguageID);
	           }
           }
           


	       
	       if(is_array($where)){
			   foreach($where as $k => $v){
				 $CI->db->where($k,$v);
			   }
		   }else {
			  $CI->db->where_in('b.CategoryTextID',$where,FALSE); 
		   }
	       
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
        	
			$data =  $result->$rec();

			if($rec != 'row' && $rec != 'num_rows'){

				foreach ($data as $row) {
                if($rec == 'result_array') {

                    if($row[$title_key] == ''){

                        $row[$title_key] = GetDefaultLangDataH($join_field , $row[$join_field],$title_key,'categories_text');
                    }
                } else {

                    if($row->$title_key == ''){
                        $row->$title_key = GetDefaultLangDataH($join_field , $row->$join_field,$title_key,'categories_text');
                    }
                }
            
            }
			}else if($rec == 'row' && $data->$title_key == ''){

				$data  = getRowCategory($where);

			}
			
			
			return $data;
            
		}else
		{
			return false;
		}
 	}
}


function getRowCategory($where)
{

	   $CI = & get_Instance();
       $default_lang = getDefaultLanguageByDefault();
       $CI->db->select('*');
       $CI->db->from('categories AS a');
       $CI->db->join('categories_text AS b','a.CategoryID = b.CategoryID' );           
       $CI->db->join('system_languages','system_languages.SystemLanguageID
        = b.SystemLanguageID' );           
       $CI->db->where('a.Hide',0);
       $CI->db->where('a.IsActive',1);
       $CI->db->where('system_languages.SystemLanguageID',$default_lang->SystemLanguageID);
       if(is_array($where)){
		   foreach($where as $k => $v){
			 $CI->db->where($k,$v);
		   }
	   }else {
		  $CI->db->where_in('b.CategoryTextID',$where,FALSE); 
	   }
       
	   $CI->db->order_by('b.Title','ASC'); 
       $result = $CI->db->get();
    if ($result->num_rows() > 0) {
		$data =  $result->row();
		
		

        return $data;
	}else
	{
		return false;
	}
	}
	function getSubCategoryBy($where, $isArr = false,$active_subcategory=false)
{

	   $CI = & get_Instance();
      $default_lang = getDefaultLanguageByDefault();
       $CI->db->select('*');
       $CI->db->from('categories AS a');
       $CI->db->join('categories_text AS b','a.CategoryID = b.CategoryID' );           
       $CI->db->join('system_languages','system_languages.SystemLanguageID
        = b.SystemLanguageID' );           
       $CI->db->where('a.Hide',0);
       $CI->db->where('a.IsActive',1);
       $CI->db->where('system_languages.SystemLanguageID',$default_lang->SystemLanguageID);
       if(is_array($where)){
		   foreach($where as $k => $v){
			 $CI->db->where($k,$v);
		   }
	    }else {
		  $CI->db->where_in('b.CategoryTextID',$where,FALSE); 
	    }
	    
	    if($active_subcategory){
	    	$CI->db->where($active_subcategory);
	    }
       
	   $CI->db->order_by('b.Title','ASC'); 
       $result = $CI->db->get();
       //echo $CI->db->last_query();
    if ($result->num_rows() > 0) {

    	if($isArr)
    	{
			$data =  $result->result_array();
		}
		else
		{
			$data =  $result->result();
		}
		

        return $data;
	}else
	{
		return false;
	}
	}
function GetDefaultLangDataH($join_field , $CurrntID,$title_key,$table="categories_text")
    {
    	$CI = & get_Instance();
        $CI->db->select($title_key);
        $CI->db->join('system_languages','system_languages.SystemLanguageID = '.$table.'.SystemLanguageID');
        $result = $CI->db->get_where($table, array($table.'.'.$join_field => $CurrntID , 'system_languages.IsDefault' => 1 ));
        $data = $result->row_array();
        return $data[$title_key];
    }

if (!function_exists('getPopularCategory')) {
	
function getPopularCategory(array $where,$language = false,$limit)// please for now not usable function in text model 
    {
	       $CI = & get_Instance();
           $CI->db->select('a.Slug,COUNT(c.`MachineID`) as counter,a.CategoryID,b.CategoryTextID,b.Title');
	       $CI->db->from('categories AS a');
           $CI->db->join('categories_text AS b','a.CategoryID = b.CategoryID' );
		   $CI->db->join('machines AS c','a.CategoryID = c.CategoryID' ); 
		   $CI->db->join('machines_text AS d','d.MachineID = c.MachineID' );
	       $CI->db->Join('users','users.UserID = c.UserID AND users.IsActive = 1');
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
		   $CI->db->where('c.Hide',0);
           $CI->db->where('c.IsActive',1);
	       //$CI->db->where('b.SystemLanguageID',$language);
		   $CI->db->where('d.SystemLanguageID',$language);	
	       if(is_array($where)){
			   foreach($where as $k => $v){
				 $CI->db->where($k,$v);
			   }
		   }
	       if(!empty($limit)){
	       $CI->db->limit(0, $limit);  // Produces: LIMIT 20, 10 (in MySQL.  Other databases have slightly different syntax)
		   }
		   $CI->db->group_by('a.CategoryID'); 
		   $CI->db->order_by('counter','DESC');
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			$data =  $result->result();
			//echo $CI->db->last_query();exit;

			foreach ($data as $row) {
                if($row->Title == ''){
                        $row->Title = GetDefaultLangDataH('CategoryID' , $row->CategoryID,'Title','categories_text');
                }
            
            }

            return $data;




		}else
		{
			return false;
       }
    }
}

if (!function_exists('getPopularCategories')) {
	
	function getPopularCategories(array $where,$language = false,$limit)// please for now not usable function in text model 
		{
			   $CI = & get_Instance();
			   $CI->db->select('a.Slug,COUNT(c.`MachineID`) as counter,a.CategoryID,b.CategoryTextID,b.Title');
			   $CI->db->from('categories AS a');
			   $CI->db->join('categories_text AS b','a.CategoryID = b.CategoryID' );
			   $CI->db->join('machines AS c','a.CategoryID = c.CategoryID' ); 
			   $CI->db->join('machines_text AS d','d.MachineID = c.MachineID' );
			   $CI->db->Join('users','users.UserID = c.UserID AND users.IsActive = 1');
			   $CI->db->where('a.Hide',0);
			   $CI->db->where('a.IsActive',1);
			   $CI->db->where('c.Hide',0);
			   $CI->db->where('c.IsActive',1);
			   $CI->db->where('b.SystemLanguageID',$language);
			   $CI->db->where('a.ParentID','!=',0);
			   $CI->db->group_by('a.ParentID');
			   $CI->db->order_by('a.ParentID','ASC');
			   $CI->db->where('d.SystemLanguageID',$language);	
			   if(is_array($where)){
				   foreach($where as $k => $v){
					 $CI->db->where($k,$v);
				   }
			   }
			   if(!empty($limit)){
			   $CI->db->limit(0, $limit);  // Produces: LIMIT 20, 10 (in MySQL.  Other databases have slightly different syntax)
			   }
			   $CI->db->group_by('a.CategoryID'); 
			   $CI->db->order_by('counter','DESC');
			   $result = $CI->db->get();
			if ($result->num_rows() > 0) {
				$data =  $result->result();
				//echo $CI->db->last_query();exit;
	
				foreach ($data as $row) {
					if($row->Title == ''){
							$row->Title = GetDefaultLangDataH('CategoryID' , $row->CategoryID,'Title','categories_text');
					}
				}

				return $data;
			}else {
				return false;
		   }
		}
	}

if (!function_exists('get_machine_row')) {

function get_machine_row(array $where,$rec = 'num_rows')
	{
		   $CI = & get_Instance();
	       $default_lang = getDefaultLanguage();
           $CI->db->select('*');
	       $CI->db->from('machines AS a');
           $CI->db->join('machines_text AS b','a.MachineID = b.MachineID' );   
	       $CI->db->Join('users','users.UserID = a.UserID AND users.IsActive = 1');
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
	       $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID);
			if(is_array($where)){
	       foreach($where as $key => $val){
			   if($val){
		          $CI->db->where($key,$val); 
			   }
		   }
			}
	       $CI->db->order_by('b.MachineTextID','ASC'); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			$data = $result->$rec();
			 if(!in_array($rec, array('row','num_rows'))){

                foreach ($data as $row) {

                if($rec == 'result_array') {

                    if($row['Title'] == ''){

                        $row['Title'] = $CI->Machine_model->GetDefaultLangData('MachineID' , $row['MachineID'],'Title');
                    }
                } else {
                    
                    if($row->Title == ''){

                        $row->Title = $CI->Machine_model->GetDefaultLangData('MachineID' , $row->MachineID,'Title');

                    }
                 }
               }
            }else if($rec == 'row' && $data->Title == ''){

				$data->Title  =$CI->Machine_model->GetDefaultLangData('MachineID' , $data->MachineID,'Title');

			}
			
			
			return $data;
		}else
		{
			return false;
		}
 	}
}

if (!function_exists('get_category_dealer')) {

function get_category_dealer($where,$rec = 'num_rows')
	{
		   $CI = & get_Instance();
	       $default_lang = getDefaultLanguage();
           $CI->db->select('*');
	       $CI->db->from('user_package_purchase AS a');
           $CI->db->join('user_payment AS b','a.PurchaseID = b.PurchaseID' );           
           $CI->db->where('b.Status',2);
	      
			if($where){
		   $CI->db->where($where); 
			}
	       $CI->db->group_by('a.UserID');
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			return $result->$rec();
		}else
		{
			return false;
		}
 	}
}

if (!function_exists('get_dealer')) {

function get_dealer($rec = 'num_rows')
	{
		   $CI = & get_Instance();
	       $default_lang = getDefaultLanguage();
           $CI->db->select('*');
	       $CI->db->from('users AS a');
           $CI->db->join('users_text AS b','a.UserID = b.UserID' );           
           $CI->db->where('a.RoleID',4);
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
           $CI->db->where('a.IsVerified',1);
	      
	       $CI->db->group_by('a.UserID');
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			return $result->$rec();
		}else
		{
			return false;
		}
 	}
}
if (!function_exists('YearList')) {

function YearList()
	{ 
	    $arr = array();
		$year = range(1980,date('Y'));
	
	     foreach($year as $val){
			 $arr[$val]= $val;
 		 }
	    return $arr;
 	}
}
if (!function_exists('MonthList')) {

function MonthList()
	{ 
	    $arr = array();
		$year = range(1,12);
	
	     foreach($year as $val){
			 $arr[$val]= $val;
 		 }
	    return $arr;
 	}
}
if (!function_exists('MenuPosition')) {

function MenuPosition($ele = '')
	{ 
	    $arr = array('1' => lang('menu_header'),'2' => lang('menu_footer'),'3' => lang('left'),'4' => lang('right'));
	    if($ele){
		 return $arr[$ele];
		}else{
		 return $arr;
		  }
 	}
}

if (!function_exists('CurrencyPosition')) {

function CurrencyPosition()
	{ 
	    $arr = array('1' => lang('left'),'2' => lang('right'),'3' => lang('left-space'),'4' => lang('right-space'));
	    return $arr;
 	}
}
if (!function_exists('AdminMenu')) {
    function AdminMenu(array $menu, $id, $menutype ,$level = 0,$itemMenu='')
    {
		$html = '';
        if(count(has_child($menu,$id))<=0 && $level==0)
        {

            if($menutype == 4){
             $html .=  '<li class="dd-item" data-id="0" data-link="'.$itemMenu->link.'" data-subtitle="'.$itemMenu->subtitle.'"  data-menu-type="'.$menutype.'">
                 <span class="menu-remove pull-right">x</span><span class="menu-type pull-right">External Link</span>
                        <div class="dd-handle">'.$itemMenu->subtitle.' </div>
                    </li>';
            } if($menutype == 1){
               $page = getPages(array('a.PageID'=> $id),'row');
				$pagetype = '<span class="menu-type pull-right">Page</span>';
            }else{
                $page =  getCategory(array('b.CategoryID' => $id),'row');
				$pagetype = '<span class="menu-type pull-right">Category</span>';
            }
            
            if($page){
            $html .=  '<li class="dd-item" data-id="'.$id.'" data-menu-type="'.$menutype.'"><span class="menu-remove pull-right">x</span>'.$pagetype.'
                        <div class="dd-handle">'.$page->Title.'</div>
                    </li>';
			}
        }

        else if(count(has_child($menu,$id))<=0 && $level>0)
        {
			
             if($menutype == 1){
               $page = getPages(array('a.PageID'=> $id),'row');
				$pagetype = '<span class="menu-type pull-right">Page</span>';
				}else{
				$page = getCategory(array('b.CategoryID' => $id),'row');
				$pagetype = '<span class="menu-type pull-right">Category</span>';
				}
			if($page){	
            $html .=  '<li class="dd-item" data-id="'.$id.'" data-menu-type="'.$menutype.'"><span class="menu-remove pull-right">x</span>'.$pagetype.'

                        <div class="dd-handle">'.$page->Title.'</div>

                    </li>';
			}
        }

        else if(count(has_child($menu,$id))>0)
        {			

            $childs = has_child($menu,$id);
            $flag = 0;
            foreach ($childs as $child)
            {
                if($menutype == 1){
               $page = getPages(array('a.PageID'=> $id),'row');
				$pagetype = '<span class="menu-type pull-right">Page</span>';
				}else{
				$page = getCategory(array('b.CategoryID' => $id),'row');
				$pagetype = '<span class="menu-type pull-right">Category</span>';
				}

                if($flag==0)
                {
					if($page){	
                    $html .= '<li class="dd-item" data-id="'.$id.'" data-menu-type="'.$menutype.'"><span class="menu-remove pull-right">x</span>'.$pagetype.'
                                <div class="dd-handle">'.$page->Title.'</div>
                                    <ol class="dd-list">';     
                    $flag = 1;                      
                    $level++;
					}
                }
				
               $html .= AdminMenu($menu,$child['id'],$child['menutype'],$level,$itemMenu);
            }
               $html .= '</ol></li>';
        }

        return	 $html;

    }
}

if (!function_exists('Menu')) {
    function Menu(array $menu, $id, $menutype ,$level = 0,array $class,$itemMenu='')
    {
		$html =  $li = $url = $a = '';
         if(isset($class['li']) and !empty($class['li'])){
			 $li = 'class="'.$class['li'].'"';
		 } if(isset($class['a']) and !empty($class['a'])){
			 $a = 'class="'.$class['a'].' dropdown-toggle"';
		 }
		if(count(has_child($menu,$id))<=0 && $level==0)
        {

             if($menutype == 4 && !empty($itemMenu->link)){
               $html .=  '<li '.$li.'><a href="'.$itemMenu->link.'"  target="_blank" '.$a.'>'.$itemMenu->subtitle.'</a></li>';
          $page=0;  }else if($menutype == 1){
               $page = getPages(array('a.PageID'=> $id),'row');
				if($page){
            	$url =  friendly_url('', $page->Url);
				}
            }else{
                $page = getCategory(array('b.CategoryID' => $id),'row');
				if($page){
				$url  = friendly_url('kategorie/', $page->Slug);
				}
            }
			if($page){
            $html .=  '<li '.$li.'><a href="'.$url.'" '.$a.'>'.$page->Title.'</a></li>';
			}
        }

        else if(count(has_child($menu,$id))<=0 && $level>0)
        {
			
            if($menutype == 1){
               $page = getPages(array('a.PageID'=> $id),'row');
				if($page){
           	    $url =  friendly_url('', $page->Url);
				}
            }else{
                $page = getCategory(array('b.CategoryID' => $id),'row');
				if($page){
				$url  = friendly_url('kategorie/', $page->Slug);
				}
            }
			if($page){				
            $html .=  '<li '.$li.'><a href="'.$url.'" '.$a.'>'.$page->Title.'</a></li>';
			}
        }

        else if(count(has_child($menu,$id))>0)
        {			

            $childs = has_child($menu,$id);
            $flag = 0;
            foreach ($childs as $child)
            {
                if($menutype == 1){
               $page = getPages(array('a.PageID'=> $id),'row');
					if($page){
                	$url =  friendly_url('', $page->Url);
					}
            }else{
                $page = getCategory(array('b.CategoryID' => $id),'row');
					if($page){
				$url  = friendly_url('kategorie/', $page->Slug);
					}
            }

               if(count(has_child($menu,$id))>0 and $level > 0){
				$sub = ' dropdown-submenu';
				}else{
					$sub = '';
				}
                if($flag==0)
                {
					if($page){		
                    $html .= '<li class="'.$li.$sub.'"><a href="'.$url.'" '.$a.'>'.$page->Title.'</a><a data-toggle="dropdown"  class="dropdown-o"><i class="fa fa-caret-down" aria-hidden="true"></i></a> 
                                    <ul class="dropdown-menu">';
					}
                    $flag = 1;                      
                    $level++;
                }
				if($page){
               $html .= Menu($menu,$child['id'],$child['menutype'],$level,$class);
				}
            }
               $html .= '</ul></li>';
        }

        return	 $html;

    }
}

if (!function_exists('has_child')) {

    function has_child($menu,$id)
    {

        $child = array();
     	$i = 0;
        foreach ($menu as $row) 
        {
            if($row->parent==$id && $row->parent!=0)

            {
                $child[$i]['id'] = $row->id;
				$child[$i]['parent'] = $row->parent;
                $child[$i]['menutype'] = $row->menutype;
                $i++; 

            }

        }
        
        return $child;
    }
}

if(!function_exists('buildTree')){
function buildTree(array $data,$language) {
	            $CI = & get_Instance();
	            $id = $for = $addclass= '';
				$option = '<ul class="sorting">';		
				$n = 0;	

				foreach($data as $k){
$addclass= '';
			    $n++;  
				

					 $id = 'id="menuinner'.$k->CategoryID.'"';
					$for = 'for="menuinner'.$k->CategoryID.'"';
				$child_categories =  getCategories($language,'categories.ParentID = '.$k->CategoryID);
                                if(!empty($child_categories))
                                $addclass='class="eChild" ' ;

                $option .= '<li id="item-'.$k->CategoryID.'"><input type="checkbox" '.$id.'/><label '.$addclass.$for.'><span class="arrow11"></span>'.$k->Title;
				 if($child_categories){
				 $option .= ' <span class="badge badge-primary badge-pill">'.count($child_categories).'</span>';
				 }
				 $option .= '</label><span class="pull-right">';
				 if($k->IsShow = 1){
					$option .='<a href="'.base_url('cms/category/isshow/'.$k->CategoryID.'/0').'" class="btn btn-simple btn-success btn-icon ShowHideCategory" title="'.lang('is_show').'"><i class="material-icons">check_circle_outline</i></a>';
				 }else{ 
					$option .='<a href="'.base_url('cms/category/isshow/'.$k->CategoryID.'/0').'" class="btn btn-simple btn-secondary btn-icon ShowHideCategory" title="'.lang('not_is_show').'"><i class="material-icons">check_circle_outline</i></a>';
				 } 
				  if(checkUserRightAccess(50,$CI->session->userdata['admin']['UserID'],'CanEdit')){
					$option .= '<a href="'.base_url('cms/category/edit/'.$k->CategoryID).'" class="btn btn-simple btn-warning btn-icon edit" title="'.lang('edit') .' '.lang('category').'" style="padding:0px; margin:0px;"><i class="material-icons">edit</i></a>';
				  }
                  if(checkUserRightAccess(50,$CI->session->userdata['admin']['UserID'],'CanDelete')){
					  $action = "onclick=\"deleteRecord('$k->CategoryID','cms/category/action','')\"'";
					$option .= '<a href="#" '.$action.' class="btn btn-simple btn-danger btn-icon remove" title="'.lang('delete') .' '.lang('category').'"  style="padding:0px; margin:0px;"><i class="material-icons">close</i></a>';
                   }
					$option .= '</span>';
 					$sub = buildTree($child_categories,$language);
				 if($sub != '<ul></ul>')
                $option .= $sub;
                $option .= '</li>';	
				
        }
				$option .= '</ul>';	

	         return $option;

	}
}

if(!function_exists('MachineCategoryTree')){
function MachineCategoryTree(array $data, $language) {
	            $CI = & get_Instance();
	            $id = $for =  '';
				$option = '<ul>';		
				foreach($data as $k){
				$arr_child = getCategories($language,'categories.ParentID = '.$k->CategoryID);
					 $id = 'id="menu'.$k->CategoryID.'"';
					$for = 'for="menu'.$k->CategoryID.'"';
				
                $option .= '<li><input type="radio" value="'.$k->CategoryID.'" data-title="'.$k->Title.'" name="choose-category" class="category">
					<input type="checkbox" '.$id.'/><label '.$for.'>'.$k->Title.'</label><span class="pull-right">';
					$option .= '</span>';
 					$sub = MachineCategoryTree($arr_child,$language);
				 if($sub != '<ul></ul>')
                $option .= $sub;
                $option .= '</li>';	
						
        }
				$option .= '</ul>';	

	         return $option;

	}
}
if(!function_exists('MachineCategoryOptionTree')){
function MachineCategoryOptionTree(array $data, $language, $margin='20', $selectedCategory = '') {
	            $CI = & get_Instance();
	            $option='';	
				foreach($data as $k){
				$arr_child = getCategories($language,'categories.ParentID = '.$k->CategoryID);
				$selectedVal = '';
				if($selectedCategory != '')
				{
					$selectedVal = ($k->CategoryID == $selectedCategory ? 'selected' : '');
				}

                $option .= '<option '.$selectedVal.' style="margin-left:'.$margin.'px" value="'.$k->CategoryID.'">'.$k->Title.'</option>';
 					$option .= MachineCategoryOptionTree($arr_child,$language,$margin+20, $selectedCategory);
				 
						
        }
		

	         return $option;

	}
}

if(!function_exists('MachineCategoryArray')){
	function MachineCategoryArray($CategoryID, $language, $CategoryIDArr) {
        
        $CI = & get_Instance();

		$arr_child = getCategories($language,'categories.ParentID = '.$CategoryID);
		if(!empty($arr_child))
		{
			foreach($arr_child as $k)
			{
				$CategoryIDArr[] = $k->CategoryID;
				$CategoryIDArr = MachineCategoryArray($k->CategoryID,$language, $CategoryIDArr);
			}

		}
		return $CategoryIDArr;
	}
}

if(!function_exists('calculateSize')){
function calculateSize($size){

	$unit = null;
	$units = array('B', 'KB', 'MB', 'GB', 'TB');
	for($i = 0, $c = count($units); $i < $c; $i++)
	{
		if ($size > 1024)
		{
			$size = $size / 1024;
		}
		else
		{
			$unit = $units[$i];
			break;
		}
	}
	return round($size, 2).$unit;
	}
}

if(!function_exists('friendly_url')){
function friendly_url($segment,$title){
     return base_url().$segment.string_replace(strtolower($title),'/\s+/', '-').'/';
  }
}

if(!function_exists('string_replace')){
function string_replace($string,$find,$replace){
     return preg_replace($find, $replace,$string);
  }
}

  /**
   * getImage()
   */
   if (!function_exists('getImage')) {
  function getImage(array $where,$return_array = true)
  {
	   $CI = & get_Instance();
	   $CI->load->Model('Site_images_model');
	   return $CI->Site_images_model->getMultipleRows($where, $return_array);

     }  
   }

    if (!function_exists('site_setting')) {
	 	function site_setting()
	 	{
		   $CI = & get_Instance();
		   $CI->load->Model('Site_setting_model');
	       $rec = $CI->Site_setting_model->get(1, false, 'SiteSettingID');
	       return $rec;
		   
	   }  
   }

  if (!function_exists('currency')) {
  function currency($price)
  {
	   $CI = & get_Instance();
	   $CI->load->Model('Site_setting_model');
       $rec = $CI->Site_setting_model->get(1, false, 'SiteSettingID');
	   $left = $right = '';
	   if($rec->CurrencyPosition == 1){
		  $left = $rec->Currency;
	   }else if($rec->CurrencyPosition == 3){
		  $left = $rec->Currency.' ';
	   }
	   if($rec->CurrencyPosition == 2){
		  $right = $rec->Currency;
	   }else if($rec->CurrencyPosition == 4){
		  $right = ' '.$rec->Currency;
	   }
	   return $left.number_format($price,$rec->NumberDecimals,$rec->DecimalSeparator,$rec->ThousandSeparator).$right;
     }  
   }

  if (!function_exists('condition_list')) {

function condition_list($cod = ''){
	$arr = array(lang('new'),lang('new-ex-display'),lang('refurbished'),lang('reconditioned'),lang('good-condition-used'),lang('excellent'),lang('ready-operation'),lang('unexamined'),lang('repair'));
    if($cod == 'true'){
	 return $arr;
      }else{
		
	 return $arr[$cod];
	}
    }
  }

if(! function_exists('payment_status')){
	function payment_status($status){
		
		if($status == 2){
			return lang('payment-complete');
		}else if($status == 1){
			return lang('payment-cancel');
		}else if($status == 4){
			return lang('payment-pending');//through bank
		}else if($status == 5){
			return lang('payment-complete');//through bank
		}else {
			return lang('payment-pending');
		}
	}
}
if(! function_exists('mail_tpl_shortcode')){
function mail_tpl_shortcode($shortcode = ''){
	$arr =  array('Name' => '[name]',
         'Email' => '[email]',
         'Password' => '[password]',
		 'Category' => '[category]',		 
		 'Machine' => '[machine]',
		 'Machine Report Reason' => '[machine-report-reason]',		 
		 'Company Name' => '[company-name]', 
		 'Company Name' => '[company]', 
		 'Phone' => '[phone]',
		 'Mobile' => '[mobile]',  		  
		 'Fax' => '[fax]',
		 'City' => '[city]',	
		 'Country' => '[country]',		  
		 'Address' => '[address]', 
		 'PostalCode' => '[postal-code]', 
		 'PostalCode' => '[zip]', 
		 'Message'=> '[message]',
		 'Invoice ID' => '[invoice-id]',
		 'Package Name' => '[package-name]',
		 'Verification Link' => '[verification-link]',
     	 'Expiry Date' => '[expiry-date]', 
		 'Per Advertisement' => '[per_advertisement]',
		 'Per Month Equal' => '[per_month_equal]',  
		 'Contract Period' => '[contract_period]',
		 'Total Amount' => '[total_amount]',
		 'Reply By' => '[reply-by]',
		 'Dealer/Reseller' => '[dealer]', 		 
		 'Site Name' => '[site_name]',
		 'Site URL' => '[site_url]',
		 'Link' => '[link]',
		 lang('contact-form') => '[contact-form]',
		 lang('google-map-embed') => '[google-map]',
		 'Title' => '[title]',
		 'Customer No' => '[customer_no]',
		 'Status' => '[status]',
		 'Registered' => '[registered]'	,		  
		 'Registered Date' => '[created_at]',			  
		 'Active days' => '[active_days]'	,		  
		 'Document Status' => '[document_status]',			  
		 'Similar Machines' => '[similarmachine]'			  
		  );
	 if($shortcode){
		 return $arr[$shortcode];
		}else{
		 return $arr;
		  }
     }
}

function getSiteSetting(){
	  $CI = & get_Instance();
	   $CI->load->model('Site_setting_model');
     return $CI->Site_setting_model->get(1, false, 'SiteSettingID');
}

if (!function_exists('TplList')) {

function TplList($ele = '')
	{ 
	    $arr = array('1' => lang('register'),'2' => lang('reset-password') ,'3' => lang('payment-pending'),'4' => lang('payment-cancel'),'5' => lang('payment-complete'),'6' => lang('send-inquiry'),'7' => lang('share'),'8' => lang('report'),'9' => lang('send-inquiry-all-dealer'),'10' => lang('purchase-package'),'13' => lang('upgrade-package'),'11' => lang('expire-package'),'12' => lang('contact-us'),'14' => lang('info_email'),'15' => lang('invoice'),'16' => lang('before-expire-notification'),'17' => lang('user-status-email'),'18' => lang('user_received_document_email'),'19' => lang('notify_customer_status'));
	    if($ele){
		 return $arr[$ele];
		}else{
		 return $arr;
		  }
 	}
}

/**
   * get_email_tpl()
   */
if (!function_exists('get_email_tpl')) {
    function get_email_tpl(array $where,$language = '')
    {
        $CI = get_instance();
		if($language){
		$CI->session->set_userdata('lang', $language);
		}
		$default_lang = getDefaultLanguage();
        $CI->db->select('*');
        $CI->db->from('email_templates AS a');
		$CI->db->join('email_templates_text AS b','a.Email_templateID = b.Email_templateID' );
		$CI->db->where('a.Hide',0);
        $CI->db->where('a.IsActive',1);
	    $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID);
		  if(is_array($where)){
			   foreach($where as $k => $v){
				 $CI->db->where($k,$v);
			   }
		   }
        $query = $CI->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        }
    }
}


/**
   * show_counter()
   */
if (!function_exists('show_counter')) {
    function show_counter($val)
    {
	   $html = '';
       $CI = get_instance();
	   $CI->load->Model('Site_setting_model');
       $rec = $CI->Site_setting_model->get(1, false, 'SiteSettingID');
       if($rec->CategoryCount == 1){
		   	if($val){
				$html ='<span class="badge badge-primary badge-pill">';
				$html .= (!empty($val))?'('.$val.')':'';
				$html .='</span>'; 
	   	 	}
	   }
	 return $html;
    }
}

function getCategroyThirdLevelCount($ParentID = 0)
{
	if($ParentID == 0)
	{
		return array();
	}
		$CI = & get_Instance();

		if(!empty($CI->session->userdata('lang'))){
			$systemlang = $CI->session->userdata('lang');
		}else{
			$systemlang = "de";
		}
		$totalCategoryCount = 0;
		$subCategoryData = array();

		$subCategory = $CI->db->query('SELECT *

						FROM
							categories
						INNER JOIN categories_text ON categories.CategoryID = categories_text.CategoryID
						INNER JOIN system_languages ON categories_text.SystemLanguageID = system_languages.SystemLanguageID
						WHERE
							system_languages.ShortCode = "'.$systemlang.'" AND 
							categories.IsActive = 1 AND 
							categories.Hide = 0 AND 
							categories.ParentID = '.$ParentID)->result_array();

		foreach($subCategory as $scKey=>$sc)
		{
			$CI->db->select('*');
			$CI->db->from('machines AS a');
			$CI->db->join('machines_text AS b','a.MachineID = b.MachineID' );   
			$CI->db->Join('system_languages','system_languages.SystemLanguageID = b.SystemLanguageID');
			$CI->db->where('a.Hide',0);
			$CI->db->where('a.IsActive',1);
			$CI->db->where('system_languages.ShortCode',$systemlang);
			$CI->db->where('a.CategoryID',$sc['CategoryID']);
			
			$CategoryMachineCount = $CI->db->get()->num_rows();

			$SubSubCategory = $CI->db->query('SELECT *

						FROM
							categories
						INNER JOIN categories_text ON categories.CategoryID = categories_text.CategoryID
						INNER JOIN system_languages ON categories_text.SystemLanguageID = system_languages.SystemLanguageID
						WHERE
							system_languages.ShortCode = "'.$systemlang.'" AND 
							categories.IsActive = 1 AND 
							categories.Hide = 0 AND 
							categories.ParentID = '.$sc['CategoryID'])->result_array();
			if($SubSubCategory)
			{
				foreach ($SubSubCategory as $ssc) {
					$CI->db->select('*');
					$CI->db->from('machines AS a');
					$CI->db->join('machines_text AS b','a.MachineID = b.MachineID' );   
					$CI->db->Join('system_languages','system_languages.SystemLanguageID = b.SystemLanguageID');
					$CI->db->where('a.Hide',0);
					$CI->db->where('a.IsActive',1);
					$CI->db->where('system_languages.ShortCode',$systemlang);
					$CI->db->where('a.CategoryID',$ssc['CategoryID']);
					
					$CategoryMachineCount += $CI->db->get()->num_rows();
				}
			}

			$subCategoryData[$scKey]['subCategoryDetail'] = $sc;
			$subCategoryData[$scKey]['CategoryMachineCount'] = $CategoryMachineCount;
			$totalCategoryCount += $CategoryMachineCount;
		}
		$returnArr['subCategoryData'] = $subCategoryData;
		$returnArr['totalCategoryCount'] = $totalCategoryCount;
		return $returnArr;
}

/**
   * confirmationBox()
   */
if (!function_exists('confirmationBox')) {

function confirmationBox()
	{
	
		   $CI = & get_Instance();
	       $default_lang = getDefaultLanguage();
           $CI->db->select('*');
	       $CI->db->from('confirmation_boxes AS a');
           $CI->db->join('confirmation_boxes_text AS b','a.Confirmation_boxID = b.Confirmation_boxID' );           
           $CI->db->where('a.Hide',0);
           $CI->db->where('a.IsActive',1);
	       $CI->db->where('b.SystemLanguageID',$default_lang->SystemLanguageID); 
           $result = $CI->db->get();
        if ($result->num_rows() > 0) {
			
                    if($result->row()->Content == ''){
                        $result->row()->Content = GetDefaultLangDataH('Confirmation_boxID' , $result->row()->Confirmation_boxID,'Content','confirmation_boxes_text');
                    }
		  return $result;
	
		}else
		{
			return false;
		}
 	}
}

/**
   * getSeo()
   */
if (!function_exists('getSeo')) {

function getSeo($id,$type = 1,$language = 'en')
	{
	$CI = & get_Instance();
	$CI->load->model('Site_setting_model');
    $site_setting = $CI->Site_setting_model->get(1, false, 'SiteSettingID');
	$res = false;
	if($id){
		if($type == 1){
			
		$page = getPages(array('a.Url'=> $id),'row');
			
		}else{
	    $data = getCategories($language,'categories.CategoryID = '.$id);
	    if(!empty($data)){
	    	$page = $data[0];
	    }else{
	    	$page = array();
	    }
		
			
		}
		$res = true;
	}
	   if(isset($page) and $res and !empty($page->Seo)){
		   
	    $seo = json_decode($page->Seo);
	
		$data['title']  = $seo->seo_title;	

		$data['meta_desc'] = $seo->meta_desc;

		$data['key_words'] = $seo->keywords;

		$data['crawl_after'] = $seo->crawl_after_day;
		
		$data['follow'] = $seo->follow;
		
		$data['index'] = $seo->index;
		   
	   }else{
		   
		$data['title']  = ($res and !empty($page->Seo))?$page->Title:$site_setting->SiteName;	

		$data['meta_desc'] = '';

		$data['key_words'] = '';

		$data['crawl_after'] = 0;
		
		$data['follow'] = 'nofollow';
		
		$data['index'] = 'noindex'; 
	   }
	
	   return $data;
	
 	}
 	
    function lang_translate($text)
    {
        $CI = & get_Instance();
		$CI->load->library('GoogleTranslate');
		$gt = new GoogleTranslate();
 
		/* Convert from English */
		return $gt->translate("de", "en",$text);		        
    }
}

if (!function_exists('allcategoriesformenu')) {
	function allcategoriesformenu($ParentID = 0){

		$CI = & get_Instance();

		if(!empty($CI->session->userdata('lang'))){
			$systemlang = $CI->session->userdata('lang');
		}else{
			$systemlang = "de";
		}

		$menus = $CI->db->query('SELECT
								categories.CategoryID,
								categories.ParentID,
								categories.Slug,
								categories.SortOrder,
								categories_text.Title,
								categories_text.ShortTitle
						FROM
							categories
						INNER JOIN categories_text ON categories.CategoryID = categories_text.CategoryID
						INNER JOIN system_languages ON categories_text.SystemLanguageID = system_languages.SystemLanguageID
						WHERE
							system_languages.ShortCode = "'.$systemlang.'" AND 
							categories.IsActive = 1 AND 
							categories.IsShow = 1 AND 
							categories.Hide = 0 AND 
							categories.CategoryType = 0 AND
							categories.ParentID = '.$ParentID.' 
						Order by categories.SortOrder ASC')->result_array();

		return $menus;
	}
}
