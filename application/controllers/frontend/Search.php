<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
        parent::__construct();
              
       
        $this->load->Model(array('Menu_model','Category_model','Machine_model','User_model','Advertise_model'));
        $this->data['language']      = $this->language;
        $this->data['site_setting'] = $this->getSiteSetting();
		$this->data['advertise'] = $this->Advertise_model->getActiveAllJoinedData(false,'AdvertiseID',$this->language,false,'DESC','AdvertiseID','result');
		$this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 1');
		$this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2');
        $this->data['default_lang'] = getDefaultLanguage();
        $this->data['per_page'] = 10; 
		if($this->session->userdata('admin')){
        $this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows'); 
		}else{
		$this->data['user_wishlist'] = 0;	
		}
    }
    
    public function index()
    {
		if(!$this->input->post())
		{
			redirect('home');
		}	
		  $rec = $this->input->post('search');
		  /*$manufacturer = $this->input->post('manufacturer');
		  $CategoryID = $this->input->post('CategoryID');*/
		//   $concatStr = "";
		//   if($rec != '')
		//   {
		// 	  $recArr = explode(' ', $rec);
		// 	  if(!empty($recArr))
		// 	  {
		// 	  	$concatStr .= " OR (";
		// 		  foreach ($recArr as $key => $value) {
		// 		  	$concatStr .= "CONCAT(`categories_text`.`Title`, ' ', `machines_text`.`Title`, ' ', `machines`.`Manufacturer`, ' ', `machines`.`Model`, ' ',  `machines_text`.`MachineType`) LIKE '%".$value."%' AND ";
		// 		  }

		// 		  $concatStr = rtrim($concatStr, ' AND ');
		// 		  $concatStr .= ")";
		// 	  }
		//   }

		  $this->data['cat_name'] =  $rec;
		  //$this->data['data'] = $this->Machine_model->getMachineActiveAllJoinedData(false,'MachineID',$this->language,"(machines_text.Title LIKE '%".$rec."%' OR machines_text.MachineType LIKE '%".$rec."%' OR machines.Manufacturer LIKE '%".$rec."%' OR machines.Model LIKE '%".$rec."%')".$concatStr,'DESC','MachineID','result',array($this->data['per_page'],0));

		  $this->data['data'] = $this->Machine_model->getMachines($rec);

		  /*echo $this->db->last_query();
		  exit();*/
          $this->data['top_list'] = getPopularCategory(array(),$this->data['default_lang']->SystemLanguageID,10);
          //$this->data['top_list'] = getPopularCategories(array(),$this->data['default_lang']->SystemLanguageID,10);
          $this->data["pages"]    = configPagination('search/',get_machine_row(array('b.Title' => $rec)),2,$this->data['per_page']);
		  $this->data['seo'] = getSeo('');
		  $this->data['view'] = 'frontend/category';

          $this->load->view('frontend/layouts/default',$this->data);
		
    }
    public function search()
    {

		  $rec = $this->input->post('search');
		  /*$manufacturer = $this->input->post('manufacturer');
		  $CategoryID = $this->input->post('CategoryID');*/
//		  $concatStr = "";
//		  if($rec != '')
//		  {
//			  $recArr = explode(' ', $rec);
//			  if(!empty($recArr))
//			  {
//			  	$concatStr .= " OR (";
//				  foreach ($recArr as $key => $value) {
//				  	$concatStr .= "CONCAT(`categories_text`.`Title`, ' ', `machines_text`.`Title`, ' ', `machines`.`Manufacturer`, ' ', `machines`.`Model`, ' ',  `machines_text`.`MachineType`) LIKE '%".$value."%' AND ";
//				  }
//
//				  $concatStr = rtrim($concatStr, ' AND ');
//				  $concatStr .= ")";
//			  }
//		  }


            $this->data['cat_name'] =  $rec;
            $MachineTitle  = $this->Machine_model->getMachineTitle($rec);
            $categoriesTitle  = $this->Machine_model->getcategoriesTitle($rec);
            $Manufacturer  = $this->Machine_model->getManufacturer($rec);

            $m_html=$c_html=$Manufacturer_html='';
            foreach ( $MachineTitle as $key){
             $m_html.='<li class="liClick" >'.$key['mTitle'].'</li>';
              }
             foreach ( $categoriesTitle as $key){
             $c_html.='<li class="liClick" >'.$key['cTitle'].'</li>';
              }
            foreach ( $Manufacturer as $key){
             $Manufacturer_html.='<li class="liClick">'.$key['Manufacturer'].'</li>';
              }
            $html = '';
            if (!empty($m_html)) {
                $html .= '<li class="liTitle" >'.lang('title') .'</li>';
                $html .= $m_html;
            }
            if (!empty($c_html)) {
              $html .= '<li class="liTitle" >'.lang('categorys') .'</li>';
              $html .= $c_html;
            }
            if (!empty($Manufacturer_html)) {
                $html .= '<li   class="liTitle">'.lang('manufacturer') .'</li>';
                $html .= $Manufacturer_html;
            }
            if (!empty($html)) {
                $res['html'] = '<ul>' . $html . '</ul>';
            } else {
                $res['html'] = ' <li>'.lang('record-not-found').'</li> ';
            }

            $res['code']='01';
              exit(json_encode($res));
                 
		
    }
	public function advance_search($id = ''){
		 $url = $this->uri->segment(1);
		 $page_where = $this->data["pages"] = '';
		if(empty($id)){
		  $post_data = $this->input->post();
		  $this->data['post']	= $post_data;	   
	
		}else{
			$post_data = array('UserID' => $id);
			$page_where = array('a.UserID' => $id);

		}
		   $this->data['seo'] = getSeo('');  
		   $conditions = [];
		  if($post_data){
		  	$CategoryIDArr[] = $post_data['CategoryID'];

			 $CategoryIDs = MachineCategoryArray($post_data['CategoryID'], $this->language, $CategoryIDArr);
			 foreach($post_data as $k => $vl){
				 if($vl){
					 if($k == "ManufacturerYear"){
					 $conditions[] = 'machines.ManufacturerYear  >= '.$post_data[$k];
					 }else if($k == "ManufacturerYear2"){
					 $conditions[] = 'machines.ManufacturerYear  <= '.$post_data[$k];	 
					 }else if($k == "PriceFrom"){
					 $conditions[] = 'machines.Price  >= '.$post_data[$k];
					 }else if($k == "PriceTo"){
					 $conditions[] = 'machines.Price  <= '.$post_data[$k];	 
					 }else if($k == "Photo"){
					 $conditions[] = 'machines.TotalPhoto  >= 1';
					 }else if($k == "Video"){
					 $conditions[] =  'machines.'.$k.' IS NOT NULL ';
					 }else if($k == "MachineType" or $k == "Description"){
					 $conditions[] =  "machines_text.$k = '".$vl."'";	 
					 }else if($k == "CategoryID"){
					 $CategoryCond = '(';
					 foreach($CategoryIDs as $CatID)
					 {
					 	$CategoryCond .= "machines.$k = '".$CatID."' OR ";
					 }	
					 $CategoryCond = rtrim($CategoryCond, 'OR ');
					 $CategoryCond .= ')';
					 $conditions[] =  $CategoryCond;	 
					 }else{
			          $conditions[] = "machines.$k = '".$vl."'";
					 }
				 }
			 }

		  $q = implode(" AND ", $conditions);
		  $this->data['result'] = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,$q,'DESC','MachineID','result',array($this->data['per_page'],0));
		  /*echo $this->db->last_query();
		  exit;*/

		   if($page_where){
          $this->data["pages"]    = configPagination($url.'/',get_machine_row($page_where),2,$this->data['per_page']);  
		   }
		  $this->data['view'] = 'frontend/search-result';
          $this->load->view('frontend/layouts/default',$this->data);
			
		  }else{
		  
		  $this->data['results'] = $this->User_model->getAllJoinedData(false,'UserID',$this->language,"users.RoleID ='4'");

		  
		  $this->data['view'] = 'frontend/advanced-search';
          $this->load->view('frontend/layouts/default',$this->data);
			  
		  }
	}

	public function loadMoreMachines()
    {
        $data = [];
        $page = $this->input->post('page');
        $limit = 5;
        $data['data'] = $this->Machine_model->loadMoreMachines($page, $limit);
        $view = $this->load->view('frontend/layouts/machine-listing', $data, TRUE);
        $return = [
            'html' =>  $view,
            'count' =>  count($data['data'])
        ];
        echo json_encode($return);
    }
}