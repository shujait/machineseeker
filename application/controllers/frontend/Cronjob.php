<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjob extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
                
            $this->load->Model(['User_model','Purchase_model','Advertise_model','Site_documents_model'
        ]);
        
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
        $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
        $this->data['TableKey'] = 'PageID';
        $this->data['Table'] = 'pages';
       
    }
    
    
    public function index()
    {
	 $purchase =  $this->Purchase_model->getPurchaseData(array('b.Status' => 2));
          
    }

    public function deactivateUserAccounts()
    {


     
      $purchase =  $this->Purchase_model->getPurchaseData(array('c.RoleID != ' => 1,'c.IsActive' => 1 ,'b.Status' => 4,'DATE_ADD(a.CreatedAt,INTERVAL '.$this->data['site_setting']->FreePackageRelaxation.' DAY) < ' => date('Y-m-d')));
      
    
     
      if($purchase){
        foreach($purchase as $value){
            if($value->IsPaid == 0){
                $this->User_model->update(array('IsActive' => 0),array('UserID' => $value->UserID));
            }

        }
      }
    }


    public function makeBackup(){
       set_time_limit(0);
       $rootPath = FCPATH;//realpath(dirname(__FILE__));

        // Initialize archive object
        $zip = new ZipArchive();
        $file_name = 'files_backup/'.date('d-m-Y').'.zip';
        $zip->open($file_name, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

 //dirname($file_name);

        foreach ($files as $name => $file)
        {

           
            // Skip directories (they would be added automatically)
            if (!$file->isDir())
            {
               
                //echo dirname($file->getFilename()).'<br>';
                $name = '';
                $dir_name = dirname($file->getRealPath());
                $dir_name = explode('\\',$dir_name);
                $name = end($dir_name);

                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath));

                // Add current file to archive
                if($name != '/home/www/files_backup'){
                  $zip->addFile($filePath, $relativePath);  
                }
                
            }
            
        }
        // Zip archive will be created only after closing object
        $zip->close();
    }


    public function exportData() {
        $this->load->dbutil();
        $prefs = array(
            'format' => 'zip',
            'filename' => 'machine.sql'
        );
        $backup = $this->dbutil->backup($prefs);
        $db_name = date("Y-m-d") . '.zip';
        $save = 'database_backup/' . $db_name;
        $this->load->helper('file');
        write_file($save, $backup);
       // $this->load->helper('download');
       // force_download($db_name, $backup);
    }


    public function sendNotificationBeforeExpiry(){
         $tpl = get_email_tpl(array('TplList' => 16));
        // $language_data = getDefaultLanguage();
         $notify_day = 7;
         if($tpl &&  $tpl->NotificationBeforeDays != 0){
            $notify_day = $tpl->NotificationBeforeDays;
         }
        $users_list = $this->Purchase_model->getPurchaseData(array('a.IsExpiryEmailSent' => 0, 'a.IsAutoPayment' => 1, 'c.IsActive' => 1,'DATEDIFF(a.ExpiryDate, CURDATE()) = ' => $notify_day));

        $edata = array();
       $site_setting = $this->getSiteSetting();
       if($users_list){
        foreach ($users_list as $key => $value) {
            $language_data = getLanguageByCode($value->Lang);
           $this->data['user_data'] =  $user_data = $this->User_model->getAllDetail($value->PurchaseID,$language_data->SystemLanguageID);
           // echo $this->db->last_query();
            $search = array(mail_tpl_shortcode('Name'),mail_tpl_shortcode('Package Name'),mail_tpl_shortcode('Expiry Date'));   
            //print_rm($user_data);
            $replace = array($user_data['FullName'],$user_data['PackageTitle'],date('d.m.Y',strtotime($user_data['ExpiryDate'])));  
            $files = array();
            $body = str_replace($search,$replace, $tpl->Description);


            if(file_exists('assets/invoice_pdf/'.$value->PurchaseID.'Invoice.pdf')){
                $files[] = 'assets/invoice_pdf/'.$value->PurchaseID.'Invoice.pdf';
            }else{
                $html = $this->load->view('frontend/layouts/invoice',$this->data, true);
                $this->pdf->CreatePDF($html,$value->PurchaseID.'Invoice');
                $files[] = 'assets/invoice_pdf/'.$value->PurchaseID.'Invoice.pdf';
            }

            
            $edata['from'] = $site_setting->FromEmail;
            $edata['from_name'] = (isset($site_setting->FromName) and !empty($site_setting->FromName))?$site_setting->FromName:$site_setting->SiteName;  
            $edata['to']   = $user_data['Email'];
            $edata['subject']   = $tpl->Subject;     
            $edata['body'] = $body;
           
            if(sendEmail($edata,$files)){
                $this->Purchase_model->update(array('IsExpiryEmailSent' => 1),array('PurchaseID' => $value->PurchaseID));
            }

        }
       }
    }

    public function NotifyAdminCustomerStatus()
    {
        $tpl = get_email_tpl(array('TplList' => 17));
        $site_setting = $this->getSiteSetting();


        //5 Days Check Cronjob
        $time = strtotime(date('Y-m-d').' -5 days');
        $date5 = date("Y-m-d", $time);
        
        $getUsers5DayStatus = $this->User_model->getUserStatus($this->language, $date5);

        
        if($getUsers5DayStatus)
        {   
            foreach($getUsers5DayStatus as $user)
            {
                $Status = '';
                if($user['IsCertifiedTrader'] == 0)
                {
                    $Status .= lang('is_certified_trader').': FALSE <br>';
                }
                if($user['IsPaid'] == 0)
                {
                    $Status .= lang('is_paid').': FALSE <br>';
                }
                if($user['TaxStatusConfirmed'] == 0)
                {
                    $Status .= lang('tax_status_confirmed').': FALSE <br>';
                }
                if($user['VATConfirmed'] == 0)
                {
                    $Status .= lang('vat_confirmed').': FALSE <br>';
                }

                $searchAdmin = array(mail_tpl_shortcode('Name'),mail_tpl_shortcode('Title'),mail_tpl_shortcode('Customer No'),mail_tpl_shortcode('Status'));   
                //print_rm($user_data);
                $replaceAdmin = array('Admin',$user['UName'].' '.$user['Title'],'K-'.str_pad($user['UserID'], 7, '0', STR_PAD_LEFT), $Status);  
                $body = str_replace($searchAdmin,$replaceAdmin, $tpl->Description);

                $edata['from'] = $site_setting->FromEmail;
                $edata['from_name'] = (isset($site_setting->FromName) and !empty($site_setting->FromName))?$site_setting->FromName:$site_setting->SiteName;  
                //$edata['to']   = 'admin@maschinenpilot.de,webmaster@maschinenpilot.de,office@maschinenpilot.de';
                $edata['to']   = 'usmanmazhar51@gmail.com,usman.mazhar@schuja.com';
                $edata['subject']   = $tpl->Subject;     
                $edata['body'] = $body;

                sendEmail($edata);

            }
        }


        //14 Days Check Cronjob
        $time = strtotime(date('Y-m-d').' -14 days');
        $date14 = date("Y-m-d", $time);
        
        $getUsers14DayStatus = $this->User_model->getUserStatus($this->language, $date14);

        
        if($getUsers14DayStatus)
        {   
            foreach($getUsers14DayStatus as $user)
            {
                $Status = '';
                if($user['IsCertifiedTrader'] == 0)
                {
                    $Status .= lang('is_certified_trader').': FALSE <br>';
                }
                if($user['IsPaid'] == 0)
                {
                    $Status .= lang('is_paid').': FALSE <br>';
                }
                if($user['TaxStatusConfirmed'] == 0)
                {
                    $Status .= lang('tax_status_confirmed').': FALSE <br>';
                }
                if($user['VATConfirmed'] == 0)
                {
                    $Status .= lang('vat_confirmed').': FALSE <br>';
                }

                $searchAdmin = array(mail_tpl_shortcode('Name'),mail_tpl_shortcode('Title'),mail_tpl_shortcode('Customer No'),mail_tpl_shortcode('Status'));   
                //print_rm($user_data);
                $replaceAdmin = array('Admin',$user['UName'].' '.$user['Title'],'K-'.str_pad($user['UserID'], 7, '0', STR_PAD_LEFT), $Status);  
                $body = str_replace($searchAdmin,$replaceAdmin, $tpl->Description);

                $edata['from'] = $site_setting->FromEmail;
                $edata['from_name'] = (isset($site_setting->FromName) and !empty($site_setting->FromName))?$site_setting->FromName:$site_setting->SiteName;  
                $edata['to']   = 'admin@maschinenpilot.de,webmaster@maschinenpilot.de,office@maschinenpilot.de';
                //$edata['to']   = 'usmanmazhar51@gmail.com,usman.mazhar@schuja.com';
                $edata['subject']   = $tpl->Subject;     
                $edata['body'] = $body;

                sendEmail($edata);

            }
        }



        //21 Days Check Cronjob
        $time = strtotime(date('Y-m-d').' -21 days');
        $date21 = date("Y-m-d", $time);
        
        $getUsers21DayStatus = $this->User_model->getUserStatus($this->language, $date21);

        
        if($getUsers21DayStatus)
        {   
            foreach($getUsers21DayStatus as $user)
            {
                $this->User_model->update(array('IsActive' => 0),array('UserID' => $user['UserID']));
            }
        }
    }
    public function NotifyAdminCustomerUnCompleteStatus() {
        $notify_day = 3;
        $time = strtotime(date('Y-m-d') . ' -' . $notify_day . ' days');
        $date5 = date("Y-m-d", $time);
        $this->sendEmailToAdmin($date5);
        $notify_day = 5;
        $time = strtotime(date('Y-m-d') . ' -' . $notify_day . ' days');
        $date5 = date("Y-m-d", $time);
        $this->sendEmailToAdmin($date5);
    }

    public function sendEmailToAdmin($date5) {
        $tpl = get_email_tpl(array('TplList' => 19));
        $getUsers = $this->User_model->getUserStatusWithDocument($this->language, $date5);
        $site_setting = $this->getSiteSetting();

        if ($getUsers) {
            foreach ($getUsers as $key => $value) {
                $status = '';
                $true = false;
                $language_data = getLanguageByCode($value['Lang']);
                $documentStatus = $this->Site_documents_model->getUserDocs($value['UserID']);

                if (empty($documentStatus['businessRegistration'])) {
                    $status .= '-Business registration<br>';
                    $true = true;
                }
                if (empty($documentStatus['taxCertificate'])) {
                    $status .= '-Tax status confirmed<br>';
                    $true = true;
                }
                if (!$value['IsPaid']) {
                    $status .= '-Invoice amount: not marked as paid<br>';
                    $true = true;
                }
                $search = array(mail_tpl_shortcode('Customer No'),
                    mail_tpl_shortcode('Name'),
                    mail_tpl_shortcode('Company Name'),
                    mail_tpl_shortcode('Email'),
                    mail_tpl_shortcode('Registered Date'),
                    mail_tpl_shortcode('Active days'),
                    mail_tpl_shortcode('Document Status')
                );
                $replace = array('K-' . str_pad($value['UserID'], 7, '0', STR_PAD_LEFT),
                    $value['Title'],
                    $value['CompanyName'],
                    $value['Email'],
                    date('d.m.Y', strtotime($value['CreatedAt'])),
                    $site_setting->FreePackageRelaxation,
                    $status
                );
                $files = array();
                $body = str_replace($search, $replace, $tpl->Description);
                $edata['from'] = $site_setting->FromEmail;
                $edata['from_name'] = (isset($site_setting->FromName) and ! empty($site_setting->FromName)) ? $site_setting->FromName : $site_setting->SiteName;
                $edata['to'] = $site_setting->FromEmail;
                $edata['subject'] = $tpl->Subject;
                $edata['body'] = $body;
                if ($true)
                    sendEmail($edata);
            }
        }
    }

    public function WaterMarkImages()
    {
        $this->load->library('image_lib');

        $Images = getImage(array('ImageType'=>'MachineFile'), true);

        foreach ($Images as $key => $value) {
            $this->watermarkimage($value['ImageName']);
        }

        $this->load->Model('Machine_model');

        $results = $this->Machine_model->getAllJoinedData(false,'MachineID',$this->language,'');

        foreach ($results as $key => $value) {
            if($value->FeaturedImage != '')
            {
                $this->watermarkimage($value->FeaturedImage);
            }
        }
    }
}