<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
                
            $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            ucfirst($this->router->fetch_class()).'_text_model','User_model','Menu_model','Category_model','Machine_model','Inquiry_model','Wishlist_model','Advertise_model'
        ]);

        
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
        $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
        $this->data['TableKey'] = 'PageID';
        $this->data['Table'] = 'pages';

        $this->data['site_setting'] = $this->getSiteSetting();

		$this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 1');

		$this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2');
		$this->data['advertise'] = $this->Advertise_model->getActiveAllJoinedData(false,'AdvertiseID',$this->language,false,'DESC','AdvertiseID','result');
		
		if($this->session->userdata('admin')){
        $this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows');
		}else{
		$this->data['user_wishlist'] = 0;	
		}
		$this->data['confimation_box'] = confirmationBox();
        $this->data['default_lang'] = getDefaultLanguage();
        $this->data['per_page'] = 10; 
       
    }
     
    
    public function index($url = '')
    {


          $parent                             = $this->data['Parent_model'];
          $child                              = $this->data['Child_model'];
        if($url == '')
        {
            $url = 'home';
        }

        
		 $this->data['result'] = $this->$parent->getActiveAllJoinedData(false,$this->data['TableKey'],$this->language,$this->data['Table'].".Url='". $url."'",'DESC','PageID','row');
		
         $this->data['left'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',$this->language,'menus.MenuPosition = 3 AND ShowOn = '.$this->data['result']->PageID.'');
		
		$this->data['seo'] = getSeo($url,1,$this->language);
		
        $this->data['view'] = 'frontend/content';
          
        $this->load->view('frontend/layouts/default',$this->data);
    }
	
	public function setlanguage($lang){

		$this->changeLanguage($lang);
		
	}
	
		 public function listings()
    {
          //$this->data['top_list'] = getPopularCategory(array(),$this->data['default_lang']->SystemLanguageID,10);
          $this->data['top_list'] = getPopularCategories(array(),$this->data['default_lang']->SystemLanguageID,10);
		  $this->data['seo'] = getSeo('listings');
		  $this->data['view'] = 'frontend/listings';
          $this->load->view('frontend/layouts/default',$this->data);
    }
    
    public function verfiy_account(){
    	if(isset($_GET['key']) && $_GET['key'] != ''){
    		$user_data = $this->User_model->get($_GET['key'],false,'VerificationKey');
    		if($user_data){
    			$this->User_model->update(array('VerificationKey' => '','IsVerified' => 1),array('UserID' => $user_data->UserID));
    			redirect(base_url('login'));
    		}else{
    			redirect(base_url());
    		}

    	}else{
    		redirect(base_url());
    	}
    }
	
	public function category($category = '',$page = '')
    {
		  $top_list = $cat_arr = array();
		  $page = intval($page) ? ($page - 1) * $this->data['per_page'] : 0;

		  if($category)
		  {
              $para = explode('-',$category);
              $this->data['category'] = $category;

              $category_data  = $this->Category_model->getMultipleRows(array('Slug' => $category),false);
              $this->data['categoryID'] = $category_data[0]->CategoryID;

              $where = '';
              if($category_data){
                $categories = array_column($category_data, 'CategoryID');

                foreach ($categories as $key => $value) {
                    $where .= ' OR (machines.CategoryID = '.$value. ')';
                    $get_more_parent = $this->Category_model->getMultipleRows(array('ParentID' => $value),true);
                    if($get_more_parent){
                        $p_categories = array_column($get_more_parent, 'CategoryID');
                        foreach ($p_categories as $p_value) {
                            $where .= ' OR (machines.CategoryID = '.$p_value.')';
                            $get_more_grant_parent = $this->Category_model->getMultipleRows(array('ParentID' => $p_value),true);
                            if($get_more_grant_parent){
                              $g_p_categories = array_column($get_more_grant_parent, 'CategoryID');
                              foreach ($g_p_categories as $g_p_value) {
                                $where .= ' OR (machines.CategoryID = '.$g_p_value.')';
                              }
                            }
                        }
                    }
                }
                $where .= '';
              }
              $query = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,"(machines.CategoryID='".$this->data['categoryID']."') $where ",'DESC','MachineID','result',array($this->data['per_page'],$page),'Title');
              $top_list  = array('a.CategoryID !=' =>  $this->data['categoryID']);
              $this->data['cat_name'] =  str_replace($this->data['categoryID'],' ',ucwords(string_replace($category,'/\-+/',' ')));
              $cat_arr = array('a.CategoryID' => $this->data['categoryID']);
              $this->data['cat_data'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.Slug = "'.$category.'"');
              if($this->data['cat_data']){
                $this->data['cat_name'] = $this->data['cat_data'][0]->Title;
              }

		  }else{
              $query = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,false,'DESC','MachineID','result',array($this->data['per_page'],$page));
              $this->data['cat_name'] =  lang('all-categories');
              $this->data['categoryID']	 = '';
		  }
		  $this->data['data'] = $query;

          $this->data['top_list'] = getPopularCategory($top_list,$this->data['default_lang']->SystemLanguageID,10);
          //$this->data['top_list'] = getPopularCategories($top_list,$this->data['default_lang']->SystemLanguageID,10);
          //echo $this->db->last_query();exit;

          $this->data["pages"]    = configPagination('category/'.$category,get_machine_row($cat_arr),2,$this->data['per_page']);

		 if($category){
		  $this->data['seo'] = getSeo($this->data['categoryID'],2);
		 }else{
		  $this->data['seo'] = getSeo('');
		 }
		  $this->data['view'] = 'frontend/category';
          
          $this->load->view('frontend/layouts/default',$this->data);
    }
	
	
    
    public function detail($title = '')
    {

    	 $this->data['URL_Title'] = $title;
		 $last_str = explode('-',$title);
		 $id = end($last_str);

		$this->data['result'] = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,"machines.MachineID='".$id."'",'DESC','MachineID','row',[],'Title',true,false);

		$this->data['dealerData']=$this->User_model->getUserDataByID($this->data['result']->UserID,$this->language);
		
		if($this->data['result']){

			$this->data['category'] = $this->Category_model->getActiveAllJoinedData(false,'CategoryID',$this->language,"categories_text.CategoryID='". $this->data['result']->CategoryID."'",'DESC','CategoryID','row');

			if(!$this->data['category']){
				$this->data['category'] = $this->Category_model->getActiveAllJoinedData(false,'CategoryID',false,"categories_text.CategoryID='". $this->data['result']->CategoryID."'",'DESC','CategoryID','row');
			}
		}

		if($this->session->userdata('admin')){
		 $this->data['wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID'],'MachineID' => $id),'num_rows');
		}else{
		 $this->data['wishlist'] = 0;
		}
		$key = 'TotatSeen';
		$this->db->set($key, $key.'+1', false);
        $this->db->where('MachineID', $id);
        $this->db->update('machines');
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['seo'] = getSeo($this->data['result']->CategoryID,2);
        $this->data['seo']['title'] = $this->data['result']->Title;
        $this->data['view'] = 'frontend/detail';
          
        $this->load->view('frontend/layouts/default',$this->data);
    }
    public function details($title = '')
    {
        $title='test-'.$title;
        $this->data['URL_Title'] = $title;
		 $last_str = explode('-',$title);
		 $id = end($last_str);
 
 		 $this->data['result'] = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,"machines.MachineID='".$id."'",'DESC','MachineID','row',[],'Title',true,false);


$this->data['dealerData']=$this->User_model->getUserDataByID($this->data['result']->UserID,$this->language);
		
		if($this->data['result']){

			$this->data['category'] = $this->Category_model->getActiveAllJoinedData(false,'CategoryID',$this->language,"categories_text.CategoryID='". $this->data['result']->CategoryID."'",'DESC','CategoryID','row');

			if(!$this->data['category']){
				$this->data['category'] = $this->Category_model->getActiveAllJoinedData(false,'CategoryID',false,"categories_text.CategoryID='". $this->data['result']->CategoryID."'",'DESC','CategoryID','row');
			}

		}
 
		if($this->session->userdata('admin')){
		 $this->data['wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID'],'MachineID' => $id),'num_rows');
		}else{
		 $this->data['wishlist'] = 0;
		}
		$key = 'TotatSeen';
		$this->db->set($key, $key.'+1', false);
        $this->db->where('MachineID', $id);
        $this->db->update('machines');
        $this->data['seo'] = getSeo($this->data['result']->CategoryID,2);
        $this->data['seo']['title'] = $this->data['result']->Title;
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['view'] = 'frontend/details';
          
        $this->load->view('frontend/layouts/default',$this->data);
    }
	
	 public function dealer($page = '')
    {
    	$this->data['dealers'] = $this->User_model->getActiveAllJoinedData(false,'UserID',$this->language,'users.RoleID = 4 AND users.Hide=0 AND users.IsActive=1 AND users.IsVerified=1', 'ASC', 'SortOrder', 'result', [$this->data['per_page'],0]);
    	/*echo $this->db->last_query();

    	print_rm($this->data['dealers']);*/
		 $this->data['seo'] = getSeo('');
         $this->data['view'] = 'frontend/dealer';
         $this->load->view('frontend/layouts/default',$this->data);
    }
	
	 public function dealer_listing($category = '',$page = '')
    {
		  $query = array();
		  $page = intval($page) ? ($page- 1) * $this->data['per_page'] : 0;
		  if($category){
		  $categoryID = explode('-',$category);
		  $query = $this->User_model->getCategoryUsers('UserID',"find_in_set('".end($categoryID)."', user_package_purchase.Category) <> 0",'DESC','UserID','result',array($this->data['per_page'],$page));
		  $this->data['cat_name'] =  str_replace(end($categoryID),' ',ucwords(string_replace($category,'/\-+/',' ')));
		  $this->data['cat_data'] = $this->Category_model->getAllJoinedData(false,'CategoryID',$this->language,'categories.CategoryID = '.end($categoryID));
		  if($this->data['cat_data']){
		  	$this->data['cat_name'] = $this->data['cat_data'][0]->Title;
		  }
		  }
		  $this->data['result'] = $query;
		  $this->data['data'] = '';
          $this->data["pages"]    = configPagination('dealer/'.$category,get_category_dealer('find_in_set("'.end($categoryID).'", Category) <> 0'),2,$this->data['per_page']);
		 $this->data['seo'] = getSeo(end($categoryID),2);
         $this->data['view'] = 'frontend/dealer-listing';
         $this->load->view('frontend/layouts/default',$this->data);
    }
	
	 public function dealer_ads($dealer = '',$page = '')
    {
		  $query = $cat_arr = array();
		  $page = intval($page) ? ($page- 1) * $this->data['per_page'] : 0;
		  if($dealer){
		  $dealerID = explode('-',$dealer);
		  $dealer_name = str_replace(end($dealerID),' ',ucwords(string_replace($dealer,'/\-+/',' '))); 	  
		  $query = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,"machines.UserID='".end($dealerID)."'",'DESC','MachineID','result',array($this->data['per_page'],$page));
		  $this->data['user_info'] = $this->User_model->getUser(end($dealerID));
		  $this->data['cat_name'] =  $dealer_name;
		  $cat_arr = array('a.UserID' => end($dealerID));	  
	  
		  }
		 $this->data['data'] = $query;
		 $this->data['result'] = '';
         $this->data["pages"]    = configPagination('dealer-listing/'.$dealer,get_machine_row($cat_arr),2,$this->data['per_page']);
		 $this->data['seo'] = getSeo('');
         $this->data['view'] = 'frontend/dealer-listing';
         $this->load->view('frontend/layouts/default',$this->data);
    }

    public function getSubCategory()
    {
        $CategoryID = $this->input->post('CategoryID');
        $CategoryType = $this->input->post('CategoryType');
        $PageType = $this->input->post('PageType');
        if($CategoryID == '')
        {
            $success['error']   = true;
            $success['success'] = false;
            $success['option'] = '';
            echo json_encode($success);
            exit();   
        }
        $active_subcategory="`a`.`CategoryID` IN (SELECT `CategoryID` FROM `machines` where `machines`.`IsActive`=1)";
        $categoryData = getSubCategoryBy(array('a.ParentID'=>$CategoryID),$isArr = false,$active_subcategory);
        $select = '';
        if($categoryData)
        {
        	if($PageType == 'result')
        	{
        		$select = '<div class="row">
								<label class="col-md-12 control-label" for="Category">'.lang('sub_category').'</label>
								<div class="col-md-12">
									<select class="select2 form-control" data-type="'.($CategoryType == 'category' ? 'subcategory' : 'subsubcategory').'" data-page-type="result" name="CategoryID" id="category" required>
                						<option value="">--- '.lang('select_sub_category').' ---</option>';
										foreach ($categoryData as $key => $value) {
											$select .= '<option value="'.$value->CategoryID.'">'.$value->Title.'</option>';
										}
	            $select .= '</select></div></div>';
        	}
        	else
        	{
        		$select = '<div class="row"><label class="offset-1 col-md-3 control-label pdtp-10" for="Category">'.lang('category').'</label><div class="col-md-6"><select class="select2 form-control" data-type="'.($CategoryType == 'category' ? 'subcategory' : 'subsubcategory').'" data-page-type="search" name="CategoryID" id="category" required>
                <option value="">--- '.lang('select_sub_category').' ---</option>';
	            foreach ($categoryData as $key => $value) {
	                $select .= '<option value="'.$value->CategoryID.'">'.$value->Title.'</option>';
	            }
	            $select .= '</select></div></div>';
        	}

            $success['error']   = false;
            $success['success'] = true;
            $success['option'] = $select;
        }
        else{
            $success['error']   = true;
            $success['success'] = false;
            $success['option'] = $select;
        }
        echo json_encode($success);
        exit();    
    }
	
	
	public function add_wishlist(){	

		if(!($this->session->userdata('admin'))) {
	        $errors['error'] =  lang('user-not-login');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'login';
            echo json_encode($errors);
            exit;  		
			
		}else{
			
		$arr = array(
		'UserID' =>  $this->session->userdata['admin']['UserID'],
		'MachineID' =>  $this->input->post('machine_id')
		);
			
       	$wishlist = getValue('wishlist',$arr,'num_rows'); 

		if($wishlist == 0) {

			if($this->Wishlist_model->save($arr)){
			$success['error']   = false;	
			$success['success'] =  lang('save-wishlist');
			$success['redirect'] = false;
			$success['reload'] = true;	
			echo json_encode($success);
			exit;		
				}
			}else{
			
			$this->Wishlist_model->delete($arr);
			$success['error']   = false;	
			$success['success'] =  lang('remove-wishlist');
			$success['redirect'] = false;
			$success['reload'] = true;	
			echo json_encode($success);
			exit;		
			
		}
			
		}

	}
	
	public function remove_wishlist(){	

		if(!($this->session->userdata('admin'))) {
	        $errors['error'] =  lang('user-not-login');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'login';
            echo json_encode($errors);
            exit;  		
			
		}else{
			
		$rec = $this->input->post('machine_id');
		foreach($rec as $v){
		$arr = array(
		'UserID' =>  $this->session->userdata['admin']['UserID'],
		'MachineID' => $v
		);
			
       	$wishlist = getValue('wishlist',$arr,'num_rows'); 

		if($wishlist > 0) {

			$this->Wishlist_model->delete($arr);
			$success['error']   = false;	
			$success['success'] =  lang('remove-wishlist');
			$success['redirect'] = false;
			$success['reload'] = true;	
			echo json_encode($success);
			exit;
			
		    }
		  }
		}
	}

    public function wishlist(){
        if(!($this->session->userdata('admin'))) {
            redirect(base_url('login'));
        }
     $this->data['record'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'result'); 
	 $this->data['view'] = 'frontend/wish-list';
	 $this->data['seo'] = '';
     $this->load->view('frontend/layouts/default',$this->data);

	}
	
	public function send_inquiry()
    {
    	$post_data = $this->input->post();
		$errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('name', lang('name'), 'required|trim');
		$this->form_validation->set_rules('company_name', lang('company_name'), 'required|trim');
		$this->form_validation->set_rules('street_address', lang('street_address'), 'required|trim');
		$this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
		$this->form_validation->set_rules('postcode', lang('postcode'), 'required|trim');
		$this->form_validation->set_rules('phone', lang('ph'), 'required|trim');
		$this->form_validation->set_rules('city', lang('city'), 'required|trim');
		if(isset($post_data['dealer']) and $post_data['dealer'] == 1){
			$this->form_validation->set_rules('similarmahines', lang('enquiry-similarmahines-desc'), 'required|trim');
			$this->form_validation->set_rules('third_party_dealer', lang('enquiry-dealer-check'), 'required|trim');
		}	
		if($this->data['confimation_box']){
			$this->form_validation->set_rules('policy_read', lang('privacy-policy'), 'required|trim');
		}	
        if ($this->form_validation->run() == FALSE)
        {
                $errors['error']='';
//              $errors['error'] = validation_errors(); 
                if(empty($post_data['policy_read']))
                    $errors['error'].= "<div class='error'>".lang('agree_with_privacy_policy')."</div>";
                else 
                    $errors['error'].= "<div class='error'>".lang('please_fill_all_fields')."</div>";
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {     
	   //$post_data = $this->input->post();
			
	   $machine_id       = $post_data['machine_id'];
	   unset($post_data['machine_id']);	
	   $dealerCheck = 0;
	    if(isset($post_data['dealer']) && $post_data['dealer'] == 1){
	    	$dealerCheck = 1;
		    unset($post_data['dealer']);	
			$post_data['dealer'] = $post_data['similarmahines'];
		}	
			
			
	   $save_data['MachineID']                    = $machine_id;
	   $save_data['InquiryDetail']                = json_encode($post_data);
       $this->Inquiry_model->save($save_data);	
			
		
	   $rec = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,"machines.MachineID='".$machine_id."'",'DESC','MachineID','row');
	   $link = "<a href=\"".friendly_url('detail/',$rec->Title.' '.$machine_id)."\" target=\"_blank\">".$rec->Title."</a>";

	   $getCategory = getRowCategory(array('a.CategoryID'=>$rec->CategoryID));

	   $getAllUsersOfCategory = $this->User_model->getAllUsersOfCategory($rec->CategoryID, $getCategory->ParentID);

	   $user = get_user_info(array('a.UserID' => $rec->UserID));

	   $CheckUser = get_user_info(array('a.Email' => $post_data['email']));
			
	   $tpl = get_email_tpl(array('TplList' => 9), $user->Lang);
 if($tpl){

   		   $search = array(mail_tpl_shortcode('Dealer/Reseller'),mail_tpl_shortcode('Category'),mail_tpl_shortcode('Link'),mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Phone'),mail_tpl_shortcode('Registered'),
                       mail_tpl_shortcode('Company Name'),mail_tpl_shortcode('Address'),mail_tpl_shortcode('PostalCode'),mail_tpl_shortcode('City')
                       ,mail_tpl_shortcode('Similar Machines')
                       );
				//print_rm($rec);
		   $replace = array($user->UName.' '.$user->Title.(($dealerCheck == 1 && isset($post_data['third_party_dealer']) && $post_data['third_party_dealer'] == 1) ? '<br><br>'.lang('dealer_inquiry_line') : ''),$getCategory->Title,$link,$post_data['name'],$post_data['email'],$post_data['CountryCodeInquiry'].$post_data['phone'],(!empty($CheckUser) ? lang('registered') : lang('not-registered'))
                       ,$post_data['company_name']  
                       ,$post_data['street_address']   
                       ,$post_data['postcode']   
                       ,$post_data['city']   
                       , $post_data['similarmahines']);
		   $body = str_replace($search,$replace, $tpl->Description);
		    
                   $data['from'] = $post_data['email'];
		   $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName))?$this->data['site_setting']->FromName:$this->data['site_setting']->SiteName;	
		   $data['to']   = $user->Email ;
		   $data['subject']   = $tpl->Subject;		
		   $data['body'] = $body;
 		   sendEmail($data); 


		   $search = array(mail_tpl_shortcode('Dealer/Reseller'),mail_tpl_shortcode('Category'),mail_tpl_shortcode('Link'),mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Phone'),mail_tpl_shortcode('Registered'),
                        mail_tpl_shortcode('Company Name'),mail_tpl_shortcode('Address'),mail_tpl_shortcode('PostalCode'),mail_tpl_shortcode('City') ,  mail_tpl_shortcode('Similar Machines'));
					
		   $replace = array($post_data['name'],$getCategory->Title,$link,$post_data['name'],$post_data['email'],$post_data['CountryCodeInquiry'].$post_data['phone'],(!empty($CheckUser) ? lang('registered') : lang('not-registered'))
                      ,$post_data['company_name']  
                       ,$post_data['street_address']   
                       ,$post_data['postcode']   
                       ,$post_data['city']   
                       , $post_data['similarmahines']);	
		   $body = str_replace($search,$replace, $tpl->Description);
		   $data['from'] = $post_data['email'];
		   $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName))?$this->data['site_setting']->FromName:$this->data['site_setting']->SiteName;	
		   $data['to']   = $post_data['email'] ;
		   $data['subject']   = $tpl->Subject;		
		   $data['body'] = $body;
        sendEmail($data);

	   	if($getAllUsersOfCategory && $dealerCheck == 1 && isset($post_data['third_party_dealer']) && $post_data['third_party_dealer'] == 1)
	   	{
	   	   foreach ($getAllUsersOfCategory as $key => $value) {

			   $search = array(mail_tpl_shortcode('Dealer/Reseller'),mail_tpl_shortcode('Category'),mail_tpl_shortcode('Link'),mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Phone'),mail_tpl_shortcode('Registered')
                                ,mail_tpl_shortcode('Company Name'),mail_tpl_shortcode('Address'),mail_tpl_shortcode('PostalCode'),mail_tpl_shortcode('City')   ,mail_tpl_shortcode('Similar Machines'));
					
			   $replace = array($value->UName.' '.$value->Title,$getCategory->Title,$link,$post_data['name'],$post_data['email'],$post_data['CountryCodeInquiry'].$post_data['phone'],(!empty($CheckUser) ? lang('registered') : lang('not-registered'))
                     ,$post_data['company_name']  
                       ,$post_data['street_address']   
                       ,$post_data['postcode']   
                       ,$post_data['city']   
                       , $post_data['similarmahines']);	
			   $body = str_replace($search,$replace, $tpl->Description);
			   $data['from'] = $post_data['email'];
			   $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName))?$this->data['site_setting']->FromName:$this->data['site_setting']->SiteName;	
			   $data['to']   = $value->Email ;
			   $data['subject']   = $tpl->Subject;		
			   $data['body'] = $body;
			   sendEmail($data);

		   }
		}	
	   }
	   $success['error']   = false;	
	   $success['success'] =  lang('send-inquiry-success');
	   $success['redirect'] = false;
	   $success['reload'] = true;	
	   echo json_encode($success);
	   exit;		
			
		}
    }

	public function send_inquiry_all_dealer()
    {
		$errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('category', lang('category'), 'required|trim');
		$this->form_validation->set_rules('first_name', lang('first_name'), 'required|trim');
		$this->form_validation->set_rules('last_name', lang('last_name'), 'required|trim');
		$this->form_validation->set_rules('company_name', lang('company_name'), 'required|trim');
		$this->form_validation->set_rules('street_address', lang('street_address'), 'required|trim');
		$this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
		$this->form_validation->set_rules('city', lang('city'), 'required|trim');
		$this->form_validation->set_rules('postcode', lang('postcode'), 'required|trim');
		$this->form_validation->set_rules('phone', lang('ph'), 'required|trim');
		$this->form_validation->set_rules('fax', lang('fax'), 'required|trim');
		$this->form_validation->set_rules('replyBy', lang('customer-reply'), 'required|trim');
		$this->form_validation->set_rules('message', lang('message'), 'required|trim');
		if($this->data['confimation_box']){
		$this->form_validation->set_rules('policy_read', lang('privacy-policy'), 'required|trim');
		}
        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {     
	    $post_data = $this->input->post();
	    $category       = $post_data['category'];
		$rec = get_machine_row(array('a.CategoryID' => $category),'result');
		if($rec){
		foreach($rec as $vl){
		$reseller = (isset($post_data['reseller']) and !empty($post_data['reseller']))?$post_data['reseller']:'No';
	   $user = get_user_info(array('a.UserID' => $vl->UserID));
	    //$categoryName = getCategory(array('b.CategoryTextID' => $category),'row')->Title;		
	    $categoryName = getCategories($this->language,'categories.CategoryID = '.$category);
	    $categoryName = $categoryName[0]->Title;		
	   $tpl = get_email_tpl(array('TplList' => 9), $user->Lang);
				if($tpl){
	   $search = array(mail_tpl_shortcode('Category'),mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Phone'),mail_tpl_shortcode('Fax'),mail_tpl_shortcode('Company Name'),mail_tpl_shortcode('PostalCode'),mail_tpl_shortcode('City'),mail_tpl_shortcode('Address'),mail_tpl_shortcode('Message'),mail_tpl_shortcode('Reply By'),mail_tpl_shortcode('Dealer/Reseller'),mail_tpl_shortcode('Site Name'),mail_tpl_shortcode('Site URL'));
			
	   $replace = array($categoryName,$post_data['first_name'].' '.$post_data['last_name'],$post_data['email'],$post_data['CountryCodeInquiry'].$post_data['phone'],$post_data['fax'],$post_data['company_name'],$post_data['postcode'],$post_data['city'],$post_data['street_address'],$post_data['message'],$post_data['replyBy'],$reseller,$this->data['site_setting']->SiteName,base_url());	
	    $body = str_replace($search,$replace, $tpl->Description);
	    $data['from'] = $post_data['email'];
	   $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName))?$this->data['site_setting']->FromName:$this->data['site_setting']->SiteName;	
	   $data['to']   = $user->Email;
	   $data['subject']   = $tpl->Subject;		
	   $data['body'] = $body;
	   sendEmail($data);
				}
		 }
		}
	   $success['error']   = false;	
	   $success['success'] =  lang('send-inquiry-success');
	   $success['redirect'] = false;
	   $success['reload'] = true;	
	   echo json_encode($success);
	   exit;		
			
		}
    }
	
	public function share_to_friend()
    {
		$errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('from_name', lang('your').' '.lang('name'), 'required|trim');
		$this->form_validation->set_rules('from_email', lang('your').' '.lang('email'), 'required|trim');
		$this->form_validation->set_rules('to_email',lang('recipient').' '.lang('email'), 'required|trim');
		$this->form_validation->set_rules('message', lang('message'), 'required|trim');

        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {     
	   $post_data = $this->input->post();
			
	   $machine_id       = $post_data['machine_id'];
			
	   unset($post_data['machine_id']);	
	   $save_data['MachineID']                    = $machine_id;
			
	   $save_data['InquiryDetail']                = json_encode($post_data);
			
	  $save_data['Status']                    = 2;
			
       $this->Inquiry_model->save($save_data);		
		
	   $rec = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,"machines.MachineID='".$machine_id."'",'DESC','MachineID','row');
	   $link = "<a href=\"".friendly_url('detail/',$rec->Title.' '.$machine_id)."\" target=\"_blank\">".$rec->Title."</a>";   				
				
	   $tpl = get_email_tpl(array('TplList' => 7));
				if($tpl){
	   $search = array(mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Message'),mail_tpl_shortcode('Machine'),mail_tpl_shortcode('Site Name'),mail_tpl_shortcode('Site URL'));
			
	   $replace = array($post_data['from_name'],$post_data['from_email'],$post_data['message'],$link,$this->data['site_setting']->SiteName,base_url());	
	   $body = str_replace($search,$replace, $tpl->Description);
	   $data['from'] = $post_data['from_email'];
	   $data['from_name'] = (isset($post_data['from_name']) and !empty($post_data['from_name']))?$post_data['from_name']:$this->data['site_setting']->FromName;	
	   $data['to']   = $post_data['to_email'];
	   $data['subject']   = $tpl->Subject;		
	   $data['body'] = $body;
	   sendEmail($data);		
				}
		$success['error']   = false;	
	    $success['success'] =  lang('send-request-success');
		$success['redirect'] = false;
		$success['reload'] = true;	
		echo json_encode($success);
		exit;		
			
		}
    }
	
	public function report()
    {
		$errors = array();
		$post_data = $this->input->post();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$this->form_validation->set_rules('from_name', lang('your').' '.lang('name'), 'required|trim');
		$this->form_validation->set_rules('from_email', lang('your').' '.lang('email'), 'required|trim');
		$this->form_validation->set_rules('report_type', lang('reason-type'), 'required|trim');
        if(isset($post_data['report_type']) and $post_data['report_type'] == 7){
		$this->form_validation->set_rules('message', lang('message'), 'required|trim');
		}
        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {     
			
	   $machine_id       = $post_data['machine_id'];
 	    unset($post_data['machine_id']);
	if($post_data['report_type'] == 7){
		   $reason = $post_data['message'];
		    unset($post_data['report_type']);	
			$post_data['message'] = $reason;
		}else{
		   $reason = $post_data['report_type']; 
		 }
	   		
	   $save_data['MachineID']                    = $machine_id;
			
	   $save_data['InquiryDetail']                = json_encode($post_data);
			
	   $save_data['Status']                    = 3;
			
       $this->Inquiry_model->save($save_data);		
		
	   $rec = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,"machines.MachineID='".$machine_id."'",'DESC','MachineID','row');
		
	   $link = "<a href=\"".friendly_url('detail/',$rec->Title.' '.$machine_id)."\" target=\"_blank\">".$rec->Title."</a>";   				
		
	   $user = get_user_info(array('a.UserID' => $rec->UserID));				
		
	   $tpl = get_email_tpl(array('TplList' => 8), $user->Lang);
		
				if($tpl){
	   $search = array(mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Machine Report Reason'),mail_tpl_shortcode('Machine'),mail_tpl_shortcode('Site Name'),mail_tpl_shortcode('Site URL'));
			
	   $replace = array($post_data['from_name'],$post_data['from_email'],$reason,$link,$this->data['site_setting']->SiteName,base_url());	
	   $body = str_replace($search,$replace, $tpl->Description);
	   $data['from'] = $post_data['from_email'];
	   $data['from_name'] = (isset($post_data['from_name']) and !empty($post_data['from_name']))?$post_data['from_name']:$this->data['site_setting']->FromName;	
	   $data['to']   = $user->Email;
	   $data['subject']   = $tpl->Subject;		
	   $data['body'] = $body;
	   sendEmail($data);
			
				}
		$success['error']   = false;	
	    $success['success'] =  lang('send-request-success');
		$success['redirect'] = false;
		$success['reload'] = true;	
		echo json_encode($success);
		exit;		
			
		}
    }
	
	public function contact_us_mail()
    {
		$errors = array();
		$post_data = $this->input->post();
		$this->form_validation->set_rules('company_name', lang('company_name'), 'required|trim');
		$this->form_validation->set_rules('name', lang('name'), 'required|trim');
		$this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
		//$this->form_validation->set_rules('street_address', lang('street_address'), 'required|trim');
		//$this->form_validation->set_rules('postcode', lang('postcode'), 'required|trim');
		//$this->form_validation->set_rules('city', lang('city'), 'required|trim');
		//$this->form_validation->set_rules('phone', lang('ph'), 'required|trim');
		//$this->form_validation->set_rules('fax', lang('fax'), 'required|trim');
		//$this->form_validation->set_rules('message', lang('message'), 'required|trim');
        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else{
	   $user = get_user_info(array('a.UserID' => 1));						
	   $tpl = get_email_tpl(array('TplList' => 12),$user->Lang);
	   if($tpl){
	   $search = array(mail_tpl_shortcode('Company Name'),mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Address'),mail_tpl_shortcode('PostalCode'),mail_tpl_shortcode('City'),mail_tpl_shortcode('Phone'),mail_tpl_shortcode('Fax'),mail_tpl_shortcode('Message'),mail_tpl_shortcode('Site Name'),mail_tpl_shortcode('Site URL'));
			
	   $replace = array($post_data['company_name'],$post_data['title'].' '.$post_data['name'],$post_data['email'],$post_data['street_address'],$post_data['postcode'],$post_data['city'],$post_data['phone'],$post_data['fax'],$post_data['message'],$this->data['site_setting']->SiteName,base_url());	
	   $body = str_replace($search,$replace, $tpl->Description);
	   $data['from'] = $post_data['email'];
	   $data['from_name'] = (isset($post_data['name']) and !empty($post_data['name']))?$post_data['title'].' '.$post_data['name']:$this->data['site_setting']->FromName;	
	   $data['to']   = $this->data['site_setting']->FromEmail;
	   $data['subject']   = str_replace('[date-time]', date('d.m.Y H:i:s'), $tpl->Subject);		
	   $data['body'] = $body;

	   sendEmail($data);
			
				}
		$success['error']   = false;	
	    $success['success'] =  lang('send-request-success');
		$success['redirect'] = false;
		$success['reload'] = true;	
		echo json_encode($success);
		exit;		
			
		}
    }
	
	public function autocompletes(){
		$col = $this->input->get('type');
		$arr = array();
	    $rec = $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,"machines_text.$col LIKE '%".$this->input->get('term')."%'",'DESC','MachineID','result');

		if($rec){
		foreach($rec as $val)
		{
			$arr[] = $val->$col;
		}
		}
		echo json_encode($arr);
	}
}