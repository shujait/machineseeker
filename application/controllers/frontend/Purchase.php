<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//use LinkedInLibrary\LinkedIn;
//use Happyr\LinkedIn\LinkedIn;

use Hybridauth\Hybridauth;
class Purchase extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->Model([
            'User_model',
            'User_text_model',
            'Role_model',
            'Modules_users_rights_model',
            'Module_model',
            'Feature_model',
            'Package_model',
            'Menu_model',
            'Purchase_model'
        ]);

        $this->data['language'] = $this->language;
        $this->data['Parent_model'] = 'User_model';
        $this->data['Child_model'] = 'User_text_model';
        $this->data['TableKey'] = 'UserID';
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false, 'MenuID', false, 'menus.MenuPosition = 1');
        $this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false, 'MenuID', false, 'menus.MenuPosition = 2');
        if ($this->session->userdata('admin')) {
            $this->data['user_wishlist'] = getValue('wishlist', array('UserID' => $this->session->userdata['admin']['UserID']), 'num_rows');
        } else {
            $this->data['user_wishlist'] = 0;
        }
        $this->data['confimation_box'] = confirmationBox();
    }


    //User Login
    public function login()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/dashboard'));
        }
        $this->data['seo'] = getSeo('');
        $this->data['seo']['title'] = lang('login');
        $this->data['view'] = 'frontend/login';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    //Reset Password
    public function resetPassword($UserID = '')
    {

        $this->data['UserID'] = base64_decode($UserID);
        $this->data['seo'] = getSeo('');
        $this->data['seo']['title'] = lang('reset-password');
        $this->data['view'] = 'frontend/reset-password';
        $this->load->view('frontend/layouts/default', $this->data);

    }

    public function fblogin($login="")
    {
        $_SESSION['FacebookLogin'] = isset($login) ? $login: "";
        $_SESSION['plan_id'] = $_GET['plan_id'];
        $_SESSION['plan_row'] = $_GET['duration'];

        $fb = new Facebook\Facebook([
            'app_id' => FACEBOOOK_ID,
            'app_secret' => FACEBOOOk_SECRET,
            'default_graph_version' => 'v9.0',
            'persistent_data_handler'=>'session'
        ]);
  
        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email','public_profile','user_birthday'];
        if(ENVIRONMENT == 'development')
        {
            $permissions = ['email','public_profile'];
        }
        // For more permissions like user location etc you need to send your application for review
        if($Type == "login"){
            $loginUrl = $helper->getLoginUrl(base_url("fbcallback/login"), $permissions);
        }else {
            $loginUrl = $helper->getLoginUrl(base_url("fbcallback"), $permissions);
        }
        header("location: ".$loginUrl);
    }

    public function fbcallback($login=""){

        $fb = new Facebook\Facebook([
            'app_id' => FACEBOOOK_ID,
            'app_secret' => FACEBOOOk_SECRET,
            'default_graph_version' => 'v9.0',
            'persistent_data_handler'=>'session'
        ]);
        
        $helper = $fb->getRedirectLoginHelper();  
  
        try {
            $accessToken = $helper->getAccessToken();
        }catch(Facebook\Exceptions\FacebookResponseException $e) {  
          // When Graph returns an error  
          echo 'Graph returned an error: ' . $e->getMessage();  
          exit;  
        } catch(Facebook\Exceptions\FacebookSDKException $e) {  
          // When validation fails or other local issues  
          echo 'Facebook SDK returned an error: ' . $e->getMessage();  
          exit;  
        }
 
 
        try {
          // Get the Facebook\GraphNodes\GraphUser object for the current user.
          // If you provided a 'default_access_token', the '{access-token}' is optional.
          $response = $fb->get('/me?fields=id,name,email,first_name,last_name,birthday,location,gender', $accessToken);
         // print_r($response);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          echo 'ERROR: Graph ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          echo 'ERROR: validation fails ' . $e->getMessage();
          exit;
        }
    
        // User Information Retrival begins................................................
        $me = $response->getGraphUser();
        
        $location = $me->getProperty('location');

        $_SESSION['SocialMediaSession'] = array(
            "OAuthProvider" => "facebook",
            "OAuthUid" => $me->getProperty('id'),
            "FirstName" => $me->getProperty('first_name'),
            "LastName" => $me->getProperty('last_name'),
            "Email" => $me->getProperty('email'),
            "Gender" => $me->getProperty('gender'),
            "Location" => $location['name']
        );

        if($_SESSION['FacebookLogin'] == 1){
            $user = $this->db->query("Select * from users where OAuthProvider='facebook' AND OAuthUid='".$me->getProperty('id')."' AND Email='".$me->getProperty('email')."'")->row_array();
            if($user){
                $this->User_model->update(array('LastLogin' => date('Y-m-d')), array('UserID' => $user['UserID']));
                $this->session->set_userdata('lang',$user['Lang']);
                $this->session->set_userdata('admin', $user);

                $redirect = base_url('cms/machine');
                header("location: ".$redirect);
            }else{
                $redirect = base_url('login?error=social');
                header("location: ".$redirect);
            }
        }else{
            $redirect = base_url('tarife?popup=open');
            header("location: ".$redirect);
        }
    }

    public function googlelogin($login=""){
        $_SESSION['GoogleLogin'] = isset($login) ? $login: "";
        $_SESSION['plan_id'] = $_GET['plan_id'];
        $_SESSION['plan_row'] = $_GET['duration'];
        

        //Create Client Request to access Google API
        $client = new Google_Client();
        $client->setApplicationName("MachinePilot");
        $client->setClientId(GOOGLE_ID);
        $client->setClientSecret(GOOGLE_SECRET);
        $client->setRedirectUri(base_url().GOOGLE_REDIRECT);
        $client->addScope("email");
        $client->addScope("profile");

        //Send Client Request
        $objOAuthService = new Google_Service_Oauth2($client);
        
        $authUrl = $client->createAuthUrl();

        header('Location: '.$authUrl);
    }

    public function googlecallback($Type = "")
    {
        if($Type == "login"){
            $redirect_uri = base_url().GOOGLE_REDIRECT.'/login';
        }else{
            $redirect_uri = base_url().GOOGLE_REDIRECT;
        }
        //Create Client Request to access Google API
        $client = new Google_Client();
        $client->setApplicationName("MachinePilot");
        $client->setClientId(GOOGLE_ID);
        $client->setClientSecret(GOOGLE_SECRET);
        $client->setRedirectUri($redirect_uri);
        $client->addScope("email");
        $client->addScope("profile");

        //Send Client Request
        $service = new Google_Service_Oauth2($client);
        $client->authenticate($_GET['code']);

        $_SESSION['access_token'] = $client->getAccessToken();
        
        // User information retrieval starts..............................

        $user = $service->userinfo->get(); //get user info 

        $_SESSION['SocialMediaSession'] = array(
            "OAuthProvider" => "google",
            "OAuthUid" => $user->id,
            "FirstName" => explode(" ",$user->name)[0],
            "LastName" => explode(" ",$user->name)[1],
            "Email" => $user->email,
            "Gender" => $user->gender,
            "Location" => ""
        );

        if($_SESSION['GoogleLogin'] == 1){
            $user = $this->db->query("Select * from users where OAuthProvider='google' AND OAuthUid='".$user->id."' AND Email='".$user->email."'")->row_array();
            if($user){
                $this->User_model->update(array('LastLogin' => date('Y-m-d')), array('UserID' => $user['UserID']));
                $this->session->set_userdata('lang',$user['Lang']);
                $this->session->set_userdata('admin', $user);
    
                $redirect = base_url('cms/machine');
                header("location: ".$redirect);
            }else{
                $redirect = base_url('login?error=social');
                header("location: ".$redirect);
            }
        }else{
            $redirect = base_url('tarife?popup=open');
            header("location: ".$redirect);
        }
    }

    public function linkedinlogin()
    {
        $_SESSION['GoogleLogin'] = isset($_GET['oauth_init']) ? $_GET['oauth_init'] : "";
        $_SESSION['plan_id'] = $_GET['plan_id'];
        $_SESSION['plan_row'] = $_GET['duration'];

        //Include the linkedin api php libraries
        include_once APPPATH."libraries/linkedin-oauth-client/http.php";
        include_once APPPATH."libraries/linkedin-oauth-client/oauth_client.php";

        $client = new oauth_client_class;
        $client->client_id = LINKEDIN_ID;
        $client->client_secret = LINKEDIN_SECRET;
        $client->redirect_uri = base_url().LINKEDIN_REDIRECT;
        $client->scope = LINKEDIN_SCOPE;
        $client->debug = false;
        $client->debug_http = true;
        $application_line = __LINE__;

        if(strlen($client->client_id) == 0 || strlen($client->client_secret) == 0){
            echo "Problem";
            die();
        }


        // If authentication returns success
        if($success = $client->Initialize()){
            if(($success = $client->Process())){
                if(strlen($client->authorization_error)){
                    $client->error = $client->authorization_error;
                    $success = false;
                }elseif(strlen($client->access_token)){
                    $success = $client->CallAPI(
                        'https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))',
                        'GET', array(
                        'format'=>'json'
                    ), array('FailOnAccessError'=>true), $userInfo);
                    $emailRes = $client->CallAPI(
                        'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))',
                        'GET', array(
                        'format'=>'json'
                    ), array('FailOnAccessError'=>true), $userEmail);
                }
            }
            $success = $client->Finalize($success);
        }

//
        if(strlen($client->authorization_error))
        {
            $client->error = $client->authorization_error;
            $success = false;
        }

        $_SESSION['SocialMediaSession'] = array(
            "OAuthProvider" => "linkedin",
            "OAuthUid" => $userInfo->id,
            "FirstName" => $userInfo->firstName->localized->en_US,
            "LastName" => $userInfo->lastName->localized->en_US,
            "Email" => $userEmail->elements[0]->{'handle~'}->emailAddress,
            "Gender" => "",
            "Location" => ""
        );

        $email = !empty($userEmail->elements[0]->{'handle~'}->emailAddress)?$userEmail->elements[0]->{'handle~'}->emailAddress:'';

        if($_SESSION['GoogleLogin'] == 1 && !empty($email))
        {
            $user = $this->db->query("Select * from users where OAuthProvider='linkedin' AND OAuthUid='".$userInfo->id."' AND Email='".$email."'")->row_array();
            if($user){
                $this->User_model->update(array('LastLogin' => date('Y-m-d')), array('UserID' => $user['UserID']));
                $this->session->set_userdata('lang',$user['Lang']);
                $this->session->set_userdata('admin', $user);

                $redirect = base_url('cms/machine');
                header("location: ".$redirect);
            }else{
                $redirect = base_url('login?error=social');
                header("location: ".$redirect);
            }
        }elseif($_SESSION['GoogleLogin'] == 0)
        {
            $redirect = base_url('tarife?popup=open');
            header("location: ".$redirect);
        }
    }


    public function linkedincallback()
    {
        //Include the linkedin api php libraries
        include_once APPPATH."libraries/linkedin-oauth-client/http.php";
        include_once APPPATH."libraries/linkedin-oauth-client/oauth_client.php";

        if((isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) || (isset($_GET['code']) && isset($_GET['state'])))
        {
            $client = new oauth_client_class;
            $client->client_id = '86pghnr6ihc8f1';
            $client->client_secret = 'Ur9IEDCrk80STc4C';
            $client->redirect_uri = base_url().'linkedincallback';
            $client->scope = LINKEDIN_SCOPE;
            $client->debug = false;
            $client->debug_http = true;
            $application_line = __LINE__;

            if(strlen($client->client_id) == 0 || strlen($client->client_secret) == 0){
                echo "Problem";
                die();
            }

            // If authentication returns success
            if($success = $client->Initialize()){
                if(($success = $client->Process())){
                    if(strlen($client->authorization_error)){
                        $client->error = $client->authorization_error;
                        $success = false;
                    }elseif(strlen($client->access_token)){
                        $success = $client->CallAPI(
                            'https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))',
                            'GET', array(
                            'format'=>'json'
                        ), array('FailOnAccessError'=>true), $userInfo);
                        $emailRes = $client->CallAPI(
                            'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))',
                            'GET', array(
                            'format'=>'json'
                        ), array('FailOnAccessError'=>true), $userEmail);
                    }
                }
                $success = $client->Finalize($success);
            }

            if(strlen($client->authorization_error))
            {
                $client->error = $client->authorization_error;
                $success = false;
            }

            $_SESSION['SocialMediaSession'] = array(
                "OAuthProvider" => "linkedin",
                "OAuthUid" => $userInfo->id,
                "FirstName" => $userInfo->firstName->localized->en_US,
                "LastName" => $userInfo->lastName->localized->en_US,
                "Email" => $userEmail->elements[0]->{'handle~'}->emailAddress,
                "Gender" => "",
                "Location" => ""
            );

            $email = !empty($userEmail->elements[0]->{'handle~'}->emailAddress)?$userEmail->elements[0]->{'handle~'}->emailAddress:'';

            if($_SESSION['GoogleLogin'] == 1 && !empty($email))
            {
                $user = $this->db->query("Select * from users where OAuthProvider='linkedin' AND OAuthUid='".$userInfo->id."' AND Email='".$email."'")->row_array();
                if($user){
                    $this->User_model->update(array('LastLogin' => date('Y-m-d')), array('UserID' => $user['UserID']));
                    $this->session->set_userdata('lang',$user['Lang']);
                    $this->session->set_userdata('admin', $user);

                    $redirect = base_url('cms/machine');
                    header("location: ".$redirect);
                }else{
                    $redirect = base_url('login?error=social');
                    header("location: ".$redirect);
                }
            }elseif($_SESSION['GoogleLogin'] == 0)
            {
                $redirect = base_url('tarife?popup=open');
                header("location: ".$redirect);
            }
        }
    }

    //User Price Plan
    public function price_plan()
    {
        $this->data['feature'] = $this->Feature_model->getActiveAllJoinedData(false, 'FeatureID', $this->language);
        $this->data['plan'] = $this->Package_model->getActiveAllJoinedData(false, 'PackageID', $this->language);
        $this->data['seo'] = getSeo('');
        $this->data['seo']['title'] = lang('price-plan');
        $this->data['view'] = 'frontend/plan-price';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function action()
    {
        $data = array();
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'signup':

//                $this->session->unset_userdata("last_insert_id");
//                $this->session->unset_userdata("plan");
//                $this->session->unset_userdata("company");
//                $this->session->unset_userdata("category");
//                $this->session->unset_userdata("dealer-profile");
//                $this->session->unset_userdata("dealer_img");

                $this->validate();
                $this->session->set_userdata("plan", $this->input->post());
                $data['success'] = lang('save_successfully');
                $data['error'] = false;
                $data['redirect'] = true;
                $data['url'] = 'profildaten';
                echo json_encode($data);
                break;
            case 'company':
                $this->validate();
                $this->session->set_userdata("company", $this->input->post());
                $data['success'] = lang('save_successfully');
                $data['error'] = false;
                $data['redirect'] = true;
                $data['url'] = 'auswahl-kategorien';
                echo json_encode($data);
                break;
            case 'dealer-profile':
                $this->session->set_userdata("category", $this->input->post());
                $data['success'] = lang('save_successfully');
                $data['error'] = false;
                $data['redirect'] = true;
                $data['url'] = 'firmendaten';
                echo json_encode($data);
                break;
            case 'final-setp':
//                $this->validate();
                $file = '';
                if (isset($_FILES['file']) && !empty($_FILES['file'])) {
                    $path = 'uploads/images/';
                    $file_name = rand(9999, 99999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES['file']['name']);
                    $file_tmp = $_FILES['file']["tmp_name"];
                    move_uploaded_file($file_tmp = $_FILES['file']["tmp_name"], $path . $file_name);
                    $file = $path . $file_name;
                }

                $this->session->set_userdata("dealer-profile", $this->input->post());
                $this->session->set_userdata("dealer_img", $file);
                $data['success'] = lang('save_successfully');
                $data['error'] = false;
                $data['redirect'] = true;
                $data['url'] = 'zahlungsmethode';
                echo json_encode($data);
                break;
            case 'update_password':
                $this->UpdatePassword();
                break;

        }
    }


    public function reset_password()
    {

        $post_data = $this->input->post();
        $parent = 'User_model';
        $child = 'User_text_model';
        //$id              = $this->session->userdata['admin']['UserID'];
        $save_parent_data = $update_by = array();

        if ($post_data['Password'] != $post_data['ConfirmPassword']) {

            $errors['error'] = lang('password_confirm_password_not_match');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        } else if ($post_data['Password'] == $post_data['ConfirmPassword']) {

            unset($post_data['form_type']);
            unset($post_data['ConfirmPassword']);
            $save_parent_data['Password'] = md5($post_data['Password']);
            $update_by[$this->data['TableKey']] = $post_data['UserID'];

            $this->$parent->update($save_parent_data, $update_by);

            $success['error'] = false;
            $success['redirect'] = true;
            $success['url'] = 'login';
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;

        } else {

            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;

        }
    }

    private function validate($check = true)
    {

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $form_type = $this->input->post('form_type');
        if ($form_type == 'signup') {
            $this->form_validation->set_rules('companyName', lang('company_name'), 'required|trim');
            $this->form_validation->set_rules('StreetAddress', lang('street_address'), 'required|trim');
            $this->form_validation->set_rules('Email', lang('email'),
                'required|valid_email|is_unique[users.Email]',
                array(
                    'required' => lang('form_validation_required'),
                    'valid_email' => lang('form_validation_valid_email'),
                    'is_unique' => lang('form_validation_is_unique')
                )
            );
            $this->form_validation->set_rules('postcodeTown', lang('postcode'), 'required|trim');
            $this->form_validation->set_rules('City', lang('city'), 'required|trim');
            $this->form_validation->set_rules('Vat', lang('vat'), 'required|trim');
            $this->form_validation->set_rules('PhoneNumber', lang('ph'), 'required|trim');
            $this->form_validation->set_rules('lang', lang('default') . ' ' . lang('language'), 'required|trim');
        } else if ($form_type == 'company') {
            $this->form_validation->set_rules('title', lang('title'), 'required|trim');
            $this->form_validation->set_rules('first_name', lang('first_name'), 'required|trim');
            $this->form_validation->set_rules('last_name', lang('last_name'), 'required|trim');
            $this->form_validation->set_rules('ph', lang('ph'), 'required|trim');
            $this->form_validation->set_rules('country', lang('country'), 'required|trim');
            //$this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]');
        }
//        else if ($form_type == 'final-setp' and $this->data['confimation_box']) {
//            $this->form_validation->set_rules('policy_read', lang('privacy-policy'), 'required|trim');
//        }

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }


    public function company()
    {
        $this->data['rec'] = $this->session->userdata('plan');
        $this->data['seo'] = getSeo('');
        $this->data['seo']['title'] = lang('price-plan');
        $this->data['view'] = 'frontend/company-information';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function choose_category()
    {
        $this->data['rec'] = $this->session->userdata('company');
        $this->data['seo'] = getSeo('');
        $this->data['seo']['title'] = lang('price-plan');
        $this->data['view'] = 'frontend/choose-category';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function dealer_profile()
    {
        $this->data['rec'] = $this->session->userdata('category');
        $this->data['seo'] = getSeo('');
        $this->data['seo']['title'] = lang('price-plan');
        $this->data['view'] = 'frontend/dealer-profile';
        $this->load->view('frontend/layouts/default', $this->data);
    }

    public function summary()
    {
        $plan = $this->session->userdata('plan');
        $this->data['plan'] = $plan;
        $this->data['dealer_profile'] = $this->session->userdata("dealer-profile");
        $this->data['company'] = $this->session->userdata("company");
        $this->data['category'] = $this->session->userdata("category");
        $this->data['dealer_img'] = $this->session->userdata("dealer_img");

        $package = getPackage($plan['plan_id']);
        $v = json_decode($package->MetaData)[$plan['plan_row']];
        //print_rm($package);
        // $v =  json_decode($package->MetaData)[$plan['plan_row']];
        if ($package) {
            $this->data['rec'] = $package;
            $this->data['v'] = $v;
        }
//        $this->session->set_userdata("paymentMethod", $this->input->post('PaymentMethod'));
        $this->data['paymentMethod'] = $this->input->post('PaymentMethod');
        $this->data['seo']['title'] = lang('payment-summery-title');
//        $this->data['message'] = lang('payment-summery');
        $this->data['view'] = 'frontend/summery';
        $this->load->view('frontend/layouts/default', $this->data);
    }


    public function paypal()
    {
        $last_insert_id = $this->session->userdata('last_insert_id');
        if(!isset($last_insert_id))
        {
            $parent = $this->data['Parent_model'];
            $child = $this->data['Child_model'];
            $plan = $this->session->userdata('plan');

            $company = $this->session->userdata('company');
            $category = $this->session->userdata('category');
            $dealer_profile = $this->session->userdata('dealer-profile');
            $dealer_img = $this->session->userdata('dealer_img');
            $save_parent_data = array();
            $save_child_data = array();
            $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

            $sort = 0;
            if (!empty($getSortValue))
            {
                $sort = $getSortValue['SortOrder'] + 1;
            }
            $password = md5($company['Password']);
            $save_parent_data['SortOrder'] = $sort;
            $save_parent_data['IsActive'] = 1;
            //$save_parent_data['VerificationKey']       = $password;//set password encripted as key
            $save_parent_data['RoleID'] = $dealer_profile['ShowDealerProfile'];
            $save_parent_data['UName'] = $company['title'];
            $save_parent_data['Password'] = $password;
            $save_parent_data['Email'] = $plan['Email'];
            $save_parent_data['City'] = $plan['City'];
            $save_parent_data['Vat'] = $plan['Vat'];
            $save_parent_data['MobCountryCode'] = $company['MobCountryCode'];
            $save_parent_data['Mobile'] = $company['mob'];
            $save_parent_data['CountryCode'] = $company['CountryCode'];
            $save_parent_data['Phone'] = $company['ph'];
            $save_parent_data['Fax'] = $company['fax'];
            $save_parent_data['Website'] = $company['website'];
            $save_parent_data['Lang'] = $plan['lang'];
            $save_parent_data['CoverImage'] = $dealer_img;
            $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');

            if(isset($_SESSION['SocialMediaSession'])){
                $save_parent_data['OAuthProvider'] = $_SESSION['SocialMediaSession']['OAuthProvider'];
                $save_parent_data['OAuthUid'] = $_SESSION['SocialMediaSession']['OAuthUid'];
            }

            $insert_id = $this->$parent->save($save_parent_data);
            $this->session->set_userdata("last_insert_id", $insert_id);

            if ($insert_id > 0) {
                $update_verification_key['VerificationKey'] = base64_encode($insert_id);
                $this->$parent->update($update_verification_key, array('UserID' => $insert_id));

                $default_lang = getDefaultLanguageByDefault();
                $save_child_data[$this->data['TableKey']] = $insert_id;
                $save_child_data['Title'] = $company['first_name'] . ' ' . $company['last_name'];
                $save_child_data['CompanyName'] = $plan['companyName'];
                $save_child_data['StreetAddress'] = $plan['StreetAddress'];
                $save_child_data['Country'] = $company['country'];
                $save_child_data['PostalCode'] = $plan['postcodeTown'];
                $save_child_data['CompanyProfile'] = $dealer_profile['company_description'];
                $save_child_data['SystemLanguageID'] = $default_lang->SystemLanguageID;
                $this->$child->save($save_child_data);
                $extra_field_array = array();
                $extra_field_array['Country'] = $company['country'];
                $this->addAllLanguageData($child, $this->data['TableKey'], $insert_id, $extra_field_array);
                if ($dealer_profile['ShowDealerProfile'] == 4) {
                    $user_right[] = [
                        'ModuleID' => '56',
                        'UserID' => $insert_id,
                        'CanView' => 1,
                        'CanAdd' => 1,
                        'CanEdit' => 1,
                        'CanDelete' => 1,
                        'CreatedAt' => date('Y-m-d H:i:s'),
                        'CreatedBy' => $insert_id,
                        'UpdatedAt' => date('Y-m-d H:i:s'),
                        'UpdatedBy' => $insert_id
                    ];
                    $user_right[] = [
                        'ModuleID' => '57',
                        'UserID' => $insert_id,
                        'CanView' => 1,
                        'CanAdd' => 0,
                        'CanEdit' => 0,
                        'CanDelete' => 1,
                        'CreatedAt' => date('Y-m-d H:i:s'),
                        'CreatedBy' => $insert_id,
                        'UpdatedAt' => date('Y-m-d H:i:s'),
                        'UpdatedBy' => $insert_id
                    ];

                } else {
                    $user_right[] = [
                        'ModuleID' => '56',
                        'UserID' => $insert_id,
                        'CanView' => 1,
                        'CanAdd' => 1,
                        'CanEdit' => 1,
                        'CanDelete' => 1,
                        'CreatedAt' => date('Y-m-d H:i:s'),
                        'CreatedBy' => $insert_id,
                        'UpdatedAt' => date('Y-m-d H:i:s'),
                        'UpdatedBy' => $insert_id
                    ];

                }

                $verfication_link = base_url('verfiy_account?key=' . base64_encode($insert_id));

                $this->Modules_users_rights_model->insert_batch($user_right);

                if(!isset($_SESSION['SocialMediaSession']) && empty($_SESSION['SocialMediaSession'])){
                    $tpl = get_email_tpl(array('a.TplList' => 1), $plan['lang']);
                    if ($tpl) {
                        $search = array(mail_tpl_shortcode('Name'), mail_tpl_shortcode('Email'), mail_tpl_shortcode('Password'), mail_tpl_shortcode('Company Name'), mail_tpl_shortcode('Phone'), mail_tpl_shortcode('Mobile'), mail_tpl_shortcode('Fax'), mail_tpl_shortcode('PostalCode'), mail_tpl_shortcode('Country'), mail_tpl_shortcode('Address'), mail_tpl_shortcode('Site Name'), mail_tpl_shortcode('Site URL'), mail_tpl_shortcode('Verification Link'));

                        $replace = array($company['first_name'] . ' ' . $company['last_name'], $plan['Email'], $company['Password'], $plan['companyName'], $company['ph'], $company['mob'], $company['fax'], $plan['postcodeTown'], $company['country'], $plan['StreetAddress'], $this->data['site_setting']->SiteName, base_url(), '<a href="' . $verfication_link . '">' . lang('click_here') . '</a>');
                        $body = str_replace($search, $replace, $tpl->Description);

                        // Arslan Coomented Code Start
                        // $data['from'] = 'machinepilot@demo-project.info';
                        // Arslan Coomented Code END

                        $data['from'] = $this->data['site_setting']->FromEmail;
                        $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName)) ? $this->data['site_setting']->FromName : $this->data['site_setting']->SiteName;
                        $data['to'] = $plan['Email'];
                        $data['subject'] = $tpl->Subject;
                        $data['body'] = $body;
                        sendEmail($data);
                    }
                }

                $this->checkoutInvoice($this->session->userdata('plan'), $insert_id);
                    //$this->PackagePurchase($insert_id);
                if(isset($_SESSION['SocialMediaSession']))
                {
                    unset($_SESSION['SocialMediaSession']);
                }
            } else {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
        }else{
            $this->checkoutInvoice($this->session->userdata('plan'), $last_insert_id);
        }
    }


    public function upgrade_package()
    {
        $this->session->set_userdata("plan", $this->input->post());
        $this->PackagePurchase($this->session->userdata['admin']['UserID']);
    }

    public function PackagePurchase($insert_id)
    {
        $plan = $this->session->userdata('plan');

        $get_user_data = $this->User_model->get($insert_id, false, 'UserID');
        /*if($get_user_data){
            if($get_user_data->IsVerified == 0){

                $this->checkoutInvoice($plan,$insert_id,lang('account-not-verified'));
                return false;
            }
        } */

        if (!($this->session->userdata('admin')))
        {
            $purchase = '';
            $company = $this->session->userdata('company');
            $category = $this->session->userdata('category');
        } else {
            $purchase = $this->Purchase_model->getPurchaseData(array('a.UserID' => $insert_id), 'row');
        }

        $save_parent_data = array();
        $save_child_data = array();

        $package = getPackage($plan['plan_id']);
        $v = json_decode($package->MetaData)[$plan['plan_row']];
        //$chekifmultiplepkg =chekifMultiplePkg($insert_id);
        $old_qty = getOldQty($insert_id);
        $old = $old_qty[0]->Qty ?? 0;
        $qty = $old;
        $price = ($v->RegularPrice - ($v->RegularPrice / 100) * $v->Discount);
        $expiry = date("Y-m-d", strtotime(date('Y-m-d')." +".$v->Duration." months"));
        $duration = $v->Duration;
        $discount = $v->Discount;
        if($this->session->userdata('coupon_discount_total')){
            $save_parent_data['CouponDiscount'] = $this->session->userdata('coupon_discount_total');
            $discount = $discount + $this->session->userdata('coupon_discount_total');
        }
        $save_parent_data['UserID'] = $user_id = $insert_id;
        $save_parent_data['PackageID'] = $plan['plan_id'];
        $save_parent_data['Category'] = ($purchase) ? $purchase->Category : (isset($category['sub_id'])) ? implode(',', $category['sub_id']) : '';
        $save_parent_data['Qty'] = $qty + $v->QTY;
        $save_parent_data['Rate'] = $price;
        $save_parent_data['PerMonth'] = $v->QTY * $price;
        $save_parent_data['ExpiryDate'] = $expiry;
        $save_parent_data['Duration'] = $duration;
        $save_parent_data['Discount'] = $discount;

        $insert_id = $this->User_model->save_rec('user_package_purchase', $save_parent_data);

        if ($insert_id > 0) {
            $save_child_data['PurchaseID'] = $insert_id;
            $get_data = site_setting();
            if ($get_data->PaypalActive == 1) {
                $save_child_data['Status'] = 0;

            } else {
                $save_child_data['Status'] = 4;    //will receieve payment through bank
            }
            $this->User_model->save_rec('user_payment', $save_child_data);
            $this->session->unset_userdata('plan');
            if (!($this->session->userdata('admin'))) {
                $this->session->unset_userdata('company');
                $this->session->unset_userdata('last_insert_id');
                $this->session->unset_userdata('category');
                $this->session->unset_userdata('dealer-profile');
                $this->session->unset_userdata('dealer_img');
                $this->session->unset_userdata('plan_id');
                $this->session->unset_userdata('plan_row');
            }

//            if ($get_data->PaypalActive == 0) {
//
//                $this->sendInvoiceEmail($insert_id, $user_id);
//                $this->session->set_flashdata('message', lang('send_invoice_pdf'));
//                redirect(base_url(''));
//            } else {
//                redirect(base_url() . "checkout/" . $insert_id);
//            }
            $this->session->set_userdata('paymentMethod', $this->input->post('paymentMethod'));
            $this->sendInvoiceEmail($insert_id, $user_id);
            $this->session->set_flashdata('message', lang('send_invoice_pdf'));
            redirect(base_url('willkommen'));
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
        //redirect(base_url()."checkout/".$insert_id);
    }


    private function sendInvoiceEmail($PurchaseID, $user_id)
    {
        $udata = $this->User_model->get($user_id, false, 'UserID');
        $language_data = getLanguageByCode($udata->Lang);
        $this->data['user_data'] = $user_data = $this->User_model->getAllDetail($PurchaseID, $language_data->SystemLanguageID);
        $this->data['paymentMethod'] = $this->session->userdata('paymentMethod');
        $html = $this->load->view('frontend/layouts/invoice', $this->data, true);
//        $invoice_title = 'R-'.$PurchaseID;
        $invoice_title = 'R-' . str_pad($PurchaseID, 7, '0', STR_PAD_LEFT);
        $save_parent_data['filePath'] = $invoice_title . '.pdf';
        $update_by['PurchaseID'] = $PurchaseID;
        $this->User_model->update_rec('user_package_purchase', $save_parent_data, $update_by);
        $this->pdf->CreatePDF($html, $invoice_title);
        $tpl = get_email_tpl(array('TplList' => 15), $user_data['Lang']);
        if ($tpl) {
            if ($tpl->Description == '') {
                $default_language = getDefaultLanguageByDefault();
                $tpl = get_email_tpl(array('TplList' => 15), $default_language->ShortCode);
            }
            $search = array(mail_tpl_shortcode('Name'), mail_tpl_shortcode('Package Name'), mail_tpl_shortcode('Expiry Date'));
            //print_rm($user_data);
            $replace = array($user_data['FullName'], $user_data['PackageTitle'], date('d.m.Y', strtotime($user_data['ExpiryDate'])));
            $body = str_replace($search, $replace, $tpl->Description);
            $data['from'] = $this->data['site_setting']->FromEmail;
            $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName)) ? $this->data['site_setting']->FromName : $this->data['site_setting']->SiteName;
            $data['to'] = $user_data['Email'];
            $data['subject'] = $tpl->Subject;
            $data['body'] = $body;
            $files = array();
            $files[0] = 'assets/invoice_pdf/' . $invoice_title . '.pdf';
            $email_attachments = getImage(array('FileID' => $tpl->Email_templateID, 'ImageType' => 'EmailFile'), false);

            if ($email_attachments) {
                $i = 1;
                foreach ($email_attachments as $value) {
                    $files[$i] = $value->ImageName;
                    $i++;
                }
            }
            return sendEmail($data, $files);
        }
    }


    public function verfiy_account()
    {
        if (isset($_GET['key']) && $_GET['key'] != '') {
            $UserID = base64_decode($_GET['key']);
            $user_data = $this->User_model->get($UserID, false, 'UserID');
            if ($user_data) {
                $this->User_model->update(array('VerificationKey' => '', 'IsVerified' => 1), array('UserID' => $UserID));
                $Purchase = $this->User_model->getSingleRecord('user_package_purchase', $UserID, false, 'UserID');
                $this->sendInvoiceEmail($Purchase->PurchaseID, $UserID);
                redirect(base_url('login'));
            } else {
                redirect(base_url());
            }

        } else {
            redirect(base_url());
        }
    }

    public function checkoutInvoice($plan, $insert_id, $message = '')
    {
        $package = getPackage($plan['plan_id']);
        $v = json_decode($package->MetaData)[$plan['plan_row']];
        if ($package) {
            $this->data['UserID'] = $insert_id;
            $this->data['rec'] = $package;
            $this->data['v'] = $v;
            $this->data['message'] = $message;
            $this->data['seo'] = getSeo('');
            $this->data['seo']['title'] = lang('price-plan');
            $this->data['view'] = 'frontend/invoice';
            $this->load->view('frontend/layouts/default', $this->data);
        } else {
            redirect(base_url());
        }
    }

    public function checkout()
    {

        //$this->sendInvoiceEmail(65,120);
        $purchase = $this->Purchase_model->getPurchaseData(array('b.Status' => 0, 'a.PurchaseID' => $this->uri->segment(2)), 'row');
        if ($purchase) {
            $this->data['rec'] = $purchase;
            $this->data['seo'] = getSeo('');
            $this->data['seo']['title'] = lang('price-plan');
            $this->data['view'] = 'frontend/paypal';
            $this->load->view('frontend/layouts/default', $this->data);
        } else {
            redirect(base_url());
        }
    }

    public function paypal_status($id)
    {
        $purchase = $this->Purchase_model->getPurchaseData(array('a.PurchaseID' => $id), 'row');
        $checkUser = get_user_info(array('a.UserID' => $purchase->UserID));

        $status = 0;
        if (strpos($this->uri->segment(1), 'cancel') !== false) {
            $status = 1;

            $tpl = get_email_tpl(array('TplList' => 4), $checkUser->Lang);

        } else if (strpos($this->uri->segment(1), 'complete') !== false) {
            if (strtolower($this->input->get('st')) == 'pending') {
                $status = 0;

                $tpl = get_email_tpl(array('TplList' => 3), $checkUser->Lang);

            } else if (strtolower($this->input->get('st')) == 'completed') {
                $status = 2;
                $this->user_active($id);
                $tpl = get_email_tpl(array('TplList' => 5));
                $package = getPackage($purchase->PackageID);
                if (!($this->session->userdata('admin'))) {
                    $pur_tpl = get_email_tpl(array('a.TplList' => 10), $checkUser->Lang);
                } else {
                    $pur_tpl = get_email_tpl(array('a.TplList' => 13), $checkUser->Lang);
                }
                if ($pur_tpl) {
                    $contract_period = dateformat($purchase->CreatedAt, 'de') . ' - ' . dateformat($purchase->ExpiryDate, 'de');
                    $total = $purchase->Qty * $purchase->Rate * $purchase->Duration;
                    $search = array(mail_tpl_shortcode('Name'), mail_tpl_shortcode('Package Name'), mail_tpl_shortcode('Per Advertisement'), mail_tpl_shortcode('Per Month Equal'), mail_tpl_shortcode('Contract Period'), mail_tpl_shortcode('Expiry Date'), mail_tpl_shortcode('Total Amount'), mail_tpl_shortcode('Site Name'), mail_tpl_shortcode('Site URL'));

                    $replace = array($checkUser->Title, $package->Title, $purchase->Qty, currency($purchase->Qty * $purchase->Rate), $contract_period, dateformat($purchase->ExpiryDate, 'de'), currency($total), $this->data['site_setting']->SiteName, base_url());
                    $body = str_replace($search, $replace, $pur_tpl->Description);
                    $data['from'] = $this->data['site_setting']->FromEmail;
                    $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName)) ? $this->data['site_setting']->FromName : $this->data['site_setting']->SiteName;
                    $data['to'] = $checkUser->Email;
                    $data['subject'] = $pur_tpl->Subject;
                    $data['body'] = $body;
                    sendEmail($data);
                }
            }
        }
        if ($tpl) {
            $search = array(mail_tpl_shortcode('Name'), mail_tpl_shortcode('Email'), mail_tpl_shortcode('Invoice ID'), mail_tpl_shortcode('Site Name'), mail_tpl_shortcode('Site URL'));
            $replace = array($checkUser->Title, $checkUser->Email, $id, $this->data['site_setting']->SiteName, base_url());
            $body = str_replace($search, $replace, $tpl->Description);
            $data['from'] = $this->data['site_setting']->FromEmail;
            $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName)) ? $this->data['site_setting']->FromName : $this->data['site_setting']->SiteName;
            $data['to'] = $checkUser->Email;
            $data['subject'] = $tpl->Subject;
            $data['body'] = $body;
            sendEmail($data);
        }
        $arr = array('Status' => $status, 'PaypalResponse' => ($_GET) ? json_encode($_GET) : '');
        $this->Purchase_model->update_payment_status($arr, array('PurchaseID' => $id));
        redirect(base_url());

    }

    public function paypal_ipn($id)
    {

        $purchase = $this->Purchase_model->getPurchaseData(array('a.PurchaseID' => $id), 'row');
        $checkUser = get_user_info(array('a.UserID' => $purchase->UserID));
        $package = getPackage($purchase->PackageID);
        // STEP 1: Read POST data

        // reading posted data from directly from $_POST causes serialization

        // issues with array data in POST

        // reading raw POST data from input stream instead.

        $raw_post_data = file_get_contents('php://input');

        sendEmail(array('from' => 'info@machinepilot.com', 'to' => 'muhammadzulfiqar@splashpk.com', 'subject' => 'paypal ipn', 'body' => $raw_post_data));


        $raw_post_array = explode('&', $raw_post_data);

        $myPost = array();

        foreach ($raw_post_array as $keyval) {

            $keyval = explode('=', $keyval);

            if (count($keyval) == 2)

                $myPost[$keyval[0]] = urldecode($keyval[1]);

        }

        // read the post from PayPal system and add 'cmd'

        $req = 'cmd=_notify-validate';

        if (function_exists('get_magic_quotes_gpc')) {

            $get_magic_quotes_exists = true;

        }

        foreach ($myPost as $key => $value) {

            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {

                $value = urlencode(stripslashes($value));

            } else {

                $value = urlencode($value);

            }

            $req .= "&$key=$value";

        }


        // STEP 2: Post IPN data back to paypal to validate


        $ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');

        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));


        // In wamp like environments that do not come bundled with root authority certificates,

        // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path

        // of the certificate as shown below.

        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');

        if (!($res = curl_exec($ch))) {

            // error_log("Got " . curl_error($ch) . " when processing IPN data");

            curl_close($ch);

            exit;

        }

        curl_close($ch);


        // STEP 3: Inspect IPN validation result and act accordingly


        if (strcmp($res, "VERIFIED") == 0) {

            // check whether the payment_status is Completed

            // check that txn_id has not been previously processed

            // check that receiver_email is your Primary PayPal email

            // check that payment_amount/payment_currency are correct

            // process payment


            // assign posted variables to local variables


            $arr = array(

                'PaypalResponse' => json_encode($_POST),

                'Status' => (strtolower($_POST['payment_status']) == 'completed') ? 2 : 1

            );

            // <---- HERE you can do your INSERT to the database
            if (strtolower($_POST['payment_status']) == 'completed') {
                $this->user_active($id);
                $tpl = get_email_tpl(array('TplList' => 5), $checkUser->Lang);
                $pur_tpl = get_email_tpl(array('a.TplList' => 10), $checkUser->Lang);
                if ($pur_tpl) {
                    $contract_period = dateformat($purchase->CreatedAt, 'de') . ' - ' . dateformat($purchase->ExpiryDate, 'de');
                    $total = $purchase->Qty * $purchase->Rate * $purchase->Duration;
                    $search = array(mail_tpl_shortcode('Name'), mail_tpl_shortcode('Package Name'), mail_tpl_shortcode('Per Advertisement'), mail_tpl_shortcode('Per Month Equal'), mail_tpl_shortcode('Contract Period'), mail_tpl_shortcode('Expiry Date'), mail_tpl_shortcode('Total Amount'), mail_tpl_shortcode('Site Name'), mail_tpl_shortcode('Site URL'));

                    $replace = array($checkUser->Title, $package->Title, $purchase->Qty, currency($purchase->Qty * $purchase->Rate), $contract_period, dateformat($purchase->ExpiryDate, 'de'), currency($total), $this->data['site_setting']->SiteName, base_url());
                    $body = str_replace($search, $replace, $pur_tpl->Description);
                    $data['from'] = $this->data['site_setting']->FromEmail;
                    $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName)) ? $this->data['site_setting']->FromName : $this->data['site_setting']->SiteName;
                    $data['to'] = $checkUser->Email;
                    $data['subject'] = $pur_tpl->Subject;
                    $data['body'] = $body;
                    sendEmail($data);
                }

            }
        } else if (strcmp($res, "INVALID") == 0) {

            $arr = array(

                'PaypalResponse' => json_encode($_POST),

                'Status' => 1

            );
            $tpl = get_email_tpl(array('TplList' => 4), $checkUser->Lang);

        }

        $this->Purchase_model->update_payment_status($arr, array('PurchaseID' => $id));
        if ($tpl) {
            $search = array(mail_tpl_shortcode('Name'), mail_tpl_shortcode('Package Name'), mail_tpl_shortcode('Invoice ID'), mail_tpl_shortcode('Site Name'), mail_tpl_shortcode('Site URL'));
            $replace = array($checkUser->Title, $package->Title, $id, $this->data['site_setting']->SiteName, base_url());
            $body = str_replace($search, $replace, $tpl->Description);
            $data['from'] = $this->data['site_setting']->FromEmail;
            $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName)) ? $this->data['site_setting']->FromName : $this->data['site_setting']->SiteName;
            $data['to'] = $checkUser->Email;
            $data['subject'] = $tpl->Subject;
            $data['body'] = $body;
            sendEmail($data);
        }
        redirect(base_url());
    }


    public function user_active($id)
    {
        $parent = $this->data['Parent_model'];
        $rec = getValue('user_package_purchase', array('PurchaseID' => $id));
        $this->$parent->update(array('IsActive' => 1), array('UserID' => $rec->UserID));
    }


}