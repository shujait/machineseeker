<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends Base_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 
	 */
	 public $data = array();

	function __construct()
	{
    	// Construct the parent class
    	parent::__construct();
		$this->load->model(array('Page_model','User_model','Menu_model','Advertise_model','Machine_model','Category_model'));
        $this->data['site_setting'] = $this->getSiteSetting();
		$this->data['default_lang'] = getDefaultLanguage();
		if($this->session->userdata('admin')){
        $this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows'); 
		}else{
		$this->data['user_wishlist'] = 0;	
		}
		$this->data['language'] = $this->language;
		//$this->data['language_id'] = getLanguageByShortCode($this->language);
 		$this->data['advertise'] = $this->Advertise_model->getActiveAllJoinedData(false,'AdvertiseID',$this->language,false,'DESC','AdvertiseID','result');
		$this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition=1');
		$this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2');
	}
	public function index()
	{
		$this->data['sell_product'] =  $this->Machine_model->getActiveAllJoinedData(false,'MachineID',$this->language,false,'DESC','MachineID','result',array(3,0));
		$this->data['seo'] = getSeo('home',1);
        $this->data['display_category'] = $this->Category_model->gethomecategory($this->language);
		$this->data['view'] = 'frontend/index';

        $this->load->view('frontend/layouts/default',$this->data);
	}
	 //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('admin');
        $this->session->unset_userdata('admin');
        $this->session->sess_destroy();
        redirect($this->config->item('base_url'));

    }
	
}
