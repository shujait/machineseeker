<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Puchasehistory extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                     
                $this->load->Model([ 'Purchase_model','Menu_model','']);                          
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
				$this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 1');
		        $this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2'); 
		        if($this->session->userdata['admin']['UserID']){
				$this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows'); 
				}else{
				$this->data['user_wishlist'] = 0;	
				}
    }
     
    
    public function index()
    {
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
          if($this->session->userdata['admin']['RoleID'] > 2){
			$arr = array('a.UserID' => $this->session->userdata['admin']['UserID']);  
		  }else{
			$arr = array();
		  }
          $this->data['results'] = $this->Purchase_model->getPurchaseData($arr);
          $this->load->view('backend/layouts/default',$this->data);
    }
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    private function delete(){
        
         if(!checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        
        $deleted_by = array();
        $deleted_by['PurchaseID'] = $this->input->post('id');
        $this->Purchase_model->delete($deleted_by);
      
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }

    public function update_status(){
        $PurchaseID = $this->input->post('id');
        $Status = $this->input->post('status');
        if($Status == 4){
            $this->Purchase_model->update_payment_status(array('Status' => '5'),array('PurchaseID' => $PurchaseID));
           
        }else if($Status == 5){
            $this->Purchase_model->update_payment_status(array('Status' => '4'),array('PurchaseID' => $PurchaseID));
        }

        $success['error']   = false;
        $success['success'] = lang('update_successfully');
        
        echo json_encode($success);
        exit;
    }

    public function invoicePDF($path)
    {
        $file = 'filename';
        $filepath = base_url('assets/invoice_pdf/'.$path.'.pdf');
        // Header content type
        header("Content-type: application/pdf");
        // Send the file to the browser.
        readfile($filepath);
    }
    
    
    

}