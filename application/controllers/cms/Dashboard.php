<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
		parent::__construct();
		checkAdminSession();
                
        $this->load->Model(['Menu_model', 'Machine_model', 'Feature_model', 'Package_model', 'User_model']);
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();

        $this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 1');
        $this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2');
        if($this->session->userdata['admin']['UserID']){
        $this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows');
        }else{
        $this->data['user_wishlist'] = 0;
        }
	}
	 
    
    public function index()
	{
		redirect(base_url('cms/machine'));
          
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
         
        $this->load->view('backend/layouts/default',$this->data);
	}

	public function myPackage()
    {
        $default_lang = getDefaultLanguage();
        $this->data['purchase']  = getValue('user_package_purchase',array('UserID' => $this->session->userdata['admin']['UserID']));
        $packageId = $this->data['purchase']->PackageID;
        $this->data['plan'] = getPackage($this->data['purchase']->PackageID);
        $this->data['planDetails'] = getValue('packages_text',array('PackageID' => $packageId, 'SystemLanguageID' => $default_lang->SystemLanguageID));
        $this->data['addedMachinesCount'] = $this->Machine_model->getMachinesByUserId($this->session->userdata['admin']['UserID']);
        $this->data['view'] = 'backend/dashboard/my_package';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function upgradePackage()
    {
        $this->data['current_package'] = $this->User_model->getPackageByUserId($this->session->userdata['admin']['UserID']);
        
        $this->data['package_details'] = $this->Package_model->getPackage($this->data['current_package']->PackageID);
        $this->data['features'] = $this->Feature_model->getActiveAllJoinedData(false, 'FeatureID', $this->language);
        $this->data['plan'] = $this->Package_model->getActiveAllJoinedData(false, 'PackageID', $this->language);
        $plans = [];

        foreach ($this->data['plan'] as $plan)
        {
            $features = [];
            $FeatureIds = explode(',', $plan->FeatureID);
            foreach ($this->data['features'] as $feature)
            {
                foreach ($FeatureIds as $id)
                {
                    if($feature->FeatureID === $id)
                    {
                        $features[] = $feature;
                    }
                }
            }

            if($this->session->userdata['admin']['UserID'] === '1')
            {
                $plans[] = [
                    'PackageID' => $plan->PackageID,
                    'ExpiryDate' => $plan->ExpiryDate,
                    'MetaData' => $plan->MetaData,
                    'Title' => $plan->Title,
                    'Type' => $plan->Type,
                    'features' => $features,
                    'SortOrder' => $plan->SortOrder
                ];
            }else{
                if($plan->Type === $this->data['package_details']->Type)
                {
                    $plans[] = [
                        'PackageID' => $plan->PackageID,
                        'ExpiryDate' => $plan->ExpiryDate,
                        'MetaData' => $plan->MetaData,
                        'Title' => $plan->Title,
                        'Type' => $plan->Type,
                        'features' => $features,
                        'SortOrder' => $plan->SortOrder
                    ];
                }
            }
        }

        $this->data['plans'] = $plans;
        $this->data['seo'] = getSeo('');
        $this->data['seo']['title'] = lang('upgrade-plan');
        $this->data['view'] = 'backend/dashboard/upgrade_package';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function cancelAutomatedBilling()
    {
        $this->data['current_package'] = $this->User_model->getPackageByUserId($this->session->userdata['admin']['UserID']);
        $this->data['view'] = 'backend/dashboard/cancel_automate_billing';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function updateAutomateBilling()
    {   $is_auto_payment = 1;
        $paymentType = $this->input->post('is_auto_payment');
        if($paymentType == 1){
            $is_auto_payment = 0;
        }
        $status = $this->User_model->addUpdateCancelPayment($is_auto_payment);
        $return = [
            'status' => false,
            'message' => lang('something_went_wrong'),
        ];
        if($status)
        {
            $return = [
                'status' => true,
                'message' => lang('update_successfully'),
            ];
        }
        echo json_encode($return, true);
        exit;
    }

    private function sendInvoiceEmail($PurchaseID, $user_id)
    {
        $udata = $this->User_model->get($user_id, false, 'UserID');
        $language_data = getLanguageByCode($udata->Lang);
        $this->data['user_data'] = $user_data = $this->User_model->getAllDetail($PurchaseID, $language_data->SystemLanguageID);
        $this->data['paymentMethod'] = $this->session->userdata('paymentMethod');
        $html = $this->load->view('frontend/layouts/invoice', $this->data, true);
//        $invoice_title = 'R-'.$PurchaseID;
         $tpl = get_email_tpl(array('TplList' => 13), $user_data['Lang']);

        if ($tpl) {
            if ($tpl->Description == '') {
                $default_language = getDefaultLanguageByDefault();
                $tpl = get_email_tpl(array('TplList' => 15), $default_language->ShortCode);
            }
            $search = array(
                mail_tpl_shortcode('Name'),
                mail_tpl_shortcode('Package Name'),
                mail_tpl_shortcode('Per Advertisement'),
                mail_tpl_shortcode('Per Month Equal'),
                mail_tpl_shortcode('Contract Period'),
                mail_tpl_shortcode('Total Amount'),
                mail_tpl_shortcode('Site Name'),
                mail_tpl_shortcode('Site URL')
            );
            //print_rm($user_data);
            $replace = array(
                $user_data['FullName'],
                $user_data['PackageTitle'],
                0,
                $user_data['PerMonth'],
                date('d.m.Y', strtotime($user_data['ExpiryDate'])),
                $user_data['PerMonth']*$user_data['Duration'],
                $this->data['site_setting']->SiteName,
                base_url()
            );
            $body = str_replace($search, $replace, $tpl->Description);
            $data['from'] = $this->data['site_setting']->FromEmail;
            $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName)) ? $this->data['site_setting']->FromName : $this->data['site_setting']->SiteName;
            $data['to'] = $user_data['Email'];
            $data['subject'] = $tpl->Subject;
            $data['body'] = $body;
            return sendEmail($data);
        }
    }

    public function updatePackage()
    {
        $UserID = $this->session->userdata['admin']['UserID'];
        $paymentType = $this->input->post('paymentType');
        $updatePackageDetails = $this->session->userdata('packageUpgradeDetails');

        $updateData = [
            'Qty' => $updatePackageDetails['Qty'],
            'Rate' => $updatePackageDetails['RegularPrice'],
            'PerMonth' => $updatePackageDetails['RegularPrice'],
            'Duration' => $updatePackageDetails['Duration'],
            'Discount' => $updatePackageDetails['Discount'],
            'ExpiryDate' => $updatePackageDetails['expiryDate'],
            'newPackage' => $updatePackageDetails['newPackage'],
            'PaymentMethod' => $paymentType,
            'IsExpiryEmailSent' => 0
        ];
        $historyData = [
            'UserID' => $UserID,
            'OldPackageID' => $updatePackageDetails['currentPackage'],
            'NewPackageID' => $updatePackageDetails['newPackage'],
            'CreatedAt' => date('Y-m-d h:i:s')
        ];
        $userPayment = [
            'PurchaseID' => $updatePackageDetails['PurchaseID'],
            'Status' => 0,
            'PaypalResponse' => '',
            'CreatedAt' => date('Y-m-d h:i:s')
        ];
        $return = [
            'status' => false,
            'message' => lang('something_went_wrong'),
        ];

        $update = $this->Package_model->upgradePackage($updateData, $updatePackageDetails['PurchaseID']);
        if($update)
        {
            $this->Package_model->addPackagePurchaseHistory($historyData);
            $this->User_model->addUserPayment($userPayment);
            $return = [
              'status' => true,
              'message' => lang('update_successfully'),
            ];
            $this->sendInvoiceEmail($updatePackageDetails['PurchaseID'], $UserID);
        }
        echo json_encode($return, true);
        exit;
    }

    public function upgradePackageModal()
    {
        $this->data['params'] = $this->input->get();
        $newPackage = $this->Package_model->getPackage($this->data['params']['packageId']);
        $price = $this->data['params']['RegularPrice'] - ( $this->data['params']['RegularPrice'] / 100 ) * $this->data['params']['Discount']; // calculate one month price
        $totalPriceNewPackage = $price * $this->data['params']['Duration']; // total package price
        $durationNewPackage = ($this->data['params']['Duration'] == 12)?365:$this->data['params']['Duration']*30; // duration days
        $newPackageOneDayPrice = $totalPriceNewPackage/$durationNewPackage; // package one day price

        $current_package = $this->User_model->getPackageByUserId($this->session->userdata['admin']['UserID']); // current package
        $totalOfCurrentPackage = ($current_package->Rate*$current_package->Duration)-$current_package->Discount; // calculate total price
        $duration = ($current_package->Duration == 12)?365:$current_package->Duration*30; // calculate duration days
        $oneDayPrice = round($totalOfCurrentPackage/$duration); // calculate one day price of current package

        $remainingPrice = 0;

        if($current_package->ExpiryDate >= date('Y-m-d'))
        {
            $now = time(); // or your date as well
            $your_date = strtotime($current_package->ExpiryDate);
            $datediff = $now - $your_date;

            $remainingDays =  abs(round($datediff / (60 * 60 * 24)));
            $remainingPrice = $oneDayPrice*$remainingDays;


            $totalRemainingDays = round($remainingPrice/$newPackageOneDayPrice);

            $Date = date('Y-m-d');
            $newExpiryDate = date('Y-m-d', strtotime($Date. ' + '.$totalRemainingDays.' days'));
            $updateData = [
              'PurchaseID' => $current_package->PurchaseID,
              'newPackage' => $this->data['params']['packageId'],
              'currentPackage' => $this->data['params']['current'],
              'RegularPrice' => $this->data['params']['RegularPrice'],
              'Duration' => $this->data['params']['Duration'],
              'Qty' => $this->data['params']['Qty'],
              'Discount' => $this->data['params']['Discount'],
              'expiryDate' => $newExpiryDate,
              'status' => 'new'
            ];
        }else{
            $Date = date('Y-m-d');
            $newExpiryDate = date('Y-m-d', strtotime($Date. ' + '.$duration.' days'));
            $updateData = [
                'PurchaseID' => $current_package->PurchaseID,
                'newPackage' => $this->data['params']['packageId'],
                'currentPackage' => $this->data['params']['current'],
                'RegularPrice' => $this->data['params']['RegularPrice'],
                'Duration' => $this->data['params']['Duration'],
                'Qty' => $this->data['params']['Qty'],
                'Discount' => $this->data['params']['Discount'],
                'expiryDate' => $newExpiryDate,
                'status' => 'existing'
            ];
        }
        $this->session->set_userdata('packageUpgradeDetails', $updateData);
        $html = $this->load->view('backend/dashboard/upgrade_package_modal', $this->data, TRUE);
        echo $html;
    }
    
    
	

}