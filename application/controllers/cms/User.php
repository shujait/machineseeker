<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Base_Controller {
	public 	$data = array();
	
	public function __construct() 
	{
           
		parent::__construct();
		checkAdminSession();
                
                $this->load->Model([
			ucfirst($this->router->fetch_class()).'_model',
			ucfirst($this->router->fetch_class()).'_text_model',
                        'Role_model',
                        'User_model',
                        'Modules_users_rights_model',
                        'Module_model','Menu_model'
            
                       
		]);
                
                
                
                
                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
                $this->data['TableKey'] = 'UserID';
                $this->data['Table'] = 'users';
			    $this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 1');
		        $this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2');
		if($this->session->userdata['admin']['UserID']){
				$this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows'); 
				}else{
				$this->data['user_wishlist'] = 0;	
				}
       
		
	}
	 
    
    public function index() {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        $this->load->model('Site_documents_model');
        // $this->data['results'] = $this->$parent->getAllUsers(false,$this->data['TableKey'],$this->language);
        
        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language);
        foreach ($this->data['results'] as $key) {
            $res = $key;
            $res->document = $this->Site_documents_model->getUserDocs($key->UserID);
            $result[] = $res;
        }

        $this->data['results'] = $result;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add()
    {
        
         if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
        $this->data['roles'] = $this->Role_model->getAllJoinedData(false,'RoleID',$this->language,'roles.IsActive = 1');
        if(!$this->data['roles']){
           $this->session->set_flashdata('message',lang('please_add_role_first'));
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    public function destroySession()
    {
       $sessionkey= $this->input->post('key');
        if(!empty($sessionkey)){
             $this->session->unset_userdata($sessionkey);
        }
            
    }
    
    public function view($id = '')
	{
         if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
        $id = base64_decode($id);
        $parent                             = $this->data['Parent_model'];
        $this->data['result']		    = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
	
        
        if(!$this->data['result']){
            $this->session->set_flashdata('message',lang('some_thing_went_wrong'));
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
        
        $this->data['roles'] = $this->Role_model->getAllJoinedData(false,'RoleID',$this->language,'roles.IsActive = 1');
        if(!$this->data['roles']){
           $this->session->set_flashdata('message',lang('please_add_role_first'));
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        
       
       
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/view';
       
	$this->load->view('backend/layouts/default',$this->data);
		
	}
        
	public function edit($id = '')
	{
	    if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
        if(!$this->data['result']){
            $this->session->set_flashdata('message',lang('some_thing_went_wrong'));
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $this->data['roles'] = $this->Role_model->getAllJoinedData(false,'RoleID',$this->language,'roles.IsActive = 1');
        if(!$this->data['roles']){
           $this->session->set_flashdata('message',lang('please_add_role_first'));
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
	    $this->load->view('backend/layouts/default',$this->data);
	}    
    
    public function change_password()
    {
	
		$post_data       = $this->input->post();
		$parent          = $this->data['Parent_model'];
		$child           = $this->data['Child_model'];
		$id              = $this->session->userdata['admin']['UserID'];
		$save_parent_data = $update_by = array();

			if($post_data){
				$user = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id.' AND '.$this->data['Table'].".Password = '".md5($post_data['OldPassword'])."'",'DESC','');

				if(!$user){

				   $errors['error'] =  lang('invalid_current_password');
				   $errors['success'] =   false;
				   echo json_encode($errors);
				   exit;
				} else if($post_data['Password'] != $post_data['ConfirmPassword']){

			   $errors['error'] =  lang('password_confirm_password_not_match');
			   $errors['success'] =   false;
			   $success['redirect'] = true;
			   $success['url'] = 'cms/'.$this->router->fetch_class();
			   echo json_encode($errors);
			   exit;
			}else if($post_data['Password'] == $post_data['ConfirmPassword']){       

				  unset($post_data['form_type']);
				  unset($post_data['ConfirmPassword']);
				  $save_parent_data['Password']        = md5($post_data['Password']);
				  $update_by[$this->data['TableKey']]  = $id;

                  $this->$parent->update($save_parent_data,$update_by);
                  
				  $success['error']   = false;
			      $success['redirect'] = true;
			      $success['url'] = 'cms/account/logout';
				  $success['success'] = lang('update_successfully');

				  echo json_encode($success);
				  exit;  
   
					}else{

					$errors['error'] =  lang('some_thing_went_wrong');
					$errors['success'] = false;
					$success['redirect'] = true;
					$success['url'] = 'cms/'.$this->router->fetch_class();
					echo json_encode($errors);
					exit;

				}
			 }
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/change-password';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function document()
    {
        $post_data       = $this->input->post();
        $parent          = $this->data['Parent_model'];
        $child           = $this->data['Child_model'];
        $id              = $this->session->userdata['admin']['UserID'];
        $this->load->model('Site_documents_model');
        $this->data['document']=$this->Site_documents_model->getUserDocs($id);
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/document';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function rights($user_id = ''){
        
        if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') && !checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
       if($user_id == ''){
           $user_id = $this->session->userdata['admin']['UserID'];
       }
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $this->data['UserID']               = $user_id;
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/rights';
        $this->data['results'] = $this->Modules_users_rights_model->getModulesWithRights($user_id,$this->language);
        
      
        if(empty($this->data['results'])){
            
            $this->session->set_flashdata('message',lang('please_add_module_first'));
            redirect(base_url('cms/'.$this->router->fetch_class()));
        }
        $this->data['users']   = $this->User_model->getAllJoinedData(false,'UserID',$this->language);
        
        if(empty($this->data['users'])){
            
            $this->session->set_flashdata('message',lang('please_add_user_first'));
            redirect(base_url('cms/user'));
        }
        
        $this->load->view('backend/layouts/default',$this->data);
        
    }
  
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
                $this->validate();
                $this->update();
          break;
            case 'delete':
                //$this->validate();
                $this->delete();
          break; 

            case 'save_rights':
                $this->saveRights();
          break; 

            case 'change_password':
                $this->change_password();
          break;

            case 'send_information_email':
                $this->sendInformationEmail();
          break;
            case 'document':
                $this->savedocuments();
          break;
                 
        }
    }
    
	private function validate($check = true)
    {

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $form_type = $this->input->post('form_type');
        $this->form_validation->set_rules('Title', lang('title'), 'required|trim');
       
		if($form_type == 'save'){
             $this->form_validation->set_rules('RoleID', lang('role'), 'required');
            $this->form_validation->set_rules('Email', lang('email'), 'required|valid_email|is_unique[users.Email]');
            $this->form_validation->set_rules('Password', lang('password'), 'required|min_length[8]|matches[ConfirmPassword]');
            $this->form_validation->set_rules('ConfirmPassword', lang('confirm_password'), 'required');
		}

        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {
                return true;
        }

    }	

    private function save()
    {
        
        if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
       
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['RoleID']         = $post_data['RoleID'];
        $save_parent_data['Password']       = md5($post_data['Password']);
        $save_parent_data['Email']          = $post_data['Email'];


        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');		
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
        
        
        

        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {
                
            
            
            
                $default_lang = getDefaultLanguageByDefault();
                
                
                
                $save_child_data['Title']                        = $post_data['Title'];
                $save_child_data[$this->data['TableKey']]        = $insert_id;
                $save_child_data['SystemLanguageID']             = $default_lang->SystemLanguageID;
                //$save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
                //$save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
                $this->addAllLanguageData($child,$this->data['TableKey'],$insert_id);


                $modules = $this->Module_model->getAll();
                foreach($modules as $key => $value)
                {
                    /*if($post_data['RoleID'] == 1 || $post_data['RoleID'] == 2){
                        $rights_all = 1;
                    }else{
                        $rights_all = 0;
                    }*/

                        $other_data[] = [
                                'ModuleID'  => $value->ModuleID,
                                'UserID'    => $insert_id,
                                'CanView'   => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanView') ? 1 : 0),
                                'CanAdd'    => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanAdd') ? 1 : 0),
                                'CanEdit'   => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanEdit') ? 1 : 0),
                                'CanDelete' => (checkRightAccess($value->ModuleID, $post_data['RoleID'], 'CanDelete') ? 1 : 0),
                                'CreatedAt' => date('Y-m-d H:i:s'),
                                'CreatedBy' => $this->session->userdata['admin']['UserID'],
                                'UpdatedAt' => date('Y-m-d H:i:s'),
                                'UpdatedBy' => $this->session->userdata['admin']['UserID']
                        ];



                }
                
                
               
                $this->Modules_users_rights_model->insert_batch($other_data);
                
                
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
	}
    
    private function update()
	{
		if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        if(isset($post_data[$this->data['TableKey']])){
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result']		    = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');


        if(!$this->data['result']){

           $errors['error'] =  lang('some_thing_went_wrong');
           $errors['success'] =   false;
           $success['redirect'] = true;
           $success['url'] = 'cms/'.$this->router->fetch_class();
           echo json_encode($errors);
           exit;
        }
        unset($post_data['form_type']);
		$save_parent_data                   = array();
                $save_child_data                    = array();
                if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1){
                    $UserID = base64_decode($post_data['UserID']);
                    $save_parent_data['Vat']          = $post_data['Vat'];
                    $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                    $save_parent_data['IsCompany']       = (isset($post_data['IsCompany']) ? 1 : 0 );
                    $save_parent_data['RoleID']         = $post_data['RoleID'];
                    $save_parent_data['IsCertifiedTrader']       = (isset($post_data['IsCertifiedTrader']) ? 1 : 0 );
                    $save_parent_data['IsPaid']       = (isset($post_data['IsPaid']) ? 1 : 0 );
                    $save_parent_data['TaxStatusConfirmed']       = (isset($post_data['TaxStatusConfirmed']) ? 1 : 0 );
                    $save_parent_data['VATConfirmed']       = (isset($post_data['VATConfirmed']) ? 1 : 0 );
                    $save_parent_data['IsTillActivated']       = (isset($post_data['IsTillActivated']) ? 1 : 0 );
                    $save_parent_data['TillActivated']       = ( date('Y-m-d H:i:s', strtotime($post_data['TillActivated'] ) ));
                    $save_parent_data['IsManualExtended']       = (isset($post_data['IsManualExtended']) ? 1 : 0 );
                    $save_parent_data['ManualExtendedTo']       = ( date('Y-m-d H:i:s', strtotime($post_data['ManualExtendedTo'] ) ));
                    
                    $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');		
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    $this->$parent->update($save_parent_data,$update_by);
                    $save_child_data['Title']                        = $post_data['Title'];
                   
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $this->$child->update($save_child_data,$update_by);
                    
                    $expiry_date = $save_parent_data['ManualExtendedTo'];
                    $UserPackagePurchaseID = $this->db->query("SELECT * FROM user_package_purchase WHERE UserID = ".$UserID." Order by PurchaseID DESC LIMIT 1")->row('PurchaseID');
                    $this->db->where('PurchaseID',$UserPackagePurchaseID)->update("user_package_purchase",array('ExpiryDate'=>$expiry_date));
                }else{
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $get_data = $this->$child->getWithMultipleFields($update_by);
                    
                    if($get_data){
                        
                        $save_child_data['Title']                        = $post_data['Title'];
                       
                        $this->$child->update($save_child_data,$update_by);
                        
                    }else{
                      
                        $save_child_data['Title']                        =  $post_data['Title'];
                        $save_child_data[$this->data['TableKey']]        =  base64_decode($post_data[$this->data['TableKey']]);
                        $save_child_data['SystemLanguageID']             =  base64_decode($post_data['SystemLanguageID']);
                       
                        $this->$child->save($save_child_data);
                    }
                }
		
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;
        
	}else{
           
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $success['redirect'] = true;
            $success['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    

   private function saveRights(){
       
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $user_id   = $this->input->post('UserID');
      
        $modules = $this->Modules_users_rights_model->getModulesWithRights($user_id,$this->language);
        
        $can_view   = $this->input->post('CanView');
        
        $can_add    = $this->input->post('CanAdd');
        $can_edit   = $this->input->post('CanEdit');
        $can_delete = $this->input->post('CanDelete');
         
        if(!empty($modules)){
            
            foreach($modules as $module){
              
                $update_data[] = [
                                    'ModuleRightID'     => $module['ModuleRightID'],
                                    'CanView'       => (isset($can_view[$module['ModuleRightID']]) ? 1 : 0),
                                    'CanAdd'            => (isset($can_add[$module['ModuleRightID']]) ? 1 : 0),
                                    'CanEdit'           => (isset($can_edit[$module['ModuleRightID']]) ? 1 : 0),
                                    'CanDelete'         => (isset($can_delete[$module['ModuleRightID']]) ? 1 : 0),
                                    'UpdatedAt'         => date('Y-m-d H:i:s'),
                                    'UpdatedBy'         => $this->session->userdata['admin']['UserID']
                    
                ];
            }
            
            $this->Modules_users_rights_model->update_batch($update_data,'ModuleRightID');
            $success['error']   = false;
            $success['success'] = lang('update_successfully');
            $success['reload'] = true;

            echo json_encode($success);
            exit;  
        }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] =   false;
            
            echo json_encode($errors);
            exit;
        }
    }
 
    private function delete(){
        
        if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');

        $this->Modules_users_rights_model->delete($deleted_by);
        
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
	
    public function profile()
	{
        $post_data = $this->input->post();
		$parent               = $this->data['Parent_model'];
		$child                = $this->data['Child_model'];
		$save_parent_data     = $save_child_data = array();	
        $id = $this->session->userdata['admin']['UserID'];
		
		if($post_data){
			
		$errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('title', lang('title'), 'required|trim');
        $this->form_validation->set_rules('uname', lang('name'), 'required|trim');
        $this->form_validation->set_rules('companyName', lang('company_name'), 'required|trim');
        $this->form_validation->set_rules('ph', lang('ph'), 'required|trim');
        $this->form_validation->set_rules('country', lang('country'), 'required|trim');
		$this->form_validation->set_rules('StreetAddress', lang('street_address'), 'required|trim');
		$this->form_validation->set_rules('postcodeTown', lang('postcode'), 'required|trim');	

        if ($this->form_validation->run() == FALSE)
        {
                $errors['error'] = validation_errors();
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
        }else
        {
		$img = get_user_info(array('a.UserID' => $id));
		if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
			 $image = $this->uploadImage("Image", "uploads/images/");
			if($image){
			if($img->CoverImage){			
	         unlink($img->CoverImage);
			  }
			$save_parent_data['CoverImage'] = $image;
			}else{
			$success['error'] = lang('invalid-format');
			$success['success'] = 'false';
			echo json_encode($success);
			exit;	
			}
        }
		$save_parent_data['UName']          = $post_data['uname'];
        $save_parent_data['MobCountryCode'] = $post_data['MobCountryCode'];
		$save_parent_data['Mobile']         = $post_data['mob'];
        $save_parent_data['CountryCode']    = $post_data['CountryCode'];
		$save_parent_data['Phone']          = $post_data['ph'];
        $save_parent_data['CountryFaxCode'] = $post_data['CountryFaxCode'];
		$save_parent_data['Fax']            = $post_data['fax'];
		$save_parent_data['Website']		= $post_data['website'];
		$save_parent_data['Lang']		    = $post_data['lang'];
		$save_parent_data['City']		    = $post_data['City'];
		$save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');
		$save_parent_data['UpdatedBy']      = $id;
		$update_by  = array();
		$update_by[$this->data['TableKey']] = $id;
		$this->$parent->update($save_parent_data,$update_by);	
		$save_child_data['Title']            = $post_data['title'];
		$save_child_data['CompanyName']      = $post_data['companyName'];
		$save_child_data['StreetAddress']    = $post_data['StreetAddress'];
		$save_child_data['Country']          = $post_data['country'];
		$save_child_data['PostalCode']       = $post_data['postcodeTown'];
		$save_child_data['CompanyProfile']   = $post_data['company_description'];
		$this->$child->update($save_child_data,$update_by);
		$this->session->set_userdata('lang',$post_data['lang']);
            $success['error']   = false;
            $success['success'] = lang('update_successfully');
            $success['reload'] = true;
            echo json_encode($success);
            exit;
		}
	 }

    $this->data['result'] = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
    $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/profile';
	$this->load->view('backend/layouts/default',$this->data);
		
	}



    private function sendInformationEmail(){

        if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $parent               = $this->data['Parent_model'];

        $id = $this->input->post('UserID');

        $user = $this->$parent->getUserDataByID($id);
        if($user){
            
            $tpl = get_email_tpl(array('a.TplList' => 14),$this->language);
           
            if($tpl){
                $search = array(mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Company Name'),mail_tpl_shortcode('Phone'),mail_tpl_shortcode('Mobile'),mail_tpl_shortcode('Fax'),mail_tpl_shortcode('Address'),mail_tpl_shortcode('Country'),mail_tpl_shortcode('Site Name'),mail_tpl_shortcode('Site URL'));   
                
                $replace = array($user['Title'] ,$user['Email'],$user['CompanyName'],$user['Phone'],$user['Mobile'],$user['Fax'],$user['StreetAddress'],$user['CountryTitle'], $this->data['site_setting']->SiteName,base_url()); 
                $body = str_replace($search,$replace, $tpl->Description);
                $data['from'] = $this->data['site_setting']->FromEmail;
                $data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName))?$this->data['site_setting']->FromName:$this->data['site_setting']->SiteName;  
                $data['to']   = $user['Email'];
                $data['subject']   = $tpl->Subject;     
                $data['body'] = $body;
               
                if(sendEmail($data)){
                     $success['error']   = false;
                     $success['success'] = lang('email_sent');
                     echo json_encode($success);
                      exit;  
                }else{
                     $errors['error'] = lang('email_no_sent');
                    $errors['success'] = 'false';
                    echo json_encode($errors);
                    exit;

                }

               }else{
                $errors['error'] = lang('email_no_sent');
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;

               }

        }else{
            $errors['error'] = lang('email_no_sent');
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
        

    }
    
    public function savedocuments() {
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = $save_child_data = array();
        $id = $this->session->userdata['admin']['UserID'];
        if ($post_data) {
            $errors = array();
            $email=[];    
            if (isset($_FILES['businessRegistration']["name"]) && $_FILES['businessRegistration']["name"] != '') {
                    $image = $this->fileupload($id, "businessRegistration");
                    if ($image) {
                        $email[] =  ($image);
                  } else {
                        $success['error'] = lang('invalid-format');
                        $success['success'] = 'false';
                        echo json_encode($success);
                        exit;
                    }
                }
                if (isset($_FILES['taxCertificate']["name"]) && $_FILES['taxCertificate']["name"] != '') {
                $image = $this->fileupload($id, "taxCertificate");
                if ($image) {
                    $email[] =  ($image);
                } else {
                    $success['error'] = lang('invalid-format');
                    $success['success'] = 'false';
                    echo json_encode($success);
                    exit;
                }
            }
               if (!empty($email)) {
                $checkUser = get_user_info(array('a.UserID' => $id));
                $tpl = get_email_tpl(array('TplList' => 18), $this->language);
                if ($tpl) {
                    $search = array('[name]', '[customer_no]');
                    $replace = array($checkUser->UName, 'R-000000'.$id);
                    $body = str_replace($search, $replace, $tpl->Description);
                    $data['from'] = $this->data['site_setting']->FromEmail;
                    $data['from_name'] = (isset($this->data['site_setting']->FromName) and ! empty($this->data['site_setting']->FromName)) ? $this->data['site_setting']->FromName : $this->data['site_setting']->SiteName;
                    $data['to'] =  $checkUser->Email.','.$this->data['site_setting']->FromEmail ;
                    $data['subject'] = $tpl->Subject;
                    $data['body'] = $body;
                    sendEmail($data,$email);
            
                }
            }
            $errors['error'] = lang('email_no_sent');
            $errors['success'] = lang('update_successfully');
            $errors['redirect'] = true;
            $errors['url'] = 'cms/machine';
            echo json_encode($errors);
            exit;
            
        }

    }

    private function fileupload($insert_id, $fileName) {
        if (isset($_FILES[$fileName]) && !empty($_FILES[$fileName])) {
            $file = $fileName;
            $path = 'uploads/documents/';
            $key = $insert_id;
            $type = $fileName;
            return $this->uploadImageforDocs($file, $path, $key, $type, TRUE);
        }
    }

}