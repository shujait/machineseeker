<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            ucfirst($this->router->fetch_class()).'_model',
            ucfirst($this->router->fetch_class()).'_text_model','Menu_model'
        ]);

                $this->data['language']      = $this->language;
                $this->data['ControllerName'] = $this->router->fetch_class();
                $this->data['Parent_model']   = ucfirst($this->router->fetch_class()).'_model';
                $this->data['Child_model']    = ucfirst($this->router->fetch_class()).'_text_model';
                $this->data['TableKey'] = 'MenuID';
                $this->data['Table'] = 'menus';
		$this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 1');
		        $this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2'); 
		if($this->session->userdata['admin']['UserID']){
				$this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows'); 
				}else{
				$this->data['user_wishlist'] = 0;	
				}
  
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
          $child                              = $this->data['Child_model'];
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
          $this->data['results'] = $this->$parent->getAllJoinedData(false,$this->data['TableKey'],$this->language);
          
          $this->load->view('backend/layouts/default',$this->data);
    }
    public function add()
    {
         if(!checkUserRightAccess(53,$this->session->userdata['admin']['UserID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
       
        
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
        if(!checkUserRightAccess(53,$this->session->userdata['admin']['UserID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
    
        
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']]   = $id;
    $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
				$this->validate();
                $this->update();
          break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$form_type = $this->input->post('form_type');
        $title = ($form_type == 'save')?'|is_unique['.$this->data['Table'].'_text.Title]':'';
        $this->form_validation->set_rules('Title', lang('title'), 'required'.$title);
		if(base64_decode($this->input->post('SystemLanguageID')) == 1){
        $this->form_validation->set_rules('menu_position', lang('menu_position'), 'required');     		
        $this->form_validation->set_rules('menu', lang('menu_structure'), 'required');   
		$this->form_validation->set_rules('show_page', lang('menu_show_on_selected_page'), 'required');      
		}

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        
        if(!checkUserRightAccess(53,$this->session->userdata['admin']['UserID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        $save_parent_data                   = array();
        $save_child_data                    = array();
       
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
        
	    $save_parent_data['MenuPosition']   = $post_data['menu_position'];
	    $save_parent_data['Menu']           = $post_data['menu'];
		$save_parent_data['ShowOn']         = $post_data['show_page'];
        $save_parent_data['SortOrder']      = $sort;
        $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['CreatedAt']      = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');        
        $save_parent_data['CreatedBy']      = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
            {

                $default_lang = getDefaultLanguageByDefault();
                
                $save_child_data['Title']                        = $post_data['Title'];
                $save_child_data[$this->data['TableKey']]        = $insert_id;
                $save_child_data['SystemLanguageID']             = $default_lang->SystemLanguageID;
                $save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
                $save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
                $this->$child->save($save_child_data);
                $this->addAllLanguageData($child,$this->data['TableKey'],$insert_id);
                
                
                $success['error']   = false;
                $success['success'] = lang('save_successfully');
                $success['redirect'] = true;
                $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
                echo json_encode($success);
                exit;


            }else
            {
                $errors['error'] =  lang('some_thing_went_wrong');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
    }
    
        private function update()
    {
        

                if(!checkUserRightAccess(53,$this->session->userdata['admin']['UserID'],'CanEdit')){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/'.$this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }
                $post_data = $this->input->post();
                $parent                             = $this->data['Parent_model'];
                $child                              = $this->data['Child_model'];
                if(isset($post_data[$this->data['TableKey']])){
                    $id = base64_decode($post_data[$this->data['TableKey']]);
                    $this->data['result']          = $this->$parent->getJoinedData(false,$this->data['TableKey'],$this->data['Table'].'.'.$this->data['TableKey'].'='.$id,'DESC','');
    
        
                if(!$this->data['result']){
                   $errors['error'] =  lang('some_thing_went_wrong');
                   $errors['success'] =   false;
                   $errors['redirect'] = true;
                   $errors['url'] = 'cms/'.$this->router->fetch_class();
                   echo json_encode($errors);
                   exit;
                }            
                
                unset($post_data['form_type']);
                $save_parent_data =  $save_child_data = array();
                if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1){
					if(base64_decode($this->input->post('SystemLanguageID')) == 1){
                    $save_parent_data['MenuPosition']   = $post_data['menu_position'];
					$save_parent_data['Menu']           = $post_data['menu'];
					$save_parent_data['ShowOn']         = $post_data['show_page'];
                    $save_parent_data['IsActive']       = (isset($post_data['IsActive']) ? 1 : 0 );
                    $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');     
                    $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    
                    $this->$parent->update($save_parent_data,$update_by);
					}
                    $save_child_data['Title']                        = $post_data['Title'];
                    $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];
                    $update_by['SystemLanguageID']                   =  base64_decode($post_data['SystemLanguageID']);
					
				
                    
                    $this->$child->update($save_child_data,$update_by);
                    
                }else{
                    
                    $update_by  = array();
                    $update_by[$this->data['TableKey']]  = base64_decode($post_data[$this->data['TableKey']]);
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    
                    $get_data = $this->$child->getWithMultipleFields($update_by);
                    
                    if($get_data){
                        
                        $save_child_data['Title']                        = $post_data['Title'];
                        $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
	
                        $this->$child->update($save_child_data,$update_by);
                        
                    }else{
                        
                        $save_child_data['Title']                        = $post_data['Title'];
                        $save_child_data[$this->data['TableKey']]        =  base64_decode($post_data[$this->data['TableKey']]);
                        $save_child_data['SystemLanguageID']             =  base64_decode($post_data['SystemLanguageID']);
                        $save_child_data['CreatedAt']                    =  $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['CreatedBy']                    =  $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];
                        $this->$child->save($save_child_data);
                    }
            
                }
        
              $success['error']   = false;
              $success['success'] = lang('update_successfully');
        
              echo json_encode($success);
              exit;  
        
    }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    private function delete(){
        
         if(!checkUserRightAccess(53,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);

        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }

}