<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends Base_Controller {
    public  $data = array();
    
    public function __construct() 
    {
           
        parent::__construct();
        checkAdminSession();
                
                $this->load->Model([
            'System_language_model','Menu_model','System_language_text_model'
        ]);
                
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model']   = 'System_language_model';
        $this->data['Child_model']   = 'System_language_text_model';
      
        $this->data['TableKey'] = 'SystemLanguageID';
        $this->data['Table'] = 'system_languages';
		$this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 1');
		$this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2');
		if($this->session->userdata['admin']['UserID']){
				$this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows'); 
				}else{
				$this->data['user_wishlist'] = 0;	
				}
       
        
    }
     
    
    public function index()
    {
          $parent                             = $this->data['Parent_model'];
         $default = getDefaultLanguage();
          $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        
          $this->data['results'] = $this->$parent->getActiveAllJoinedDataForSystemLanguage(false,'LanguageID',false,'system_languages_text.SystemLanguageID = '.$default->SystemLanguageID);
         // print_rm($this->data['results']);
          
          $this->load->view('backend/layouts/default',$this->data);
    }
    public function add()
    {
         if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanAdd')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent = $this->data['Parent_model'];
        

        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
       
        
        
        $this->load->view('backend/layouts/default',$this->data);
    }
    
    public function edit($id = '')
    {
        if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        $parent                             = $this->data['Parent_model'];
        $this->data['result']           = $this->$parent->getJoinedDataForSystemLanguage(true,'LanguageID','system_languages.SystemLanguageID = '.$id);
       // echo $this->db->last_query();exit;
        if(!$this->data['result']){
           redirect(base_url('cms/'.$this->router->fetch_class())); 
        }
        //print_rm($this->data['result']);
       //$this->data['result'] = $this->data['result'][0];
        
       
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->data[$this->data['TableKey']]     = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }

    public function lang($id = '')
    {
        if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanEdit')){
             $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
             redirect(base_url('cms/'.$this->router->fetch_class())); 
        }

        $this->data['SystemLanguageID'] = $id;

        $parent                             = $this->data['Parent_model'];
         $language  =       $this->$parent->getJoinedDataForSystemLanguage(true,'LanguageID','system_languages.SystemLanguageID = '.$id);
         $language = $this->data['langText'] = $language[0];
       // $language  = $this->data['langText']           = $this->$parent->get($id,true,'SystemLanguageID');
		
        $this->data['languageTexts'] = $this->lang->load('rest_controller', $language['ShortCode'],true);

		
        //$this->data['languageTexts'] = lang('', '', array(), TRUE);
      
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/lang';
        $this->data[$this->data['TableKey']]     = $id;
        $this->load->view('backend/layouts/default',$this->data);
        
    }
    
    
    
    
    
    public function action()
    {
        $form_type = $this->input->post('form_type');
        switch($form_type){
            case 'save':
                $this->validate();
                $this->save();
          break; 
            case 'update':
				 $this->validate();
                $this->update();
          break;
          case 'langindexadd':
              $this->langindexadd();
          break;
          case 'langupdate':
              $this->langupdate();
            break;
            case 'delete':
                $this->delete();
          break;      
                 
        }
    }
    
    
    private function validate(){
        $errors = array();
		$form_type = $this->input->post('form_type');
		$short_code = '';
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('SystemLanguageTitle', lang('title'), 'required');
		if($form_type == 'save'){
		    $short_code = '|is_unique[system_languages.ShortCode]';
		}
        //$this->form_validation->set_rules('ShortCode', lang('short_code'), 'required'.$short_code);

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
    
    private function save()
    {
        
        if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanAdd')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data                          = $this->input->post();
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
       
        $save_parent_data                   = array();
        $save_child_data                   = array();
    
        $save_parent_data['IsActive']                   = (isset($post_data['Hide']) ? 0 : 1 );
        $save_parent_data['Hide']                       = (isset($post_data['Hide']) ? 1 : 0 );
		
        if (isset($_FILES['logo']["name"][0]) && $_FILES['logo']["name"][0] != '') {
			$image  = $this->uploadImage("logo", "uploads/");
			if($image){
            $save_parent_data['Logo'] = $image;
			}else{
        $success['error'] = lang('invalid-format');
        $success['success'] = 'false';
        echo json_encode($success);
        exit;	
			}
		 }


        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
           
        $sort = 0;
        if(!empty($getSortValue))
        {
           
            $sort = $getSortValue['SortOrder'] + 1;
        }
       
        
        $save_parent_data['SortOrder']      = $sort; 


		//$save_parent_data['SystemLanguageTitle']      = $post_data['SystemLanguageTitle'];
        $save_parent_data['ShortCode']                = strtolower($post_data['ShortCode']); 
        $save_parent_data['IconClass']                = $post_data['IconClass']; 
        $save_parent_data['Direction']                = $post_data['Direction'];
        $save_parent_data['CreatedAt']                = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');      
        $save_parent_data['CreatedBy']                = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];


        $insert_id                          = $this->$parent->save($save_parent_data);
        if($insert_id > 0)
        {
            
            $default_lang = getDefaultLanguageByDefault();
            $save_child_data['SystemLanguageTitle']                        = $post_data['SystemLanguageTitle'];
            $save_child_data['LanguageID']                   = $insert_id;
            $save_child_data['SystemLanguageID']             = $default_lang->SystemLanguageID;
            $save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
            $save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
            $this->$child->save($save_child_data);
            
            $this->addAllLanguageData($child,'LanguageID',$insert_id);



            $languageTexts = lang('', '', array(), TRUE);
            $lang_file_content = '';
            foreach ($languageTexts as $key => $value) {
                $lang_file_content .= "\n\$lang['".$key."'] = '".addslashes($value)."';";
            }
            $path = APPPATH . "language/";
            if (!is_dir($path.$save_parent_data['ShortCode'])) {
                mkdir($path.$save_parent_data['ShortCode'], 0777, TRUE);
                $new_path = $path.$save_parent_data['ShortCode'].'/';
                $data = "<?php
                            ".$lang_file_content."
                            ";
                write_file($new_path. 'rest_controller_lang' . '.php', $data);
            }
        
        
            $success['error']   = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/'.$this->router->fetch_class().'/edit/'.$insert_id;
            echo json_encode($success);
            exit;


        }else
        {
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }
    
    private function update()
    {
        
            if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanEdit')){
                $errors['error'] =  lang('you_dont_have_its_access');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/'.$this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }

            $post_data = $this->input->post();
            $parent                             = $this->data['Parent_model'];
            $child                             = $this->data['Child_model'];
      
        if(isset($post_data[$this->data['TableKey']])){
            $id = $post_data[$this->data['TableKey']];
            $this->data['result']           =  $this->$parent->getJoinedDataForSystemLanguage(false,'SystemLanguageID','system_languages.SystemLanguageID = '.base64_decode($post_data['LanguageID']));
              

    
            if(!$this->data['result']){
               $errors['error'] =  lang('some_thing_went_wrong');
               $errors['success'] =   false;
               $errors['redirect'] = true;
               $errors['url'] = 'cms/'.$this->router->fetch_class();
               echo json_encode($errors);
               exit;
            }            
               $this->data['result'] =  $this->data['result'][0];  
            unset($post_data['form_type']);
            $save_parent_data                   = array();
			if (isset($_FILES['logo']["name"][0]) && $_FILES['logo']["name"][0] != '') {
			$image  = $this->uploadImage("logo", "uploads/posts/");

			$save_parent_data['Logo'] = $image;
			}
           
            
        

$save_child_data                    = array();
        if(isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1){
                   
                   
                    
            $save_parent_data['IconClass']                = $post_data['IconClass']; 
            $save_parent_data['Direction']                = $post_data['Direction']; 
            $save_parent_data['IsActive']                   = (isset($post_data['Hide']) ? 0 : 1 );
            $save_parent_data['Hide']                       = (isset($post_data['Hide']) ? 1 : 0 );
            
            
            
            $save_parent_data['UpdatedAt']      = date('Y-m-d H:i:s');      
            $save_parent_data['UpdatedBy']      = $this->session->userdata['admin']['UserID'];
            
            $update_by  = array();
            $update_by[$this->data['TableKey']]  = base64_decode($post_data['LanguageID']);
                



            $this->$parent->update($save_parent_data,$update_by);
                   
                    $save_child_data['SystemLanguageTitle']                        = $post_data['SystemLanguageTitle'];
                   
                    $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];
                    
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                    $update_by['LanguageID']  = base64_decode($post_data['LanguageID']);
                    $this->$child->update($save_child_data,$update_by);
                    
                }else{
                    
                    $update_by  = array();
                    $update_by['LanguageID']  = base64_decode($post_data['LanguageID']);
                    $update_by['SystemLanguageID']       =  base64_decode($post_data['SystemLanguageID']);
                  // print_rm($update_by);
                    $get_data = $this->$child->getWithMultipleFields($update_by);
                    
                    if($get_data){
                        
                        $save_child_data['SystemLanguageTitle']                        = $post_data['SystemLanguageTitle'];
                       
                        
                        $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];



                        $this->$child->update($save_child_data,$update_by);
                        
                    }else{
                        
                        $save_child_data['SystemLanguageTitle']                        =  $post_data['SystemLanguageTitle'];
                        $save_child_data[$this->data['TableKey']]        =  base64_decode($post_data[$this->data['TableKey']]);
                        $save_child_data['SystemLanguageID']             =  base64_decode($post_data['SystemLanguageID']);
                        $save_child_data['CreatedAt']                    =  $save_child_data['UpdatedAt']                    = date('Y-m-d H:i:s');
                        $save_child_data['CreatedBy']                    =  $save_child_data['UpdatedBy']                    = $this->session->userdata['admin']['UserID'];



                        $this->$child->save($save_child_data);
                    }
                    
                    
                    
                    
                }









            $success['error']   = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;
        
       }else{
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
            
        }
     }
    
    
    private function langindexadd()
    {
        if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanEdit')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $parent = $this->data['Parent_model'];

        $LanguageIndex = $this->input->post('LanguageIndex');
        $LanguageValue = $this->input->post('LanguageValue');
        
        $languageTexts = lang('', '', array(), TRUE);
        foreach ($languageTexts as $key => $value) {
            if($key == $LanguageIndex)
            {
                $errors['error'] =  lang('duplicate_index');
                $errors['success'] = false;
                echo json_encode($errors);
                exit;
            }
        }

        $lang_file_content = "\n\$lang['".$LanguageIndex."'] = '".addslashes($LanguageValue)."';";
        $path = APPPATH . "language/";

        $results = $this->$parent->getAll(false);

        foreach ($results as $key => $value) {
            if (is_dir($path.$value->ShortCode)) {
                $LangFilepath = $path.$value->ShortCode.'/';

                $my_file = $LangFilepath.'rest_controller_lang.php'; //filename

                $handle = fopen($my_file, 'a') or die('Cannot open file:  '.$my_file);

                fwrite($handle, $lang_file_content);
            }
        }

        $success['error']   = false;
        $success['success'] = lang('save_successfully');
        $success['redirect'] = true;
        $success['url'] = 'cms/'.$this->router->fetch_class().'/lang/'.$this->input->post('SystemLanguageID');
        echo json_encode($success);
        exit;
    }
    
    private function langupdate()
    {
        if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanEdit')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $languageTexts = $this->input->post('LanguageValue');
        
        $lang_file_content = '';
        foreach ($languageTexts as $key => $value) {
            $lang_file_content .= "\n\$lang['".$key."'] = '".addslashes($value)."';";
        }
        $path = APPPATH . "language/";
        if (is_dir($path.strtolower($this->input->post('ShortCode')))) {
            $new_path = $path.strtolower($this->input->post('ShortCode')).'/';
            $data = "<?php
                        ".$lang_file_content."
                        ";
            write_file($new_path. 'rest_controller_lang' . '.php', $data);
        }

        $success['error']   = false;
        $success['success'] = lang('update_successfully');

        echo json_encode($success);
        exit;
    }
    
   /*
    
    private function delete(){
        
         if(!checkRightAccess(40,$this->session->userdata['admin']['RoleID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
           $errors['success'] = false;
           
            echo json_encode($errors);
            exit;
        }
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];
        
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);
       
        
        
        
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');
        
        echo json_encode($success);
        exit;
    }
    */
    
    

}