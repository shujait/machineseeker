<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use Intervention\Image\ImageManagerStatic as Image;

class Machine extends Base_Controller {

    public $data = array();

    public function __construct() {

        parent::__construct();
        checkAdminSession();

        $this->load->Model([
            ucfirst($this->router->fetch_class()) . '_model',
            ucfirst($this->router->fetch_class()) . '_text_model', 'User_model', 'User_text_model', 'Site_images_model', 'Menu_model', 'Inquiry_model', 'Purchase_model', 'Site_documents_model'
        ]);

        $this->data['language'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'MachineID';
        $this->data['Table'] = 'machines';
        $this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false, 'MenuID', false, 'menus.MenuPosition = 1');
        $this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false, 'MenuID', false, 'menus.MenuPosition = 2');
        if ($this->session->userdata['admin']['UserID']) {
            $this->data['user_wishlist'] = getValue('wishlist', array('UserID' => $this->session->userdata['admin']['UserID']), 'num_rows');
        } else {
            $this->data['user_wishlist'] = 0;
        }
    }

    public function index() {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $userid = $this->session->userdata['admin']['UserID'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        if (in_array($this->session->userdata['admin']['RoleID'], array(1, 2))) {
            $this->data['check_package_active'] = true;
            $where = $this->data['purchase'] = '';
        } else {
            $this->data['check_package_active'] = check_user_package_active();

            $where = $this->data['Table'] . '.UserID=' . $this->session->userdata['admin']['UserID'];
            $this->data['purchase'] = $this->Purchase_model->getPurchaseData(array('b.Status IN (2,4,5)', 'a.UserID' => $this->session->userdata['admin']['UserID']), 'row');
            $this->data['package'] = ($this->data['purchase']) ? getPackage($this->data['purchase']->PackageID) : '';
        }
        $documentStatus = $this->Site_documents_model->getDocumentStatus($userid);
        if ($documentStatus && $this->session->userdata['admin']['RoleID'] == 4) {
            $this->data['errorMsg'] = lang("missing_documents");
        }
        $MachinAdsTypes = -1;
        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language, $where, $sort = 'ASC', $sort_field = 'SortOrder', $title_key = 'Title', $limit = false, $start = 0, $other_sort_field = false, $MachinAdsTypes);
        $this->data['InquiriesOfMachine'] = $this->$parent->getInquiriesOfMachine($this->session->userdata['admin']['UserID']);
        /* echo $this->db->last_query();
          exit(); */

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function add() {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanAdd') || ( $this->session->userdata['admin']['RoleID'] == 4 && !check_user_package_active())) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';
        $this->data['helpingUrl'] = 'wie-erstelle-ich-ein-inserat/';
        if ($this->session->userdata['admin']['RoleID'] == 1) {
            $this->data['dealers'] = $this->User_model->getAllDealers($this->language);
        }

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '') {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');
        $this->data['site_images'] = $this->Site_images_model->getMultipleRows(array('FileID' => $id, 'ImageType' => 'MachineFile'), true);

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        //print_rm($this->data['result']);


        $this->data['helpingUrl'] = 'wie-erstelle-ich-ein-inserat/';
        if ($this->session->userdata['admin']['RoleID'] == 1) {
            $this->data['dealers'] = $this->User_model->getAllDealers($this->language);
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);
    }

    private function publish() {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $post_data = $this->input->post();
        $save_parent_data['IsActive'] = 1;
        $update_by[$this->data['TableKey']] = $post_data['id'];
        $this->$parent->update($save_parent_data, $update_by);
        $success['error'] = false;
        $success['success'] = lang('update_successfully');
        $success['redirect'] = true;
        $success['url'] = 'cms/' . $this->router->fetch_class();
        echo json_encode($success);
        exit;
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->validate();
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'deleteimage':
                $this->removeImage();
                break;
            case 'inquiry_delete':
                $this->inquiry_delete();
                break;
            case 'publish':
                $this->publish();
                break;
            case 'import_data':
                $this->import_data();
                break;
        }
    }

    private function validate() {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        //$this->form_validation->set_rules('title', lang('title'), 'required');
        // for machine ads type
        if (empty($this->input->post('machine_ads_type'))) {
            $this->form_validation->set_rules('machine_type', lang('machine_type'), 'required');
        }
        if ($this->input->post('IsDefault') == 1) {
            $this->form_validation->set_rules('category', lang('category'), 'required');
            if ($this->input->post('category') == '111') {
                $this->form_validation->set_rules('service', lang('Article/Services'), 'required');
            } else {
                // for machine ads type
                if (empty($this->input->post('machine_ads_type'))) {
                    $this->form_validation->set_rules('manufacturer', lang('manufacturer'), 'required');
                }
            }
            //$this->form_validation->set_rules('model', lang('model'), 'required');
            // for machine ads type
            if (empty($this->input->post('machine_ads_type'))) {
                $this->form_validation->set_rules('manufacturer_year', lang('manufacturer_year'), 'required');
            }
            $this->form_validation->set_rules('codition', lang('codition'), 'required');
            //$this->form_validation->set_rules('location', lang('location'), 'required');
        }

        //$this->form_validation->set_rules('description', lang('Description'), 'required');
        //$this->form_validation->set_rules('images', lang('Image'), 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save() {

        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        if (isset($post_data['IsActive']) && !isset($post_data['TermsAndCondition'])) {
            $errors['error'] = lang('terms_condition_machine_msg');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue))
        {
            $sort = $getSortValue['SortOrder'] + 1;
        }
        $FeaturedImageName = '';
        $sort = 0;
        if(!empty($this->input->post('sortArray')))
        {
            $sort = explode(',', $this->input->post('sortArray'))[0];
        }

        if(isset($_FILES['images']) && !empty($_FILES['images']) && $_FILES['images']['name'][$sort] != '') {
            $UserDir='uploads/user/' .$this->session->userdata['admin']['UName'].'-00'. $this->session->userdata['admin']['UserID'] . '/images/'.date('Y').'/';
            $FeaturedImageName = $this->uploadfeature_image('images', $UserDir, $sort);
            if ($FeaturedImageName != '') {
                $this->watermarkimage($FeaturedImageName);
                unset($_FILES['images']['name'][$sort]);
                unset($_FILES['images']['tmp_name'][$sort]);
                unset($_FILES['images']['type'][$sort]);
                unset($_FILES['images']['size'][$sort]);
            }
        }

        // for machine ads type
        if (isset($post_data['machine_ads_type']) && !empty($post_data['machine_ads_type']) && $post_data['machine_ads_type'] == 1) {
            $save_parent_data['MachinAdsTypes'] = 1;
            if (isset($post_data['ads_type']) && $post_data['ads_type'] == 1) {
                $save_parent_data['AdsVisitor'] = 1;
                $save_parent_data['AdsOffering'] = 0;
            } else if (isset($post_data['ads_type']) && $post_data['ads_type'] == 2) {
                $save_parent_data['AdsOffering'] = 1;
                $save_parent_data['AdsVisitor'] = 0;
            }
        }
        // translate desc for german case
        $lang_code = "";
        if (isset($this->session->userdata['lang'])) {
            $lang_code = $this->session->userdata['lang'];
        }
        $extra_field_array = array();
       if ($lang_code == "de") {
           $translated_desc = "";
           if (!empty(trim($post_data['description']))) {
               try {
                    $translated_desc = lang_translate($post_data['description']);
                   $extra_field_array['Description'] = $translated_desc;
                   $extra_field_array['autoTranslated'] = 1;
               } catch (Exception $e) {
                   //echo 'Caught exception: ', $e->getMessage(), "\n";
               }
           }
       }
        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['TermsAndCondition'] = (isset($post_data['TermsAndCondition']) ? 1 : 0 );
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) && $post_data['IsActive'] != '0' ) ? 1 : 0;
		if($save_parent_data['IsActive'] != '0' ){
            $save_parent_data['IsActive'] = 1;
            $save_parent_data['PublishDate'] = date('Y-m-d');
        }else{
            $save_parent_data['IsActive'] = 0;
        }        $save_parent_data['UserID'] = (!empty($post_data['UserID']) ? $post_data['UserID'] : $this->session->userdata['admin']['UserID']);
        $save_parent_data['CategoryID'] = $post_data['category'];
        if ($post_data['category'] == '111') {
            $save_parent_data['Service'] = $post_data['service'];
        } else {
            // for machine ads type
            if (empty($this->input->post('machine_ads_type'))) {
                $save_parent_data['Manufacturer'] = $post_data['manufacturer'];
            }
        }
        $save_parent_data['Condition'] = $post_data['codition'];
        //$save_parent_data['Model']          = $post_data['model'];
        // for machine ads type
        if (empty($this->input->post('machine_ads_type'))) {
            $save_parent_data['ManufacturerYear'] = $post_data['manufacturer_year'];
        }
        $save_parent_data['Price'] = $post_data['price'];
        //$save_parent_data['Vat']            = $post_data['vat'];
        $save_parent_data['Location'] = $post_data['location'];
        $save_parent_data['Lat'] = $post_data['Lat'];
        $save_parent_data['Lng'] = $post_data['Lng'];
        $save_parent_data['CountryShort'] = $post_data['CountryShort'];
        $save_parent_data['ReferenceNumber'] = $post_data['reference_number'];
        $save_parent_data['SerialNumber'] = $post_data['serial_number'];
        if (!empty($FeaturedImageName)) {
            $save_parent_data['FeaturedImage'] = $FeaturedImageName;
        }
        $save_parent_data['Video'] = $post_data['video'];
        $save_parent_data['TotalPhoto'] = count($_FILES['images']);
        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

        //print_r($save_parent_data); die();
        $insert_id = $this->$parent->save($save_parent_data);
        if ($insert_id > 0) {

            $default_lang = getDefaultLanguageByDefault();
            $this->fileupload($insert_id,$sort);

            $save_child_data['Title'] = $post_data['manufacturer'].' '.$post_data['machine_type'];//$post_data['title'];
            $save_child_data['keywords'] = $post_data['keywords'];
            // for machine ads type
            if (empty($this->input->post('machine_ads_type'))) {
                $save_child_data['MachineType'] = $post_data['machine_type'];
            }
            $save_child_data['Description'] = $post_data['description'];
            $save_child_data[$this->data['TableKey']] = $insert_id;
            $save_child_data['SystemLanguageID'] = $default_lang->SystemLanguageID;
            $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
            $this->$child->save($save_child_data);
            $this->addAllLanguageData($child, $this->data['TableKey'], $insert_id, $extra_field_array);

            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            if ($this->session->userdata['admin']['RoleID'] == 4) {
                $success['url'] = 'details/' . string_replace(strtolower($save_child_data['Title']), '/\s+/', '-') . '-' . $insert_id . '/';
            } else {
                // for machine ads type
                if (isset($post_data['machine_ads_type']) && !empty($post_data['machine_ads_type']) && $post_data['machine_ads_type'] == 1) {

                    $success['url'] = 'cms/machine/otherads';
                } else {
                    $success['url'] = 'cms/' . $this->router->fetch_class();
                }
            }
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    public function imgSorting() {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];

        $result = $this->$parent->getMachineWithWhere($post_data);
        if ($result) {
            $res['status'] = 1;
            echo json_encode($res);
            exit;
        }
        $res['status'] = 0;
        echo json_encode($res);
        exit;
    }

    private function update() {

        // print_rm($this->input->post());

        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $post_data = $this->input->post();

        if (isset($post_data['IsActive']) && !isset($post_data['TermsAndCondition'])) {
            $errors['error'] = lang('terms_condition_machine_msg');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }

        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');

            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }

			$sort = 0;
            $FeaturedImageName = '';
            $featuredImageExist = $this->checkFeaturedImageExist($id);

            if ($featuredImageExist) {
                if (isset($_FILES['images']) && !empty($_FILES['images']) && $_FILES['images']['name'][$sort] != '') {
                    $UserDir='uploads/user/' .$this->session->userdata['admin']['UName'].'-00'. $this->session->userdata['admin']['UserID'] . '/images/'.date('Y').'/';
                    $FeaturedImageName = $this->uploadfeature_image('images', $UserDir, $sort);
                    if ($FeaturedImageName != '') {
                        $this->watermarkimage($FeaturedImageName);
                    }
                }
            }

            unset($post_data['form_type']);
            $save_parent_data = $save_child_data = array();
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {


                if (base64_decode($this->input->post('SystemLanguageID')) == 1) {
//                    $this->fileupload(base64_decode($post_data[$this->data['TableKey']]));

                    $file = $this->Site_images_model->getMultipleRows(array('FileID' => $id, 'ImageType' => 'MachineFile'), true);
                    
                    // if(empty($file)){
                        // $this->fileupload(base64_decode($post_data[$this->data['TableKey']]));
                    // }
        // print_rm($file);
                    if (isset($_FILES['feature_image']) && !empty($_FILES['feature_image']) && $_FILES['feature_image']['name'][0] != '') {
                        // echo "433"; die();
                        $FeaturedImageName = $this->uploadImage('feature_image', 'uploads/posts/');
                        if ($FeaturedImageName != '') {
                            $this->watermarkimage($FeaturedImageName);
                        }
                    }
                    // for machine ads type
                    if (isset($post_data['machine_ads_type']) && !empty($post_data['machine_ads_type']) && $post_data['machine_ads_type'] == 1) {
                        $save_parent_data['MachinAdsTypes'] = 1;
                        if (isset($post_data['ads_type']) && $post_data['ads_type'] == 1) {
                            $save_parent_data['AdsVisitor'] = 1;
                            $save_parent_data['AdsOffering'] = 0;
                        } else if (isset($post_data['ads_type']) && $post_data['ads_type'] == 2) {
                            $save_parent_data['AdsOffering'] = 1;
                            $save_parent_data['AdsVisitor'] = 0;
                        }
                    }


                    $save_parent_data['TermsAndCondition'] = (isset($post_data['TermsAndCondition']) ? 1 : 0 );
                    $save_parent_data['IsActive'] = (isset($post_data['IsActive']) && $post_data['IsActive'] != '0' ) ? 1 : 0;
					if($save_parent_data['IsActive'] != '0' )
					{
                        $save_parent_data['IsActive'] = 1;
                        $save_parent_data['PublishDate'] = date('Y-m-d');
                    }else{
                        $save_parent_data['IsActive'] = 0;
                    }
                    if (!empty($post_data['UserID'])) {
                        $save_parent_data['UserID'] = $post_data['UserID'];
                    }
                    $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                    $save_parent_data['CategoryID'] = $post_data['category'];
                    if ($post_data['category'] == '111') {
                        $save_parent_data['Service'] = $post_data['service'];
                    } else {
                        // for machine ads type
                        if (empty($this->input->post('machine_ads_type'))) {
                            $save_parent_data['Manufacturer'] = $post_data['manufacturer'];
                        }
                    }
                    $save_parent_data['Condition'] = $post_data['codition'];
                    //$save_parent_data['Model']          = $post_data['model'];
                    // for machine ads type
                    if (empty($this->input->post('machine_ads_type'))) {
                        $save_parent_data['ManufacturerYear'] = $post_data['manufacturer_year'];
                    }
                    $save_parent_data['Price'] = $post_data['price'];
                    //$save_parent_data['Vat']            = $post_data['vat'];
                    $save_parent_data['Location'] = $post_data['location'];
                    $save_parent_data['Lat'] = $post_data['Lat'];
                    $save_parent_data['Lng'] = $post_data['Lng'];
                    $save_parent_data['CountryShort'] = $post_data['CountryShort'];
                    $save_parent_data['ReferenceNumber'] = $post_data['reference_number'];
                    $save_parent_data['SerialNumber'] = $post_data['serial_number'];
                    if ((isset($_FILES['feature_image']) && !empty($_FILES['feature_image']) && $_FILES['feature_image']['name'][0] != '') ||
                        (isset($_FILES['images']) && !empty($_FILES['images']) && $_FILES['images']['name'][0] != '')) {
                        $save_parent_data['FeaturedImage'] = $FeaturedImageName;
                    }
                    $save_parent_data['Video'] = $post_data['video'];
                    if (!empty($file))
                        $save_parent_data['TotalPhoto'] = count($file);
                    $update_by = array();
                    $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);

                    $this->$parent->update($save_parent_data, $update_by);
                    $this->fileupload(base64_decode($post_data[$this->data['TableKey']]),$sort);
                }
                $save_child_data['keywords'] = $post_data['keywords'];
                $save_child_data['Title'] = $post_data['manufacturer'].' '.$post_data['machine_type'];//$post_data['title'];

                // for machine ads type
                if (empty($this->input->post('machine_ads_type'))) {
                    $save_child_data['MachineType'] = $post_data['machine_type'];
                }
                $save_child_data['Description'] = $post_data['description'];
                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);
            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    //$save_child_data['Title'] = $post_data['title'];
                    $save_child_data['Title'] = $post_data['manufacturer'].' '.$post_data['machine_type'];//$post_data['title'];
                    $save_child_data['MachineType'] = $post_data['machine_type'];
                    $save_child_data['Description'] = $post_data['description'];
                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


                    $save_child_data['autoTranslated'] = 0;
                    $this->$child->update($save_child_data, $update_by);
                } else {

                    //$save_child_data['Title'] = $post_data['title'];
                    $save_child_data['Title'] = $post_data['manufacturer'].' '.$post_data['machine_type'];//$post_data['title'];
                    $save_child_data['MachineType'] = $post_data['machine_type'];
                    $save_child_data['Description'] = $post_data['description'];
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                    $this->$child->save($save_child_data);
                }
            }

            $success['error'] = lang('update_successfully');
            $success['success'] = true;

            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
    }

    private function delete() {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);

        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    private function removeImage() {

        // if (!checkUserRightAccess(45, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
        //     $errors['error'] = lang('you_dont_have_its_access');
        //     $errors['success'] = false;

        //     echo json_encode($errors);
        //     exit;
        // }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $image_type = $this->input->post('image_type');
        $get_site_image = $this->input->post('image_path');
        if($image_type == 'o')
        {
            if ($get_site_image) {
                if (file_exists($this->input->post('image_path'))) {
                    unlink($this->input->post('image_path'));
                }
            }
            $image_deleted_by['SiteImageID'] = $this->input->post('image_id');
            $this->Site_images_model->delete($image_deleted_by);
        }
        else
        {
            if (file_exists($this->input->post('image_path'))) {
                unlink($this->input->post('image_path'));
            }
            $update_by['MachineID'] = $this->input->post('image_id');
            $update_data['FeaturedImage'] = '';
            $this->$parent->update($update_data, $update_by);
        }

        /*$deleted_by = array();
        $deleted_by['SiteImageID'] = $this->input->post('image_id');

        $this->Site_images_model->delete($deleted_by);*/

        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    private function fileupload($insert_id,$sort = 0)
    {
        if (isset($_FILES['images']) && !empty($_FILES['images'])) {
            $file = 'images';
            $UserDir='uploads/user/' .$this->session->userdata['admin']['UName'].'-00'. $this->session->userdata['admin']['UserID'] . '/images/'.date('Y').'/';
            $path =$UserDir;
            $key = $insert_id;
            $type = 'MachineFile';
            $this->uploadImage($file, $path, $key, $type, TRUE);
        }
        //else {

        //     $machineData = $this->checkFeaturedImageExist($insert_id);
        //     // $machineData = $this->Machine_model->get($insert_id, $as_array = true, $field_name = 'MachineID');
        //     if(empty($machineData)){
        //         $data['FileID'] = $insert_id;
        //         $data['ImageType'] = 'MachineFile';
        //         $data['ImageName'] = 'assets/backend/images/no_image.jpg';
        //         $this->Site_images_model->save($data);
        //     }
        // }
        //End
    }

    private function checkFeaturedImageExist($insert_id){
        $machineData = $this->Machine_model->get($insert_id, $as_array = true, $field_name = 'MachineID');
        $featuredImage = explode('/', $machineData['FeaturedImage']);
        return (end($featuredImage) == 'no_image.png')? true: false;
    }

    public function reporting($id) {
        $type = $this->uri->segment(3);
        if ($type == 'report') {
            $status = 3;
        } else if ($type == 'share') {
            $status = 2;
        } else {
            $status = 1;
            $type = 'send-inquiry';
        }
        $this->data['type'] = $type;
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/reporting';
        $this->data['results'] = $this->Inquiry_model->getMultipleRows(array('MachineID' => $id, 'Status' => $status));

        $this->load->view('backend/layouts/default', $this->data);
    }

    private function inquiry_delete() {

        $deleted_by = array();
        $deleted_by['InquiryID'] = $this->input->post('id');
        $this->Inquiry_model->delete($deleted_by);
        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    // machine ads page controll function
    public function machine_ads() {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanAdd') || ( $this->session->userdata['admin']['RoleID'] == 4 && !check_user_package_active())) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];


        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/machine_ads';
        $this->data['helpingUrl'] = 'wie-erstelle-ich-ein-inserat/';
        if ($this->session->userdata['admin']['RoleID'] == 1) {
            $this->data['dealers'] = $this->User_model->getAllDealers($this->language);
        }

        $this->load->view('backend/layouts/default', $this->data);
    }

    // machine ads edit function
    public function ads_edit($id = '') {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');
        $this->data['site_images'] = $this->Site_images_model->getMultipleRows(array('FileID' => $id, 'ImageType' => 'MachineFile'), true);

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        //print_rm($this->data['result']);


        $this->data['helpingUrl'] = 'wie-erstelle-ich-ein-inserat/';
        if ($this->session->userdata['admin']['RoleID'] == 1) {
            $this->data['dealers'] = $this->User_model->getAllDealers($this->language);
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/ads_edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function otherads() {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $userid = $this->session->userdata['admin']['UserID'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage_other_ads';
        if (in_array($this->session->userdata['admin']['RoleID'], array(1, 2))) {
            $this->data['check_package_active'] = true;
            $where = $this->data['purchase'] = '';
        } else {
            $this->data['check_package_active'] = check_user_package_active();

            $where = $this->data['Table'] . '.UserID=' . $this->session->userdata['admin']['UserID'];
            $this->data['purchase'] = $this->Purchase_model->getPurchaseData(array('b.Status IN (2,4,5)', 'a.UserID' => $this->session->userdata['admin']['UserID']), 'row');
            $this->data['package'] = ($this->data['purchase']) ? getPackage($this->data['purchase']->PackageID) : '';
        }
        $documentStatus = $this->Site_documents_model->getDocumentStatus($userid);
        if ($documentStatus && $this->session->userdata['admin']['RoleID'] == 4) {
            $this->data['errorMsg'] = lang("missing_documents");
        }
        $MachinAdsTypes = 1;
        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language, $where, $sort = 'ASC', $sort_field = 'SortOrder', $title_key = 'Title', $limit = false, $start = 0, $other_sort_field = false, $MachinAdsTypes);
        $this->data['InquiriesOfMachine'] = $this->$parent->getInquiriesOfMachine($this->session->userdata['admin']['UserID']);
        /* echo $this->db->last_query();
          exit(); */

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function import() {
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanAdd') || ( $this->session->userdata['admin']['RoleID'] == 4 && !check_user_package_active())) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/import';
        if ($this->session->userdata['admin']['RoleID'] == 1) {
            $this->data['dealers'] = $this->User_model->getAllDealers($this->language);
        }

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function import_data() {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $userid = $this->session->userdata['admin']['UserID'];
        if (!checkUserRightAccess(56, $this->session->userdata['admin']['UserID'], 'CanAdd') || ( $this->session->userdata['admin']['RoleID'] == 4 && !check_user_package_active())) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $default_lang = getDefaultLanguageByDefault();
        $this->load->library('PHPExcel');
        $this->excel = new PHPExcel();

        if ($_FILES["import_file"]["name"] == '') {

            $errors['error'] = 'please_upload_a_file';
            $errors['success'] = false;
            echo json_encode($errors);
            // exit;
        }

        $file_info = pathinfo($_FILES["import_file"]["name"]);
        $file_directory = "uploads/excelfile/";
        $new_file_name = date("d-m-Y ") . rand(000000, 999999) . "." . $file_info["extension"];

        if ($file_info["extension"] != 'xlsx' && $file_info["extension"] != 'xls' && $file_info["extension"] != 'csv') {
            $errors['error'] = 'invalid_file_format';
            $errors['success'] = false;
            echo json_encode($errors);
            // exit;
        }

        if (move_uploaded_file($_FILES["import_file"]["tmp_name"], $file_directory . $new_file_name)) {
            $file_type = PHPExcel_IOFactory::identify($file_directory . $new_file_name);
            $objReader = PHPExcel_IOFactory::createReader($file_type);
            $objPHPExcel = $objReader->load($file_directory . $new_file_name);
            $objPHPExcel->setActiveSheetIndex(0);
            $data = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

            // Feature Image Index
            $FI_index = '';
            $DB_Keys = array();
            $MainMachineData = array();
            foreach ($data as $key => $one) {
                $MachineData = array();
                $ImagesData = array();
                $IKMatch = false;
                if ($key == 1) {
                    foreach ($one as $key_inner => $one_inner) {
                        if ($one_inner == 'Image1') {
                            $FI_index = $key_inner;
                            $IKMatch = true;
                        }
                        if (!$IKMatch) {
                            $DB_Keys[$key_inner] = $one_inner;
                        }
                    }
                } else {
                    foreach ($one as $key_inner => $one_inner) {
                        if ($key_inner == $FI_index) {
                            $IKMatch = true;
                        }

                        if ($IKMatch) {
                            $ImagesData[] = $one_inner;
                        } else {
                            $MachineData[$DB_Keys[$key_inner]] = $one_inner;
                        }
                    }

                    if ($MachineData) {

                        $CategoryID = 10;

                        $save_parent_data['SortOrder'] = 0;
                        $save_parent_data['TermsAndCondition'] = 0;
                        $save_parent_data['IsActive'] = 1;
                        $save_parent_data['UserID'] = $this->session->userdata['admin']['UserID'];
                        $save_parent_data['CategoryID'] = $CategoryID;
                        $save_parent_data['Service'] = $MachineData['Service'];
                        $save_parent_data['Manufacturer'] = $MachineData['Manufacturer'];

                        $AllConditions = array('New', 'As good as new (ex-display)', 'Partially reconditioned (used)', 'Reconditioned (used)', 'Good condition (used)', 'Excellent (used)', 'Ready for operation (used)', 'Unexamined (used)', 'Repair required (used)');
                        $Condition = 0;
                        foreach ($AllConditions as $ConditionKey => $ConditionVal) {
                            if ($ConditionVal = $MachineData['Condition']) {
                                $Condition = $ConditionKey;
                            }
                        }

                        $save_parent_data['Condition'] = $Condition;
                        $save_parent_data['ManufacturerYear'] = $MachineData['ManufacturerYear'];

                        $save_parent_data['Price'] = $MachineData['Price'];
                        $save_parent_data['Location'] = $MachineData['Location'];
                        $save_parent_data['ReferenceNumber'] = $MachineData['ReferenceNumber'];
                        $save_parent_data['SerialNumber'] = $MachineData['SerialNumber'];
                        if (!empty($ImagesData[0])) {
                            $save_parent_data['FeaturedImage'] = $ImagesData[0];
                        }
                        $save_parent_data['Video'] = $MachineData['Video'];
                        $save_parent_data['TotalPhoto'] = count($ImagesData);
                        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                        $save_parent_data['IsImport'] = 1;
                        // echo '<pre>';print_r($save_parent_data);
                        $insert_id = $this->$parent->save($save_parent_data);
                        if ($insert_id) {
                            $save_child_data['Title'] = $MachineData['Title_de'];
                            $save_child_data['keywords'] = $MachineData['keywords_de'];
                            $save_child_data['MachineType'] = $MachineData['MachineType_de'];
                            $save_child_data['Description'] = $MachineData['Description_de'];
                            $save_child_data[$this->data['TableKey']] = $insert_id;
                            $save_child_data['SystemLanguageID'] = $default_lang->SystemLanguageID;
                            $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                            $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                            $this->$child->save($save_child_data);

                            $extra_field_array['Title'] = $MachineData['Title_en'];
                            $extra_field_array['keywords'] = $MachineData['keywords_en'];
                            $extra_field_array['MachineType'] = $MachineData['MachineType_en'];
                            $extra_field_array['Description'] = $MachineData['Description_en'];

                            $this->addAllLanguageData($child, $this->data['TableKey'], $insert_id, $extra_field_array);
                            unset($ImagesData[0]);
                            $ImgData = array();
                            foreach ($ImagesData as $OneImg) {
                                $ImgData['FileID'] = $insert_id;
                                $ImgData['ImageType'] = 'MachineFile';
                                $ImgData['ImageName'] = $OneImg;
                                $this->Site_images_model->save($ImgData);
                            }
                        }
                    }
                }
            }
            unlink('uploads/excelfile/' . $new_file_name);
            $success['error'] = false;
            $success['success'] = 'save_successfully';
            $success['reload'] = true;
            echo json_encode($success);
            // exit;
        } else {
            $errors['error'] = 'some_thing_went_wrong';
            $errors['success'] = false;
            echo json_encode($errors);
            // exit;
        }
    }

    public  function getLists()
    {
        $data = $row = array();
        $memData = array();

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';
        if (in_array($this->session->userdata['admin']['RoleID'], array(1, 2))) {
            $this->data['check_package_active'] = true;
            $where = $this->data['purchase'] = '';
        } else {
            $this->data['check_package_active'] = check_user_package_active();

            $where = $this->data['Table'] . '.UserID=' . $this->session->userdata['admin']['UserID'];
        }

        try{
            $memData = $this->Machine_model->getRows($_POST, $where, $this->language);
        }catch (Exception $exception){
            print_r($exception);
            die('--------excepiont');
        }

        $i = $_POST['start'];
        if(!empty($memData))
        {
            foreach($memData as $value){
                $baseUrl = base_url('detail/'.str_replace(" ","-", $value->Title).'-').$value->MachineID;
                $i++;
                if (file_exists($value->FeaturedImage)) {
                    $img_url = base_url() . $value->FeaturedImage;
                } else if ($value->IsImport == 1) {
                    $img_url = $value->FeaturedImage;
                } else {

                    $img_url = base_url('uploads/no_image.png');
//                    if ($this->session->userdata['lang'] == "en") {
//                    } else {
//                        $img_url = base_url('uploads/Maschinenpilot-600.png');
//                    }
                }
                $CreatedAt = dateformat($value->CreatedAt,'de');
                $PublishDate = dateformat($value->PublishDate,'de');
                $status = ($value->IsActive)?lang('yes') : lang('no');
                $data[] = array(
                    $img_url,
                    $value->Title,
                    $value->UsersTextTitle,
                    $value->MachineID,
                    $PublishDate,
                    $CreatedAt,
                    $value->IsActive,
                    $value->MachineID,
                    $value->UsersTextTitle,
                    $value->UserID,
                    $baseUrl
                );
            }
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Machine_model->countFiltered($_POST),
            "recordsFiltered" => $this->Machine_model->countFiltered($_POST),
            "data" => $data,
        );

        // Output to JSON format
        echo json_encode($output);
    }
}
