<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends Base_Controller
{
    public $data = array();
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = ucfirst($this->router->fetch_class()).'s';
        $this->load->Model([
            $this->model.'_model',
            $this->model.'_text_model',
            'Role_model',
            'User_model'
        ]);
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['language']      = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model']   = $this->model.'_model';
        $this->data['Child_model']    = $this->model.'_text_model';
        $this->data['TableKey'] = 'VouchersID';
        $this->load->helper('inflector');
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->validate();
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
        }
    }

    private function validate() {
        $errors = array();

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $form_type = $this->input->post('form_type');
        $this->form_validation->set_rules('Title', lang('title'), 'required|trim');
        $is_unique =  '';
        if($form_type == 'save'){
            if($this->input->post('VoucherCode') != $original_value) {
                $is_unique =  '|is_unique[voucherses.VoucherCode]';
            }
            $this->form_validation->set_rules('VoucherCode', lang('voucher_code'), 'required|trim'.$is_unique);
        }
        $this->form_validation->set_rules('DiscountType', lang('type'), 'required');
        $this->form_validation->set_rules('VoucherValue', lang('voucher_value'), 'required|greater_than[0]');
        $this->form_validation->set_rules('ExpiryDate', lang('expiry_date'), 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }

    public function index()
    {
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getAll();
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/manage';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function addVoucher()
    {
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/add';
        $this->load->view('backend/layouts/default',$this->data);
    }

    public function editVoucher($id = '')
    {
        if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){
            $this->session->set_flashdata('message',lang('you_dont_have_its_access'));
            redirect(base_url('cms/'.$this->router->fetch_class()).'s');
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getVoucherById($id);
        if(!$this->data['result']){
            $this->session->set_flashdata('message',lang('some_thing_went_wrong'));
            redirect(base_url('cms/'.$this->router->fetch_class()).'s');
        }
        $this->data['roles'] = $this->Role_model->getAllJoinedData(false,'RoleID',$this->language,'roles.IsActive = 1');
        if(!$this->data['roles']){
            $this->session->set_flashdata('message',lang('please_add_role_first'));
            redirect(base_url('cms/'.$this->router->fetch_class()).'s');
        }
        $this->data['view'] = 'backend/'.$this->data['ControllerName'].'/edit';
        $this->load->view('backend/layouts/default',$this->data);
    }

    private function save()
    {
        $post_data = $this->input->post();
        $parent          = $this->data['Parent_model'];
        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);
        $sort = 0;
        if(!empty($getSortValue))
        {
            $sort = $getSortValue['SortOrder'] + 1;
        }

        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['Title'] = $post_data['Title'];
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['VoucherCode'] = $post_data['VoucherCode'];
        $save_parent_data['DiscountType'] = $post_data['DiscountType'];
        $save_parent_data['VoucherValue'] = $post_data['VoucherValue'];
        $save_parent_data['ExpiryDate'] = date('Y-m-d h:i:s', strtotime($post_data['ExpiryDate']));
        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt']    = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
        $insert_id = $this->$parent->save($save_parent_data);

        if($insert_id > 0)
        {
            $success['error']   = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/'.$this->router->fetch_class().'s';
            echo json_encode($success);
            exit;
        }else
        {
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update()
    {
        $post_data = $this->input->post();
        $ExpiryDate = date('Y-m-d h:i:s', strtotime($post_data['ExpiryDate']));

        $parent = $this->data['Parent_model'];
        $save_parent_data['VouchersID'] = $post_data['VouchersID'];
        $save_parent_data['Title'] = $post_data['Title'];
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['DiscountType'] = $post_data['DiscountType'];
        $save_parent_data['VoucherValue'] = $post_data['VoucherValue'];
        $save_parent_data['ExpiryDate'] = $ExpiryDate;

        $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

        $update = $this->$parent->updateVoucher($save_parent_data);

        if($update)
        {
            $success['error']   = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            $success['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($success);
            exit;
        }else
        {
            $errors['error'] =  lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function delete()
    {
        if(!checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete')){
            $errors['error'] =  lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/'.$this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $parent                             = $this->data['Parent_model'];
        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');
        $this->$parent->delete($deleted_by);
        $success['error']   = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }

    public function validateVoucherCode()
    {
//        $this->data['site_setting']->PaypalFee

        $parent = $this->data['Parent_model'];
        $voucherCode = $this->input->post('code');
        $total = $this->input->post('total');
        $result = $this->$parent->getVoucherCodeDetail($voucherCode);
        $status = 'not';
        $message = lang('coupon_code_expire');
        $expiry = date('Y-m-d', strtotime($result['ExpiryDate']));
        $today = date('Y-m-d');
        if($expiry < $today){
            $message = lang('coupon_code_expire');
            $status = 'expire';
        }
        if(isset($result['VouchersID']) && $status === 'not')
        {
            $discountValue = $result['VoucherValue'];
            if($result['DiscountType'] == 'percentage')
            {
                $dis_total = ($total/100)*$discountValue;
                $this->session->set_userdata('coupon_discount_total', $dis_total);
                $total = $total - $dis_total;
            }
            if($result['DiscountType'] == 'flat')
            {
                $this->session->set_userdata('coupon_discount_total', $discountValue);
                $total = $total-$discountValue;
            }
            $status = 'yes';
            $message = lang('code_successfully_availed');
        }
        $return = [
          'status' => $status,
          'message' => $message,
          'g_total' => currency($total),
          'total' => ($total+$this->data['site_setting']->PaypalFee)
        ];
        echo json_encode($return);
        exit;
    }

}