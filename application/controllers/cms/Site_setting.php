<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_setting extends Base_Controller {

    public $data = array();

    public function __construct() {
        parent::__construct();
        checkAdminSession();
        $this->load->model(['Site_setting_model','Menu_model','Category_model']);
        
        $this->data['language'] = $this->language;
			$this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 1');
		        $this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false,'MenuID',false,'menus.MenuPosition = 2');
		if($this->session->userdata['admin']['UserID']){
				$this->data['user_wishlist'] = getValue('wishlist',array('UserID' => $this->session->userdata['admin']['UserID']),'num_rows'); 
				}else{
				$this->data['user_wishlist'] = 0;	
				}
    }

    public function index() {
        $this->edit(1);
    }
    
    public function edit($SiteSettingID) {

        $this->data['result'] = $this->Site_setting_model->get($SiteSettingID, false, 'SiteSettingID');
        $this->data['categories'] = $this->Category_model->getall($this->language);
        $this->data['home_categories'] = $this->Category_model->selectedhomecategory();

        if (!$this->data['result']) {
            redirect(base_url('cms/user'));
        }
        $this->data['view'] = 'backend/site_setting/edit';

        $this->data['SiteSettingID'] = $SiteSettingID;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {

            case 'update';
                $this->validate();
                $this->update();
                break;
        }
    }

    private function validate() {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('SiteName', lang('SiteName'), 'required');
		$this->form_validation->set_rules('PhoneNumber', lang('ph'), 'required');
        $this->form_validation->set_rules('FromEmail', lang('from').' '.lang('email'), 'required|valid_email');
        $this->form_validation->set_rules('FromName', lang('from').' '.lang('name'));
		$this->form_validation->set_rules('OnwerName', lang('onwer'),'required');
		$this->form_validation->set_rules('PaypalUrl', lang('paypal-url'), 'required');
		$this->form_validation->set_rules('PaypalEmail', lang('paypal-email'), 'required|valid_email');
		$this->form_validation->set_rules('PaypalFee', lang('paypal-fee'), 'required');
        $this->form_validation->set_rules('PaypalActive', lang('paypal-active'), 'required');
        $this->form_validation->set_rules('BankName', lang('bank-name'), 'required');
        $this->form_validation->set_rules('IBAN', lang('iban'), 'required');
		$this->form_validation->set_rules('BIC', lang('bic'), 'required');
		$this->form_validation->set_rules('Currency', lang('currency'), 'required');
		$this->form_validation->set_rules('CurrencyPosition', lang('currency-position'), 'required');
		$this->form_validation->set_rules('ThousandSeparator', lang('thousand-separator'), 'required');
		$this->form_validation->set_rules('DecimalSeparator', lang('decimal-separator'), 'required');
        $this->form_validation->set_rules('NumberDecimals', lang('number-decimals'), 'required');
		$this->form_validation->set_rules('FreePackageRelaxation', lang('free-package-relax'), 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function update() {
        $post_data = $this->input->post();
        $display_categories = $post_data['display_categories'];
        unset($post_data['display_categories']);
        $this->data['result'] = $this->Site_setting_model->get(1, false, 'SiteSettingID');
        
        $post_data['UpdatedAt'] = date('Y-m-d H:i:s');

        unset($post_data['form_type']);
        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
			$image  = $this->uploadImage("Image", "uploads/");
			if($image){
            unlink($this->data['result']->SiteImage);
            $post_data['SiteImage'] = $image;
			}else{
			
        $success['error'] = lang('invalid-format');
        $success['success'] = 'false';
        echo json_encode($success);
        exit;	
			}
        }

        if (isset($_FILES['BannerImage']["name"][0]) && $_FILES['BannerImage']["name"][0] != '') {
            $post_data['BannerImage'] = $this->uploadImage("BannerImage", "uploads/");
        }
        if (isset($_FILES['SingleAdImage']["name"][0]) && $_FILES['SingleAdImage']["name"][0] != '') {
            $post_data['SingleAdImage'] = $this->uploadImage("SingleAdImage", "uploads/headeradbanner/");
        }
        $update_by = array();
        $update_by['SiteSettingID'] = $post_data['SiteSettingID'];
        unset($post_data['form_type']);
        $this->Site_setting_model->update($post_data, $update_by);

        $this->data['categories'] = $this->Category_model->DisplayHomeCategories($display_categories);

        $success['error'] = false;
        $success['success'] = 'Updated Successfully';
        $success['redirect'] = true;
        $success['url'] = 'cms/Site_setting';
        echo json_encode($success);
        exit;
    }

    
}
