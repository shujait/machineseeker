<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Base_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->data['site_setting'] = $this->getSiteSetting();

    }

    public function index()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/machine'));

        }

        redirect(base_url('login'));
    }

    //User Login
    public function login()
    {
        if ($this->session->userdata('admin')) {
            redirect(base_url('cms/machine'));

        }
		redirect(base_url('login'));
        //$this->data['view'] = 'backend/login';
        //$this->load->view('backend/login', $this->data);

    }


    public function checkLogin()
    {
        //status 1 for super password
        $SuperStatus = $_GET['super'];

        $data = array();
        $post_data = $this->input->post();
        $this->loginValidation();
        $checkUser = $this->checkUser($post_data,$SuperStatus);

        if ($checkUser != true) {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = lang('email_or_password_incorrect');
            echo json_encode($data);
            exit();
        } else {
            $data = array();
            $data['success'] = lang('login_successfully');
            $data['error'] = false;
            $data['redirect'] = true;
            $data['url'] = 'cms/machine';
            echo json_encode($data);
            exit();
        }
    }
	
	 public function forgot_password()
    {
        $data = array();
		$pass = $this->getUniqueCode(8);
		$reset_pass =  md5($pass);
        $email = $this->input->post('email');
        $this->FotgotPasswordValidation();
        $checkUser = get_user_info(array('a.Email' => $email));

        if ($checkUser) {
			$this->User_model->update(array('Password' => $reset_pass), array('UserID' => $checkUser->UserID));
			$tpl = get_email_tpl(array('TplList' => 2));
			if($tpl){
    			$search = array(mail_tpl_shortcode('Name'),mail_tpl_shortcode('Email'),mail_tpl_shortcode('Link'),mail_tpl_shortcode('Site Name'),mail_tpl_shortcode('Site URL'));	
    			$replace = array($checkUser->UName.' '.$checkUser->Title,$checkUser->Email,'<a href="'.base_url('reset-password/'.base64_encode($checkUser->UserID)).'" target="_blank">'.lang('click_here').'</a>', $this->data['site_setting']->SiteName,base_url());	
    			$body = str_replace($search,$replace, $tpl->Description);
    			$data['from'] = $this->data['site_setting']->FromEmail;
    			$data['from_name'] = (isset($this->data['site_setting']->FromName) and !empty($this->data['site_setting']->FromName))?$this->data['site_setting']->FromName:$this->data['site_setting']->SiteName;	
    			$data['to']   = $checkUser->Email;
    			$data['subject']   = $tpl->Subject;		
    			$data['body'] = $body;
    			sendEmail($data);
			}
            $data['success'] = lang('reset-password-successfully');
            $data['error'] = false;
            $data['redirect'] = false;
            echo json_encode($data);
            exit();
        } else {
            $data['success'] = 'false';
            $data['error'] = lang('email_invalid');
            echo json_encode($data);
            exit();
        }
    }

    private function loginValidation()
    {


        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('Email', lang('email'), 'required');
        $this->form_validation->set_rules('Password', 'Password', 'required');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }
	
	  private function FotgotPasswordValidation()
    {

        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('email', lang('email'), 'required|valid_email');


        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }

    }

    private function checkUser($post_data, $SuperStatus = null)
    {
        if($SuperStatus == 1){
            $post_data['SuperPassword'] = $post_data['Password'];
            unset($post_data['Password']);
        }else{
            $post_data['Password'] = md5($post_data['Password']);
        }

        $user = $this->User_model->getUserData($post_data, false);

        if (!empty($user)) {
            if($user['IsActive'] == 0)
            {
                $data = array();
                $data['success'] = 'false';
                $data['error'] = lang('account-deactivate');
                echo json_encode($data);
                exit();
            }else{
                if($user['RoleID'] > 2 && $user['IsVerified'] == 0){
                    $data = array();
                    $data['success'] = 'false';
                    $data['error'] = lang('account-not-verified');
                    echo json_encode($data);
                    exit();
                }
                $this->User_model->update(array('LastLogin' => date('Y-m-d')), array('UserID' => $user['UserID']));
            }
            if(strtotime(date('d.m.Y')) < strtotime($user['TillActivated'])){
                $msg=lang('account_has_been_deactivated')." ".date('d.m.Y',strtotime($user['TillActivated']) ).'.';
                $this->session->set_userdata('DeactivatedNotification', $msg  );
             }  
             if(!$user['IsPaid'] && $user['RoleID']==4){
                $msg=lang('un_paid_invoice_notification');
                $this->session->set_userdata('InvoiceNotification', $msg);
             }
            $this->session->set_userdata('lang',$user['Lang']);
            $this->session->set_userdata('admin', $user);
            //$this->updateUserLoginStatus();
            return true;
        } else {
            $data = array();
            $data['success'] = 'false';
            $data['error'] = lang('account-not-found');
            echo json_encode($data);
            exit();
        }

    }


    //Logout
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $user = $this->session->userdata('admin');
        $this->session->unset_userdata('admin');
        redirect($this->config->item('base_url') . 'cms/account/login');

    }
	  /**
	   * Users::getUniqueCode()
	   */
	   
	  public function getUniqueCode($length = "")
	  {
		  $code = md5(uniqid(rand(), true));
		  if ($length != "") {
			  return substr($code, 0, $length);
		  } else
			  return $code;
	  }
}