<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Base_Controller {

    public $data = array();

    public function __construct() {

        parent::__construct();
        checkAdminSession();

        $this->load->Model(['Category_model', 'Category_text_model', 'Site_images_model', 'Menu_model']);

        $this->data['language'] = $this->language;
        $this->data['language2'] = $this->language;
        $this->data['ControllerName'] = $this->router->fetch_class();
        $this->data['Parent_model'] = ucfirst($this->router->fetch_class()) . '_model';
        $this->data['Child_model'] = ucfirst($this->router->fetch_class()) . '_text_model';
        $this->data['TableKey'] = 'CategoryID';
        $this->data['Table'] = 'categories';
        $this->data['header'] = $this->Menu_model->getActiveAllJoinedData(false, 'MenuID', false, 'menus.MenuPosition = 1');

        $this->data['footer'] = $this->Menu_model->getActiveAllJoinedData(false, 'MenuID', false, 'menus.MenuPosition = 2');
        if ($this->session->userdata['admin']['UserID']) {
            $this->data['user_wishlist'] = getValue('wishlist', array('UserID' => $this->session->userdata['admin']['UserID']), 'num_rows');
        } else {
            $this->data['user_wishlist'] = 0;
        }
    }

    public function index()
    {
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage';

        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language, 'categories.ParentID = 0',$sort = 'ASC',$sort_field = 'SortOrder',$title_key = 'Title',$limit = false,$start = 0,$other_sort_field = false,$MachinAdsTypes=false,$CategoryType=-1);
        //echo $this->db->last_query();exit;
        // print_rm($this->data['results']);exit;

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function sorting() {
        $items = $this->input->post('item');
//          if(!checkUserRightAccess(50,$this->session->userdata['admin']['UserID'],'CanEdit')){
//                $errors['error'] =  lang('you_dont_have_its_access');
//                $errors['success'] = false;
//                $errors['redirect'] = true;
//                $errors['url'] = 'cms/'.$this->router->fetch_class();
//                echo json_encode($errors);
//                exit;
//            }
        $parent = $this->data['Parent_model'];
        $i = 0;
        if (!empty($items))
            foreach ($items as $item) {
                $update_by['CategoryID'] = $item;
                $save_parent_data['SortOrder'] = $i;
                $this->$parent->update($save_parent_data, $update_by);
                $i++;
            }
    }

    public function add() {
        if (!checkUserRightAccess(50, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add';

        $this->load->view('backend/layouts/default', $this->data);
    }

    public function edit($id = '') {
        if (!checkUserRightAccess(50, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');
        //	$this->data['file'] = getValue('site_images',array('ImageType' => 'Category', 'FileID' => $id));

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function action() {
        $form_type = $this->input->post('form_type');
        switch ($form_type) {
            case 'save':
                $this->validate();
                $this->save();
                break;
            case 'update':
                $this->validate();
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
        }
    }

    private function validate() {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $form_type = $this->input->post('form_type');
        $title = ($form_type == 'save') ? '|is_unique[' . $this->data['Table'] . '_text.Title]' : '';
        $this->form_validation->set_rules('Title', lang('title'), 'required');
        if (base64_decode($this->input->post('IsDefault')) == 1) {
            $this->form_validation->set_rules('ParentID', lang('parent_category'), 'required|trim');
        }

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function save() {

        if (!checkUserRightAccess(50, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }

        $post_data = $this->input->post();
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $save_parent_data = $arr = $save_child_data = array();

        $getSortValue = $this->$parent->getLastRow($this->data['TableKey']);

        $sort = 0;
        if (!empty($getSortValue)) {

            $sort = $getSortValue['SortOrder'] + 1;
        }
        $category_type=0;
        // for other category case
        if(!empty($post_data['category_type']) &&($post_data['category_type']==1)){
            $category_type=$post_data['category_type'];
        }
        $slug = special_characterDe($post_data['Title']);
        $slug = slug($slug);

        $save_parent_data['CategoryType'] = $category_type;
        $save_parent_data['Slug'] = $slug;
        $save_parent_data['SortOrder'] = $sort;
        $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0 );
        $save_parent_data['ParentID'] = $post_data['ParentID'];
        $save_parent_data['IsShow'] = (isset($post_data['IsShow']) && $save_parent_data['IsActive'] == 1 ? 1 : 0 );
//        $save_parent_data['ShortTitle'] = $post_data['ShortTitle'];

        if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
            $save_parent_data['Image'] = $this->uploadImage("Image", "uploads/category/");
        }
        if (isset($_FILES['AdImage']["name"][0]) && $_FILES['AdImage']["name"][0] != '') {
            $save_parent_data['AdImage'] = $this->uploadImage("AdImage", "uploads/category/Ad/");
        }

        $save_parent_data['CreatedAt'] = $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
        $save_parent_data['CreatedBy'] = $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];


        $insert_id = $this->$parent->save($save_parent_data);
        //$HomeCategorySaved = $this->Category_model->SaveHomeCategory($insert_id);

        if ($insert_id > 0) {

            $default_lang = getDefaultLanguageByDefault();
            $arr = array('seo_title' => isset($post_data['seo_title']) ? $post_data['seo_title'] : '', 'keywords' => isset($post_data['keywords']) ? $post_data['keywords'] : '', 'meta_desc' => isset($post_data['meta_desc']) ? $post_data['meta_desc'] : '', 'crawl_after_day' => isset($post_data['crawl_after_day']) ? $post_data['crawl_after_day'] : '', 'follow' => isset($post_data['follow']) ? $post_data['follow'] : '', 'index' => isset($post_data['index']) ? $post_data['index'] : '');

            $save_child_data['Title'] = $post_data['Title'];
            $save_child_data['ShortTitle'] = $post_data['ShortTitle'];
            $save_child_data['Content'] = $post_data['content'];
            $save_child_data['Seo'] = (isset($arr) and ! empty($arr)) ? json_encode($arr) : '';
            $save_child_data[$this->data['TableKey']] = $insert_id;
            $save_child_data['SystemLanguageID'] = $default_lang->SystemLanguageID;
            $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
            $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
            $this->$child->save($save_child_data);

            //$extra_field_array = array();
            // $extra_field_array['ParentID'] = $post_data['ParentID'];

            $this->addAllLanguageData($child, $this->data['TableKey'], $insert_id);

            $success['error'] = false;
            $success['success'] = lang('save_successfully');
            $success['redirect'] = true;
            // for other category case
            if(!empty($post_data['category_type']) &&($post_data['category_type']==1)){

                $success['url'] = 'cms/' . $this->router->fetch_class() . '/othercategory';
            }else{   
                $success['url'] = 'cms/' . $this->router->fetch_class();
            }
            //$success['url'] = 'cms/' . $this->router->fetch_class() . '/edit/' . $insert_id;
            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }

    private function update() {

        $save_parent_data = $arr = $save_child_data = array();
        $post_data = $this->input->post();
        $arr = array('seo_title' => isset($post_data['seo_title']) ? $post_data['seo_title'] : '', 'keywords' => isset($post_data['keywords']) ? $post_data['keywords'] : '', 'meta_desc' => isset($post_data['meta_desc']) ? $post_data['meta_desc'] : '', 'crawl_after_day' => isset($post_data['crawl_after_day']) ? $post_data['crawl_after_day'] : '', 'follow' => isset($post_data['follow']) ? $post_data['follow'] : '', 'index' => isset($post_data['index']) ? $post_data['index'] : '');

        if (!checkUserRightAccess(50, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        if (isset($post_data[$this->data['TableKey']])) {
            $id = base64_decode($post_data[$this->data['TableKey']]);
            $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');


            if (!$this->data['result']) {
                $errors['error'] = lang('some_thing_went_wrong');
                $errors['success'] = false;
                $errors['redirect'] = true;
                $errors['url'] = 'cms/' . $this->router->fetch_class();
                echo json_encode($errors);
                exit;
            }

            unset($post_data['form_type']);
            if (isset($post_data['IsDefault']) && $post_data['IsDefault'] == 1) {

                $save_parent_data['Slug'] = $post_data['slug'];
                $save_parent_data['IsActive'] = (isset($post_data['IsActive']) ? 1 : 0 );
                $save_parent_data['IsShow'] = (isset($post_data['IsShow']) && $save_parent_data['IsActive'] == 1 ? 1 : 0 );
                $CategoryID = base64_decode($post_data[$this->data['TableKey']]);
                //if($save_parent_data['IsActive'] == 0){
                    //$display_off = $this->Category_model->UpdateHomeCategories($CategoryID);
                //}
                $save_parent_data['ParentID'] = $post_data['ParentID'];
                $save_parent_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_parent_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                if (isset($_FILES['Image']["name"][0]) && $_FILES['Image']["name"][0] != '') {
                    $save_parent_data['Image'] = $this->uploadImage("Image", "uploads/category/");
                }
                if (isset($_FILES['AdImage']["name"][0]) && $_FILES['AdImage']["name"][0] != '') {
                    $save_parent_data['AdImage'] = $this->uploadImage("AdImage", "uploads/category/ad/");
                }
                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                // if($save_parent_data['IsShow'] == 0){
                //     $this->$parent->update(['IsShow' => 0])->where("ParentID",$update_by);
                // }
                $this->$parent->update($save_parent_data, $update_by);

                $save_child_data['Title'] = $post_data['Title'];
                $save_child_data['ShortTitle'] = $post_data['ShortTitle'];
                $save_child_data['Content'] = $post_data['content'];
                $save_child_data['Seo'] = (isset($arr) and ! empty($arr)) ? json_encode($arr) : '';
                $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $this->$child->update($save_child_data, $update_by);
            } else {

                $update_by = array();
                $update_by[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                $update_by['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);

                $get_data = $this->$child->getWithMultipleFields($update_by);

                if ($get_data) {

                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Content'] = $post_data['content'];
                    $save_child_data['Seo'] = (isset($arr) and ! empty($arr)) ? json_encode($arr) : '';
                    $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                    $this->$child->update($save_child_data, $update_by);
                } else {
                    $save_child_data['Title'] = $post_data['Title'];
                    $save_child_data['Content'] = $post_data['content'];
                    $save_child_data['Seo'] = (isset($arr) and ! empty($arr)) ? json_encode($arr) : '';
                    $save_child_data[$this->data['TableKey']] = base64_decode($post_data[$this->data['TableKey']]);
                    $save_child_data['SystemLanguageID'] = base64_decode($post_data['SystemLanguageID']);
                    $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                    $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];

                    $this->$child->save($save_child_data);
                }
            }

            $success['error'] = false;
            $success['success'] = lang('update_successfully');

            echo json_encode($success);
            exit;
        } else {
            $errors['error'] = lang('some_thing_went_wrong');
            $errors['success'] = false;
            $errors['redirect'] = true;
            $errors['url'] = 'cms/' . $this->router->fetch_class();
            echo json_encode($errors);
            exit;
        }
    }

    public function getSubCategory() {
        $CategoryID = $this->input->post('CategoryID');
        $CategoryType = $this->input->post('CategoryType');
        if ($CategoryID == '') {
            $success['error'] = true;
            $success['success'] = false;
            $success['option'] = '';
            echo json_encode($success);
            exit();
        }

        $categoryData = getSubCategoryBy(array('a.ParentID' => $CategoryID));
        $select = '';
        if ($categoryData) {
            $select = '<div class="col-md-12">
                        <div class="form-group label-floating">
                            <select class="select2 form-control genTitle" data-type="' . ($CategoryType == 'category' ? 'subcategory' : 'subsubcategory') . '" data-style="select-with-transition" name="category" id="category" required>
                                <option value="">' . lang('select_sub_category') . '</option>';
            foreach ($categoryData as $key => $value) {
                $select .= '<option value="' . $value->CategoryID . '">' . $value->Title . '</option>';
            }
            $select .= '</select></div></div>';

            $success['error'] = false;
            $success['success'] = true;
            $success['option'] = $select;
        } else {
            $success['error'] = true;
            $success['success'] = false;
            $success['option'] = $select;
        }
        echo json_encode($success);
        exit();
    }

    public function delete() {

        if (!checkUserRightAccess(50, $this->session->userdata['admin']['UserID'], 'CanDelete')) {
            $errors['error'] = lang('you_dont_have_its_access');
            $errors['success'] = false;

            echo json_encode($errors);
            exit;
        }
        //$CategoryID = $this->input->post('id');
        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];

        $deleted_by = array();
        $deleted_by[$this->data['TableKey']] = $this->input->post('id');

        //$this->Category_model->DeleteHomeCategory($CategoryID);
        $this->$child->delete($deleted_by);
        $this->$parent->delete($deleted_by);

        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }
    public function othercategory() {

        $parent = $this->data['Parent_model'];
        $child = $this->data['Child_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/manage_other_category';

        $this->data['results'] = $this->$parent->getAllJoinedData(false, $this->data['TableKey'], $this->language, 'categories.ParentID = 0',$sort = 'ASC',$sort_field = 'SortOrder',$title_key = 'Title',$limit = false,$start = 0,$other_sort_field = false,$MachinAdsTypes=false,$CategoryType=1);
        //echo $this->db->last_query();exit;
        // print_rm($this->data['results']);exit;

        $this->load->view('backend/layouts/default', $this->data);
    }
    public function add_other_category() {
        if (!checkUserRightAccess(50, $this->session->userdata['admin']['UserID'], 'CanAdd')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/add_other_category';



        $this->load->view('backend/layouts/default', $this->data);
    }
    public function edit_other_category($id = '') {
        if (!checkUserRightAccess(50, $this->session->userdata['admin']['UserID'], 'CanEdit')) {
            $this->session->set_flashdata('message', lang('you_dont_have_its_access'));
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }
        $parent = $this->data['Parent_model'];
        $this->data['result'] = $this->$parent->getJoinedData(false, $this->data['TableKey'], $this->data['Table'] . '.' . $this->data['TableKey'] . '=' . $id, 'DESC', '');
        //  $this->data['file'] = getValue('site_images',array('ImageType' => 'Category', 'FileID' => $id));

        if (!$this->data['result']) {
            redirect(base_url('cms/' . $this->router->fetch_class()));
        }

        $this->data['view'] = 'backend/' . $this->data['ControllerName'] . '/edit_other_category';
        $this->data[$this->data['TableKey']] = $id;
        $this->load->view('backend/layouts/default', $this->data);
    }

    public function isshow($CategoryID,$isshowstatus){
       
        $this->db->query("Update categories set IsShow='". $isshowstatus."' where CategoryID='".$CategoryID."'");

        $this->index();
    }
}
