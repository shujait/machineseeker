<style type="text/css">
    .select2-container--default .select2-selection--single {
        display: block;
        width: 100%;
        height: calc(2.25rem + 2px);
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        -o-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    }
</style>
<?php 
    $ParentID = 0;
    $ChildData = '';
    $SubChildData = '';
    $rec='';
    $active_category=" (`categories`.`CategoryID` IN (SELECT `CategoryID` FROM `machines` where `machines`.`IsActive`=1)
                    OR (`categories`.`CategoryID` IN (SELECT `categories`.`ParentID`
                    FROM `categories`
                    where  `categories`.`CategoryID` IN (SELECT `CategoryID` FROM `machines` where `machines`.`IsActive`=1)
                    AND `categories`.`Hide` = '0'
                    AND `categories`.`ParentID` != 0
                    group by `categories`.`ParentID`)))";
    $rec = getCategories($language,'categories.CategoryID = '.$post['CategoryID'],$limit = false,$start = 0,$categorytype=false,$active_category); 
    if(!empty($rec)){
    $rec = $rec[0];
    if($rec->ParentID > 0)
    {
        $checkRec = getCategories($language,'categories.CategoryID = '.$rec->ParentID,$limit = false,$start = 0,$categorytype=false,$active_category);
        $checkRec = $checkRec[0];
        $active_subcategory="`a`.`CategoryID` IN (SELECT `CategoryID` FROM `machines` where `machines`.`IsActive`=1)";
        if($checkRec->ParentID > 0)
        {
            $ParentID = $checkRec->ParentID;
            $categoryData = getSubCategoryBy(array('a.ParentID'=>$checkRec->ParentID),$isArr = false,$active_subcategory);
            $ChildData = '<div class="row"><label class="col-md-12 control-label" for="Category">'.lang('sub_category').'</label><div class="col-md-12"><select class="select2 form-control getSubCategory" data-type="subcategory" data-page-type="result" name="CategoryID" id="category" required>
                <option value="">--- '.lang('select_sub_category').' ---</option>';
            foreach ($categoryData as $key => $value) {
                $ChildData .= '<option '.($rec->ParentID == $value->CategoryID ? 'selected' : '').' value="'.$value->CategoryID.'">'.$value->Title.'</option>';
            }
            $ChildData .= '</select></div></div>';

            $categoryData = getSubCategoryBy(array('a.ParentID'=>$rec->ParentID),$isArr = false,$active_subcategory);
            $SubChildData = '<div class="row"><label class="col-md-12 control-label" for="Category">'.lang('sub_category').'</label><div class="col-md-12"><select class="select2 form-control getSubCategory" data-type="subcategory" data-page-type="result" name="CategoryID" id="category" required>
                <option value="">--- '.lang('select_sub_category').' ---</option>';
            foreach ($categoryData as $key => $value) {
                $SubChildData .= '<option '.($post['CategoryID'] == $value->CategoryID ? 'selected' : '').' value="'.$value->CategoryID.'">'.$value->Title.'</option>';
            }
            $SubChildData .= '</select></div></div>';
        }
        else
        {
            $ParentID = $rec->ParentID;
            $categoryData = getSubCategoryBy(array('a.ParentID'=>$rec->ParentID),$isArr = false,$active_subcategory);
            $ChildData = '<div class="row"><label class="col-md-12 control-label" for="Category">'.lang('sub_category').'</label><div class="col-md-12"><select class="select2 form-control getSubCategory" data-type="subcategory" data-page-type="result" name="CategoryID" id="category" required>
                <option value="">--- '.lang('select_sub_category').' ---</option>';
            foreach ($categoryData as $key => $value) {
                $ChildData .= '<option '.($post['CategoryID'] == $value->CategoryID ? 'selected' : '').' value="'.$value->CategoryID.'">'.$value->Title.'</option>';
            }
            $ChildData .= '</select></div></div>';
        }
    }
    else
    {
        $ParentID = $post['CategoryID'];
    }
}
?>
<section class="breadcrumb-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul>
                    <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                    <li><a href="<?php echo base_url('erweiterte-suche'); ?>"><?php echo lang('advanced-search'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="cat-posts-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
              <form action="<?php echo base_url('erweiterte-suche'); ?>" method="post">
                    <div class="row-container mb-2">
                        <div class="row">
                             <label class="col-md-12 control-label" for="Category"><?php echo lang('category'); ?></label>
                            <div class="col-md-12">
                                <select class="form-control getSubCategory select2" name="CategoryID" id="category" data-type="category" data-page-type="result">
								 <option value="">--- <?php echo lang('select_main_category'); ?> ---</option>

                                    <?php 
									$arr = getCategories($language,'categories.ParentID = 0',$limit = false,$start = 0,$categorytype=false,$active_category);
									if(!empty($arr)){
										foreach($arr as $v){
								     ?>
                                            <option value="<?php echo $v->CategoryID; ?>"  <?php echo getSelected(@$ParentID,$v->CategoryID); ?>><?php echo $v->Title; ?></option>
									<?php 
                                        } 
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row-container mb-2" id="subCategoryDiv">
                        <?php echo $ChildData; ?>
                    </div>
                    <div class="row-container mb-2" id="subSubCategoryDiv">
                        <?php echo $SubChildData; ?>
                    </div>
                    <div class="row-container mb-2">
                        <div class="row">
                            <label class="col-md-12 control-label" for="Machinetype"><?php echo lang('machine_type'); ?></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="Machinetype" name="MachineType" value="<?php echo @$post['MachineType']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row-container mb-2">
                        <div class="row">
                           <label class="col-md-12 control-label" for="Manufacturer"><?php echo lang('manufacturer'); ?></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="Manufacturer" name="Manufacturer" value="<?php echo @$post['Manufacturer']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row-container mb-2">
                        <div class="row">
                           <label class="col-md-12 control-label" for="Model"><?php echo lang('model');?></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="Model" id="Model"  value="<?php echo @$post['Manufacturer']; ?>">
							</div>
                        </div>
                    </div>
                    <div class="row-container mb-2">
                        <div class="row">
							<label class="col-md-12 control-label" for="ManufacturerYear"><?php echo lang('manufacturer_year'); ?></label>
                            <div class="col-md-6">
                                <select class="form-control" name="ManufacturerYear" id="ManufacturerYear">
                                    <option value="">from</option>
									<?php 
									$arr = YearList();
									if(!empty($arr)){ 
										foreach($arr as $year){ ?>
											<option value="<?php echo $year; ?>"  <?php echo getSelected(@$post['ManufacturerYear'],$year); ?> ><?php echo $year; ?> </option>
								<?php } } ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label class="d-none" for="ManufacturerYear"></label>
                                <select class="form-control" name="ManufacturerYear2" id="ManufacturerYear">
                                    <option value="">to</option>	
									<?php 
									$arr = YearList();
									if(!empty($arr)){ 
										foreach($arr as $year){ ?>
											<option value="<?php echo $year; ?>"  <?php echo getSelected(@$post['ManufacturerYear'],$year); ?>><?php echo $year; ?> </option>
								<?php } } ?>
                                </select>
							</div>
						</div>
                    </div>
                    <div class="row-container">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-block btn-primary"><?php echo lang('search'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-9">
<!--
                <div class="row cat-main-title">
                    <div class="col-md-12">
                        <h2><?php echo $cat_name; ?> <?php echo lang('listing'); ?></h2>
                    </div>
                </div>
-->
                <div class="row cat-page-list">
					<?php if($result){
				        foreach($result as $key=>$val){
						$user = get_user_info(array('a.UserID' => $val->UserID));
						$images = getImage(array('FileID'=>$val->MachineID, 'ImageType'=>'MachineFile'));
						$cod = $val->Condition;
					?>
                    <!-- Category post Start -->
                    <div class="col-md-12">
                        <div class="cpl-post">
                            <div class="row">
                                <div class="col-lg-4 col-md-12">
									<?php if(($val->FeaturedImage != '' && file_exists($val->FeaturedImage)) || $images){ 
                                        if($val->FeaturedImage != '' && file_exists($val->FeaturedImage)){
                                        ?>
                                        <div class="img-thumbnail post-img">
                                            <a href="<?php echo base_url($val->FeaturedImage); ?>" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url($val->FeaturedImage); ?>" class="html5lightbox<?php echo $key;?>" data-group="set1" data-width="600" data-height="400" title="Image 1"><img src="<?php echo base_url($val->FeaturedImage); ?>" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                                        </div>
                                    <?php
                                        }
                                        if($images)
                                        {
                                            foreach($images as $k => $image){
                                             $ext = pathinfo($image['ImageName'], PATHINFO_EXTENSION);
                                             if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
                                            ?>
                                            <div class="<?php echo (($val->FeaturedImage != '' && file_exists($val->FeaturedImage)) ? 'd-none' : ($k == 0 ? 'img-thumbnail post-img' : 'd-none'));?>">
                                                <a href="<?php echo base_url($image['ImageName']); ?>" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url($image['ImageName']); ?>" class="html5lightbox<?php echo $key;?>" data-group="set1" data-width="600" data-height="400" title="Image 1"><img src="<?php echo base_url($image['ImageName']); ?>" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                                            </div>
                                            <?php       
                                                }   
                                            }
                                        }
										  }else {?> 
									 <div class="img-thumbnail post-img">
                                        <a href="<?php echo base_url(); ?>uploads/dummy.jpg" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url(); ?>uploads/dummy.jpg" class="html5lightbox<?php echo $key;?>" data-group="set1" data-width="600" data-height="400" title="Image 1"><img src="<?php echo base_url(); ?>uploads/dummy.jpg" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                                    </div>
									<?php } ?>
                                </div>
                                <div class="col-lg-8 col-md-12 cpl-post-content">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <h2>
                                                <a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>">
                                                    <span class="pro-name"><?php echo $val->Title; ?></span>
                                                    <span class="pro-manufacturer"><?php echo $val->Manufacturer; ?></span><span class="pro-model"> <?php echo $val->Model; ?></span>
                                                </a>
                                            </h2>
                                        </div>
										<?php /*if($user->IsCertifiedTrader == 1){ ?>
                                        <div class="col-md certified-img"><a href="#"><img src="<?php echo base_url(); ?>assets/frontend/images/certified-trade.png" alt="" width="120" height="120" class="img-fluid"></a></div>
										<?php }*/ ?>
                                    </div>
                                    <div class="row cpl-posy-col">
                                        <div class="col-md-6">
                                            <div class="pro-country">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                <span class="pro-value"> <?php echo $val->Location; ?></span>
                                                <!-- <div class="flag-wrapper">
                                                    <span href="#" class="flag flag-icon-background flag-icon-tr" title="ad"></span>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <span class="pro-value"> <?php echo $val->ManufacturerYear; ?></span>
                                        </div>
                                        <div class="col-md-6">
                                            <i class="fa fa-wrench" aria-hidden="true"></i>
                                            <span class="pro-value"> <?php echo condition_list($cod); ?></span>
                                        </div>
                                    </div>
                                    <div class="row pro-shrt-desc">
                                        <div class="col-md-12">
											<?php echo $val->Description; ?>
                                            <p><a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>"><?php echo lang('more'); ?></a></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 cpl-btn-wrap">
                                    <div class="row flex-lg-row-reverse">
                                        <div class="col-lg-6 col-md-12">
                                            <a href="#" data-toggle="modal" data-target="#inquiryModal" class="btn btn-primary inquiry" data-id="<?php echo $val->MachineID; ?>"><?php echo lang('request-price'); ?></a>
                                            <a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>" class="btn btn-primary"><?php echo lang('read-more'); ?></a>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="pro-rm-btn">
                                                <a href="#cpl-post-<?php echo $val->MachineID; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="row collapse cat-click-description" id="cpl-post-<?php echo $val->MachineID; ?>">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h4><?php echo lang('Description'); ?></h4>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="cat-text">
                                                       <?php echo $val->Description; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h4><?php echo lang('dealer'); ?></h4>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="cat-text">
                                                        <p><?php echo $user->Title; ?></p>
                                                        <p><?php echo $user->StreetAddress; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 cpl-btn-wrap">
                                                    <div class="row flex-lg-row-reverse">
                                                        <div class="col-lg-6 col-md-12">
                                                            <!-- <a href="#" class="btn btn-success"><?php echo lang('sort-by'); ?></a>-->
                                                            <a href="#" class="btn btn-primary inquiry" data-toggle="modal" data-target="#inquiryModal"  data-id="<?php echo $val->MachineID; ?>"><?php echo lang('submit-request'); ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(".html5lightbox<?php echo $key;?>").html5lightbox();                  
                    </script>
					
					<?php }  
						if($pages){
					echo $pages; 
						}
					}else{ ?>
					<div class="col-md-12">
							
						<div class="no-found">
							<img src="<?php echo base_url(); ?>assets/frontend/images/grey.png">
							<h3><?php echo lang('record-not-found'); ?></h3></div>
						</div>
					<?php } ?>
                    <!-- Category post End -->
                </div>
            </div>
        </div>
    </div>
</section>