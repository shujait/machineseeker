<style>
.google{
	background-color:#DB4437 !important; 
	color:#ffffff !important; 
	border-color:#DB4437 !important;
    font-size:14px !important;
}
.facebook{
	background-color:#3b5998 !important; 
	color:#ffffff !important; 
	border-color:#3b5998 !important;
    font-size:14px !important;
}

.linkedin{
	background-color:#0e76a8 !important; 
	color:#ffffff !important; 
	border-color:#0e76a8 !important;
    font-size:14px !important;
}

</style>

    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                        <li><?php echo lang('login'); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="login-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo lang('account').' '.lang('login'); ?></h1>
                </div>
                <div class="col-md-12 login-content">
                    <p><?php echo lang('login-instruction-text'); ?></p>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="panel-wrapper">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2 class="panel-heading"><?php echo lang('login'); ?></h2>
                                        <div class="panel-body">
                                            <form action="<?php echo base_url(); ?>cms/account/checkLogin" method="post" onsubmit="return false;" class="form_data form-signin">
                                                <div class="form-label-group">
                                                    <label for="CustomerNumber"><?php echo lang('email'); ?></label>
                                                    <input type="text" name="Email" id="CustomerNumber" class="form-control" required>
                                                </div>
                                                <div class="form-label-group">
                                                    <label for="inputPassword"><?php echo lang('password'); ?></label>
                                                    <input type="password" name="Password" id="inputPassword" class="form-control" required>
                                                </div>
                                                <div class="custom-control custom-checkbox mb-3">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1"><?php echo lang('remember-me'); ?></label>
                                                </div>
                                                <input type="submit" class="btn btn-primary text-uppercase mb-3" value="<?php echo lang('login'); ?>">
                                                <br>
                                                <a href="<?= base_url("fblogin/1"); ?>" class="btn facebook" data-type="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> <?= lang("ContinueWith") ?> Facebook </a>

                                                <a href="<?= base_url("googlelogin/1"); ?>" class="btn google" data-type="google"><i class="fa fa-google" aria-hidden="true"></i> <?= lang("ContinueWith") ?> Google </a>
                                                <!-- <a href="<?php //echo base_url("linkedinlogin?oauth_init=1"); ?>" class="btn linkedin" data-type="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i> <?php //echo lang("ContinueWith") ?> Linkedin </a> -->
                                            </form>
                                            <br>
                                            <p><a href="#" data-toggle="modal" data-target="#forgotCredentials" data-dismiss="modal"><?php echo lang('forgot-password-text'); ?></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-wrapper">
                                        <h2 class="panel-heading"><?php echo lang('login-machine-privacy'); ?></h2>
                                        <div class="card-body">
                                            <p><?php echo lang('ssl-text'); ?></p>
                                            <img src="<?php echo base_url(); ?>assets/frontend/images/ssl-img.png" alt="Secure" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel-wrapper">
                                        <h3 class="panel-heading"><?php echo lang('still-not-account'); ?></h3>
                                        <div class="card-body">
                                            <p><?php echo lang('login-service-price'); ?></p>
                                            <a class="btn btn-primary" href="<?php echo base_url('tarife'); ?>"><?php echo lang('read-more'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
$(document).ready(function(){
    var error = '<?= $_GET['error'] ?>';
    if(error == 'social'){
        var message = '<?= lang('SocialAccountNotExisted')?>';
        showError(message);
    } else if(error == 'notverified'){
        var message = '<?= lang('account-not-verified')?>';
        showError(message);
    }
});

</script>