    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                        <li><?php echo lang('reset-password'); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="login-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo lang('reset-password'); ?></h1>
                </div>
                <div class="col-md-12 login-content">
                    <p><?php echo lang('hello');?></p>
                    <p><?php echo lang('reset-password-instruction-text'); ?></p>
                    <p><?php echo lang('machine-pilot-support');?></p>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="panel-wrapper">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2 class="panel-heading"><?php echo lang('reset-password'); ?></h2>
                                        <div class="panel-body">
                                            <form action="<?php echo base_url(); ?>frontend/purchase/reset_password" method="post" onsubmit="return false;" class="form_data form-signin">
                                                <input type="hidden" name="UserID" value="<?php echo $UserID;?>">
                                                <div class="form-label-group">
                                                    <label for="inputPassword"><?php echo lang('password'); ?></label>
                                                    <input type="password" name="Password" id="inputPassword" class="form-control" required>
                                                </div>
                                                <div class="form-label-group">
                                                    <label for="inputPassword"><?php echo lang('confirm_password'); ?></label>
                                                    <input type="password" name="ConfirmPassword" id="ConfirmPassword" class="form-control" required>
                                                </div>
                                                <input type="submit" class="btn btn-primary text-uppercase mb-3" value="<?php echo lang('submit'); ?>">
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-wrapper">
                                        <h2 class="panel-heading"><?php echo lang('login-machine-privacy'); ?></h2>
                                        <div class="card-body">
                                            <p><?php echo lang('ssl-text'); ?></p>
                                            <img src="<?php echo base_url(); ?>assets/frontend/images/ssl-img.png" alt="Secure" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel-wrapper">
                                        <h3 class="panel-heading"><?php echo lang('still-not-account'); ?></h3>
                                        <div class="card-body">
                                            <p><?php echo lang('login-service-price'); ?></p>
                                            <a class="btn btn-primary" href="<?php echo base_url('tarife'); ?>"><?php echo lang('read-more'); ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>