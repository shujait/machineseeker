
<style>
.google{
	background-color:#DB4437 !important;
	color:#ffffff !important;
	border-color:#DB4437 !important;
}
.facebook{
	background-color:#3b5998 !important;
	color:#ffffff !important;
	border-color:#3b5998 !important;
}

.linkedin{
	background-color:#0e76a8 !important;
	color:#ffffff !important;
	border-color:#0e76a8 !important;
}

</style>
<link href="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/css/intlTelInput.min.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput-jquery.min.js"></script>

    <section class="pricing-sec">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-12">
    				<h1><?php echo lang('sell-more-label'); ?></h1>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="comparison table-responsive">
    					<table class="table">
    						<thead>
    							<!-- <tr class="tr-1">
								<th width="30%"></th>
								<?php /*if ($plan) {
									$n = 0;
									foreach ($plan as $v) {
										$n++;
										$label = '';
										if ($n == 2) {
											$label = lang('our-recommendation-label');
										}
								?>
                                <th width="17.5%"> <?php if (!empty($label)) {
														echo '<span>' . $label . '</span>';
													} ?></th>
								<?php
									}
								}*/
								?>
                            </tr>-->
    							<tr class="tr-2">
    								<th></th>
    								<?php if ($plan) {
										foreach ($plan as $v) {
									?>
    										<!--<th><?php //echo lang('dealer'); ?><br /><span><?php //echo $v->Title; ?></span></th>-->
    										<th><span><?php echo $v->Title; ?></span></th>
    								<?php
										}
									}
									?>
    							</tr>
    							<!--<tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>-->
    						</thead>
    						<tbody>

    							<tr>
    								<td></td>
    								<td colspan="4"><strong><?php echo lang('max_number_advertisements'); ?></strong>
    									<button type="button" class="help btn btn-default" data-container="body" data-toggle="popover" data-trigger="hover click" data-placement="top" data-html="true" data-content="<?php echo lang('max_advertisements_desc'); ?>" data-original-title="">
    										?
    									</button>


    								</td>
    							</tr>
    							<tr class="compare-row">
    								<td><strong><?php echo lang('max_number_advertisements'); ?></strong>
    									<button type="button" class="help btn btn-default" data-container="body" data-toggle="popover" data-trigger="hover click" data-placement="top" data-html="true" data-content="<?php echo lang('max_advertisements_desc'); ?>" data-original-title="">
    										?
    									</button>
    								</td>
    								<?php if ($plan) {

										foreach ($plan as $v) {
                                            $mac =  json_decode($v->MetaData);
									?>
<!--    										<td>-->
<!--    											<label for="selPro" class="d-none"></label>-->
<!--    											<select class="form-control plan" id="selPro" data-plan="--><?php //echo $v->PackageID; ?><!--" size="1">-->
<!--    												--><?php //$mac =  json_decode($v->MetaData);
//													foreach ($mac as $k => $rec) {
//													?>
<!--    													<option value="--><?php //echo $v->PackageID; ?><!--" data-duration="--><?php //echo $rec->Duration ?><!--" data-discount="--><?php //echo $rec->Discount ?><!--" data-discount_date="--><?php //echo lang('only_until'); ?><!-- <strong>--><?php //echo dateformat($v->ExpiryDate, 'shortdate'); ?><!--.</strong>" data-discount_incl="--><?php //echo lang("discount_incl") ?><!--" data-price="--><?php //echo $rec->RegularPrice; ?><!--" data-qty="--><?php //echo $rec->QTY; ?><!--" data-row="--><?php //echo $k; ?><!--">--><?php //echo $rec->QTY; ?><!-- --><?php //echo lang('machines'); ?><!--</option>-->
<!--    												--><?php //} ?>
<!--    											</select>-->
<!--    										</td>-->
                                            <td>
                                                <span id="duration_<?php echo $v->PackageID; ?>"><?php echo $mac[0]->QTY; ?></span> <?php echo ($mac[0]->QTY > 1)?lang('machines').'n':lang('machines'); ?><br>
                                            </td>
    								<?php }
									} ?>
    							</tr>


    							<tr>
    								<td>&nbsp;</td>

    								<td colspan="4"><?php echo lang('contract_period'); ?></td>
    							</tr>

    							<tr>

    								<td><?php echo lang('contract_period'); ?></td>
    								<?php if ($plan) {
										foreach ($plan as $v) {
											$mac =  json_decode($v->MetaData);
									?>
                                        <td>
                                            <label for="selPro" class="d-none"></label>
                                            <?php if(count($mac) > 1){ ?>
                                                <select class="form-control plan" id="selPro<?php echo $v->PackageID; ?>" data-plan="<?php echo $v->PackageID; ?>" size="1">
                                                    <?php $mac =  json_decode($v->MetaData);
                                                    foreach ($mac as $k => $rec) {
                                                        ?>
                                                        <option value="<?php echo $v->PackageID; ?>" data-duration="<?php echo $rec->Duration ?>" data-discount="<?php echo $rec->Discount ?>" data-discount_date="<?php echo lang('only_until'); ?> <strong><?php echo dateformat($v->ExpiryDate, 'shortdate'); ?>.</strong>" data-discount_incl="<?php echo lang("discount_incl") ?>" data-price="<?php echo $rec->RegularPrice; ?>" data-qty="<?php echo $rec->QTY; ?>" data-row="<?php echo $k; ?>"><?php echo $rec->Duration; ?> <?php echo ($rec->Duration > 1)?lang('month').'e':lang('month'); ?></option>
                                                    <?php } ?>
                                                </select>
                                            <?php }else{ ?>
                                                <span><?php echo $mac[0]->Duration." "; echo ($mac[0]->Duration > 1)?lang('month').'e':lang('month'); ?></span>
                                            <?php } ?>
                                        </td>
<!--    							    <td>-->
<!--                                        <span id="duration_--><?php //echo $v->PackageID; ?><!--">--><?php //echo $mac[0]->Duration; ?><!--</span> --><?php //echo lang('month'); ?><!--<br>-->
<!--                                    </td>-->
    								<?php }
									} ?>
    							</tr>

    							<?php if ($feature) {
									$i = 0;
									foreach ($feature as $val) {
										$description = GetDefaultLangDataH('FeatureID', $val->FeatureID, 'Description', 'features_text');
										if ($val->Description) {
											$description = $val->Description;
										}
										$i++;
										if ($i % 2 == 0) {
											$class = 'class="compare-row"';
										} else {
											$class = '';
										}
								?>
    									<tr>
    										<td>&nbsp;</td>
    										<td colspan="4"><?php echo $val->Title; ?>
    											<?php if ($description) { ?>
    												<button type="button" id="example" class="help btn btn-default" data-container="body" data-toggle="popover" data-trigger="hover click" data-placement="top" data-html="true" data-content="<?php echo $description; ?>" data-original-title="" title="">
    													?
    												</button>
    											<?php } ?>
    										</td>
    									</tr>
    									<tr <?php echo $class; ?>>
    										<td><?php echo $val->Title; ?>
    											<?php if ($description) { ?>
    												<button type="button" id="example" class="help btn btn-default" data-container="body" data-toggle="popover" data-trigger="hover click" data-placement="top" data-html="true" data-content="<?php echo $description; ?>" data-original-title="" title="">
    													?
    												</button>
    											<?php } ?>

    										</td>
    										<?php
											foreach ($plan as $v) {
												if (strpos($v->FeatureID, $val->FeatureID) !== false) {
													$check = '<span class="tickgreen">✔</span>';
												} else {
													$check = '<span class="crossred">✖</span>';
												}
											?>
    											<td><?php
													echo $check;
													?></td>
    										<?php
											}
											?>
    									</tr>

    							<?php
									}
								}
								?>
    							<tr>
    								<td>&nbsp;</td>
    								<td colspan="4"><?php echo lang('savings'); ?></td>
    							</tr>
    							<tr>
    								<td><?php echo lang('savings'); ?></td>
    								<?php if ($plan) {
										$i = 0;
										foreach ($plan as $v) {
											$i++;
											$class = 'rabattstar';
											if ($i == 2) {
												$class = 'rabattstar_blue';
											}
											$arr =  json_decode($v->MetaData);
									?>
    										<td>
    											<div class="<?php echo $class; ?>">
    												<span class="rabatttext rabattpro25"> <span class="bigrab" id="discount_<?php echo $v->PackageID; ?>">
    														<?php
															if ($arr[0]->Discount) {
																echo $arr[0]->Discount . '%';
															}
															?></span> <br> <span id="discount_next_<?php echo $v->PackageID; ?>">
    														<?php if ($arr[0]->Discount) {
																echo lang('discount_incl') . '.';
															}
															?></span></span>
    												<span class="rabatttext2" id="discount_date_<?php echo $v->PackageID; ?>"><?php
																																if ($arr[0]->Discount) {
																																	echo lang('only_until');
																																?>
    														<strong><?php echo dateformat($v->ExpiryDate, 'shortdate'); ?>.</strong>
    													<?php } ?>
    												</span>
    											</div>
    										</td>
    								<?php }
									} ?>

    							</tr>
    							<tr>
    								<td>&nbsp;</td>
    								<td>&nbsp;</td>
    							</tr>
    							<tr>
    								<td></td>
    								<?php if ($plan) {
										$i = 0;
										foreach ($plan as $v) {
											$i++;
											$class = 'rabattstar';
											if ($i == 2) {
												$class = 'rabattstar_blue';
											}
											$ar =  json_decode($v->MetaData);
											$price = $ar[0]->RegularPrice - ($ar[0]->RegularPrice / 100) * $ar[0]->Discount
									?>
    										<td class="proTarif25">
    											<div class="price-amount">
    												<span class="linethrouh">
    													<span class="oldprice" id="oldprice_<?php echo $v->PackageID; ?>">
    														<?php if ($ar[0]->Discount) {
																echo currency($ar[0]->RegularPrice);
															}
															?>
    													</span>
    												</span>
    												<div class="price-amount2">
                                                        <span id="newprice_<?php echo $v->PackageID; ?>">
    														<?php echo currency($price); ?>
    													</span>
                                                        <span class="sternchen"></span>
                                                    </div>
    											</div>
    											<div><?php echo lang('per_advertisement'); ?> / <?php echo lang('month'); ?></div>
    											<div class="price-unit">
    												(<?php echo lang('equals_to'); ?> <b id="total_<?php echo $v->PackageID; ?>"><?php echo currency($price * $ar[0]->Duration); ?></b> <b></b> <?php echo lang('for_whole_subscription_duration'); ?> )
    											</div>
    										</td>
    								<?php }
									} ?>
    							</tr>
    							<tr>
    								<td>&nbsp;</td>
    								<td>&nbsp;</td>
    							</tr>
    						</tbody>
    						<tfoot>
    							<tr class="letzterow">
    								<td class="description"></td>
    								<?php if ($plan) {
										$form_open = $form_close = '';
										foreach ($plan as $v) {
											if ($this->session->userdata('admin')) {
												$form_open = '<form action="' . base_url('upgrade-package') . '" method="post" id="package-' . $v->PackageID . '">';
												$form_close = '<button type="submit" name="submit" class="btn btn-primary">' . lang('upgrade_plan') . '</button>
																<input type="hidden" name="plan_id" value="' . $v->PackageID . '">
																<input type="hidden" name="plan_row" value="0">
																</form>';
											}
									?>
    										<td>
    											<?php
												echo $form_open;
												if (!($this->session->userdata('admin'))) {
												?>
    												<a href="javascript:;" data-toggle="modal" data-id="<?php echo $v->PackageID; ?>" data-target="#planModal-1" class="btn btn-primary select_plan">
                                                        <i class="fa fa-envelope" aria-hidden="true"></i> <?php echo lang('select_plan'); ?>
    												</a>
													<br>
													<br>
													<a data-href="<?= base_url("googlelogin/0?plan_id=").$v->PackageID; ?>" data-id="<?php echo $v->PackageID; ?>" class="btn google signUpWithSocialIcon" data-type="google"><i class="fa fa-google" aria-hidden="true"></i> <?= lang("ContinueWith") ?> Google </a>
													<br>
													<br>
													<a data-href="<?= base_url("fblogin/0?plan_id=").$v->PackageID; ?>" data-id="<?php echo $v->PackageID; ?>" class="btn facebook signUpWithSocialIcon" data-type="facebook"><i class="fa fa-facebook" aria-hidden="true"></i> <?= lang("ContinueWith") ?> Facebook </a>
													<br>
													<!-- <br>
													<a data-href="<?php //echo base_url("linkedinlogin?oauth_init=0&plan_id=").$v->PackageID; ?>" data-id="<?php //echo $v->PackageID; ?>" class="btn linkedin signUpWithSocialIcon" data-type="linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i> <?php //echo lang("ContinueWith") ?> Linkedin </a>
													<br> -->
    											<?php }
												echo $form_close; ?>
    										</td>
    								<?php }
									}  ?>
    							</tr>
    							<tr>
    								<td></td>
    								<td colspan="2">
    									<p><?php echo lang('plan_instruction'); ?></p>
    								</td>
    							</tr>
    						</tfoot>
    					</table>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
<script>

    $('.signUpWithSocialIcon').on('click', function(){
       var id = $(this).data('id');
       var duration = 0;
       if(typeof $('#selPro'+id).find(':selected').data('row') !== "undefined")
       {
           duration = $('#selPro'+id).find(':selected').data('row');
       }
       var link = $(this).data('href');
       window.location.href = link+'&duration='+duration;
    });

    $(document).ready(function (){
        	var openpopup = '<?= $_GET['popup']; ?>';
        	var email = '<?= $_SESSION['SocialMediaSession']['Email']; ?>';
        	var plan_id = '<?= $_SESSION['plan_id']; ?>';
        	var plan_row = '<?= $_SESSION['plan_row']; ?>';

        	if(openpopup == "open")
        	{
               $("input[name='plan_id']").val('');
               $("input[name='plan_id']").val(plan_id);
               $("input[name='plan_row']").val('');
               $("input[name='plan_row']").val(plan_row);
        		$('#Email').val(email);
        		$('#Email').attr("readonly","readonly");
        		$('#planModal-1').modal('toggle');
        	}

           var input = document.querySelector("#PhoneNumber");
           var iti = intlTelInput(input, {
             preferredCountries: ['de'],
             separateDialCode: true,
             utilsScript: "assets/backend/plugins/Intl-tel/js/utils.js"
           });


           iti.setNumber('+49');
           $("#CountryCode").val('+49');
           var countryData = iti.getSelectedCountryData();

           input.addEventListener("countrychange", function() {
             var changeCountryCode = '';
             changeCountryCode = iti.getSelectedCountryData();
             $("#CountryCode").val("+"+changeCountryCode.dialCode);
           });
    });
</script>
