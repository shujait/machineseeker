    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                        <li><a href="<?php echo base_url('listings'); ?>"><?php echo lang('categorys'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php
        $sidead = false;
        $AdImage = $cat_data[0]->AdImage;
        $AdImageURL = $cat_data[0]->AdImageURL;
        $GoogleAdCode = $cat_data[0]->GoogleAdCode;
        if((isset($AdImage) && !is_null($AdImage) && !empty($AdImage)) || (isset($GoogleAdCode) && !is_null($GoogleAdCode) && !empty($GoogleAdCode)) ){
            $sidead = true;
        }
    $cnclass = "";
    $bnclass = "";
    $showhide = "";
    if ($sidead == true){
        $cnclass = "col-md-6";
        $showhide = "inline";
    } else {
        $cnclass = "col-md-8";
        $showhide = "hidden";
    }
    ?>
    <section class="cat-posts-sec catpage">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <?php $this->load->view('frontend/layouts/left-category-list'); ?>
                </div>
                <div class="<?= $cnclass; ?>">
                    <div class="row cat-page-search">
                        <div class="col-md-12">
                            <div class="search-cat">
                                <h1><?php echo lang('category').': '.$cat_name; ?></h1>
                            </div>
                            <div class="search-cat-form">
                                <form action="<?php echo base_url('search');?>" method="post">
                                    <div class="input-group mb-3">
                                        <input type="text" name="search" id="Title" placeholder="<?php echo lang('search').' in '.$cat_name.' '.lang('for'); ?>..." aria-label="<?php echo lang('search').' in '.$cat_name.' '.lang('for'); ?>..." class="form-control">
                                        <div class="input-group-append">
                                            <button type="submit" name="Search" class="input-group-text"><?php echo lang('search'); ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <?php 
                    if(!(userAgent())){
                        $this->load->view('frontend/layouts/advertise'); 
                    }?>
                    <div class="row cat-main-title">
                        <div class="col-md-12">
                            <h2><?php echo $cat_name; ?> <?php echo lang('listing'); ?></h2>
                        </div>
                    </div>
                    <div class="row cat-page-list">
                        <?php if($data){
					    $this->load->view('frontend/layouts/machine-listing',$data);
						}else{ ?>
                        <div class="col-md-12">
                            <div class="no-found">
                                <!-- <?php if($this->session->userdata['lang'] =="en"){?>
                                <img src="<?php echo base_url(); ?>uploads/EN-Maschinenpilot-1200.png">
                                <?php } else { ?>
                                <img src="<?php echo base_url(); ?>uploads/Maschinenpilot-1200.png">
                                <?php } ?> -->
                                <h3><?php echo lang('record-not-found'); ?></h3>
                            </div>
                        </div>
                        <?php } ?>
                        <!-- Category post End -->
                    </div>
                    <div id="loadMoreMachines">
                        <div class="loadMoreMachines" id="loadMoreMachines">
                            <button class="btn btn-primary" id="load-more" data-page="1">Load More</button>
                        </div>
                    </div>
                </div>
                <div class="sideAd <?= $showhide; ?>">
                    <?php 
                        if($GoogleAdCode){
                    ?>
                        <?= $GoogleAdCode; ?>
                    <?php }if(empty($GoogleAdCode) && !empty($AdImage)){ ?>
                        <a href="<?= $AdImageURL; ?>" target="_blank"><img src="<?= base_url($AdImage);?>"></a>
                    <?php }?>
                </div>
            </div>
        </div>
    </section>
    <script src="https://malsup.github.io/jquery.blockUI.js"></script>
    <script>
        // $('body').on('click','.load-more', function (){
        //     let page = $(this).data('page');
        //
        //     $(this).attr('data-page', ++page);
        // });

        $('#load-more').click(function (e) {
            e.preventDefault();
            var page = +$('#load-more').attr("data-page");
            page = page + 1;

            var request = $.ajax({
                type: "POST",
                url: base_url + 'search/load-more-machines',
                data: {
                    'page': page,
                },
                dataType: "json",
                cache: false,
                beforeSend: function () {
                $.blockUI({
                    message: 'Processing...',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: '.5',
                        color: '#fff',
                        fontSize: '18px',
                        fontFamily: 'Verdana,Arial',
                        fontWeight: 200,
                    } });
                }
            }).done(function(response) {

                $('#loadMoreMachines').prepend(response.html);
                if(response.count != 0)
                {
                    $('#load-more').attr('data-page', page);
                }
                // console.log(response);
                $.unblockUI();
            }).fail(function( jqXHR, textStatus ) {

                $.unblockUI();
                alert( "Request failed: " + textStatus );
            });

        });
    </script>
