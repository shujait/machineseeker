    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                        <li><?php echo lang('dealers'); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="cat-listing pt-4">
        <div class="container">
            <div class="row fw-search mb-4">
                <div class="col-md-12">
                    <h2><?php echo lang('search').' '.lang('for'); ?>:</h2>
                    <div class="bform-wrap p-0">
                        <form action="#" class="form-inline full-form">
                            <div class="input-group">
                                <input type="search" class="form-control" placeholder="<?php echo lang('search-term'); ?>">
                            </div>
                            <div class="btn-search">
                                <button type="submit" class="btn btn-secondary"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-12">
                    <h4 class="mb-4"><?php //echo lang('dealer-category-machine'); ?> </h4>
                </div>
            </div> -->
            
            <?php
                foreach($dealers as $val){
                        ?>
                        <div class="row">
                        <!-- Dealer Start -->
                            <div class="col-md-12">
                                <div class="cpl-post">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-12 gallery">
                                            <?php
                                            if($val->CoverImage != '' && file_exists(FCPATH.$val->CoverImage)){
                                            ?>
                                            <div>
                                                <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>"  title="<?php echo $val->Title; ?>"><img src="<?php echo base_url($val->CoverImage); ?>" alt="" width="200" class="img-fluid imgToCSS"></a>
                                            </div>
                                            <?php
                                                  }else {?> 
                                             <div class="img-thumbnail post-img">
                                                <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>" title="<?php echo $val->Title; ?>"><img src="<?php echo base_url(); ?>uploads/user_dummy.jpg" alt="" width="200" class="img-fluid imgToCSS"></a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-lg-8 col-md-12 cpl-post-content">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <h2>
                                                        <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>">
                                                            <span class="pro-name"><?php echo $val->Title; ?></span>
                                                        </a>
                                                    </h2>
                                                </div>
                                            </div>
                                            <div class="row cpl-posy-col">
                                                <div class="col-md-5">
                                                    <?php if(isset($val->StreetAddress) && !empty($val->StreetAddress)){?>
                                                    <div class="pro-country">
                                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        <span class="pro-value"> <?php echo $val->StreetAddress; ?></span>
                                                    </div>
                                                    <br>
                                                    <?php }?>
                                                    <?php if((isset($val->PostalCode) && !empty($val->PostalCode)) || (isset($val->City) && !empty($val->City))){?>
                                                    <span class="pro-value"> <?php echo $val->PostalCode.' '.$val->City; ?></span>
                                                    <br>
                                                    <br>
                                                    <?php }?>
                                                    <?php if(isset($val->Country) && !empty($val->Country)){?>
                                                    <span class="pro-value"> <?php echo CountryList($val->Country)->Title; ?></span>
                                                    <?php }?>
                                                </div>
                                                <div class="col-md-5">
                                                    <span class="pro-value"> <?php echo $val->CompanyName; ?></span>
                                                    <br><br>
                                                    <span class="pro-value"> <?php echo $val->CompanyProfile; ?></span>
                                                </div>
                                                <div class="col-md-2 cpl-btn-wrap">
                                                    <div class="row flex-lg-row-reverse">
                                                        <div class="col-lg-6 col-md-12">
                                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#UserDetail<?php echo $val->UserID; ?>" class="btn btn-primary"><?php echo lang('view'); ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>            
                        <?php } 
                            ?>
            
            <?php
                foreach($dealers as $val){
                        ?>
            <div class="modal fade" id="UserDetail<?php echo $val->UserID; ?>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><?php echo $val->CompanyName; ?></h5>
                            <button type="button" class="closed" data-dismiss="modal">&times;</button>
                        </div>
                        <br />
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    if($val->CoverImage != '' && file_exists(FCPATH.$val->CoverImage)){
                                    ?>
                                    <div>
                                        <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>"  title="<?php echo $val->Title; ?>"><img src="<?php echo base_url($val->CoverImage); ?>" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                                    </div>
                                    <?php
                                          }else {?> 
                                     <div class="img-thumbnail post-img">
                                        <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>" title="<?php echo $val->Title; ?>"><img src="<?php echo base_url(); ?>uploads/user_dummy.jpg" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('email');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->Email;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('company_name');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->CompanyName;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('website');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->Website;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('ph');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->Phone;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('mob');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->Mobile;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('fax');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->Fax;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('country');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo CountryList($val->Country)->Title;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('street_address');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->StreetAddress;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('postcode_place');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->PostalCode.' '.$val->City;?></span>
                                </div>
                                <br>
                                <hr>
                                <br>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('company_name');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->CompanyName;?></span>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label"><strong><?php echo lang('company-description');?>: </strong></label>
                                </div>
                                <div class="col-md-6">
                                    <span><?php echo $val->CompanyProfile;?></span>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            <?php } 
                            ?>
            <br>
            <br>
            <br>
            <br>
            <div class="row">
                <div class="col-md">
                    <h2><?php echo lang('popular-machine-categories'); ?></h2>
                </div>
            </div>
            <div class="row">
				<?php 
				$arr = getCategories($language,'categories.ParentID = 0',6,0);
				if(!empty($arr)){
					foreach($arr as $result){
					$rr = getCategories($language,'categories.ParentID = '.$result->CategoryID);
                    if(file_exists($result->Image)){
                        $image = $result->Image;
                    }else{
                        $image = 'uploads/dummy.jpg';

                    }
					
					$cat_mc_row = get_category_dealer('find_in_set("'.$result->CategoryID.'", Category) <> 0');		
					
				 if($rr){
			?>
            <div class="col-md-4">
                <div class="cat-box">
                    <div class="img-box">
                        <img src="<?php echo base_url($image);?>" alt="" width="1920" height="700" class="img-fluid">
                    </div>
                    <h3 class="d-flex justify-content-between"> <?php echo $result->Title; ?> 
						<?php 
							 echo show_counter($cat_mc_row);
							?>
					</h3>
                    <ul class="list-group">
							<?php
							foreach($rr as $sub){
							$subcat_mc_row = get_category_dealer('find_in_set("'.$sub->CategoryID.'", Category) <> 0');		
							?>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
							<a href="<?php echo friendly_url('dealer/',$sub->Slug.' '.$sub->CategoryID); ?>"><?php echo $sub->Title; ?></a> 
							<?php 
							 echo show_counter($subcat_mc_row);
							?>
						</li>
						<?php } ?>
                    </ul>
                </div>
            </div>
			<?php  } } } ?>
            </div>
        </div>
    </section>
    <section class="alphabetical-cat-list mb-0">
        <div class="container">
            <div class="row">
                <div class="col-md">
                    <h2><?php echo lang('machine-category-label'); ?></h2>
                </div>
            </div>
			<div class="row">
            <div class="col-md-4">
                <h3>A - M</h3>
                <ul class="list-group">
					<?php $rec = getCategories($language,'categories.ParentID = 0 AND categories_text.Title >= "A" AND categories_text.Title <= "K"'); 
					if($rec){
					foreach($rec as $val){
						$cat_mc_count = get_category_dealer('find_in_set("'.$val->CategoryID.'", Category) <> 0');		
					?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
						<a href="<?php echo friendly_url('dealer/',$val->Slug.' '.$val->CategoryID); ?>"><?php echo $val->Title; ?></a> 
							<?php 
						echo show_counter($cat_mc_count); 
						?>
					</li>
					<?php } } ?>
                </ul>
            </div>
            <div class="col-md-4">
                <h3>N - P</h3>
                <ul class="list-group">
					<?php $rr = getCategories($language,'categories.ParentID = 0 AND categories_text.Title >= "L" AND categories_text.Title <= "R"'); 
					if($rr){
					foreach($rr as $val){
						$cat_mc_count =  get_category_dealer('find_in_set("'.$val->CategoryID.'", Category) <> 0');		
					?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
						<a href="<?php echo friendly_url('dealer/',$val->Slug.' '.$val->CategoryID); ?>"><?php echo $val->Title; ?></a> 
						<?php  
						echo show_counter($cat_mc_count); 
						?>
					</li>
					<?php } } ?>
					</ul>
            </div>
            <div class="col-md-4">
                <h3>Q - Z</h3>
                <ul class="list-group">
					<?php $ss = getCategories($language,'categories.ParentID = 0 AND categories_text.Title >= "S" AND categories_text.Title <= "Z"');
					if($ss){
					foreach($ss as $val){
						$cat_mc_count =  get_category_dealer('find_in_set("'.$val->CategoryID.'", Category) <> 0');		
					?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
						<a href="<?php echo friendly_url('dealer/',$val->Slug.' '.$val->CategoryID); ?>"><?php echo $val->Title; ?></a> 
							<?php 
						echo show_counter($cat_mc_count); 
						?>
					</li>
					<?php } } ?>
					</ul>
            </div>
        </div>
        </div>
    </section>