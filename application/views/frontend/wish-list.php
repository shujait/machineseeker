    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                        <li><?php echo lang('wishlist'); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="about-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo lang('wishlist'); ?></h1>
<!--
                    <div class="alert alert-warning">
                        <p><?php echo lang('wishlist-note'); ?></p>
                    </div>
-->
                </div>
                <div class="col-md-12">
                    <div class="btn-block mb-4">
                        <a href="#" class="btn btn-secondary mb-3 mark-all"><span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span> <?php echo lang('select-all-ads'); ?></a>
                        <a href="#" class="btn btn-secondary mb-3 unmark-all"><span class="glyphicon glyphicon-check" aria-hidden="true"></span> <?php echo lang('deselect-all-ads'); ?></a>
                        <button type="submit" class="btn btn-secondary mb-3 dlt-pro remove-all-wishlist"><?php echo lang('remove-selected-ads'); ?></button>
                    </div>
                </div>
<!--
                <div class="col-md-12">
                    <div id="w0-success" class="alert-success alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Advertisements removed.
                    </div>
                </div>
-->
                <div class="col-md-12" id="WatchProList">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
								<?php if($record){
								$n = 0;		
								foreach($record as $val){
								$n++;	
								$row = get_machine_row(array('a.MachineID' => $val->MachineID),'row');
									if($row){
								$images = getValue('site_images',array('FileID'=>$val->MachineID, 'ImageType'=>'MachineFile'),'row');
	
								?>
                                <tr>
                                    <td width="240">
                                        <div class="checkbox">
                                            <input type="checkbox" name="watchPro-<?php echo $n; ?>" id="watchPro-<?php echo $n; ?>" value="<?php echo $val->MachineID; ?>">
                                            <label for="watchPro-<?php echo $n; ?>"></label>
                                        </div>
										<?php
								        if($images){
									     $ext = pathinfo($images->ImageName, PATHINFO_EXTENSION);
										 if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
											$img = base_url($images->ImageName); 
										 }else{
											$img = base_url()."uploads/dummy.jpg"; 
										 }
										} else{
											$img = base_url()."uploads/dummy.jpg"; 
										 }
										?>
                                          <img src="<?php echo $img; ?>" alt="" width="150" class="img-fluid">
										
                                    </td>
                                    <td>
                                        <h3> <a href="<?php echo friendly_url('detail/',$row->Title.' '.$row->MachineID); ?>">
                                                        <span class="pro-name"><?php echo $row->Title; ?></span>
                                             </a>
										</h3>
                                        <p class="h5 alert p-1 badge-danger d-inline-block"><?php echo condition_list($row->Condition); ?></p>
                                    </td>
                                    <td>
                                        <span class="proYear"><?php echo $row->ManufacturerYear; ?></span>
                                    </td>
                                </tr>
                                <?php } } } ?>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </section>