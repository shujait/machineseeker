    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                        <li><a href="<?php echo base_url('dealer'); ?>"><?php echo lang('dealer'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="cat-posts-sec">
        <div class="container">
            <div class="row">
<!--                <div class="col-md-4">-->
<!--                    --><?php //$this->load->view('frontend/layouts/left-dealer-category-list'); ?>
<!--                </div>-->
                <div class="col-md-12">
<!--                    <div class="row cat-page-search">-->
<!--                        <div class="col-md-12">-->
<!--                            <div class="search-cat">-->
<!--                                <h2>--><?php //echo $cat_name.' '.lang('for').' '.lang('dealer'); ?><!--</h2>-->
<!--                            </div>-->
<!--                            <div class="search-cat-form">-->
<!--                                <form action="--><?php //echo base_url('search');?><!--" method="post">-->
<!--                                    <div class="input-group mb-3">-->
<!--                                        <input type="text" name="search" placeholder="--><?php //echo lang('search').' in '.$cat_name.' '.lang('for'); ?><!--..." aria-label="--><?php //echo lang('search').' in '.$cat_name.' '.lang('for'); ?><!--..." class="form-control">-->
<!--                                        <div class="input-group-append">-->
<!--                                            <button type="submit" name="Search" class="input-group-text">--><?php //echo lang('search'); ?><!--</button>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </form>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                   <?php  $this->load->view('frontend/layouts/advertise'); ?>	
                    <div class="row cat-main-title">
                        <div class="col-md-12">
                            <h2><?php echo $cat_name.' '. lang('listing'); echo (isset($user_info))?" - ".$user_info['CompanyName']:"" ?></h2>
                        </div>
                    </div>
                    <div class="row cat-page-list">
						<?php if($result){
					     $this->load->view('frontend/layouts/dealer',$result);
						}else if($data){
					     $this->load->view('frontend/layouts/machine-listing',$data);
						}else{ ?>
						<div class="col-md-12">
								
							<div class="no-found">
								<img src="<?php echo base_url(); ?>assets/frontend/images/grey.png">
								<h3><?php echo lang('record-not-found'); ?></h3></div>
							</div>
						<?php } ?>
                        <!-- Category post End -->
                    </div>
                </div>
            </div>
        </div>
    </section>