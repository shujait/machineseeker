<?php include('header.php'); ?>
    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li>FAQ</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="faq-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>FAQ</h1>
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#Buyer">Buyer</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Seller">Seller</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div id="Buyer" class="tab-pane active">
                            <div id="faqBuyer" class="accordion">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#faqBuyerQ-1" role="button" aria-expanded="false" aria-controls="faqBuyerQ-1">
                                        <a class="card-title">What is Machinepilot.com?</a>
                                    </div>
                                    <div class="collapse" id="faqBuyerQ-1">
                                        <div class="card-body">
                                            <p>Machinepilot is the world's leading online marketplace for used machines. More than 150,000 listings from more than 5,100 dealers are currently online. If you are interested, you can send an inquiry to the seller <strong>free of charge and without registration</strong> or contact the seller directly by telephone.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-2">
                                        <a class="card-title">What are my benefits as a buyer on Machinepilot.com? </a>
                                    </div>
                                    <div id="faqBuyerQ-2" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body ">
                                            <p>As a buyer, you will find over 150,000 industrial machines of all categories on the marketplace. <strong>A sales inquiry is free of charge and without any obligations to you.</strong> The dealer in each case will usually contact you as soon as possible. A potential transaction is not processed via our marketplace and we do <strong>not receive any commission</strong>.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-3">
                                        <a class="card-title">How do I find a suitable machine?</a>
                                    </div>
                                    <div id="faqBuyerQ-3" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body ">
                                            <p>Enter your query on our <a href="#">start page</a> as you would with Google Search. Type in the name of the machine you are looking for and press Enter or click on the magnifying glass. In the search results, you will find all relevant listings that match your search query. Click on the appropriate machine to get further details and to send an inquiry. You can also search in the individual <a href="category-page.php">machine categories</a>.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-4">
                                        <a class="card-title">How do I contact the seller?</a>
                                    </div>
                                    <div id="faqBuyerQ-4" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body ">
                                            <p>You can send a direct inquiry to the advertiser via e-mail. Simply use the blue inquiry form on the right in the listing or the green contact dealer button (bottom left). You will also find the telephone or fax number below the inquiry form. If you click on the name of the advertiser, the dealer profile (if available) opens with additional information about your potential business partner.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-5">
                                        <a class="card-title">What do I do if the seller does not answer? </a>
                                    </div>
                                    <div id="faqBuyerQ-5" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                            <p>A message may get lost or end up in the spam folder of the other party. Please contact the seller again - if necessary by other channels, e.g. by phone or fax. We ourselves only hold the contact details available in the advertisement.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-6">
                                        <a class="card-title">How do I search for specific dealers? </a>
                                    </div>
                                    <div id="faqBuyerQ-6" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                            <p>You can use the <a href="dealers.php">dealer search</a> to display individual dealers. Use the search bar at the top left or click your way through the sellers by selecting a category. Via the menu item <i>Search &gt; Send request</i> you can send your request to all dealers in a category.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-7">
                                        <a class="card-title">Why do the advertisements show a watermark/letter or number code? </a>
                                    </div>
                                    <div id="faqBuyerQ-7" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                            <p>Data protection is very important to us. To prevent data theft, the watermark and/or code is entered in the text.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-8">
                                        <a class="card-title">The requested machine is sold. What can I do?</a>
                                    </div>
                                    <div id="faqBuyerQ-8" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                            <p>Sellers must adhere to our marketplace rules when creating advertisements. This includes, for example, the prompt deletion or suspension of sold machines. You can send us a message via the button “<strong>Report</strong>“ in the respective advertisement if an advertisement violates our marketplace rules and has already been sold, for example.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-9">
                                        <a class="card-title">I found a violation. What can I do? </a>
                                    </div>
                                    <div id="faqBuyerQ-9" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                            <p>Please contact us immediately so that we can investigate the matter and take further action. Should an advertisement violate our marketplace rules, you can also forward your complaint to us via the “<strong>Report</strong>“ button in the respective advertisement.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-10">
                                        <a class="card-title">The seller is displayed as a „certified dealer“. What does that mean? </a>
                                    </div>
                                    <div id="faqBuyerQ-10" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                            <p>The Machinepilot Trust Seal gives you the opportunity to see at a glance whether you are dealing with a trustworthy seller. We check the trade licence or extract from the commercial register of the dealers as well as the postal address, verify the bank details and the availability by telephone before we finally obtain a credit report which may not contain any negative features. In addition, the dealers certified need to hold a perfect rep sheet with us.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-11">
                                        <a class="card-title">How can I enter a general machine request on Machinepilot? </a>
                                    </div>
                                    <div id="faqBuyerQ-11" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                        <p>General machine requests are free of charge. Select “<strong>Send request</strong>“ from the “<strong>Search</strong>“ menu and follow the instructions. You choose a main category and the subcategory that matches your search query. Now enter your data in the fields provided. All dealers - in the selected category - will receive your request. This way you reach an enormous number of suppliers and can expect a high response rate. Therefore, please remember to click on your preferred “<strong>answer method</strong>“. Finally, click on “<strong>Send request</strong>“.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-12">
                                        <a class="card-title">Why do I need to enter a four-digit number before I can send my request? </a>
                                    </div>
                                    <div id="faqBuyerQ-12" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                            <p>By entering the security code, we can ensure that you are not a robot that automatically sends requests. This way we protect our dealers from spam messages and an overcrowded e-mail inbox.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqBuyerQ-13">
                                        <a class="card-title">I cannot read the security code! Now what? </a>
                                    </div>
                                    <div id="faqBuyerQ-13" class="collapse" data-parent="#faqBuyer">
                                        <div class="card-body">
                                            <p>Just click on “<strong>Different number</strong>“ until you see a combination that you can decipher without problems.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="Seller" class="tab-pane fade">
                            <div id="faqSeller" class="accordion">
                                <div class="card mb-0">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#faqSellerQ-1">
                                        <a class="card-title">What is Machinepilot.com?</a>
                                    </div>
                                    <div id="faqSellerQ-1" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>Machinepilot is the world's leading online marketplace for second-hand stationary machines. More than 150,000 used and partly new machines are offered on over 60 international portals. The more than 1.5 million website visitors per month can send an e-mail inquiry to the sellers or contact them directly by telephone.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-2">
                                        <a class="card-title">Can I sell my machine to Machinepilot?</a>
                                    </div>
                                    <div id="faqSellerQ-2" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>No, Machinepilot does not buy or sell any machines themselves. We are an internet-company and operator of the online-marketplace that 5,100 dealers currently advertise and sell their equipment with.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-3">
                                        <a class="card-title">What are my benefits as a seller on Machinepilot.com?</a>
                                    </div>
                                    <div id="faqSellerQ-3" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>More than 1.5 million relevant and highly interested visitors visit Machinepilot every month. Your advertisement addresses exactly the target group that is of interest to you. Every month, used machines worth over 700 million euros are inquired for on Machinepilot. A potential buyer can contact you directly by phone or e-mail. Selling machines is faster, easier and more profitable than anywhere else is.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-4">
                                        <a class="card-title">Does Machinepilot charge commission? </a>
                                    </div>
                                    <div id="faqSellerQ-4" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>No! As Seller, you are only charged for your listings. For Buyers our services are free of charge.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-5">
                                        <a class="card-title">What are the differences between Dealer and One-Time-Seller plans?</a>
                                    </div>
                                    <div id="faqSellerQ-5" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>The dealer tariffs are intended for machine dealers and offer additional features such as data import or advertisement spider (Dealer Standard). Our One-Time-Seller plans are not allowed for dealers, but aim at e.g. manufacturing companies that want to sell individual machines quickly and profitably.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-6">
                                        <a class="card-title">Are my advertisements only published in German-speaking countries? </a>
                                    </div>
                                    <div id="faqSellerQ-6" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>No! All listings uploaded and listed on <a href="https://www.machinepilot.com/">Machinepilot.com</a> are automatically published in more than 60 country-specific versions of our marketplace – free of charge. Our platforms reach potential buyers from over 150 countries around the world in more than 45 languages!</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-7">
                                        <a class="card-title">Are my listings translated automatically? </a>
                                    </div>
                                    <div id="faqSellerQ-7" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>When you place your ad, you can provide information in several languages. If you do not do this manually, your offer is automatically translated automatically. Unfortunately, a minor error rate cannot be ruled out here.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-8">
                                        <a class="card-title">How can I advertise on Machinepilot? </a>
                                    </div>
                                    <div id="faqSellerQ-8" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <ol>
                                                <li>Go to the Machinepilot main page <a href="#">www.Machinepilot.com</a> and click on <b><a href="#">sell now</a></b> in the top centre or sign up in the top right. </li>
                                                <li>Take your time when reviewing our subscription plans. Move your cursor on the question marks, to find additional information. If you do have any additional questions, please reach out to your personal contact Max Mertens with +1 312 94 04 581.</li>
                                                <li>Choose a plan and click on “<b>select plan</b>”.</li>
                                                <li>Enter your data and click on “<b>Next</b>”.</li>
                                                <li>You have successfully registered and will be provided an account number, which you will need to login with your password. </li>
                                                <li>After your login, you will find the button “<b>Create Listing</b>” next to your performance statistics. Click it. </li>
                                                <li>Follow the instructions. If you do have any questions, please call our support with +1 312 94 04 581.</li>
                                            </ol>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-9">
                                        <a class="card-title">What information may my listing contain?</a>
                                    </div>
                                    <div id="faqSellerQ-9" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>Please follow our <a href="#">marketplace rules</a> when creating your advertisement.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-10">
                                        <a class="card-title">How can a potential buyer contact me? </a>
                                    </div>
                                    <div id="faqSellerQ-10" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>The interested party can send you an e-mail inquiry using the “Send inquiry“ form in the advertisement or have your telephone and fax number displayed.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-11">
                                        <a class="card-title">How long will my listings be published?</a>
                                    </div>
                                    <div id="faqSellerQ-11" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>When placing your listings, you can choose a publication period between 1-6 months. After the expiry of the period you have chosen, the advertisements will then be automatically removed from our database. However, you have the option of extending the advertisements before they expire. We will inform you by e-mail ten days before an advertisement is deactivated.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-12">
                                        <a class="card-title">Why can’t I find my listing on Machinepilot?</a>
                                    </div>
                                    <div id="faqSellerQ-12" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>The advertisements of one-time sellers appear with a time delay of 24 hours on our marketplace. In this period, the offers are controlled by us and offered exclusively for sale to registered dealers. This is why you are already receiving initial offers to buy your machine, although your offer is not yet online on our platforms. After 24 hours your advertisement will be regularly displayed on our marketplace.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-13">
                                        <a class="card-title">Why does my listing display the status “paused“ immediately after it was placed?</a>
                                    </div>
                                    <div id="faqSellerQ-13" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>The advertisements of one-time sellers appear with a time delay of 24 hours on our marketplace. In this period, the offers are controlled by us and offered exclusively for sale to registered dealers. This is why you are already receiving initial offers to buy your machine, although your offer is not yet online on our platforms. After 24 hours your advertisement will be regularly displayed on our marketplace.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-14">
                                        <a class="card-title">How can I increase or optimise the popularity of my listings? </a>
                                    </div>
                                    <div id="faqSellerQ-14" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>Analyses indicate that you will receive more inquiries from potential buyers with a detailed description of your offers. Advertisements with pictures, technical details, year of construction, location and condition are generally more frequently requested than advertisements with incomplete information. <br>For dealers (!): With the <a href="https://service.maschinepilot.com/vertrauenssiegel/">Machinepilot Trust Seal</a> as well as an informative <a href="https://.maschinepilot.com">dealer profile</a>, you can significantly increase inquiries and sales. </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-15">
                                        <a class="card-title">How and where can I manage my advertisements (display/change/delete)?</a>
                                    </div>
                                    <div id="faqSellerQ-15" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>This requires you to be logged in. If this is not the case, please click on “<b>Login</b>“ on the top right and log in with your account details. You will be redirected to your user account. Now use the blue button “<b>Manage advertisements</b>“. If you are still logged in, click on “<b>Your machines</b>“ at the top of the website. <br>Now you can easily manage advertisements by clicking on the blue button “<b>Show</b>“, “<b>Edit</b>“, “<b>Pause</b>“, “<b>Delete</b>“, “<b>Extend</b>“ or the respective “<b>Statistics</b>“. Via the menu item “<b>Changes to all offers</b>“ you can also activate, pause or extend all advertisements simultaneously. Especially dealers with many listings can easily find their ads using the “<b>Quick Search</b>“ or “<b>Power Search</b>“. By clicking on “<b>More</b>“, all machine listings can also be sorted according to specific parameters.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-16">
                                        <a class="card-title">Why are new photos not being displayed/replaced in my advertisement? </a>
                                    </div>
                                    <div id="faqSellerQ-16" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>Please clear your <a href="https://de.wikipedia.org/wiki/Browser-Cache">browser cache</a>. When you return to the ad, you should see the current photos.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-17">
                                        <a class="card-title">What does “Listings in search results”, “listings displayed” and “Contacts made” mean?</a>
                                    </div>
                                    <div id="faqSellerQ-17" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>The number with <b>listings in search results</b> represents the number of times your listings have been displayed in the search results. <b>Listings displayed</b> the number of times your listings have been visited. The <b>contacts made</b> counter stands for the number of times your equipment has been inquired for via email and the number of times your phone / fax number has been displayed.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-18">
                                        <a class="card-title">Is it possible to import data?</a>
                                    </div>
                                    <div id="faqSellerQ-18" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>The data import to Machinepilot is included with every subscription plan, starting at our Standard plan (25 advertisements). With this import interface you can insert your machine database into our system and keep it automatically updated. To do this, you need an import file generated by your system, the technical specifications of which can be found in our <a href="#">documentation</a>. For technical questions regarding the import function, please contact our e-mail address <a href="mailto:import@machinepilot.com">import@maschinepilot.com</a> or our <a href="#">customer support</a>.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-19">
                                        <a class="card-title">How can I place banner advertisements in Machinepilot and Maschinensucher?</a>
                                    </div>
                                    <div id="faqSellerQ-19" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>With our cost-effective advertising system, you can target over 1.5 million users per month on <a href="#">Machinepilot</a>. Start advertising now for as little as 5 euros with a focus on field of business. We will be happy to consult you at +1 312 94 04 581. Registered users can book advertising campaigns directly via their <a href="#">customer account</a>.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-20">
                                        <a class="card-title">What is the Machinepilot Trust Seal and how do I get it? </a>
                                    </div>
                                    <div id="faqSellerQ-20" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>We certify trustworthy dealers with the Machinepilot <a href="#">Trust Seal</a>. Interested Buyers from around the world can spot at first glance, which of our dealers have been certified. As a dealer you will profit from increased buyer confidence, advertisement displays, inquiries and turnover. <a href="#">More Info here</a>. </p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-21">
                                        <a class="card-title">What do you mean by money-back guarantee? </a>
                                    </div>
                                    <div id="faqSellerQ-21" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>We at Machinepilot are convinced that we offer a great product for a very fair price. Convince yourself and sell machines with us for 12 months – free of any risk. We only require one fair condition for the refund: You are a machine dealer and offer an average of at least 10 machines during the contract term. If, contrary to expectations, you are not convinced of our offer after the contract period has expired, we will refund the amount invoiced in full!</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-22">
                                        <a class="card-title">What do I do if I have forgotten my password? </a>
                                    </div>
                                    <div id="faqSellerQ-22" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>As a registered customer, you can recover your password <a href="#" data-toggle="modal" data-target="#forgotCredentials" data-dismiss="modal">here</a>.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-23">
                                        <a class="card-title">How do I pay my invoice?</a>
                                    </div>
                                    <div id="faqSellerQ-23" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>After your registration, you will receive an invoice from us by e-mail. Bank transfer, credit card or PayPal can settle the invoice. We also offer <a href="login.php">direct debit mandate</a>.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-24">
                                        <a class="card-title">I want to cancel my contract. How do I do that?</a>
                                    </div>
                                    <div id="faqSellerQ-24" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>Please send us your cancellation in writing by fax or mail to: <a href="contact.php">Contact</a>.<br>We are happy to process e-mails - but please make sure that you receive a cancellation confirmation from us.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-25">
                                        <a class="card-title">I found a violation. What can I do?</a>
                                    </div>
                                    <div id="faqSellerQ-25" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>Please <a href="contact.php">contact</a> us immediately so that we can investigate the matter and take further action. Should an advertisement violate our <a href="#">marketplace</a> rules, you can also forward your complaint to us via the “Report“ button in the respective advertisement.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-26">
                                        <a class="card-title">Why do the advertisements show a watermark/letter or number code?</a>
                                    </div>
                                    <div id="faqSellerQ-26" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>Data protection is very important to us. To prevent data theft, the watermark and/or code is entered in the text.</p>
                                        </div>
                                    </div>
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#faqSeller" href="#faqSellerQ-27">
                                        <a class="card-title">Any other questions? </a>
                                    </div>
                                    <div id="faqSellerQ-27" class="collapse" data-parent="#faqSeller">
                                        <div class="card-body">
                                            <p>Our support will be happy to help you. You can contact the Helpdesk with +1 312 94 04 581 or by mail to <a href="mailto:info@machinepilot.com">info@machinepilot.com</a>.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        var url = "http://localhost/Maschine Pilot?lang=";
    </script>
<?php include('footer.php'); ?>