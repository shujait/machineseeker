<?php
  foreach($result as $val){
						?>
                        <!-- Dealer Start -->
                        <div class="col-md-12">
                            <div class="cpl-post">
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 gallery">
										<?php
								        if($val->CoverImage){
										?>
                                        <div>
                                            <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>"  title="<?php echo $val->Title; ?>"><img src="<?php echo base_url($val->CoverImage); ?>" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                                        </div>
										<?php
											  }else {?> 
										 <div class="img-thumbnail post-img">
                                            <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>" title="<?php echo $val->Title; ?>"><img src="<?php echo base_url(); ?>uploads/dummy.jpg" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                                        </div>
										<?php } ?>
                                    </div>
                                    <div class="col-lg-8 col-md-12 cpl-post-content">
                                        <div class="row">
                                            <div class="col-md-10">
                                                <h2>
                                                    <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>">
                                                        <span class="pro-name"><?php echo $val->Title; ?></span>
                                                    </a>
                                                </h2>
                                            </div>
                                        </div>
                                        <div class="row cpl-posy-col">
                                            <div class="col-md-6">
                                                <div class="pro-country">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    <span class="pro-value"> <?php echo $val->StreetAddress; ?></span>
                                                    <!-- <div class="flag-wrapper">
                                                        <span href="#" class="flag flag-icon-background flag-icon-tr" title="ad"></span>
                                                    </div> -->
                                                </div>
												<div class="flag-wrapper">
                                                <span class="pro-value"> <?php echo $val->PostalCode; ?></span>
													</div>
												  <div class="pro-country">
                                                <span class="pro-value"> <?php echo $val->Country; ?></span>
													</div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-12 cpl-btn-wrap">
                                        <div class="row flex-lg-row-reverse">
                                            <div class="col-lg-6 col-md-12">
                                                <a href="<?php echo friendly_url('dealer-listing/',$val->Title.' '.$val->UserID); ?>" class="btn btn-primary"><?php echo lang('view'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						<?php } 
							if($pages){
						echo $pages; 
							}
