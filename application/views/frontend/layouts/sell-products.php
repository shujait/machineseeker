<?php if($sell_product){ ?>
<section class="selling-pro-sec">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <div class="section-heading">
                    <h2><?php echo lang('latest_entries'); ?></h2>
                </div>
            </div>
        </div>
        <div class="row sp-row">
			<?php
			   foreach($sell_product as $val){
				 $val->Title= special_characterDe($val->Title); 
                                 $images = getImage(array('FileID'=>$val->MachineID, 'ImageType'=>'MachineFile'));
                                    if($val->FeaturedImage != '')
                                    {
                                        $img = ($val->IsImport ? $value->FeaturedImage : base_url($val->FeaturedImage));
                                    }
                                    else
                                    {
								        if($images){
										foreach($images as $k => $image){
									     $ext = pathinfo($image['ImageName'], PATHINFO_EXTENSION);
										 if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
										 $img = $val->IsImport?$image['ImageName']:base_url($image['ImageName']);
										 	}else {

                                             if($this->session->userdata['lang'] =="en") {
                                                 $img = base_url() . 'uploads/EN-Maschinenpilot-1200.png';
                                             }else{
                                                 $img = base_url() . 'uploads/Maschinenpilot-1200.png';
                                             }
										}
										  }  
										}else {
                                            if($this->session->userdata['lang'] =="en") {
                                                $img = base_url() . 'uploads/EN-Maschinenpilot-1200.png';
                                            }else{
                                                $img = base_url() . 'uploads/Maschinenpilot-1200.png';
                                            }
										}
                                    }
				   //echo $val->CategoryID;exit;
				$category =  getValue('categories_text',array('CategoryID' => $val->CategoryID,'SystemLanguageID' => $default_lang->SystemLanguageID));

                if(empty($category) || $category->Title == '' ){

                    $def = getDefaultLanguageByDefault();
                    $category =  getValue('categories_text',array('CategoryID' => $val->CategoryID,'SystemLanguageID' => $def->SystemLanguageID));

                }
              //  print_rm($category);
			?>
            <div class="col-md-4">
                <div class="pro-img"><a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>"><img src="<?php echo $img; ?>" alt="" width="1920" height="700" class="img-fluid"></a></div>
                <div class="pro-desc">
                    <h3><a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>"><?php echo $val->Title; ?></a></h3>
                    <p><a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>"><?php echo $category->Title; ?></a></p>
                </div>
            </div>
			<?php } ?>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <?php
                    if(!($this->session->userdata('admin'))){
                        $link = base_url('tarife');
                    }else{
                        $link = base_url('cms/dashboard');
                    }
                ?>
                <a href="<?= $link; ?>" class="btn btn-block btn-secondary"><?php echo lang('sell-machine'); ?></a>
            </div>
        </div>
    </div>
</section>
<?php } ?>