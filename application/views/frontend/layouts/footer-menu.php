<footer class="footer">
    <style type="text/css">
        .CookieDeclaration {
            display: none;
        }

    </style>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <?php 
                    
                        foreach($footer as $val){
                            $menu =  json_decode($val->Menu);
                    ?>
                    <div class="col-md-3 col-sm-12">
                        <h5 class="widget-title"><?php echo $val->Title; ?></h5>
                        <ul class="footer-menu">
                            <?php
                                        foreach ($menu as $li) {
                                            if($li->parent == 0)
                                                echo Menu($menu,$li->id,$li->menutype,0,array('li' =>'','a' => ''),$li);
                                        }
                                    ?>
                    </div>
                    <?php 
                        }
                    ?>
                </div>
                
                <div class="col-md-3">

                <?php if($site_setting->FacebookUrl || $site_setting->LinkedInUrl || $site_setting->BlogUrl || $site_setting->TwitterUrl || $site_setting->InstagramUrl){ ?>
                    <div class="social">
                        <?php if($site_setting->FacebookUrl){ ?>
                        <a href="<?= $site_setting->FacebookUrl ?>" target="_blank" class="icon"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <?php }  if($site_setting->InstagramUrl){ ?>
                        <a href="<?= $site_setting->InstagramUrl ?>" target="_blank" class="icon"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <?php }  if($site_setting->LinkedInUrl){ ?>
                        <a href="<?= $site_setting->LinkedInUrl ?>" target="_blank" class="icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        <?php }  if($site_setting->TwitterUrl){ ?>
                        <a href="<?= $site_setting->TwitterUrl ?>" target="_blank" class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <?php }  if($site_setting->BlogUrl){ ?>
                        <a href="<?= $site_setting->BlogUrl ?>" target="_blank" class="icon"><i class="fa fa-rss" aria-hidden="true"></i></a>
                        <?php } ?>
                    </div>
                <?php } ?>
                <!-- <?php //if($site_setting->FacebookUrl or $site_setting->TwitterUrl or $site_setting->LinkedInUrl or $site_setting->GoogleUrl){ ?>

                    <div class="col-md col-sm-12">
                        <h5 class="widget-title"><?php echo lang('follow-us'); ?></h5>
                        <ul class="footer-menu ft-sm-links">
                            <?php //if($site_setting->FacebookUrl){ ?>
                            <li id="social-fb"><a href="<?php echo $site_setting->FacebookUrl; ?>" target="_blank"><i class="fa fa-facebook-square fa-2x"></i> <span><?php echo lang('FacebookUrl'); ?></span></a></li>
                            <?php //}  if($site_setting->TwitterUrl){ ?>
                            <li id="social-tw"><a href="<?php echo $site_setting->TwitterUrl; ?>" target="_blank"><i class="fa fa-twitter-square  fa-2x"></i> <span><?php echo lang('TwitterUrl'); ?></span></a></li>
                            <?php //}  if($site_setting->LinkedInUrl){ ?>
                            <li id="social-gp"><a href="<?php echo $site_setting->LinkedInUrl; ?>" target="_blank"><i class="fa fa-linkedin fa-2x"></i> <span><?php echo lang('LinkedInUrl'); ?></span></a></li>
                            <?php //}  if($site_setting->GoogleUrl){ ?>
                            <li id="social-gp"><a href="<?php echo $site_setting->GoogleUrl; ?>" target="_blank"><i class="fa fa-google fa-2x"></i> <span><?php echo lang('GoogleUrl'); ?></span></a></li>
                            <?php //} ?>
                        </ul>
                    </div>
                <?php //} ?> -->
                </div>
            </div>

        </div>
    </div>
    <div class="footer-mid">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h5 class="widget-title"><?php echo lang('web-name'); ?></h5>
                    <?php 
					      $term = getPages(array('a.PageID'=> '12'),'row');
						  $privacy = getPages(array('a.PageID'=> '14'),'row');		
					?>
                    <p><?php echo lang('tag-line'); ?><a href="<?php echo base_url().$term->Url; ?>"><?php echo $term->Title; ?></a> <?php echo lang('and'); ?> <a href="<?php echo base_url().$privacy->Url; ?>"><?php echo $privacy->Title; ?></a>.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-btm">
        <div class="container">
            <div class="row">
                <div class="col-md">
                    <p><?php echo lang('Copyright'); ?> &copy; <?php echo date('Y'); ?> - <?php echo lang('all-right-reserved'); ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- this line comment because some error show in footer -->
    <!--  <script id="CookieDeclaration" src="https://consent.cookiebot.com/1de5fbf4-2de4-4b30-975b-bd5594131b8a/cd.js" type="text/javascript" async></script> -->

</footer>
