<div class="modal fade" id="requestProduct" role="dialog" aria-labelledby="MoneyBackModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel"><?php echo lang('submit-request'); ?></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><?php echo lang('request-all-dealers'); ?></p>
                <p><?php echo lang('request-customer-note'); ?></p>
                <hr>
				 <form action="<?php echo base_url(); ?>send-inquiry-all-dealer" method="post" onsubmit="return false;" class="inquiry-form form_data">
                    <div class="row">
                        <div class="col-md-12">
                          <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"  data-toggle="modal" data-target="#categoryModal" ><?php echo lang('select_category');?></button>
                        </div>
						<input type="hidden" value="" required name="category">
						<br />
						<br />
						<br />
						<div class="clearfix"></div>
                        <div class="col-md-12" id="requestProForm">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="RPcompanyName"><?php echo lang('company_name'); ?></label>
                                    <input type="text" name="company_name" id="RPcompanyName" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="RPEmail"><?php echo lang('email'); ?></label>
                                    <input type="email" name="email" id="RPEmail" required class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label for="RPFirstName"><?php echo lang('first_name'); ?></label>
                                    <input type="text" name="first_name" id="RPFirstName" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="RPlastName"><?php echo lang('last_name'); ?></label>
                                    <input type="text" name="last_name" id="RPlastName" class="form-control" required> 
                                </div>
                                <div class="col-md-12">
                                    <label for="StreetAddress"><?php echo lang('street_address'); ?></label>
                                    <input type="text" name="street_address" id="StreetAddress1" class="form-control" onKeyPress="initMap();" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="RPpostcodeTown"><?php echo lang('postcode'); ?></label>
                                    <input type="text" name="postcode" id="RPpostcodeTown" class="form-control" required> 
                                </div>
                                <div class="col-md-6">
                                    <label for="RPCity"><?php echo lang('city'); ?></label>
                                    <input type="text" name="city" id="RPCity" class="form-control" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="PhoneNumber"><?php echo lang('ph'); ?></label>
                                    <div class="input-group">
                                        <!-- <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <div class="flag-wrapper">
                                                <a href="#" class="flag flag-icon-background flag-icon-<?php //echo getCountryBYIP(@$_SERVER['REMOTE_ADDR']); ?>" title="ad" id="ad"></a>
                                            </div>
                                        </button> -->
                                        <input type="tel" name="phone" id="PhoneNumberInquiry" class="form-control" required>
                                        <input type="hidden" name="CountryCodeInquiry" id="CountryCodeInquiry" value="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="Fax"><?php echo lang('fax'); ?></label>
                                    <div class="input-group">
                                        <!-- <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <div class="flag-wrapper">
                                                <a href="#" class="flag flag-icon-background flag-icon-<?php //echo getCountryBYIP(@$_SERVER['REMOTE_ADDR']); ?>" title="ad" id="ad"></a>
                                            </div>
                                        </button> -->
                                        <input type="tel" name="fax" id="Fax" class="form-control" required> 
                                        <input type="hidden" name="CountryCodeFax" id="CountryCodeFax" value="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="d-none" for="Message"></label>
                                    <textarea name="message" id="Message" class="form-control" placeholder="Your Message" rows="5" cols="20" required></textarea>
                                </div>
                                <div class="col-md-12">
                                    <label><strong><?php echo lang('customer-reply'); ?>:</strong></label>
                                    <div class="form-group">
                                        <div class="radio">
                                            <input type="radio" name="replyBy" id="replyEmail" value="email" class="form-control">
                                            <label for="replyEmail"><?php echo lang('email'); ?></label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="replyBy" id="replyPhone" value="Phone" class="form-control">
                                            <label for="replyPhone"><?php echo lang('ph'); ?></label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="replyBy" id="replyFax" value="Fax" class="form-control">
                                            <label for="replyFax"><?php echo lang('fax'); ?></label>
                                        </div>
                                        <div class="radio">
                                            <input type="radio" name="replyBy" id="replyPost" value="Post" class="form-control">
                                            <label for="replyPost"><?php echo lang('post'); ?></label>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="col-md-12">
                                    <div class="checkbox">
                                        <input type="checkbox" name="reseller" id="reseller" value="Yes" class="form-control">
                                        <label for="reseller"><?php echo lang('i-am-dealer'); ?></label>
                                    </div>
									<br />
                                </div>
									<?php $con = confirmationBox();
										if($con){
										?>	
								<div class="col-md-12">
								    <label><strong><?php echo lang('privacy-policy'); ?>:</strong></label>	

                                   <div class="checkbox">
									<input type="checkbox" name="policy_read" id="policy" value="yes">
									<label for="policy"><?php echo $con->row()->Content; ?></label>
								  </div>
                                </div>
								
								<?php } ?>
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-secondary" value="<?php echo lang('send-inquiry');?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade product-modal" id="categoryModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('all-categories'); ?></h5>
                <button type="button" class="closed" data-dismiss="modal">&times;</button>
            </div>
			<br />
            <div class="modal-body" style="height: 300px; overflow-y: auto;">
                <ul class="tree-menu">
					<?php
						$category = array();
						$arr = getCategories($language,'categories.ParentID = 0');
						if($arr){
                        foreach($arr as $value){
						 $arr_child = getCategories($language,'categories.ParentID = '.$value->CategoryID);
					?>
						<li>
							<input type="radio" value="<?php echo $value->CategoryID;  ?>" data-title="<?php echo $value->Title; ?>" name="choose-category" class="category">
							<input type="checkbox" id="menu<?php echo $value->CategoryID; ?>"/><label for="menu<?php echo $value->CategoryID; ?>"><?php echo $value->Title; ?></label>
							<?php echo  MachineCategoryTree($arr_child, $language); ?>
						</li>
					<?php					
						   }	
						}
					?>
		        </ul>
			</div>
			<div class="pull-left text-right" style="padding: 10px;">
                <input type="submit" class="btn btn-primary waves-effect waves-light add-category" value="<?php echo lang('add'); ?>">
			</div>
        </div>
    </div>
</div>