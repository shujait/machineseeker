 	<div class="modal fade product-modal" id="sharefrndModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('share-to-friend'); ?></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
				<form action="<?php echo base_url(); ?>share-to-friend" method="post" onsubmit="return false;" class="inquiry-form form_data">
									<input type="hidden" name="machine_id" value="<?php echo $id; ?>">
                                    <div class="row">
                                        <div class="col-md-6">
                                         <input type="text" name="from_name" placeholder="<?php echo lang('your').' '.lang('name'); ?>" required class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                         <input type="email" name="from_email" placeholder="<?php echo lang('your').' '.lang('email'); ?>" required class="form-control">
                                        </div>
										 <div class="col-md-6">
                                         <input type="email" name="to_email" placeholder="<?php echo lang('recipient').' '.lang('email'); ?>" required class="form-control">
                                        </div>
                                        <div class="col-md-12">
                                    <textarea name="message" placeholder="<?php echo lang('message'); ?>" cols="40" rows="9" class="form-control"></textarea>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="submit" class="btn <?php echo $class; ?>" value="<?php echo lang('share'); ?>">
                                        </div>
                                    </div>
                                </form>
				 </div>
        </div>
    </div>
</div>