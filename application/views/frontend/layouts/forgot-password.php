<div class="modal fade" id="forgotCredentials" role="dialog" aria-labelledby="forgotCredentials" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"><?php echo lang('forgot-login-credential'); ?></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <p><?php echo lang('forgot-password-note'); ?></p>
                <p><?php echo lang('forgot-password-link-note.'); ?></p>
                <div class="row">
                    <div class="col-md-8 col-sm-12 mb-3">
                        <form action="<?php echo base_url('cms/account/forgot_password'); ?>" method="post" onsubmit="return false;" class="form_data">
                            <div class="form-group input-group">
                                <input type="email" name="email" class="form-control" required>
                                <input type="submit" class="btn btn-secondary" value="<?php echo lang('send-email'); ?>">
                            </div>
                        </form>
                    </div>
                </div>
                <p><?php echo lang('contact-note'); ?></p>
            </div>
        </div>
    </div>
</div>