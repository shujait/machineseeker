                    <aside class="all-categories">
                        <h2><?php echo lang('all-categories'); ?></h2>
                        <div class="show-all-cats">
                            <p><span class="more-listings"><span class="text"><?php echo lang('show-all-listing'); ?></span></span> 
								<?php $cat_count = getCategory([],'num_rows');
									echo show_counter($cat_count); 
									?>
							</p>
                        </div>
                        <ul class="list-group category-list">
							<?php $rr = getCategory([]);
								if($rr){
									$n = 0;
								foreach($rr as $val){
									$n++;
									$cat_mc_count = get_category_dealer('find_in_set("'.$val->CategoryID.'", Category) <> 0');		
								?>
								<li class="list-group-item d-flex justify-content-between align-items-center <?php echo apply_class(ucwords($val->Title),@$cat_name); ?>" data-id="<?php echo $n; ?>">
									<a href="<?php echo friendly_url('dealer/',$val->Slug.' '.$val->CategoryID); ?>"><?php echo $val->Title; ?></a> 
										<?php
											echo show_counter($cat_mc_count); 
										?>
								</li>
								<?php } } ?>
                        </ul>
                        <div class="show-all-dealers">
							<p>							
                            <span class="btn btn-block btn-primary text-wrap more-listings"><span class="text"><?php echo lang('show-all-listing'); ?></span>
								<?php 
									echo show_counter($cat_count); 
								?>
								</span>
						      </p>
                        </div>
                    </aside>