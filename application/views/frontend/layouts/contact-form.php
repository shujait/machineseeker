<form class="form-horizontal contact-us form_data" action="<?= base_url('contact-us/email'); ?>" method="post" onsubmit="return false;">
    <div class="row">
        <div class="col-md-6">
            <div class="form-label-group">
                <label class="card-title" for="company_name">
                    <?= lang('company_name'); ?> 
                    <span class="red">*</span>
                </label>
                <input type="text" id="company_name" class="form-control" name="company_name" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-label-group">
                <label class="card-title" for="title">
                    <?= lang('title'); ?> 
                    <span class="red">*</span>
                </label>
                <select id="title" class="form-control" name="title" required>
                    <option value=""></option>
                    <option value="Mr"><?= lang('Mr'); ?></option>
                    <option value="Ms"><?= lang('Ms'); ?></option>
                    <option value="Dr"><?= lang('Dr'); ?></option>
                </select>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="form-label-group">
                <label class="card-title" for="name">
                    <?= lang('name'); ?> 
                    <span class="red">*</span>
                </label>
                <input type="text" id="name" class="form-control" name="name" required>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-label-group">
                <label class="card-title" for="email">
                    <?= lang('email'); ?> 
                    <span class="red">*</span>
                </label>
                <input type="email" name="email" class="form-control" id="email" required>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="form-label-group">
                <label class="card-title" for="street_address">
                    <?= lang('street_address'); ?> 
                </label>
                <input type="text" id="street_address" class="form-control" name="street_address">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="form-label-group">
                <label class="card-title" for="postcode">
                    <?= lang('postcode'); ?> 
                </label>
                <input type="text" id="postcode" class="form-control" name="postcode">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-label-group">
                <label class="card-title" for="city">
                    <?= lang('city'); ?> 
                </label>
                <input type="text" id="city" class="form-control" name="city">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-6">
            <div class="form-label-group">
                <label class="card-title" for="phone">
                    <?= lang('ph'); ?> 
                </label>
                <input type="number" id="phone" class="form-control MobileNumber" name="phone">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-label-group">
                <label class="card-title" for="fax">
                    <?= lang('fax'); ?> 
                </label>
                <input type="number" id="fax" class="form-control MobileNumber" name="fax">
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="form-label-group">
                <label class="card-title" for="message">
                    <?= lang('message'); ?> 
                </label>
                <textarea name="message" id="message" class="form-control" rows="5" cols="20"></textarea>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="form-label-group">
                <button type="submit" class="btn btn-primary" name="send"><?= lang('Send'); ?></button> <span class="red">* <?= lang('required_field');?></span>
            </div>
        </div>
    </div>
</form>