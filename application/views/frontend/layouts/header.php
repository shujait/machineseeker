<!doctype html>
<?php
if(isset($_GET['lang']) and !empty($_GET['lang'])){
	$this->session->set_userdata('lang_dir', $_GET['lang']);
}else{
	$this->session->set_userdata('lang_dir','ltr');
}
$lang_dir = $this->session->userdata('lang_dir');
//			For English Add Class in Body "eng"
//			For Arabic Add Class in Body "arb"
?>

<html lang="<?php echo $lang_dir; ?>" dir="<?php echo $lang_dir; ?>">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        <?php 
            if(!isset($cat_name)){
                if(!empty($result->Description)){
                    $strdes=strip_tags(  $result->Description);
                    if(strlen($strdes)>155){
                        $strdes= substr($strdes, 0, 155);;
                    }
                }
                echo @$title; 
            }else{
                echo $cat_name;
            }
        ?>
    </title>
    <meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="no-cache">
	<meta http-equiv="Expires" content="-1">
	<meta http-equiv="Cache-Control" content="no-cache">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo (isset($strdes) ?  ($strdes) :(isset($strdes)?$meta_desc:''));?>">
    <meta name="author" content="<?php echo  !empty($site_setting->OnwerName)?$site_setting->OnwerName:'Artur Gerlitz'; ?> ">
    <meta name="revisit-after" content="<?php echo (isset($crawl_after) ? $crawl_after : '');?> days"> 
    <meta name="robots" content="<?php echo (isset($follow) ? $follow : ''); ?> <?php echo isset($index)?','.$index:''; ?>">	

    <meta name="google-signin-client_id" content="1025878311393-4f7jcgujvkvioqmpng2691o0omnleohv.apps.googleusercontent.com">

    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/frontend/images/favicon2.png">
    <link href="<?php echo base_url();?>assets/frontend/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/css/jcarousel.connected-carousels.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/css/flag-icon.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/css/rrssb.css?<?php echo  CSS_VERSION_NUMBER;?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/css/<?php echo ($lang_dir=='rtl' ? 'rtl' : 'ltr'); ?>.css?<?php //echo  CSS_VERSION_NUMBER;?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/frontend/css/responsive.css?<?php echo  CSS_VERSION_NUMBER;?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/backend/css/google-roboto-300-700.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/backend/css/tree-menu.css?<?php echo  CSS_VERSION_NUMBER;?>" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/backend/plugins/select2/css/select2.min.css?<?php echo  CSS_VERSION_NUMBER;?>" rel="stylesheet" type="text/css" media="all">

	<link href="<?php echo base_url();?>assets/frontend/css/alert.css?<?php echo  CSS_VERSION_NUMBER;?>" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url();?>assets/frontend/js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/html5lightbox.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/plugin/inputmask/inputmask.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/plugin/inputmask/jquery.inputmask.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/plugin/lazyloading/lazyloading.min.js" type="text/javascript"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PJDK2TZ');</script>
    <!-- End Google Tag Manager -->
        <?php 
   if($view == 'frontend/detail' && ENVIRONMENT == 'production')
    {
        $UserDetail = get_user_info(array('a.UserID'=>$result->CreatedBy));
    ?>
      
    <link rel="canonical" href="<?php echo base_url('detail/' . $URL_Title); ?>" />     
    <meta name="keywords" content="<?php echo (isset($result->keywords) ? $result->keywords : $key_words  );?>" />
    <meta property="og:locale" content="de_DE" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?php echo $result->Title; ?>" />
    <meta property="og:description" content="<?php echo strip_tags(  $result->Description);?>" />
    <meta property="og:url" content="<?php echo base_url('detail/' . $URL_Title); ?>" />
    <meta property="og:site_name" content="<?php echo $site_setting->SiteName;?> " />
    <meta property="article:tag" content="<?php echo isset($result->keywords) ? $result->keywords : $result->Title; ?>" />
    <meta property="article:section" content="<?php echo $site_setting->SiteName.','.$result->Title.','.$result->keywords; ?>" />
    <meta property="article:author" content="<?php echo  $site_setting->OnwerName; ?>" />
    <meta property="article:published_time" content="<?php echo date('Y-m-d H:i:s', strtotime($result->CreatedAt)); ?>" />
    <meta property="article:modified_time" content="<?php echo date('Y-m-d H:i:s', strtotime($result->UpdatedAt)); ?>" />
    <meta property="og:updated_time" content="<?php echo date('Y-m-d H:i:s', strtotime($result->UpdatedAt)); ?>" />
    <meta property="og:image" content="<?php echo base_url($result->FeaturedImage); ?>" />
    <meta property="og:image:secure_url" content="<?php echo base_url($result->FeaturedImage); ?>" />
    <meta property="og:image:width" content="1173" />
    <meta property="og:image:height" content="792" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:description" content="<?php echo strip_tags(  $result->Description);?>" />
    <meta name="twitter:title" content="<?php echo $result->Title; ?>" />
    <meta name="twitter:image" content="<?php echo base_url($result->FeaturedImage); ?>" />
    <meta property="og:title" content="<?php echo $result->Title; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo base_url('detail/' . $URL_Title); ?>"/>
    <meta property="og:site_name" content="<?php echo $site_setting->SiteName;?> "/>
    <meta property="og:description" content="<?php echo strip_tags(  $result->Description);?> "/>
    <meta property="og:image" content="<?php echo base_url($result->FeaturedImage); ?>"/>
    
    <script type="application/ld+json">
                 
        {"@context":"https://schema.org",
            "@graph":[
                {"@type":"WebSite",
                    "@id":"<?php echo base_url();?>#website",
                    "url":"<?php echo base_url();?>",
                    "name":" <?php echo $site_setting->SiteName;?> ",
                    "publisher":{"@id":"<?php echo base_url();?>#organization"},
                    "potentialAction":{"@type":"SearchAction",
                    "target":"<?php echo base_url();?>/?s={search_term_string}",
                    "query-input":"required name=search_term_string"}
                } ,
                {"@type":"WebPage",
                    "@id":"https://www.maschinenpilot.de/detail/kitamura-3-xif-52/#webpage", 
                    "url":"<?php echo base_url('detail/' . $URL_Title); ?>",
                    "inLanguage":"en-DE","name":"<?php echo $result->Title; ?>",
                    "isPartOf":{"@id":"<?php echo base_url();?>#website"},
                    "image":{"@type":"ImageObject",
                    "@id":"<?php echo base_url($result->FeaturedImage); ?>#primaryimage",
                    "url":"<?php echo base_url($result->FeaturedImage); ?>",
                    "width":1024,
                    "height":576,
                    "caption":"?"},
                    "primaryImageOfPage":{"@id":"<?php echo base_url($result->FeaturedImage); ?>#primaryimage"},
                    "datePublished":"<?php echo date('Y-m-d H:i:s', strtotime($result->CreatedAt)); ?>",
                    "dateModified":"<?php echo date('Y-m-d H:i:s', strtotime($result->UpdatedAt)); ?>"
                },
                
                {"@type":"Article",
                    "@id":"<?php echo base_url('detail/' . $URL_Title); ?>/#article",
                    "isPartOf":{"@id":"<?php echo base_url('detail/' . $URL_Title); ?>#webpage"},
                    "author": {
                        "@type": "Person",
                        "name": "<?php echo $UserDetail->UName ;?>"
                        },
                    "headline":"<?php echo $result->Title; ?>","datePublished":"<?php echo date('Y-m-d H:i:s', strtotime($result->CreatedAt)); ?>",
                    "dateModified":"<?php echo date('Y-m-d H:i:s', strtotime($result->UpdatedAt)); ?>",
                    "commentCount":0,
                    "mainEntityOfPage":{
                        "@type": "WebPage",
                        "@id":"<?php echo base_url($result->FeaturedImage); ?>#webpage"
                    },
                    "publisher" : {
                        "@type" : "Organization",
                        "name" : "<?php echo  ($result->Title); ?>",
                        "logo": {
                          "@type": "ImageObject",
                          "url": "<?php echo base_url($result->FeaturedImage); ?>"
                    }},
                    "articleSection":"<?php echo ($result->Model).','.$result->Manufacturer.','.$result->ManufacturerYear; ?>",
                    "articleBody" : "<?php echo strip_tags(   $result->Description);?>",
                    "image":{"@type":"ImageObject",
                    "@id":"<?php echo base_url($result->FeaturedImage); ?>#primaryimage",
                    "url":"<?php echo base_url($result->FeaturedImage); ?>",
                    "width":1024,
                    "height":576,
                    "caption":"?"}}
                ,
                {"@type":"Person",
                        "@id":"<?php echo base_url($result->FeaturedImage); ?>#author",
                         "name":"<?php echo $UserDetail->UName ;?>","sameAs":[]
                }
            
            ]} 

            </script>

    <?php 
    }
    ?>

	<script>
	 var base_url = '<?php echo base_url(); ?>';
	 var controller =  '<?php echo $this->router->fetch_class(); ?>';
	 var method = '<?php echo $this->router->fetch_method(); ?>';
	 var delete_msg = '<?php echo lang('are_you_sure');?>';
     var Currency = '<?php echo $site_setting->Currency; ?>';
	 var CurrencyPosition = '<?php echo $site_setting->CurrencyPosition; ?>';
	 var ThousandSeparator = '<?php echo $site_setting->ThousandSeparator; ?>';
     var DecimalSeparator = '<?php echo $site_setting->DecimalSeparator; ?>';
	 var NumberDecimals = '<?php echo $site_setting->NumberDecimals; ?>';
	 var show_all = '<?php echo lang('show-all-listing'); ?>';
	 var show_less = '<?php echo lang('show-less-listing'); ?>';	
	 var user_id = '<?php echo @$this->session->userdata['admin']['UserID']; ?>';
     var cookie_heading = '<?php echo lang('cookie_heading'); ?>';
	 var cookie_description = '<?php echo lang('cookie_description'); ?>';		
     var cookie_accpet_btn = '<?php echo lang('cookie_accpet_btn'); ?>';
	 var read_more = '<?php echo lang('read-more'); ?>'	
	
	</script>
    <style type="text/css">
      .alert.alert-danger {
        display: block !important;
        margin: 15px;
        position: fixed;
        transition: all 0.5s ease-in-out 0s;
        z-index: 9999999;
        bottom: 20px !important;
        right: 28px !important;
        left: auto !important;
        max-height: fit-content;
        bottom: 0 !important;
        top: auto !important;
    }
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    /* Firefox */
    .MobileNumber {
        -moz-appearance: textfield;
    }
    </style>
 <!--  <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="1de5fbf4-2de4-4b30-975b-bd5594131b8a" data-blockingmode="auto" type="text/javascript"></script> -->
</head>
<body class=" <?php echo $lang_dir; ?>">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDK2TZ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div id="wrapper">
        <?php $this->load->view('frontend/layouts/menu'); ?>
        <main class="main">