<?php if($top_list){?>
	<aside class="top-categories">
		<h2><?php echo lang('top-categories'); ?></h2>
		<ul class="list-group">
			
			<?php if($top_list){
			foreach($top_list as $val){
			?>
			<li class="list-group-item d-flex justify-content-between align-items-center">
				<a href="<?php echo friendly_url('kategorie/',$val->Slug); ?>"><?php echo $val->Title; ?></a>
				<?php echo show_counter($val->counter); ?>
			<?php } } ?>
		</ul>
	</aside>
<?php } ?>
<aside class="all-categories">
	<h2><?php echo lang('all-categories'); ?> 
		<!--for toggle button  <span class="side-toggle-bars"><button onclick="sidebarmenu1()" class="side-toggle1"><i class="fa fa-bars"></i></button></span>-->
	</h2>

	<!-- for toggle button <div class="show-all-cats" id="side-menu1">-->
	<div class="show-all-cats"  >
		<p><span class="more-listings"><span class="text"><?php echo lang('show-all-listing'); ?></span></span> 
			<?php
				//$active_category="`categories`.`CategoryID` IN (SELECT `CategoryID` FROM `machines` where `machines`.`IsActive`=1)";
				//$cat_count = getCategories($language,(!empty($categoryID))?'categories.ParentID ='.$categoryID:false,$limit = false,$start = 0,$categorytype=false,$active_category);
				$CategoryDetails = getCategroyThirdLevelCount(!empty($categoryID)?$categoryID:0);
				//print_rm($CategoryDetails);
				/*if($cat_count){
					$cat_count =  count($cat_count);
				}else{
					$cat_count =  count(0);
				}*/
				echo show_counter($CategoryDetails['totalCategoryCount']); 
				?>
		</p>
	</div>
	<ul class="list-group category-list">
		<?php 
			/*$active2_category="`categories`.`CategoryID` IN (SELECT `CategoryID` FROM `machines` where `machines`.`IsActive`=1)";
			$rr = getCategories($language,(!empty($categoryID))?'categories.ParentID ='.$categoryID:false,$limit = false,$start = 0,$categorytype=false,$active2_category);
			if($rr){
				$n = 0;
			foreach($rr as $val){
				$n++;
				$cat_mc_count = get_machine_row(array('a.CategoryID' => $val->CategoryID));		
				$mvclass='';
				if(!$cat_mc_count){
					$mvclass='hideLiForMobile ';
				}
		?>
			<li class="list-group-item d-flex justify-content-between align-items-center <?php echo $mvclass; echo apply_class(ucwords($val->Title),@$cat_name); ?>" data-id="<?php echo $n; ?>">
				<a href="<?php echo friendly_url('category/',$val->Slug.' '.$val->CategoryID); ?>"><?php echo $val->Title; ?></a> 
					<?php
						echo show_counter($cat_mc_count); 
					?>
			</li>
			<?php } } */?>
		<?php 
			if($CategoryDetails['subCategoryData'])
			{

				foreach ($CategoryDetails['subCategoryData'] as $key => $value) {
					$MachineCount = $value['CategoryMachineCount'];
					$value = $value['subCategoryDetail'];
					if($MachineCount > 0)
					{
		?>
						<li class="list-group-item d-flex justify-content-between align-items-center <?php echo $mvclass; echo apply_class(ucwords($value['Title']),@$cat_name); ?>" data-id="<?php echo $n; ?>">
							<a href="<?php echo friendly_url('kategorie/',$value['Slug']); ?>"><?php echo $value['Title']; ?></a>
								<?php
									echo show_counter($MachineCount); 
								?>
						</li>
		<?php 		
					}
				}
			}
		?>
	</ul>
		<div class="show-all-dealers">
		<p>							
		<span class="btn btn-block btn-primary text-wrap more-listings"><span class="text"><?php echo lang('show-all-listing'); ?></span>
			<?php 
				echo show_counter($CategoryDetails['totalCategoryCount']); 
			?>
			</span>
			</p>
	</div>
	<!--for toggle button </div>-->
</aside> 
<script>
function sidebarmenu1() {
  var x = document.getElementById("side-menu1");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}
</script>