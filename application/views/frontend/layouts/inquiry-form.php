 <form action="<?php echo base_url(); ?>send-inquiry" method="post" onsubmit="return false;" class="inquiry-form form_data">
									<input type="hidden" name="machine_id" value="<?php echo $id; ?>">
                                    <div class="row">
                                        <div class="col-md-6">
                                         <input type="text" name="name" placeholder="<?php echo lang('name'); ?>" required class="form-control"  >
                                        </div>
                                        <div class="col-md-6">
                                         <input type="text" name="company_name" placeholder="<?php echo lang('company_name'); ?>" class="form-control" required >
                                        </div>
                                        <div class="col-md-6">
                                         <input type="tel" name="phone" id="phoneInquiryForm" placeholder="<?php echo lang('ph'); ?>" class="form-control" required >
                                        <input type="hidden" name="CountryCodeInquiryForm" id="CountryCodeInquiryForm" value="">
                                        </div>
                                        <div class="col-md-6">
                                           <input type="text" name="street_address" id="StreetAddress1" placeholder="<?php echo lang('street_address'); ?>" class="form-control" onKeyPress="initMap();">
                                        </div>
                                        <div class="col-md-6">
                                         <input type="email" name="email" placeholder="<?php echo lang('email'); ?>" class="form-control" required >
                                        </div>
                                        <div class="col-md-6 input-group">
                                            <input type="text" name="postcode" placeholder="<?php echo lang('postcode'); ?>" class="form-control">
                                            <!-- <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <div class="flag-wrapper">
                                                    <a href="#" class="flag flag-icon-background flag-icon-<?php //echo getCountryBYIP(@$_SERVER['REMOTE_ADDR']); ?>" title="ad" id="ad"></a>
                                                </div>
                                            </button> -->
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" name="city" placeholder="<?php echo lang('city'); ?>" class="form-control" required >
                                        </div>
                                        <div class="col-md-12">
                                    <textarea name="message" placeholder="Your message" cols="40" rows="9" class="form-control"><?php echo
										str_replace('[machine-name]',@$result->Title,lang('inquiry-desc')); ?>
                                    </textarea>
                                        </div>
                                        <!--<div class="col-md-12">
                                            <div class="checkbox">
                                                <input type="radio" name="dealer" value="I am a dealer" id="dealer" checked>
                                                <label for="dealer"><?php //echo lang('i-am-dealer'); ?></label>
                                            </div>
                                        </div>-->
                                        <div class="col-md-12">
                                            <div class="checkbox">
                                                <input type="checkbox" name="dealer" value="1" id="similarMachines">
                                                <label for="similarMachines"><?php echo lang('offer-machine'); ?></label>
                                                <div class="reveal-if-active">
                                                    <textarea name="similarmahines" placeholder="<?php echo lang('offer_for_similar_machine');?>" tabindex="110" data-name="" data-rules="bwords" rows="2" id="similarmahines" class="form-control"></textarea>
                                                    <div class="badge-light">
                                                        <div class="checkbox">
                                                            <p ><input type="checkbox" name="third_party_dealer" style="position: inherit;" value="1">
                                                            <!-- <button type="button" class="btn btn-primary float-right close-similar"><i class="fa fa-remove" aria-hidden="true"></i></button> -->
                                                           <?php echo lang('request-dealer'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
									<?php $con = confirmationBox();
										if($con){
										?>	
								<div class="col-md-12 p-white ">
								    <label><strong><?php echo lang('privacy-policy'); ?>:</strong></label>	
									<input type="checkbox" name="policy_read" id="policyread" value="yes">
									<label for="policyread"><?php echo $con->row()->Content;  ?></label>
								</div>
										<?php } ?>
                                        <div class="col-md-12">
                                            <input type="submit" class="btn <?php echo $class; ?>" value="<?php echo lang('send-inquiry'); ?>">
                                        </div>
                                    </div>
                                </form>