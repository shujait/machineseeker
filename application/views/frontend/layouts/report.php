 	<div class="modal fade product-modal" id="reportModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('report'); ?></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
				<form action="<?php echo base_url(); ?>report" method="post" onsubmit="return false;" class="inquiry-form form_data">
									<input type="hidden" name="machine_id" value="<?php echo $id; ?>">
                                    <div class="row">
                                        <div class="col-md-6">
                                         <input type="text" name="from_name" placeholder="<?php echo lang('your').' '.lang('name'); ?>"                                 class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                         <input type="email" name="from_email" placeholder="<?php echo lang('your').' '.lang('email'); ?>"                             class="form-control">
                                        </div>
										 <div class="col-md-12">
										 <div class="checkbox">
 										  <input type="radio" name="report_type" value="<?php echo lang('report-type1'); ?>" id="1">
											  <label for="1">
											 <?php echo lang('report-type1'); ?>
												  </label>
										  </div>
                                         </div>
                                        <div class="col-md-12">
                                        <div class="checkbox">
										  <input type="radio" name="report_type" value="<?php echo lang('report-type2'); ?>" id="2">
											  <label for="2">
												 <?php echo lang('report-type2'); ?>
												  </label>
										  </div>
                                        </div>
                                        <div class="col-md-12">
                                        <div class="checkbox">
										  <input type="radio" name="report_type" value="<?php echo lang('report-type3'); ?>" id="3">
											  <label for="3">
												  <?php echo lang('report-type3'); ?>
												  </label>
										  </div>
                                        </div>
                                        <div class="col-md-12">
                                        <div class="checkbox">
										  <input type="radio" name="report_type" value="<?php echo lang('report-type4'); ?>" id="4">
											  <label for="4">
												 <?php echo lang('report-type4'); ?>
												  </label>
										  </div>
                                        </div>
                                        <div class="col-md-12">
                                        <div class="checkbox">
										   <input type="radio" name="report_type" value="<?php echo lang('report-type5'); ?>" id="5">
											  <label for="5">
												  <?php echo lang('report-type5'); ?>
												  </label>
										  </div>
                                        </div>
                                        <div class="col-md-12">
                                        <div class="checkbox">
									      <input type="radio" name="report_type" value="<?php echo lang('report-type6'); ?>" id="6">
											  <label for="6">
												 <?php echo lang('report-type6'); ?>
												  </label>
										  </div>
                                        </div>
                                        <div class="col-md-12">
                                        <div class="checkbox">
											  <input type="radio" name="report_type" value="7" id="7">
											  <label for="7">
												 <?php echo lang('report-type7'); ?>
												  </label>
                                          <div class="reveal-if-active">
                                         <textarea name="message" placeholder="<?php echo lang('message'); ?>" cols="100" rows="5" class="form-control"></textarea>
                                                </div>
									      </div>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="submit" class="btn <?php echo $class; ?>" value="<?php echo lang('report'); ?>">
                                        </div>
                                    </div>
                                </form>
				 </div>
        </div>
    </div>
</div>