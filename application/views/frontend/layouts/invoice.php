<?php $site_setting = getSiteSetting();
      $d_language_data = getDefaultLanguageByDefault();
      //print_rm($user_data);

 ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice</title>
</head>

<body>
    <div style="font-family: arial; padding:50px;">
        <table cellpadding="0" cellspacing="0" style="width: 100%; font-size: 16px;line-height: 22px;">
            <tr>
                <td colspan="2" style="border-bottom: 1px solid #999999;"><img src="<?php echo FCPATH.'assets/logo1.png';?>"></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2" style="padding-top: 25px; font-size: 11px;"><?php echo $site_setting->SiteName;?> <?php echo $site_setting->OnwerName;?>, <?php echo $site_setting->Address;?>, <?php echo $site_setting->PostalCode;?> <?php echo $site_setting->City;?></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td style="padding-top: 15px; font-size: 14px;"><?php echo $user_data['FullName'];?><br><?php echo $user_data['CompanyName'];?><br><?php echo $user_data['StreetAddress'];?><br><br><?php echo $user_data['PostalCode'];?> <?php echo $user_data['City'];?></td>
                <td style="text-align: right; padding-top: 15px; font-size: 14px;">
                    <br><br><strong style="font-weight: bold; text-transform: uppercase;">Rechnung</strong> <br>Rechnungs-Datum: <?php echo date('d.m.Y',strtotime($user_data['PurchaseDate']));?><br>Rechnungs-Nr. R-<?php echo str_pad($user_data['PurchaseID'], 7, '0', STR_PAD_LEFT);?><br>Kunden-Nr. K-<?php echo str_pad($user_data['UserID'], 7, '0', STR_PAD_LEFT);?><br>
                </td>
            </tr>
            <tr>
                <td colspan="2"></td>
            </tr>
        </table>
        <br><br>

        <table cellpadding="5" cellspacing="0" style="width: 100%; font-size: 13px; line-height: 24px;" border="0">
            <tr style=" padding:5px;">
                <td colspan="2" width="260" style="padding:5px; padding: 5px; border-bottom: 1px solid #999999;">Leistungen</td>
                <td style="text-align: center; padding: 5px; border-bottom: 1px solid #999999; color: #000000;">Einzelpreis</td>
                <td style="text-align: right; padding: 5px; border-bottom: 1px solid #999999; color: #000000;">Anzahl</td>
                <td style="text-align: right; padding: 5px; border-bottom: 1px solid #999999; color: #000000;">Gesamtpreis</td>
            </tr>
            <?php $pack = getPackageDefault($user_data['PackageID']);
                $total = $user_data['Duration'] * $user_data['Rate'];

             ?>
            <tr>
                <td colspan="2" width="260">Paket: <?php echo $pack->Title;?> (Anzahl lnserate: <?php echo $user_data['Qty'];?>)<br>Zeitraum:<?php echo date('d.m.Y',strtotime($user_data['PurchaseDate'])); ?> - <?php echo date('d.m.Y',strtotime($user_data['ExpiryDate'])); ?><br>Preis pro Monat: <?php echo number_format($user_data['Rate'],2,',','.');?></td>
                <td style="text-align: center; padding: 2px;"><?php echo number_format($total,2,',','.'); ?>  €</td>
                <td style="text-align: right; padding: 2px;">1</td>
                <td style="text-align: right; padding: 2px;"><?php 
               
                echo number_format($total,2,',','.'); ?>  €</td>
            </tr>
            <?php //$amount = $total / 1.19; ?>
            <tr>
                <td colspan="2" width="260" style="border-top: 1px solid #999999;">&nbsp;</td>
                <td style="text-align: center; padding: 2px; border-top: 1px solid #999999;"></td>
                <td style="text-align: right; padding: 8px 2px; border-top: 1px solid #999999;">Zwischensumme</td>
                <td style="text-align: right; padding: 8px 2px; border-top: 1px solid #999999;"><?php echo number_format($total,2,',','.'); ?> €</td>
            </tr>
            <tr>
                <td colspan="2" width="260"></td>
                <td style="text-align: center; padding: 2px;"></td>
                <td style="text-align: right; padding: 8px 2px;"><?= lang('discount_amount') ?></td>
                <td style="text-align: right; padding: 8px 2px;"><?php echo number_format($user_data['Discount'],2,',','.'); ?>  €</td>
            </tr>
            <tr>
                <td colspan="2" width="260"></td>
                <td style="text-align: center; padding: 2px;"></td>
                <td style="text-align: right; padding: 8px 2px;">Zzgl. 19% MwSt.</td>
                <?php 
                    $tax = $total *  (19 / 100);
                    $g_total = $total + $tax;
                    $g_total = $g_total - $user_data['Discount'];
                 ?>
                <td style="text-align: right; padding: 8px 2px;"><?php echo number_format($tax,2,',','.'); ?>  €</td>
            </tr>
            <tr>
                <td colspan="2" width="260" style="border-top: 1px solid #999999; border-bottom: 1px solid #999999;"></td>
                <td style="text-align: center; padding: 2px; border-top: 1px solid #999999; border-bottom: 1px solid #999999;"></td>
                <td style="text-align: right; padding: 8px 2px; border-top: 1px solid #999999; border-bottom: 1px solid #999999;"><b>Rechnungsbetrag</b></td>
                <td style="text-align: right; padding: 8px 2px; border-top: 1px solid #999999; border-bottom: 1px solid #999999;"><b><?php echo number_format($g_total,2,',','.'); ?>  €</b></td>
            </tr>
        </table><br><br>
        <table cellpadding="0" cellspacing="0" width="100%" style="margin-top: 30px; font-size: 14px;">
            <?php if($paymentMethod == 'b') { ?>
            <tr>
                <td><label for="Paypal"><b><?php echo lang('bank_label'); ?></b> : <?php echo $site_setting->BankName;?></label></td>
            </tr>
            <tr>
                <td><label for="Paypal"><b><?php echo 'IBAN'; ?></b> : <?php echo $site_setting->IBAN;?></label></td>
            </tr>
            <tr>
                <td><label for="Paypal"><b><?php echo 'BIC'; ?></b> : <?php echo $site_setting->BIC;?></label></td>
            </tr>
            <?php } ?>
            <tr>
                <td> <span style="color:red">Notes : </span> Der Rechnungsbetrag ist zahlbar per Überweisung bis zum <?php echo date('d.m.Y',strtotime($user_data['PurchaseDate']."+7 Day")); ?> (innerhalb von 7 Tagen). Bitte geben Sie bei der Überweisung die Rechnungs-Nummer und lhre Kunden-Nummer an.</td>
            </tr>
        </table>
        <table height="200" width="100%">
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>

        <table cellpadding="5" cellspacing="0" width="100%" style="border-top: 1px solid #999999; margin-top: 20px; font-size: 10px;">
            <tr>
                <td  colspan="2">&nbsp;</td>
                
            </tr>
            <tr>
                <td style="text-align: left; vertical-align: top;">Bankverbindung:<br>Bank: <?php echo $site_setting->BankName;?><br>IBAN: <?php echo $site_setting->IBAN;?> <br>BIC: <?php echo $site_setting->BIC;?></td>
                <td style="text-align: right; vertical-align: top;"><?php echo $site_setting->SiteName;?> - <?php echo $site_setting->OnwerName;?><br><?php echo $site_setting->Address;?>, <?php echo $site_setting->PostalCode;?> <?php echo $site_setting->City;?><br>Tel. <?php echo $site_setting->PhoneNumber;?><br><br>E-Mail: <?php echo $site_setting->FromEmail;?> <br>Web:<?php echo base_url();?> <br>UST-ID:<?php echo $site_setting->USTID;?></td>
            </tr>
        </table>

        <!-- <b>Empfänger:</b> Meine Firma<br>
        <b>IBAN</b>: DE85 745165 45214 12364<br>
        <b>BIC</b>: C46X453AD<br><br> -->
    </div>
</body>

</html>