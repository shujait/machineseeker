<?php

foreach($data as $key=>$val){
    $val = (object)$val;                 
    $val->Title= special_characterDe($val->Title); 
    $user = get_user_info(array('a.UserID' => $val->UserID));
        $images = getImage(array('FileID'=>$val->MachineID, 'ImageType'=>'MachineFile'));
        $cod = $val->Condition;
          ?>
    <!-- Category post Start -->
    <div class="col-md-12">
        <div class="cpl-post">
            <div class="row">
                <?php

                ?>
                <div class="col-lg-4 col-md-12 gallery">
                    <?php /* ?><?php
                    if($images){
                    foreach($images as $k => $image){
                     $ext = pathinfo($image['ImageName'], PATHINFO_EXTENSION);
                     if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
                    ?>
                    <div class="d-none">
                        <a href="<?php echo base_url($image['ImageName']); ?>" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url($image['ImageName']); ?>" class="html5lightbox" data-group="set1" data-width="1200" data-height="400" title="Image 1"><img src="<?php echo base_url($image['ImageName']); ?>" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                    </div>
                    <?php 		}
                            }

                          }else {?>
                     <div class="img-thumbnail post-img">
                        <a href="<?php echo base_url(); ?>uploads/dummy.jpg" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url(); ?>uploads/dummy.jpg" class="html5lightbox" data-group="set1" data-width="1200" data-height="400" title="Image 1"><img src="<?php echo base_url(); ?>uploads/dummy.jpg" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                    </div>
                    <?php } ?><?php */?>
                    <?php if(($val->FeaturedImage != '' && file_exists($val->FeaturedImage)) || $images){
                        if($val->FeaturedImage != '' && file_exists($val->FeaturedImage)){
                        ?>
                        <div class="img-thumbnail post-img">
                            <a href="<?php echo base_url($val->FeaturedImage); ?>" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url($val->FeaturedImage); ?>" class="html5lightbox<?php echo $key;?>" data-group="set1" data-width="1200" data-height="400" title="Image 1"><img src="<?php echo base_url($val->FeaturedImage); ?>" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                        </div>
                    <?php
                        }
                        if($images)
                        {
                            foreach($images as $k => $image){
                             $ext = pathinfo($image['ImageName'], PATHINFO_EXTENSION);
                             if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
                            ?>
                            <div class="<?php echo (($val->FeaturedImage != '' && file_exists($val->FeaturedImage)) ? 'd-none' : ($k == 0 ? 'img-thumbnail post-img' : 'd-none'));?>">
                                <a href="<?php echo base_url($image['ImageName']); ?>" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url($image['ImageName']); ?>" class="html5lightbox<?php echo $key;?>" data-group="set1" data-width="1200" data-height="400" title="Image 1"><img src="<?php echo base_url($image['ImageName']); ?>" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                            </div>
                            <?php
                                }
                            }
                        }
                    }else{ ?>
                        <div class="img-thumbnail post-img">
                            <?php if($this->session->userdata['lang'] =="en"){?>

                            <a href="<?php echo base_url(); ?>uploads/EN-Maschinenpilot-1200.png" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url(); ?>uploads/EN-Maschinenpilot-1200.png" class="html5lightbox<?php echo $key;?>" data-group="set1" data-width="1200" data-height="400" title="Image 1"><img src="<?php echo base_url(); ?>uploads/EN-Maschinenpilot-1200.png" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                            <?php } else { ?>
                                <a href="<?php echo base_url(); ?>uploads/Maschinenpilot-1200.png" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url(); ?>uploads/Maschinenpilot-1200.png" class="html5lightbox<?php echo $key;?>" data-group="set1" data-width="1200" data-height="400" title="Image 1"><img src="<?php echo base_url(); ?>uploads/Maschinenpilot-1200.png" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-lg-8 col-md-12 cpl-post-content">
                    <div class="row">
                        <div class="col-md-10">
                            <h2>
                                <a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>">
                                    <span class="pro-name font-weight-bold"><?php echo $val->Title; ?></span>
<!--                                                        <span class="pro-manufacturer">--><?php //echo $val->Manufacturer; ?><!--</span><span class="pro-model"> --><?php //echo $val->Model; ?><!--</span>-->
                                </a>
                            </h2>
                        </div>
                        <?php /*if($user->IsCertifiedTrader == 1){ ?>
                        <div class="col-md certified-img"><a href="#"><img src="<?php echo base_url(); ?>assets/frontend/images/certified-trade.png" alt="" width="120" height="120" class="img-fluid"></a></div>
                        <?php }*/ ?>
                    </div>
                    <div class="row cpl-posy-col">
                        <?php if($val->Location){ ?>
                        <div class="col-md-6">
                            <div class="pro-country">
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span class="pro-value"> <?php echo $val->Location; ?></span>
                                <!-- <div class="flag-wrapper">
                 <a href="#" class="flag flag-icon-background flag-icon-<?php //echo strtolower($val->CountryShort); ?>" title="ad" id="ad"></a>
                                    </div> -->
                            </div>
                        </div>
                        <?php } ?>
                        <div class="col-md-6">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <span class="pro-value"> <?php echo $val->ManufacturerYear; ?></span>
                        </div>
                        <div class="col-md-6">
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                            <span class="pro-value"> <?php echo condition_list($cod); ?></span>
                        </div>
                    </div>
                    <div class="row pro-shrt-desc">
                        <div class="col-md-12">
                            <?php echo $val->Description; ?>
                            <p><a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>"><?php echo lang('more'); ?></a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 cpl-btn-wrap">
                    <div class="row flex-lg-row-reverse">
                        <div class="col-lg-6 col-md-12">
                            <a href="#" data-toggle="modal" data-target="#inquiryModal" class="btn btn-primary inquiry" data-id="<?php echo $val->MachineID; ?>"><?php echo lang('request-price'); ?></a>
                            <a href="<?php echo friendly_url('detail/',$val->Title.' '.$val->MachineID); ?>" class="btn btn-primary"><?php echo lang('read-more'); ?></a>
                        </div>
                        <div class="col-lg-6 col-md-12">
                            <div class="pro-rm-btn">
                                <a href="#cpl-post-<?php echo $val->MachineID; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row collapse cat-click-description" id="cpl-post-<?php echo $val->MachineID; ?>">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4><?php echo lang('Description'); ?></h4>
                                </div>
                                <div class="col-md-8">
                                    <div class="cat-text">
                                       <?php echo $val->Description; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <h4><?php echo lang('dealer'); ?></h4>
                                </div>
                                <div class="col-md-8">
                                    <div class="cat-text">
                                        <p><?php echo $user->Title; ?></p>
                                        <p><?php echo $user->StreetAddress; ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 cpl-btn-wrap">
                                    <div class="row flex-lg-row-reverse">
                                        <div class="col-lg-6 col-md-12">
<!--                                                                <a href="#" class="btn btn-success"><?php echo lang('sort-by'); ?></a>-->
                                            <a href="#" class="btn btn-primary inquiry" data-toggle="modal" data-target="#inquiryModal"  data-id="<?php echo $val->MachineID; ?>"><?php echo lang('submit-request'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(".html5lightbox<?php echo $key;?>").html5lightbox();
    </script>
    <?php }
    if($pages){
        echo $pages;
    }