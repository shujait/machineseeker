<?php
/**
 * Created by PhpStorm.
 * User: Xpert Solutions
 * Date: 9/13/2018
 * Time: 5:28 PM
 */
?>
</main>
</div>
<?php $this->load->view('frontend/layouts/footer-menu'); ?>
<!--Footer section end-->
<!-- Price Plan Modal 1 -->
<?php $this->load->view('frontend/layouts/plan-popup'); ?>

<!-- Price Plan Modal 1 -->
<!-- Moneback Modal -->
<div class="modal fade" id="MoneyBackModal" role="dialog" aria-labelledby="MoneyBackModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">MONEY-BACK GUARANTEE</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Here at Machinepilot, we are firmly convinced that we offer a great service at very attractive rates. Let us convince you too by offering you a no-risk opportunity to sell machines with us for 12 months.</p>
                <h5>Conditions</h5>
                <p>We apply just one condition to returning your money, which we believe is fair :</p>
                <div class="alert alert-warning">
                    <p>You are a machine dealer and offer an average <b>minimum of 5 machines</b> during the 12-month period. </p>
                    <p>At the end of the 12-month period, if you aren't happy with our platform - which would surprise us - we will <b>refund the amount that you paid!</b></p>
                </div>
                <p class="text-center">Any questions? Would you like to have an individual offer? </p>
                <h5 class="text-center">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    +1 234 56 78 901
                </h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-block btn-lg" data-dismiss="modal">Close window</button>
            </div>
        </div>
    </div>
</div>
<!-- Moneback Modal -->
<!-- The Modal -->
<div class="modal fade product-modal djn" id="inquiryModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('send-inquiry'); ?></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php $this->load->view('frontend/layouts/inquiry-form_fromModal', array('class' => 'btn-outline-success', 'id' =>  empty($result->MachineID)?'':$result->MachineID )); ?>
            </div>
        </div>
    </div>
</div>
<!--Product Modal end-->
<!--Request Product Modal start-->
<?php $this->load->view('frontend/layouts/product-inquiry'); ?>
<!--Request Product Modal end-->
<!--Request Forgot Password Modal start-->
<?php $this->load->view('frontend/layouts/forgot-password'); ?>
<!--Request Forgot Password Modal sart-->


<script src="<?php echo base_url(); ?>assets/frontend/js/popper.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/flyto.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/jquery.jcarousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/jcarousel.connected-carousels.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/select2/js/select2.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/frontend/js/jquery.jfImgToCSS-min.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/frontend/js/rrssb.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/flytowatch.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/wow.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-notify.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.blockUI.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPrjbKrBDh7lAXE4Mch0Yb7roXu8TkY1w&libraries=places"></script>
<?php if ($this->router->fetch_method() == "detail" || $this->router->fetch_method() == "details") { ?>	
    <script src="<?php echo base_url(); ?>assets/frontend/js/map.js" type="text/javascript"></script>
<?php } ?>
<script src="<?php echo base_url(); ?>assets/frontend/js/jquery.ihavecookies.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/frontend/js/scripts.min.js?<?php echo  CSS_VERSION_NUMBER;?>" type="text/javascript"></script>

<link href="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/css/intlTelInput.min.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput-jquery.min.js"></script>

<script>
    $(document).ready(function () {
        $(".select2").select2();
        var input = document.querySelector("#PhoneNumber");
        var iti = intlTelInput(input, {
          preferredCountries: ['de'],
          separateDialCode: true,
          utilsScript: "assets/backend/plugins/Intl-tel/js/utils.js",
        });


        iti.setNumber('+49');
        $("#CountryCode").val('+49');
        var countryData = iti.getSelectedCountryData();
        input.addEventListener("countrychange", function() {
          var changeCountryCode = '';
          changeCountryCode = iti.getSelectedCountryData();
          $("#CountryCode").val("+"+changeCountryCode.dialCode);
        });

        var inputInquiry = document.querySelector("#PhoneNumberInquiry");
        var itii = intlTelInput(inputInquiry, {
          preferredCountries: ['de'],
          separateDialCode: true,
          utilsScript: "assets/backend/plugins/Intl-tel/js/utils.js",
        });


        itii.setNumber('+49');
        $("#CountryCodeInquiry").val('+49');
        var countryData = itii.getSelectedCountryData();
        inputInquiry.addEventListener("countrychange", function() {
          var changeCountryCode = '';
          changeCountryCode = itii.getSelectedCountryData();
          $("#CountryCodeInquiry").val("+"+changeCountryCode.dialCode);
        });

        var inputFax = document.querySelector("#Fax");
        var itif = intlTelInput(inputFax, {
          preferredCountries: ['de'],
          separateDialCode: true,
          utilsScript: "assets/backend/plugins/Intl-tel/js/utils.js",
        });


        itif.setNumber('+49');
        $("#CountryCodeFax").val('+49');
        var countryData = itif.getSelectedCountryData();
        inputFax.addEventListener("countrychange", function() {
          var changeCountryCode = '';
          changeCountryCode = itif.getSelectedCountryData();
          $("#CountryCodeFax").val("+"+changeCountryCode.dialCode);
        });


        var inputInquiryForm = document.querySelector("#phoneInquiryForm");
        var itiInquiryForm = intlTelInput(inputInquiryForm, {
          preferredCountries: ['de'],
          separateDialCode: true,
          utilsScript: "assets/backend/plugins/Intl-tel/js/utils.js",
        });


        itiInquiryForm.setNumber('+49');
        $("#CountryCodeInquiryForm").val('+49');
        var countryData = itiInquiryForm.getSelectedCountryData();
        inputInquiryForm.addEventListener("countrychange", function() {
          var changeCountryCode = '';
          changeCountryCode = itiInquiryForm.getSelectedCountryData();
          $("#CountryCodeInquiryForm").val("+"+changeCountryCode.dialCode);
        });

    });
</script>
<?php
if (ENVIRONMENT == 'production') {
  ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-150898294-1"></script>
    <script type="text/javascript" data-cookieconsent="statistics">
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-150898294-1', {'anonymize_ip': true});
    </script>
 <?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.custom01').each(function(i, obj) {
            if($(obj).css("display") == "block"){
                setTimeout(function(){
                    $(obj).attr('style', 'display: none !important');
                }, 5000);
            }
        });
    });

    $('[data-toggle="tooltip"]').tooltip();

    $("#CountryCode").on("click", function(){
        var PhoneNumber = $(".MobileNumber").val();
        if(PhoneNumber == "+34" || PhoneNumber == "34"){
            $(".MobileNumber").val("+");
        } else {
            $(".MobileNumber").val("+34");
        }
    });

    $('.Number').keypress(function(e){
        var txt = String.fromCharCode(e.which);
        if(!txt.match(/[+0-9]/)){ return false; }
    });
</script>
</body>
</html>