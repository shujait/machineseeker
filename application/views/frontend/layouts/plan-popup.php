<div class="modal fade" id="planModal-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('select_plan');?></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('frontend/purchase/action'); ?>" method="post" onsubmit="return false;" class="form_data inquiry-form">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="companyName"><?php echo lang('company_name'); ?></label>
                            <input type="text" name="companyName" required id="companyName" class="form-control" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="StreetAddress"><?php echo lang('street_address'); ?></label>
                            <input type="text" name="StreetAddress" required id="StreetAddress" class="form-control" autocomplete="off" >
                        </div>
                        <div class="col-md-6">
                            <label for="postcodeTown"><?php echo lang('postcode'); ?></label>
                            <input type="text" name="postcodeTown" required id="postcodeTown" class="form-control" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="postcodeTown"><?php echo lang('city'); ?></label>
                            <input type="text" name="City" required id="City" class="form-control" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="postcodeTown"><?php echo lang('vat'); ?></label>
                            <input type="text" name="Vat" required id="Vat" class="form-control" autocomplete="off">
                        </div>
                        <div class="col-md-6">
                            <label for="Email"><?php echo lang('email'); ?></label>
                            <input type="email" name="Email" id="Email" required class="form-control" autocomplete="off" <?= (isset($_SESSION['SocialMediaSession']['Email']) ? "readonly" : "")?>>
                        </div>
						 <div class="col-md-6">
                            <label for="PhoneNumber"><?php echo lang('ph'); ?></label>
                            <div class="input-group">
                                <input type="text" name="PhoneNumber" id="PhoneNumber" class="form-control Number MobileNumber" data-country="DE" autocomplete="off" value="" required>
                                <input type="hidden" name="CountryCode" id="CountryCode" value="">
                                <!-- <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <div class="flag-wrapper">
                                        <a href="javascript:;" class="flag flag-icon-background flag-icon-<?php //getCountryBYIP(@$_SERVER['REMOTE_ADDR']); ?>" title="ad" id="CountryCode" ></a>
                                    </div>
                                </button> -->
                           </div>
                        </div>
						 <div class="col-md-6">
                            <label for="lang"><?php echo lang('default-language'); ?></label>
                            <div class="input-group">
								<select name="lang"  id="lang" class="form-control" required>
                                    <?php
                                        $default_lng = getDefaultLanguage();
                                        $arr = getSystemLanguages($default_lng->SystemLanguageID);
                                        foreach($arr as $val){
                                    ?>
                                    <option value="<?php echo $val->ShortCode; ?>"><?php echo $val->SystemLanguageTitle; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                           </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary"><?php echo lang('next'); ?></button>
							<input type="hidden" name="plan_id" value="">
							<input type="hidden" name="plan_row" value="">
							<input type="hidden" name="form_type" value="signup">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>