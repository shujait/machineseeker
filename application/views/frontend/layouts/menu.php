<?php $lang_dir = $this->session->userdata('lang_dir'); ?>
<header class="header">
    <div class="header-row-1">
        <div class="top-bar">
            <div class="container">
                
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-between">                  
                    <!-- <div class="col-md-4"></div> -->
                    <div class="col-md-4">
                        <div class="logo">
                            <a href="<?php echo base_url(); ?>">
                                <img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo ($this->session->userdata('lang') == 'en' ? 'logo-new-en.webp' : 'logo-new-de.webp')?>" alt="Machine Pilot" width="350" height="70" class="img-fluid">
                            </a>
                            <!--<span>United State</span>-->
                        </div>
                    </div>
                    <div class="col-md-8">
                    <div class="col-md-12 hr-1-right">
                    <div class="header-list">
                        <ul>
                            <li><a href="<?php echo base_url('favoriten'); ?>"><i class="fa fa-flag" aria-hidden="true"></i><?php echo strtoupper(lang('wishlist')); ?> (<span id="gemerkte"><?php echo $user_wishlist; ?></span>)</a></li>
                            <li>
                                <?php
                                    if(!($this->session->userdata('admin'))){
                                ?>
                                <div>
                                    <span><a href="<?php echo base_url('login'); ?>"><i class="fa fa-user" aria-hidden="true"></i> <?php echo strtoupper(lang('login')); ?></a></span>
                                    <span><span class="or"><?php echo lang('or');?></span></span>
                                    <span><a href="<?php echo base_url('tarife'); ?>"><i class="fa fa-user-plus" aria-hidden="true"></i> <?php echo strtoupper(lang('signup')); ?></a> </span>
                                </div>
                                <?php }else{ ?>
                                <div>
                                    <span><a href="<?php echo base_url('cms/dashboard'); ?>"><i class="fa fa-dashboard" aria-hidden="true"></i> <?php echo strtoupper(lang('dashboard')); ?></a></span>
                                    <span><span class="or"><?php echo lang('or');?></span></span>
                                    <span><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i><?php echo strtoupper(lang('logout')); ?></a></span>
                                </div>
                                <?php } ?>
                            </li>
                            <li>
                                <?php 
                                if($site_setting->FacebookUrl || $site_setting->LinkedInUrl || $site_setting->BlogUrl || $site_setting->TwitterUrl || $site_setting->InstagramUrl){ 
                                    ?>
                                    <div class="social">
                                        <?php if($site_setting->FacebookUrl != ''){ ?>
                                        <a href="<?= $site_setting->FacebookUrl ?>" target="_blank" class="icon"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                        <?php }  if($site_setting->InstagramUrl != ''){ ?>
                                        <a href="<?= $site_setting->InstagramUrl ?>" target="_blank" class="icon"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                        <?php }  if($site_setting->LinkedInUrl != ''){ ?>
                                        <a href="<?= $site_setting->LinkedInUrl ?>" target="_blank" class="icon"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        <?php }  
                                        if($site_setting->TwitterUrl != ''){ 
                                            ?>
                                        <a href="<?= $site_setting->TwitterUrl ?>" target="_blank" class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                        <?php }  
                                        if($site_setting->BlogUrl != ''){ ?>
                                        <a href="<?= $site_setting->BlogUrl ?>" target="_blank" class="icon"><i class="fa fa-rss" aria-hidden="true"></i></a>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </li>
                            <li class="lang_outer mobile-top-lang">
                                <?php $arr = getSystemLanguages();
                                            foreach($arr as $val){
                                                if(strtolower($val->ShortCode) == 'en'){
                                                    $lang = 'um';
                                                }else{
                                                    $lang = strtolower($val->ShortCode);
                                                }
                                            ?>
                                <a href="<?php echo base_url('lang').'/'.strtolower($val->ShortCode);?>">
                                    <img src="<?php echo base_url();?>assets/<?php echo $lang.'.webp'; ?>" class="lang-icon"></a>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                    <div class="lang_outer mobile-top-lang">

                        <?php $arr = getSystemLanguages();
                                            foreach($arr as $val){
                                                if(strtolower($val->ShortCode) == 'en'){
                                                    $lang = 'um';
                                                }else{
                                                    $lang = strtolower($val->ShortCode);
                                                }
                                            ?>
                        <a href="<?php echo base_url('lang').'/'.strtolower($val->ShortCode);?>">
                            <img src="https://www.countryflags.io/<?php echo $lang; ?>/flat/24.png"></a>
                        <?php } ?>
                    </div>
                </div>
                   <div class="hr1col-2" style="padding-top: 15px; padding-right: 25px;">
                        <!-- <p><?php echo strtoupper(lang('selling-machines')); ?>? <span><?php echo $site_setting->PhoneNumber; ?></span></p> -->
                        <?php
                            if(!($this->session->userdata('admin'))){
                                $link = base_url('tarife');
                            }else{
                                $link = base_url('cms/dashboard');
                            }
                        ?>
                        <div class="sell-btn pull-right">
                            <a href="<?= $link; ?>" class="btn btn-secondary">
                                <?php echo lang('sell').' '.lang('now'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php /*
            if($site_setting->GoogleAdCode || $site_setting->SingleAdImage){
        ?>
            <div class="hdr_banner text-center w-100">
                <?php if($site_setting->GoogleAdCode){ ?>
                <div class="innr adsense">
                    <?= $site_setting->GoogleAdCode; ?>
                </div>
                <?php }if(empty($site_setting->GoogleAdCode) && $site_setting->SingleAdImage){ ?>
                <div class="innr singleimg">
                    <a href="<?= $site_setting->SingleAdImageURL; ?>" target="_blank">
                        <img src="<?= base_url($site_setting->SingleAdImage); ?>">
                    </a>
                </div>
                <?php }if($site_setting->MultipleAdImages){ ?>
                <div class="innr slideimgs">
                    <div id="BannerControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="<?= base_url($site_setting->SingleAdImage); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        <?php 
            } */
        ?>
    </div>
    <div class="header-row-3">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-12 collapseCatList1">
                    <nav class="navbar navbar-expand-lg">
                        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#mianNavigation" aria-controls="mianNavigation" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fa fa-bars"></i>
                        </button>
                        <div class="collapse navbar-collapse" id="mianNavigation">
                            <ul class="nav navbar-nav">

                                <div class="header-list mobile-top">
                                    <ul>
                                        <li class="lang_outer">

                                            <?php $arr = getSystemLanguages();
                                            foreach($arr as $val){
                                                if(strtolower($val->ShortCode) == 'en'){
                                                    $lang = 'um';
                                                }else{
                                                    $lang = strtolower($val->ShortCode);
                                                }
                                            ?>
                                            <a href="<?php echo base_url('lang').'/'.strtolower($val->ShortCode);?>">
                                                <img src="https://www.countryflags.io/<?php echo $lang; ?>/flat/24.png"></a>
                                            <?php } ?>
                                        </li>
                                        <li><a href="<?php echo base_url('favoriten'); ?>"><i class="fa fa-flag" aria-hidden="true"></i><?php echo strtoupper(lang('wishlist')); ?> (<span id="gemerkte"><?php echo $user_wishlist; ?></span>)</a></li>
                                        <li>
                                            <?php
                                        if(!($this->session->userdata('admin'))){
                                        ?>
                                            <div>
                                                <span><a href="<?php echo base_url('login'); ?>"><i class="fa fa-user" aria-hidden="true"></i> <?php echo strtoupper(lang('login')); ?></a></span>
                                                <span><span class="or">or</span></span>
                                                <span><a href="<?php echo base_url('tarife'); ?>"><i class="fa fa-user-plus" aria-hidden="true"></i> <?php echo strtoupper(lang('signup')); ?></a> </span>
                                            </div>
                                            <?php }else{ ?>
                                            <div>
                                                <span><a href="<?php echo base_url('cms/dashboard'); ?>"><i class="fa fa-dashboard" aria-hidden="true"></i> <?php echo strtoupper(lang('dashboard')); ?></a></span>
                                                <span><span class="or">or</span></span>
                                                <span><a href="<?php echo base_url('logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> <?php echo strtoupper(lang('logout')); ?></a></span>
                                            </div>
                                            <?php } ?>
                                        </li>
                                    </ul>
                                </div>

                                <?php 
			                    // $menu =  json_decode($header[0]->Menu);
								// foreach ($menu as $li) {
                                // if($li->parent == 0)
                                //   echo Menu($menu,$li->id,$li->menutype,0,array('li' =>'','a' => ''));
                            	// }
                                ?>
                                <?php 
                                    $maincategories = allcategoriesformenu();
                                    //var_dump("<pre>",$maincategories);
                                    $html = "";
                                    foreach($maincategories as $maincategory){

                                        if(!empty($maincategory['ShortTitle']) && !is_null($maincategory['ShortTitle'])){
                                            $MenuDisplayTitle = $maincategory['ShortTitle'];
                                        }else{
                                            $MenuDisplayTitle = $maincategory['Title'];
                                        }

                                        $html .='<li><a href="'.base_url('kategorie/'.$maincategory['Slug']).'">'.$MenuDisplayTitle.'</a>';

                                        $subcategories = allcategoriesformenu($maincategory['CategoryID']);
                                        
                                        if(count($subcategories) > 0){
                                            $html .='<a data-toggle="dropdown" class="dropdown-o"><i class="fa fa-caret-down" aria-hidden="true"></i></a>';
                                            $html .='<ul class="dropdown-menu">';

                                            foreach($subcategories as $subcategory){
                                                if(!empty($subcategory['ShortTitle']) && !is_null($subcategory['ShortTitle'])){
                                                    $MenuDisplayTitle1 = $subcategory['ShortTitle'];
                                                }else{
                                                    $MenuDisplayTitle1 = $subcategory['Title'];
                                                }

                                                $subcategories2 = allcategoriesformenu($subcategory['CategoryID']);

                                                $html .='<li class="'.(count($subcategories2) > 0 ? "dropdown-submenu" : "").'"><a href="'.base_url('kategorie/'.$subcategory['Slug']).'">'.$MenuDisplayTitle1.'</a>';
                                        
                                                if(count($subcategories2) > 0){
                                                    $html .='<a data-toggle="dropdown" class="dropdown-o"><i class="fa fa-caret-down" aria-hidden="true"></i></a>';
                                                    $html .='<ul class="dropdown-menu">';

                                                    foreach($subcategories2 as $subcategory2){
                                                        if(!empty($subcategory2['ShortTitle']) && !is_null($subcategory2['ShortTitle'])){
                                                            $MenuDisplayTitle2 = $subcategory2['ShortTitle'];
                                                        }else{
                                                            $MenuDisplayTitle2 = $subcategory2['Title'];
                                                        }
                                                        
                                                        $html .='<li>
                                                            <a href="'.base_url('kategorie/'.$subcategory2['Slug']).'">'.$MenuDisplayTitle2.'</a>
                                                        </li>';

                                                    }
                                                    $html .='</ul></li>';
                                                }else{
                                                    $html .='</li>';
                                                }
                                            }
                                            $html .='</ul></li>';
                                        }else{
                                            $html .='</li>';
                                        }
                                    }
                                
                                    echo $html;
                                    
                                ?>
                            </ul>
                        </div>
                    </nav>
                </div>
                
                <div class="col-md-3 col-sm-12 cat-list-wrap">
                    <ul class="cat-list">
                        <li class="has-sub">
                            <a href="#"><?php echo strtoupper(lang('search')); ?></a>
                            <ul class="submenu">
                                <li><a href="<?php echo base_url('listings'); ?>"><i class="fa fa-bars" aria-hidden="true"></i> <span><?php echo lang('listings'); ?></span></a></li>
                                <!-- <li><a href="<?php //echo base_url('dealer'); ?>"><i class="fa fa-users" aria-hidden="true"></i> <span><?php //echo lang('dealers'); ?></span></a></li> -->
                                <?php if($this->uri->segment(1) != "cms"){ ?>
                                <li><a href="#" data-toggle="modal" data-target="#requestProduct" data-dismiss="modal"><i class="fa fa-list" aria-hidden="true"></i> <span><?php echo lang('submit-request'); ?></span></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="#"><?php echo lang('place-ad');?></a>
                            <ul class="submenu">
                                <li><a href="<?php echo base_url('login'); ?>"><i class="fa fa-bars" aria-hidden="true"></i> <span><?php echo lang('place-ads-for-machines'); ?></span></a></li>
                                <li><a href="<?php echo base_url('tarife'); ?>"><i class="fa fa-users" aria-hidden="true"></i> <span><?php echo lang('price-plan'); ?></span></a></li>
                                <?php
                                    if(!($this->session->userdata('admin'))){
                                        $link = 'login';
                                    }else{
                                        $link = 'cms/dashboard';
                                    }
                                ?>
                                <li><a href="<?php echo base_url($link); ?>"><i class="fa fa-list" aria-hidden="true"></i> <span><?php echo lang('company_advertise').' '.lang('on').' '.lang('machine'); ?> Pilot</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</header>

<?php
    $DeactivatedNotification = $this->session->userdata('DeactivatedNotification');
    $InvoiceNotification = $this->session->userdata('InvoiceNotification');
if($this->uri->segment(1) == 'cms')
{
?>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-icon alert-warning custom01 alert-dismissible  show mb-0" role="alert" style=" <?php echo empty($DeactivatedNotification) ? 'display:none !important' : ''; ?>">
            <span><i class="mdi mdi-block-helper mr-2"></i>
                <strong> </strong> <?php echo $DeactivatedNotification; ?></span>
            <button type="button" class="close closeBtn" id='DeactivatedNotification' data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
    <div class="col-md-12">
        <div class="alert alert-icon alert-warning custom01 alert-dismissible  show mb-0" role="alert" style=" <?php echo empty($InvoiceNotification) ? 'display:none !important' : ''; ?>">
            <span><i class="mdi mdi-block-helper mr-2"></i>
                <strong>Oh !</strong> <?php echo $InvoiceNotification; ?></span>
            <button type="button" class="close closeBtn" id='InvoiceNotification' data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
    <div class="col-md-12">
        <div class="uploadfile1 alert alert-icon alert-warning custom01 alert-dismissible  show mb-0" role="alert" style=" <?php echo empty($errorMsg) ? 'display:none !important' : ''; ?>">
            <span class="upload001"><?php echo empty($errorMsg)?'':$errorMsg;?></span>
            <span class="upload001 pull-right"><a href="<?php echo base_url("cms/user/document");?>" class="up-file"> <?php echo lang('upload_file'); ?></a></span>
            <button type="button" class="close1 closeBtn " data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    </div>
</div>
<?php 
}
?>