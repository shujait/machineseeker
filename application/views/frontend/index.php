<section class="main-banner" style="background-image: url(<?php echo base_url($site_setting->BannerImage);?>);">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-md-10">
                <h2><?php echo lang('search-engine-for-machines'); ?></h2>
                <div class="bform-wrap">
                    <form action="<?php echo base_url('search');?>" id="sform" method="post" class="form-inline">
                        <div class="input-group">
<!--                    <input type="search" name="search" data-toggle="dropdown"  autocomplete="off" id="Title" class="form-control" placeholder="<?php echo lang('search-placeholder'); ?>">-->
                        <input type="search" name="search" autocomplete="off" id="sTitle" class="form-control" placeholder="<?php echo lang('search-placeholder'); ?>" >
                        <div id="dropdownhtml" style="display:none;" class="search-result1">
                                
                        </div>
                        </div>
                        <div class="btn-search">
                            <button type="submit" class="btn btn-secondary"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                        <?php /*?><br>
                        <div class="col-md-6">
                            <input type="search" name="manufacturer" id="manufacturer" class="form-control" style="width: 88%;height: 35px;margin-top: 10px;" placeholder="<?php echo lang('manufacturer'); ?>">
                        </div>
                        <div class="col-md-6">
                            <select class="form-control" style="margin-top: 10px;width: 100%;" name="CategoryID" id="Category">
                                 <option value=""><?php echo lang('select_category'); ?></option>

                                        <?php 
                                    $arr =  getCategories($language,'categories.ParentID = 0');
                                    if(!empty($arr)){
                                        foreach($arr as $result){
                                     ?>
                                    <option value="<?php echo $result->CategoryID; ?>" style="background:#8499e8; color:#ffffff;"><?php echo $result->Title; ?></option>
                                    <?php 
                                            $getChildCategories = getCategories($language,'categories.ParentID = '.$result->CategoryID);;
                                            if($getChildCategories)
                                            {
                                                foreach ($getChildCategories as $child) {
                                                ?>
                                                    <option  value="<?php echo $child->CategoryID; ?>" style="background: #cdd7ff;">&nbsp;&nbsp;<?php echo $child->Title; ?></option>
                                                <?php
                                                    $getGrandChildCategories = getCategories($language,'categories.ParentID = '.$child->CategoryID);;
                                                    if($getGrandChildCategories)
                                                    {
                                                        foreach ($getGrandChildCategories as $gchild) {
                                                        ?>
                                                            <option value="<?php echo $gchild->CategoryID; ?>" style="background: #e5eaff;">&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $gchild->Title; ?></option>
                                                        <?php
                                                        }
                                                    }
                                                }
                                            }
                                        } 
                                    } ?>
                                </select>
                        </div><?php */?>
                    </form>
                    <div class="ad-search">
                        <a href="<?php echo base_url('erweiterte-suche'); ?>"><?php echo lang('advanced-search'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cat-listing">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <h2><?php echo lang('categorys'); ?></h2>
            </div>
        </div>
        <div class="row">
			<?php
				if(!empty($display_category)){
					foreach($display_category as $result){

					    $rr = getCategories($language,'categories.CategoryID = '.$result['CategoryID']);
                        $cat_mc_row = get_machine_row(array('a.CategoryID' => $result['CategoryID']));

                        if(file_exists($result['Image'])){
                            $image = $result['Image'];
                        }else{
                            $image = 'uploads/dummy.jpg';
                        }

                    if($rr){
			?>
            <div class="col-md-3">
                <div class="cat-box">
                    <div class="img-box">
                        <?php if(!(userAgent())){?>
<!--                        <img src="--><?php //echo base_url($image);?><!--" alt="" width="270" height="200" class="img-fluid">-->
                            <img
                                    data-sizes="auto"
                                    data-src="<?php echo base_url($image);?>"
                                    data-srcset="<?php echo base_url($image);?> 300w,
                            <?php echo base_url($image);?> 600w,
                            <?php echo base_url($image);?> 900w" class="img-fluid lazyload" />
                        <?php }?>
                    </div>
                    <h3 class="d-flex justify-content-between"> <?= $result['Title']; ?>
						<?php echo show_counter($cat_mc_row); ?>
					</h3>
                    <ul class="list-group">
							<?php
							foreach($rr as $sub){
							$subcat_mc_row = get_machine_row(array('a.CategoryID' => $sub->CategoryID));
							?>
                        <li class="list-group-item d-flex justify-content-between align-items-center">
							<a href="<?php echo friendly_url('kategorie/',$sub->Slug); ?>"><?php echo $sub->Title; ?></a>
							<?php 
							 echo show_counter($subcat_mc_row);
							?>
						</li>
						<?php } ?>
                    </ul>
                </div>
            </div>
			<?php  }
                   }
                    }
            ?>
        </div>
    </div>
</section>
   <?php if(!(userAgent())){?>
<section class="alphabetical-cat-list">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <h2><?php echo lang('alphabatic_categories'); ?></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h3>A - K</h3>
                <ul class="list-group">
					<?php $rec = getCategories($language,'categories.ParentID = 0 AND categories_text.Title >= "A" AND categories_text.Title <= "K"'); 
					if($rec){
					foreach($rec as $val){
                        $cat_mc_count = get_machine_row(array('a.CategoryID' => $val->CategoryID));
                        ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
						<a href="<?php echo friendly_url('kategorie/',$val->Slug); ?>"><?php echo $val->Title; ?></a>
							<?php 
						echo show_counter($cat_mc_count); 
						?>
					</li>
					<?php } } ?>
                </ul>
            </div>
            <div class="col-md-4">
                <h3>L - R</h3>
                <ul class="list-group">
					<?php $rr = getCategories($language,'categories.ParentID = 0 AND categories_text.Title >= "L" AND categories_text.Title <= "R"'); 
					if($rr){
					foreach($rr as $val){
						$cat_mc_count = get_machine_row(array('a.CategoryID' => $val->CategoryTextID));		
					?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
						<a href="<?php echo friendly_url('kategorie/',$val->Slug); ?>"><?php echo $val->Title; ?></a>
						<?php  
						echo show_counter($cat_mc_count); 
						?>
					</li>
					<?php } } ?>
					</ul>
            </div>
            <div class="col-md-4">
                <h3>S - Z</h3>
                <ul class="list-group">
					<?php $ss = getCategories($language,'categories.ParentID = 0 AND categories_text.Title >= "S" AND categories_text.Title <= "Z"');
					if($ss){
					foreach($ss as $val){
						$cat_mc_count = get_machine_row(array('a.CategoryID' => $val->CategoryID));		
					?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
						<a href="<?php echo friendly_url('kategorie/',$val->Slug); ?>"><?php echo $val->Title; ?></a>
							<?php 
						echo show_counter($cat_mc_count); 
						?>
					</li>
					<?php } } ?>
					</ul>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<?php $this->load->view('frontend/layouts/sell-products'); ?>

<?php if(!(userAgent())){?>
<section class="clients-slider">
    <div class="container">
<?php $this->load->view('frontend/layouts/advertise'); ?>   
    </div>
</section>
  <?php }?>