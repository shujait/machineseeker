    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><?php echo lang('all-offers'); ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="cat-posts-sec">
        <div class="container">
            <div class="row fw-search mb-4">
                <div class="col-md-12 m-auto">
                    <div class="row">
                        <div class="col-md-12">
                            <h2><?php echo lang('search'); ?></h2>
                            <div class="form-wrap p-0">
                                <form action="<?php echo base_url('search');?>" method="post" class="form-inline w-100">
                                    <div class="input-group">
                                        <input type="search" name="search" class="form-control shadow-sm border" placeholder="<?php echo lang('search_here');?>">
                                    </div>
                                    <div class="btn-search">
                                        <button type="submit" class="btn btn-secondary"><i class="fa fa-search" aria-hidden="true"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md mt-3">
                            <p><a href="<?php echo base_url('erweiterte-suche'); ?>"><?php echo lang('advanced-search'); ?></a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 m-auto">
				 <?php $this->load->view('frontend/layouts/left-category-list'); ?>
                </div>
            </div>
        </div>
    </section>