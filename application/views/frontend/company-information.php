<link href="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/css/intlTelInput.min.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput-jquery.min.js"></script>
<section class="signup-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo lang('company_profile'); ?>:</h1>
            </div>
        </div>
        <form action="<?php echo base_url(); ?>frontend/purchase/action" method="post" onsubmit="return false;" class="form_data inquiry-form">
            <div class="row form-group">
                <div class="col-md-12">
                    <div class="border alert p-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="mb-3"><?php echo lang('company-contact-person'); ?></h5>
                            </div>
                            <div class="col-md-4">
								
								 <label class="control-label" for="Title"><?php echo lang('title'); ?></label>
									<select name="title" id="Title" required  class="form-control" >
										<?php $arr = getGenderTitle();
										foreach($arr as $val){
										?>
										<option value="<?php echo $val; ?>"><?php echo $val; ?></option>
										<?php } ?>
										</select>
                            </div>
                            <div class="col-md-4">
                                <label for="first_name"><?php echo lang('first_name'); ?></label>
                                <input type="text" name="first_name" id="first_name" required class="form-control" autocomplete="off" value="<?php echo (isset($_SESSION['SocialMediaSession']['FirstName']) ? $_SESSION['SocialMediaSession']['FirstName'] : ""); ?>">
                            </div>
                            <div class="col-md-4">
                                <label for="last_name"><?php echo lang('last_name'); ?></label>
                                <input type="text" name="last_name" id="last_name" required class="form-control" autocomplete="off" value="<?php echo (isset($_SESSION['SocialMediaSession']['LastName']) ? $_SESSION['SocialMediaSession']['LastName'] : ""); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="border alert p-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="mb-3"><?php echo lang('company_information'); ?></h5>
                            </div>
                            <div class="col-md-6">
                                <label for="website"><?php echo lang('website'); ?></label>
                                <input type="url" name="website" id="website" class="form-control" placeholder="http://www.example.com" autocomplete="off" value="https://">
                            </div>
                            <div class="col-md-6">
								   <label  for="country"><?php echo lang('country'); ?></label>
									<select name="country"  id="country" class="form-control" required>
										<?php $arr = CountryList2($language,'countries.IsActive = 1');
										foreach($arr as $val){
										?>
										<option value="<?php echo $val->CountryID; ?>"><?php echo $val->Title; ?></option>
										<?php } ?>
                                    </select>  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="border alert p-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="mb-3"><?php echo lang('contact_details'); ?></h5>
                            </div>
                            <div class="col-md-4">
                                <label for="ph"><?php echo lang('ph'); ?></label>
                                <div class="input-group">
                                    <!-- <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <div class="flag-wrapper">
                                            <a href="javascript:;" class="flag flag-icon-background flag-icon-<?php //echo getCountryBYIP(@$_SERVER['REMOTE_ADDR']); ?>" title="ad" id="ad"></a>
                                        </div>
                                    </button> -->
                                    <input type="text" name="ph" id="ph" required class="form-control Number" value="">
                                    <input type="hidden" name="CountryCode" id="CountryCode1" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
									             <label for="fax"><?php echo lang('fax'); ?></label>
                                <div class="input-group">
                                    <!-- <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <div class="flag-wrapper">
                                            <a href="javascript:;" class="flag flag-icon-background flag-icon-<?php //echo getCountryBYIP(@$_SERVER['REMOTE_ADDR']); ?>" title="ad" id="ad1"></a>
                                        </div>
                                    </button> -->
                                    <input type="text" name="fax" id="fax" class="form-control Number" autocomplete="off" >

                                    <input type="hidden" name="CountryFaxCode" id="CountryFaxCode" value="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="mob"><?php echo lang('mob'); ?></label>
                                <div class="input-group">
                                    <!-- <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                        <div class="flag-wrapper">
                                            <a href="javascript:;" class="flag flag-icon-background flag-icon-<?php //echo getCountryBYIP(@$_SERVER['REMOTE_ADDR']); ?>" title="ad" id="CountryCode"></a>
                                        </div>
                                    </button> -->
                                    <input type="text" name="mob" id="mob" class="form-control Number MobileNumber" autocomplete="off" value="">

                                    <input type="hidden" name="MobCountryCode" id="MobCountryCode" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
                    if(!isset($_SESSION['SocialMediaSession'])){
                ?>
                <div class="col-md-12">
                    <div class="border alert p-3">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="mb-3"><?php echo lang('password'); ?></h5>
                            </div>
                            <div class="col-md-12">
                                <label for="Password"><?php echo lang('password'); ?></label>
                                <input type="password" name="Password" id="Password" required  class="form-control" autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="col-md-12 text-center">
                    <p><?php echo lang('by_registering_you_accept_our');
						
						 $term = getPages(array('a.PageID'=> '12'),'row');
						?> 
						
						<a href="<?php echo base_url().$term->Url; ?>" target="_blank"><?php echo $term->Title;?></a>.</p>
                </div>
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-primary btn-lg w-25"><?php echo lang('next'); ?></button>
					<input type="hidden" name="form_type" value="company">
                </div>
            </div>
        </form>
    </div>
</section>
<script>
$(document).ready(function(){

    var input2 = document.querySelector("#ph");
    var iti2 = intlTelInput(input2, {
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "assets/backend/plugins/Intl-tel/js/utils.js",
    });

    iti2.setNumber('<?php echo $rec['PhoneNumber']; ?>');
    $("#CountryCode1").val('<?php echo ($rec['CountryCode'] != '' ? $rec['CountryCode'] : '+49');?>');
    var countryData = iti2.getSelectedCountryData();

    input2.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti2.getSelectedCountryData();
      $("#CountryCode1").val("+"+changeCountryCode.dialCode);
    });


    var input3 = document.querySelector("#mob");

    var iti3 = intlTelInput(input3, {
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "assets/backend/plugins/Intl-tel/js/utils.js",
    });

    iti3.setNumber('<?php echo ($rec['CountryCode'] != '' ? $rec['CountryCode'] : '+49');?>');
    $("#MobCountryCode").val('<?php echo ($rec['CountryCode'] != '' ? $rec['CountryCode'] : '+49');?>');
    var countryData = iti3.getSelectedCountryData();

    input3.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti3.getSelectedCountryData();
      $("#MobCountryCode").val("+"+changeCountryCode.dialCode);

    });

    var input4 = document.querySelector("#fax");

    var iti3 = intlTelInput(input4, {
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "assets/backend/plugins/Intl-tel/js/utils.js",
    });

    iti3.setNumber('<?php echo ($rec['CountryCode'] != '' ? $rec['CountryCode'] : '+49');?>');
    $("#CountryFaxCode").val('<?php echo ($rec['CountryCode'] != '' ? $rec['CountryCode'] : '+49');?>');
    var countryData = iti3.getSelectedCountryData();

    input4.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti3.getSelectedCountryData();
      $("#CountryFaxCode").val("+"+changeCountryCode.dialCode);
    });

}); 
</script>