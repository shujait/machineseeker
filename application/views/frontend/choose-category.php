    <section class="signup-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo lang('select_categories'); ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h5><?php echo lang('category-heading'); ?></h5>
                    <p><?php echo lang('category-sub-heading'); ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo base_url(); ?>frontend/purchase/action" id="selCatForm" method="post" onsubmit="return false;" class="form_data">
                        <div class="alert border p-4">
                            <div class="row">
								<?php 
									$arr = getCategories($language);
										if(!empty($arr)){ 
											foreach($arr as $result){
												if($result->ParentID == 0){
											$rr = getCategories($language,'categories.ParentID = '.$result->CategoryID);
										 if($rr){
		
													?>												
                                <div class="col-md-12">
                                    <h4 class="signup-link">
                                        <a class="text-left" data-toggle="collapse" href="#categoryBlock-<?php echo $result->CategoryID; ?>" role="button" aria-expanded="false" aria-controls="categoryBlock-<?php echo $result->CategoryID; ?>">
                                           <?php echo $result->Title; ?>
                                            
                                        </a>

                                        <span class="badge badge-primary badge-pill float-right mt-1"><?php  $count = getCategories($language,'categories.ParentID = '.$result->CategoryID);
                                            if($count){
                                                echo count($count);
                                            }
                                         ?></span>
                                    </h4>
                                    <div class="collapse" id="categoryBlock-<?php echo $result->CategoryID; ?>">
                                        <div class="card card-body checkContainer">
                                            <div class="row">
												<?php
													foreach($rr as $sub){
													?>
												
                                                <div class="col-md-4">
                                                    <div class="checkbox">
                                                        <input type="checkbox" name="sub_id[]" id="catItem-<?php echo $sub->CategoryTextID; ?>" value="<?php echo $sub->CategoryTextID; ?>" class="form-control">
                                                        <label for="catItem-<?php echo $sub->CategoryTextID; ?>"><?php echo $sub->Title; ?></label>
                                                    </div>
                                                </div>
												<?php } ?>
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-secondary selectAll" href="#" id="<?php echo $result->CategoryID; ?>"><?php echo lang('select-all'); ?></button>
                                                    <button type="button" class="btn btn-secondary deselectAll" href="#" id="<?php echo $result->CategoryID; ?>"><?php echo lang('deselect-all'); ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<?php 
										         }
												} 
											   }
											 }
											?>
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary btn-lg w-25"><?php echo lang('next'); ?></button>
									<input type="hidden" name="form_type" value="dealer-profile">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>