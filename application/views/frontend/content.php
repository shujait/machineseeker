    <section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
                        <li><?php echo $result->Title; ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="about-sec">
        <div class="container">
            <div class="row mt-2 flex-sm-row">
				 <?php 
				$col = '12';
				if($left){
				$col = '9';
				?>
                <div class="col-md-3">
                    <ul class="nav nav-tabs nav-tabs--vertical nav-tabs--left">
							<?php 
			                    $menu =  json_decode($left[0]->Menu);
								foreach ($menu as $li) {
                                if($li->parent == 0)
                                  echo Menu($menu,$li->id,$li->menutype,0,array('li' => 'nav-item' , 'a' => 'nav-link'));
                            	}
 							?>
                    </ul>
                </div>
				<?php } ?>
                <div class="col-md-<?php echo $col; ?>">
                    <div class="fade text-justify show active" id="aboutUs" role="tabpanel">
                        <?php
						$search = array(mail_tpl_shortcode(lang('contact-form')),mail_tpl_shortcode(lang('google-map-embed')));	
				        $replace = array($this->load->view('frontend/layouts/contact-form','',true),$site_setting->GoogleMap);	
						echo str_replace($search,$replace,$result->Content); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>