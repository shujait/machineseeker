<section class="breadcrumb-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul>
                        <li><a href="<?php echo base_url(); ?>"><?php echo lang('home'); ?></a></li>
						<?php if($result){ ?>
                        <li><a href="<?php echo friendly_url('category/',$category->Slug.' '.$category->CategoryTextID); ?>"><?php echo $category->Title; ?></a></li>
                        <li><?php echo $result->Title; ?></li>
						<?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="pd-section">
        <div class="container preview-on">
			<?php if($result){ ?>
            <div class="row pd-title">
                <div class="col-md-12">
					<?php echo 
		                          $result->Title; ?>
                    <h1 class="b-detail__head-title"><?php echo 
		                          $result->Title; 
						
						if($wishlist == 0){ 
						  $wish_label = 'add-wishlist';
							}else{
						  $wish_label = 'remove-wishlist';	
							}
						  $cod = $result->Condition;
						?>
						<a class="merken big wishlist" href="#" title="<?php echo lang($wish_label); ?>" data-toggle="tooltip" data-placement="top" data-alt-title="<?php echo lang($wish_label); ?>" data-original-title="<?php echo lang($wish_label); ?>" data-id="<?php echo $result->MachineID; ?>"></a>
                                                <div class="pull-right">                
 <?php if(isset($this->session->userdata['admin']) and $this->session->userdata['admin']['UserID'] == $result->UserID){ ?>
                        <a href="<?php echo base_url('cms/machine/edit/'.$result->MachineID);?>" class="btn btn-outline-dark"><i class="fa fa-pencil"></i> <?php echo lang('edit'); ?></a>
                        <a href="#publish" class="btn btn-outline-dark" 
						   onclick="publishRecord('<?php echo $result->MachineID;?>','cms/machine/action','<?php echo base_url(); ?>');" id="publish"><i class="fa fa-publish"></i> <?php echo lang('publish'); ?></a>
						<?php } ?> </div>
                    
                    </h1>
                    	
                </div>
                <div class="col-md-12">
                    <div class="row">
						<?php if($result->ManufacturerYear){ ?>
                        <div class="col-md-3 post-info">
                            <div class="value"> <?php echo $result->ManufacturerYear; ?></div>
                            <div class="name"><?php echo lang('manufacturer_year'); ?></div>
                        </div>
						<?php } ?>
                        <div class="col-md-2 post-info">
                            <div class="value"><?php echo condition_list($cod); ?></div>
                            <div class="name"><?php echo lang('codition'); ?></div>
                        </div>
						<?php  if($result->Location){ ?>
                        <div class="col-md-2 post-info">
                            <div class="value" id="address"><?php echo $result->Location; ?></div>
                            <div class="name"><?php echo lang('location'); ?></div>
                        </div>
						<?php } ?>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="postGalleryCarousel" class="carousel slide" data-carousel="false">
						<?php $images = getImage(array('FileID'=>$result->MachineID, 'ImageType'=>'MachineFile')); ?>
                        <div class="row">
                            <div class="col-md-7 carousel-inner-wrap">
                                <!-- main slider carousel items -->
                                <div class="carousel-inner">
									<?php
								        if($images || $result->FeaturedImage != ''){
										     $n = 0;
                                        if($result->FeaturedImage != '')
                                        {
                                            $ext = pathinfo($result->FeaturedImage, PATHINFO_EXTENSION);
                                            if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
                                              $n++;
                                            if($n == 1){
                                                $class = 'active';
                                            }else{
                                                $class = '';
                                            }
                                        ?>
                                            <div class="<?php echo $class; ?> item carousel-item post-img" data-slide-number="<?php echo $n-1; ?>">
                                                <a href="<?php echo $result->IsImport?$result->FeaturedImage:base_url($result->FeaturedImage); ?>" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo $result->IsImport?$result->FeaturedImage:base_url($result->FeaturedImage); ?>" class="html5lightbox" data-group="set3" data-width="600" data-height="400" title="Image <?php echo $n; ?>"><img src="<?php echo $result->IsImport?$image['ImageName']:base_url($result->FeaturedImage); ?>" alt="" width="1024" height="768" class="img-fluid imgToCSS"></a>
                                            </div>
                                        <?php
                                            }
                                        }
                                        if($images)
                                        {
    										foreach($images as $image){
    											$ext = pathinfo($image['ImageName'], PATHINFO_EXTENSION);
    										 if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
    											  $n++;
    										if($n == 1){
    											$class = 'active';
    										}else{
    											$class = '';
    										}
    										?>
                                        <div class="<?php echo $class; ?> item carousel-item post-img" data-slide-number="<?php echo $n-1; ?>">
                                            <a href="<?php echo $result->IsImport?$image['ImageName']:base_url($image['ImageName']); ?>" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo $result->IsImport?$image['ImageName']:base_url($image['ImageName']); ?>" class="html5lightbox" data-group="set3" data-width="600" data-height="400" title="Image <?php echo $n; ?>"><img src="<?php echo $result->IsImport?$image['ImageName']:base_url($image['ImageName']); ?>" alt="" width="1024" height="768" class="img-fluid imgToCSS"></a>
                                        </div>
    									<?php 	
    										 } 	
    									  }	
                                        }		
								    }else {?> 
										 <div class="active item carousel-item post-img" data-slide-number="1">
                                            <a href="<?php echo base_url(); ?>uploads/dummy.jpg" data-fullscreenmode="true" data-transition="slide" data-thumbnail="<?php echo base_url(); ?>uploads/dummy.jpg" class="html5lightbox" data-group="set1" data-width="600" data-height="400" title="Image 1"><img src="<?php echo base_url(); ?>uploads/dummy.jpg" alt="" width="1366" height="683" class="img-fluid imgToCSS"></a>
                                        </div>
										<?php } 
									 if($result->Video){
										?>
									    <div class="item carousel-item post-img" data-slide-number="<?php echo $n; ?>">
									<iframe type="text/html" width="720" height="405"
									src="<?php echo $result->Video; ?>"
									frameborder="0" allowfullscreen></iframe>
									</div>
									<?php 	
								    }
									?>
                                </div>
                                <a class="carousel-control left" href="#postGalleryCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                                <a class="carousel-control right" href="#postGalleryCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                                <!-- main slider carousel nav controls -->
                            </div>
                            <div class="col-md-5 d-none d-md-block">
                                <!-- <div class="post-map">
									<div id="address-map-canvas">
										</div>
                                    <div class="map-btns d-flex align-items-end print-hide">
                                        <div class="row w-100">
                                            <div class="col-md-12">
                                                <a href="#" data-toggle="modal" data-target="#inquiryModal" class="col btn btn-lg btn-block btn-primary inquiry" data-id="<?php echo $result->MachineID; ?>"><?php echo lang('send-inquiry'); ?></a>
                                                <a href="#showMap" id="showMap" class="col btn btn-lg btn-block btn-outline-light"><?php echo lang('show-map'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                
                                <h3 class="company-head01"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo lang('company_details'); ?></h3>

                                <div class="company-detail-map">

                                <div class="row company-detail">
                                    <div class="col-md-6"><b><?php echo lang('country'); ?>:</b></div>
                                     <div class="col-md-6"><span><?php   echo $dealerData['CountryTitle'];   ?></span></div>

                                 </div>
                                <!-- <div class="row company-detail">

                                     <div class="col-md-6"><b><?php echo lang('zip_code'); ?>:</b></div>
                                     <div class="col-md-6"><span><?php  ?></span></div>

                                       </div> -->
                                <div class="row company-detail">
                                     <div class="col-md-6"><b><?php echo lang('city'); ?>:</b></div>
                                     <div class="col-md-6"><span><?php echo $dealerData['City']; ?></span></div>

                                       </div>
                                <div class="row company-detail">
                                     <div class="col-md-6"><b><?php echo lang('street'); ?>:</b></div>
                                     <div class="col-md-6"><span><?php echo $dealerData['StreetAddress']; ?></span></div>
                                 </div>
                                </div>


                               


                                 <div class="row">
                                            <div class="col-md-6">
                                                <a href="#" data-toggle="modal" data-target="#inquiryModal" class="col btn btn-lg btn-block btn-primary inquiry" data-id="<?php echo $result->MachineID; ?>"><?php echo lang('send-inquiry'); ?></a>
                                            </div>
                                            <div class="col-md-6"><a href="#showMap" id="showMap" class="col btn btn-map btn-lg btn-block"><?php echo lang('show-map'); ?></a></div>
                                        </div>

                                

                            </div>
                        </div>
                        <div class="row print-hide">
                            <div class="col-md">
                                <ul class="carousel-indicators">
										<?php
                                        if($images || $result->FeaturedImage != ''){
                                            $n = 0;
                                            if($result->FeaturedImage != '')
                                            {
                                                $ext = pathinfo($result->FeaturedImage, PATHINFO_EXTENSION);
                                                if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
                                                      $n++;
                                                    if($n == 1){
                                                        $class = 'active';
                                                        $selected = 'class="selected"';
                                                    }else{
                                                        $class = $selected = '';
                                                    }
                                        ?>
                                                <li class="list-inline-item <?php echo $class; ?>">
                                                    <a id="carousel-selector-<?php echo $n-1; ?>" <?php echo $selected; ?> data-slide-to="<?php echo $n-1; ?>" data-target="#postGalleryCarousel">
                                                        <img src="<?php echo $result->IsImport?$result->FeaturedImage:base_url($result->FeaturedImage); ?>" width="1024" height="768" class="img-fluid">
                                                    </a>
                                                </li>
                                        <?php 
                                                }
                                            }
								        if($images){
    										foreach($images as $image){
    											$ext = pathinfo($image['ImageName'], PATHINFO_EXTENSION);
    										 if(!in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
    											  $n++;
    										if($n == 1){
    											$class = 'active';
    											$selected = 'class="selected"';
    										}else{
    											$class = $selected = '';
    										}
    										?>
    									<li class="list-inline-item <?php echo $class; ?>">
                                            <a id="carousel-selector-<?php echo $n-1; ?>" <?php echo $selected; ?> data-slide-to="<?php echo $n-1; ?>" data-target="#postGalleryCarousel">
                                                <img src="<?php echo $result->IsImport?$image['ImageName']:base_url($image['ImageName']); ?>" width="1024" height="768" class="img-fluid">
                                            </a>
                                        </li>
    									<?php 	
    										 } 	
    									  }	
                                      }		
								    }else {
									
									?>
										<li class="list-inline-item active">
                                        <a id="carousel-selector-1" class="selected" data-slide-to="1" data-target="#postGalleryCarousel">
                                            <img src="<?php echo base_url(); ?>uploads/dummy.jpg" width="1024" height="768" class="img-fluid">
                                        </a>
                                    </li>
										<?php }
									 if($result->Video){
										 if(strpos($result->Video,'embed') !== false){
											list($a,$b) = explode('embed/',$result->Video);
										 }
										?>
									<li class="list-inline-item">
                                        <a id="carousel-selector-<?php echo $n; ?>" data-slide-to="<?php echo $n; ?>" data-target="#postGalleryCarousel">
                                            <img src="https://img.youtube.com/vi/<?php echo $b; ?>/default.jpg" width="1024" height="768" class="img-fluid">
                                        </a>
                                    </li>
									<?php 	
								    }
									?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pd-content">
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-12">
                            <h2><?php echo lang('machine_data'); ?></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('machine_type'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo $result->ManufacturerYear; ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('manufacturer'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo $result->Manufacturer; ?></p>
                        </div>
                    </div>
					<?php if($result->Model){ ?>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('model'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo $result->Model; ?></p>
                        </div>
                    </div>
					<?php } if($result->ManufacturerYear){?>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('manufacturer_year'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo $result->ManufacturerYear; ?></p>
                        </div>
                    </div>
					<?php } ?>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('codition'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo condition_list($result->Condition); ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2><?php echo lang('price_and_location'); ?></h2>
                        </div>
                    </div>
					<?php if($result->Price){?>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('price'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <div class="fw-wrap">
                               <?php echo !intval($result->Price)?$result->Price:currency($result->Price); ?>
                            </div>
                        </div>
                    </div>
					<?php } if($result->Vat){?>
					<div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('vat'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <div class="fw-wrap">
                                <?php echo $result->Vat; ?>
                            </div>
                        </div>
                    </div>
					<?php } if($result->Location){?>
					<div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('location'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <div class="fw-wrap">
                                <?php echo $result->Location; ?>
                            </div>
                        </div>
                    </div>
					<?php } ?>
                    <?php /*?>
                    <div class="row print-hide">
                        <div class="col-md-12 mb-3">
                            <a href="#" data-toggle="modal" data-target="#inquiryModal" class="btn btn-primary inquiry" data-id="<?php echo $result->MachineID; ?>"><?php echo lang('request-price'); ?></a>
                        </div>
                    </div>
                    <?php */?>
                    <div class="row">
                        <div class="col-md-12">
                            <h2><?php echo lang('offer_detail'); ?></h2>
                        </div>
                    </div>
					<?php if($result->MachineID){?>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('listing_id'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo $result->MachineID; ?></p>
                        </div>
                    </div>
					<?php } if($result->ReferenceNumber){?>
					<div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('reference_number'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo $result->ReferenceNumber; ?></p>
                        </div>
                    </div>
					<?php } if($result->SerialNumber){?>
					<div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('serial_number'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo $result->SerialNumber; ?></p>
                        </div>
                    </div>
					<?php } if($result->UpdatedAt){?>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('last_updated'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo dateformat($result->UpdatedAt,'de'); ?></p>
                        </div>
                    </div>
					<?php } if($result->TotatSeen){?>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('total_view'); ?></p>
                        </div>
                        <div class="col-md-8">
                            <p><?php echo $result->TotatSeen; ?></p>
                        </div>
                    </div>
					<?php } ?>
                    <div class="row">
                        <div class="col-md-4">
                            <p><?php echo lang('sale_inquiry'); ?></p>
                        </div>
                        <div class="col-md-8">
							<p>
						 <a href="#" data-toggle="modal" data-target="#inquiryModal" class="inquiry" data-id="<?php echo $result->MachineID; ?>"><?php echo getValue('inquiries',array('MachineID' => $result->MachineID,'Status' => 1),'num_rows'); ?></a></p>
                        </div>
                    </div>
					<?php if($result->Description){ ?>
                    <div class="row">
                        <div class="col-md-12">
                            <h2><?php echo lang('Description'); ?></h2>
                        </div>
                    </div>
					<?php } ?>
                    <div class="row">
                        <div class="col-md-12">
                           <?php
							 
							      echo $result->Description;
							     
								if($images){
								foreach($images as $image){
											$ext = pathinfo($image['ImageName'], PATHINFO_EXTENSION);
										 if(in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
											  $n++;
											 if($ext == 'doc' or $ext == 'docx'){
													$ext = 'doc';
												}else if($ext == 'xls' or $ext == 'xlsx'){
													$ext = 'xls';
												}
										?>
                                        <a href="<?php echo $result->IsImport?$image['ImageName']:base_url($image['ImageName']); ?>" target="_blank">
                                           <img src="<?php echo base_url('uploads/'.$ext.'.png'); ?>" class="img-fluid">
                                        </a>
									<?php 	
										 }	
									   }	 								 
									 }
									?>
                        </div>
                    </div>
                    <div class="row print-hide">
                        <div class="col-md-12">
                            <ul class="rrssb-buttons">
							
                                <li class="rrssb-facebook">
									
                                    <a href="https://www.facebook.com/sharer.php?u=<?php echo friendly_url('detail/',$result->Title.' '.$result->MachineID); ?>" class="popup">
                                        <span class="rrssb-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29">
                                                <path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z">
                                            </svg>
                                        </span>
                                        <span class="rrssb-text">facebook</span>
                                    </a>
                                </li>
                                <li class="rrssb-twitter">
                                    <!-- Replace href with your Meta and URL information  -->
                                    <a href="https://twitter.com/intent/share?url=<?php echo friendly_url('detail/',$result->Title.' '.$result->MachineID); ?>"
                                       class="popup">
                                        <span class="rrssb-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
                                                <path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62a15.093 15.093 0 0 1-8.86-2.32c2.702.18 5.375-.648 7.507-2.32a5.417 5.417 0 0 1-4.49-3.64c.802.13 1.62.077 2.4-.154a5.416 5.416 0 0 1-4.412-5.11 5.43 5.43 0 0 0 2.168.387A5.416 5.416 0 0 1 2.89 4.498a15.09 15.09 0 0 0 10.913 5.573 5.185 5.185 0 0 1 3.434-6.48 5.18 5.18 0 0 1 5.546 1.682 9.076 9.076 0 0 0 3.33-1.317 5.038 5.038 0 0 1-2.4 2.942 9.068 9.068 0 0 0 3.02-.85 5.05 5.05 0 0 1-2.48 2.71z">
                                            </svg>
                                        </span>
                                        <span class="rrssb-text">twitter</span>
                                    </a>
                                </li>
                                <li class="rrssb-googleplus">
                                    <!-- Replace href with your meta and URL information.  -->
                                    <a href="https://plus.google.com/share?url=<?php echo friendly_url('detail/',$result->Title.' '.$result->MachineID); ?>" class="popup">
                                        <span class="rrssb-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                <path d="M21 8.29h-1.95v2.6h-2.6v1.82h2.6v2.6H21v-2.6h2.6v-1.885H21V8.29zM7.614 10.306v2.925h3.9c-.26 1.69-1.755 2.925-3.9 2.925-2.34 0-4.29-2.016-4.29-4.354s1.885-4.353 4.29-4.353c1.104 0 2.014.326 2.794 1.105l2.08-2.08c-1.3-1.17-2.924-1.883-4.874-1.883C3.65 4.586.4 7.835.4 11.8s3.25 7.212 7.214 7.212c4.224 0 6.953-2.988 6.953-7.082 0-.52-.065-1.104-.13-1.624H7.614z">
                                            </svg>
                                        </span>
                                        <span class="rrssb-text">google+</span>
                                    </a>
                                </li>
                                <li class="rrssb-linkedin">
                                    <!-- Replace href with your meta and URL information -->
                                    <a href="http://www.linkedin.com/cws/share?url=<?php echo friendly_url('detail/',$result->Title.' '.$result->MachineID); ?>" class="popup">
                                        <span class="rrssb-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28">
                                                <path d="M25.424 15.887v8.447h-4.896v-7.882c0-1.98-.71-3.33-2.48-3.33-1.354 0-2.158.91-2.514 1.802-.13.315-.162.753-.162 1.194v8.216h-4.9s.067-13.35 0-14.73h4.9v2.087c-.01.017-.023.033-.033.05h.032v-.05c.65-1.002 1.812-2.435 4.414-2.435 3.222 0 5.638 2.106 5.638 6.632zM5.348 2.5c-1.676 0-2.772 1.093-2.772 2.54 0 1.42 1.066 2.538 2.717 2.546h.032c1.71 0 2.77-1.132 2.77-2.546C8.056 3.593 7.02 2.5 5.344 2.5h.005zm-2.48 21.834h4.896V9.604H2.867v14.73z">
                                            </svg>
                                        </span>
                                        <span class="rrssb-text">linkedin</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <aside class="col-md-12">
                            <div class="prod-sidebar">
								<?php
								$user = get_user_info(array('a.UserID' => $result->UserID));
								?>
                                <h2><?php echo lang('dealer'); ?></h2>
                                <h3><a href="javascript:void(0);"><?php echo $user->CompanyName; ?></a></h3>
                                <p><?php echo lang('contact-person');?>: <?php echo $user->Title; ?></p>
                                <p><?php echo lang('postcode');?>: <?php echo $user->PostalCode; ?></p>
                                <p><?php echo lang('city');?>: <?php echo $user->City; ?></p>
                                <p><?php echo lang('street_address');?>: <?php echo $user->StreetAddress; ?></p>
                                <p><?php echo lang('ph');?>: <?php echo $user->Phone; ?></p>
                                <p><?php echo lang('last-online');?>: <?php echo DateDiff($user->LastLogin); ?></p>
                                <p><?php echo lang('registered-since');?>: <?php echo dateformat($user->CreatedAt,'year'); ?></p>
                                <p><a href="<?php echo friendly_url('dealer-listing/',$user->Title.' '.$result->UserID); ?>"><?php echo get_machine_row(array('a.UserID' => $result->UserID)).' '.lang('advertisements-online'); ?> </a></p>
                            </div>
                        </aside>
                        <aside class="col-md-12 print-hide">
                            <div class="pros-form">
                                <h4><?php echo lang('send-inquiry'); ?></h4>
								<?php  $this->load->view('frontend/layouts/inquiry-form',array('class' => 'btn-outline-light','id' => $result->MachineID)); ?>	
                            </div>
                        </aside>
                        <?php /*?>
                        <aside class="col-md-12 print-hide">
                            <div class="pro-sidebar-pf">
                                <h2><?php echo lang('phone-and-fax'); ?></h2>
                                <div class="printArea d-none">
                                    <div class="row">
										<?php if($user->Phone){ ?>
                                        <div class="col-md-12"><?php echo lang('ph'); ?>: <b><?php echo $user->Phone; ?></b></div>
										<?php } if($user->Fax){ ?>
                                        <div class="col-md-12"><?php echo lang('fax'); ?>: <b><?php echo $user->Fax; ?></b></div>
										<?php } ?>
                                    </div>
                                </div>
                                <div class="dealer-info">
                                    <a href="#" class="btn btn-primary" id="showNumber"><i class="fa fa-phone" aria-hidden="true"></i><?php echo lang('show').' '.lang('ph'); ?></a>
                                    <div class="infoShowBlock">
										<?php if($user->Phone){ ?>
										<div class="row">
                                            <div class="col-md-5"> <?php echo lang('ph'); ?>:</div>
                                            <div class="col-md-7"><b><?php echo $user->Phone; ?></b></div>
                                        </div>
										<?php } if($user->Title){ ?>
                                        
                                        <div class="row">
                                            <div class="col-md-5"> <?php echo lang('contact-dealer'); ?>:</div>
                                            <div class="col-md-7">
                                                <strong><?php echo $user->Title; ?> </strong>
                                            </div>
                                        </div>
										<?php } ?>
                                        <div class="row">
                                            <div class="col-md-5"><?php echo lang('listing_id'); ?>:</div>
                                            <div class="col-md-7"><strong><?php echo $result->MachineID; ?> at Maschine Pilot</strong></div>
                                        </div>
                                    </div>
                                </div>
								<?php if($user->Fax){ ?>
                                <div class="dealer-info">
                                    <a href="#" class="btn btn-primary" id="button-fax"><i class="fa fa-fax" aria-hidden="true"></i> <?php echo lang('show').' '.lang('fax'); ?></a>
									
                                    <div class="infoShowBlock">
                                        <i class="fa fa-fax" aria-hidden="true"></i> <?php echo lang('fax'); ?>: <b><?php echo $user->Fax; ?></b>
                                    </div>
                                </div>
								<?php } ?>

                            </div>
                        </aside>
                        <?php */?>
                    </div>
                </div>
            </div>
            <div class="row print-hide">
                <div class="col-md-12">
                    <div class="pg-footer-links">
                        <a href="#" data-toggle="modal" data-target="#inquiryModal" class="btn btn-primary inquiry" data-id="<?php echo $result->MachineID; ?>"><i class="fa fa-comment" aria-hidden="true"></i> <?php echo lang('contact-dealer'); ?></a>
                        <a href="#" class="btn btn-outline-dark print_page" ><i class="fa fa-print" aria-hidden="true"></i> <?php echo lang('print-page'); ?></a>
                        <a href="#wishlist" class="btn btn-outline-dark wishlist" title="<?php echo lang($wish_label); ?>" data-id="<?php echo $result->MachineID; ?>" id="wishlist"><i class="fa fa-star-o"></i> <?php echo lang($wish_label); ?></a>
                        <a href="#" class="btn btn-outline-dark" data-toggle="modal" data-target="#sharefrndModal" data-id="<?php echo $result->MachineID; ?>"><i class="fa fa-envelope"></i> <?php echo lang('share'); ?></a>
                        <a href="#" class="btn btn-outline-dark" data-toggle="modal" data-target="#reportModal" data-id="<?php echo $result->MachineID; ?>"><i class="fa fa-bullhorn"></i> <?php echo lang('report'); ?></a>
                    </div>
                </div>
            </div>
				
			<?php }else{?>
				<div class="col-md-12">
								
							<div class="no-found">
								<img src="<?php echo base_url(); ?>assets/frontend/images/grey.png">
								<h3><?php echo lang('record-not-found'); ?></h3></div>
							</div>	
			<?php
			} $this->load->view('frontend/layouts/advertise'); ?>	
        </div>
    </section>
	 <?php  
	 if($result){
		$this->load->view('frontend/layouts/share-to-friend',array('class' => 'btn-outline-success', 'id' => $result->MachineID)); 
		$this->load->view('frontend/layouts/report',array('class' => 'btn-outline-success', 'id' => $result->MachineID));
	 }
		?>  