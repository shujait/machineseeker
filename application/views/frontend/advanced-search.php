<style type="text/css">
    .select2-container--default .select2-selection--single {
        display: block;
        width: 100%;
        height: calc(2.25rem + 2px);
        padding: 0.375rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
        -webkit-transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
        -o-transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    }

    .pdtp-10 {
        padding-top: 10px;
    }

</style>
<section class="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo lang('advanced-search').' '.lang('for').' '.lang('listings'); ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info" role="alert"><?php echo lang('advance_search_hit'); ?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo base_url('erweiterte-suche'); ?>" method="post" class="advance-search">
                    <div class="row-container">
                        <div class="row">
                            <label class="offset-1 col-md-3 control-label pdtp-10" for="Category"><?php echo lang('category'); ?></label>
                            <div class="col-md-6">
                                <select class="form-control getSubCategory select2" name="CategoryID" id="Category" data-type="category" data-page-type="search">
                                    <option value="">--- <?php echo lang('select_main_category'); ?> ---</option>
                                    <?php 
                                            $active_category=" (`categories`.`CategoryID` IN (SELECT `CategoryID` FROM `machines` where `machines`.`IsActive`=1)
                                                OR (`categories`.`CategoryID` IN (SELECT `categories`.`ParentID`
                                                FROM `categories`
                                                where  `categories`.`CategoryID` IN (SELECT `CategoryID` FROM `machines` where `machines`.`IsActive`=1)
                                                AND `categories`.`Hide` = '0'
                                                AND `categories`.`ParentID` != 0
                                                group by `categories`.`ParentID`)))";
                                        $arr =  getCategories($language,'categories.ParentID = 0',$limit = false,$start = 0,$categorytype=false,$active_category);
                                        if(!empty($arr)){
                                            foreach($arr as $result){
                                        ?>
                                    <option value="<?php echo $result->CategoryID; ?>"><?php echo $result->Title; ?></option>
                                    <?php 
                                            } 
                                        } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row-container" id="subCategoryDiv">
                    </div>
                    <div class="row-container" id="subSubCategoryDiv">
                    </div>
                    <div class="row-container">
                        <!-- <div class="row ">
                                <label class="col-md-3 control-label" for="MachineType"><?php echo lang('machine_type'); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" id="MachineType" name="MachineType">
                                </div>
                                <div class="col-md-3 hint">
                                    <?php echo lang('e-g'); ?>. <?php echo lang('milling').' '.lang('machine'); ?>
                                </div>
                            </div> -->
                        <div class="row">
                            <label class="offset-1 col-md-3 card-title pdtp-10" for="Manufacturer">
                                <?php echo lang('manufacturer'); ?>
                                <a href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('e-g'); ?>. <?php echo lang('maho'); ?>" class="tooltip001">?</a>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="Manufacturer" name="Manufacturer">
                            </div>
                            <!-- <div class="col-md-3 hint pdtp-10">
                                <?php echo lang('e-g'); ?>. <?php echo lang('maho'); ?>
                            </div> -->
                        </div>
                        <div class="row">
                            <label class="offset-1 col-md-3 card-title pdtp-10" for="Model">
                                <?php echo lang('model');?>
                                <a href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('e-g'); ?>. <?php echo lang('MH-800'); ?>" class="tooltip001">?</a>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="Model" id="Model">
                            </div>
                            <!-- <div class="col-md-3 hint pdtp-10">
                                <?php echo lang('e-g'); ?>. <?php echo lang('MH-800'); ?>
                            </div> -->
                        </div>
                        <div class="row">
                            <label class="offset-1 col-md-3 card-title pdtp-10" for="TechnicalData">
                                <?php echo lang('technical-data'); ?>
                                <a href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('e-g'); ?>. <?php echo lang('technical-eg'); ?>" class="tooltip001">?</a>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="Description" id="TechnicalData">
                            </div>
                            <!-- <div class="col-md-3 hint pdtp-10">
                                <?php echo lang('e-g'); ?>. <?php echo lang('technical-eg'); ?>
                            </div> -->
                        </div>
                        <div class="row">
                            <label class="offset-1 col-md-3 card-title pdtp-10" for="ManufacturerYear">
                                <?php echo lang('manufacturer_year'); ?>
                                <a href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('2000-2014'); ?>" class="tooltip001">?</a>
                            </label>
                            <div class="col-md-3">
                                <select class="form-control" name="ManufacturerYear" id="ManufacturerYear">
                                    <option value=""><?php echo lang('from'); ?></option>
                                    <?php 
										$arr = YearList();
										if(!empty($arr)){ 
											foreach($arr as $year){ ?>
                                    <option value="<?php echo $year; ?>"><?php echo $year; ?> </option>
                                    <?php } } ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label class="d-none" for="ManufacturerYear"></label>
                                <select class="form-control" name="ManufacturerYear2" id="ManufacturerYear">
                                    <option value=""><?php echo lang('to'); ?></option>
                                    <?php 
										$arr = YearList();
										if(!empty($arr)){ 
											foreach($arr as $year){ ?>
                                    <option value="<?php echo $year; ?>"><?php echo $year; ?> </option>
                                    <?php } } ?>
                                </select>
                            </div>
                            <!-- <div class="col-md-3 hint pdtp-10">
                                <?php echo lang('2000-2014'); ?>
                            </div> -->
                        </div>
                        <div class="row">
                            <label class="offset-1 col-md-3 control-label pdtp-10" for="Condition"><?php echo lang('codition'); ?></label>
                            <div class="col-md-6">
                                <select class="form-control" name="Condition" id="Condition">
                                    <option value=""><?php echo lang('all'); ?></option>
                                    <?php 
											 $condition_list = condition_list(true);
											 foreach($condition_list as $k => $val){
											 echo '<option value="'.$k.'">'.$val.'</option>';
									             }
										  ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row-container">
                            <div class="row">
                                <label class="col-md-3 control-label" for="SerialNumber"><?php echo lang('serial_number'); ?></label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="SerialNumber" id="SerialNumber">
                                </div>
                                <div class="col-md-3 hint">
                                   <?php echo lang('search-serial-number-hit'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-3 control-label" for="machinePhotos"><?php echo lang('machine').' '.lang('photo'); ?></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="Photo" id="machinePhotos">
                                        <option value=""><?php echo lang('any'); ?></option>
                                        <option value="1">1 <?php echo lang('or').' '.lang('more'); ?></option>
                                    </select>
                                </div>
                                <div class="col-md-3 hint">
                                   <?php echo lang('search-machine-photo-hit'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-md-3 control-label" for="machineVideo"><?php echo lang('machine').' '.lang('video'); ?></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="Video" id="machineVideo">
                                        <option value=""><?php echo lang('any'); ?></option>
                                        <option value="1"><?php echo lang('must-be-provided-hit'); ?></option>
                                    </select>
                                </div>
                                <div class="col-md-3 hint">
                                    <?php echo lang('machine-video-hit'); ?>
                                </div>
                            </div>
                        </div> -->

                    <div class="row-container">
                        <div class="row">
                            <label class="offset-1 col-md-3 card-title pdtp-10" for="MachineID">
                                <?php echo lang('listing_id'); ?>
                                <a href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('e-g'); ?>. <?php echo lang('machine-eg'); ?>" class="tooltip001">?</a>
                            </label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="MachineID" id="MachineID">
                            </div>
                            <!-- <div class="col-md-3 hint pdtp-10">
                                <?php echo lang('e-g'); ?>. <?php echo lang('machine-eg'); ?>
                            </div> -->
                        </div>
                        <div class="row">
                            <label class="offset-1 col-md-3 card-title pdtp-10" for="ReferenceNumber">
                                <?php echo lang('reference_number'); ?>
                                <a href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('e-g'); ?>. <?php echo lang('reference-eg'); ?>" class="tooltip001">?</a>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="ReferenceNumber" id="ReferenceNumber">
                            </div>
                            <!-- <div class="col-md-3 hint pdtp-10">
                                <?php echo lang('e-g'); ?>. <?php echo lang('reference-eg'); ?>
                            </div> -->
                        </div>
                    </div>
                    <div class="row-container">
                        <div class="row">
                            <label class="offset-1 col-md-3 card-title pdtp-10" for="PriceFrom">
                                <?php echo lang('price'); ?>
                                <a href="javascript:;" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('from').' - '.lang('to').' '.lang('in'); ?> €" class="tooltip001">?</a>
                            </label>
                            <div class="col-md-3">
                                <input type="number" class="form-control" name="PriceFrom" placeholder="<?php echo lang('from'); ?>">
                            </div>
                            <div class="col-md-3">
                                <input type="number" class="form-control" name="PriceTo" placeholder="<?php echo lang('to'); ?>">
                            </div>
                            <!-- <div class="col-md-3 hint pdtp-10">
                                <?php echo lang('from').' - '.lang('to').' '.lang('in'); ?> €
                            </div> -->
                        </div>
                    </div>

                    <!-- <div class="row-container">
                            <div class="row">
                                <label class="col-md-3 control-label" for="Dealers-1"><?php echo lang('dealer'); ?></label>
                                <div class="col-md-6">
                                    <select class="form-control" id="Dealers">
                                        <option value="-1"><?php echo lang('all-offers'); ?></option>
                                        <option value="1"><?php echo lang('dealers').' '.strtolower(lang('only')); ?></option>
                                        <option value="0"><?php echo lang('no').' '.lang('dealers'); ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="row dealers">
                                <label class="col-md-3 control-label" for="Dealers-2"><?php echo lang('dealer'); ?></label>
                                <div class="col-md-6">
                                    <select class="form-control" name="UserID" id="Dealers-2">
                                        <option value=""> --- <?php echo lang('dealer-listings'); ?> ---</option>
										<?php if($results){
										foreach($results as $val){
										?>
                                        <option value="<?php echo $val->UserID ?>"><?php echo $val->Title; ?></option>
										<?php } } ?>
                                    </select>
								</div>
                            </div>
                        </div> -->
                    <div class="row-container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary"><?php echo lang('search'); ?></button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</section>
