<section class="signup-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo lang('summary'); ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if($message != ''){ ?>
                    <div class="alert alert-danger">
                        <?php echo $message; ?>
                    </div>
                <?php } ?>
                <?php
                    $UserID = $this->session->userdata('last_insert_id');
                ?>
                <form action="<?php echo base_url('purchase/PackagePurchase/'.$UserID); ?>" class="bank-transfer-form" method="post">
                    <div class="row">
                        <div class="col-md-8 mb-4">
                            <?php
                                $default_lang = getDefaultLanguage();
                                $item = getValue('packages_text',array('PackageID' => $rec->PackageID, 'SystemLanguageID' => $default_lang->SystemLanguageID));
                                $price = ($v->RegularPrice-($v->RegularPrice/100)*$v->Discount);
                                $amount = $v->RegularPrice*$v->QTY;
                                $total = $price*$v->Duration;
//                                print_r($plan);
//                                print_r($dealer_profile);
//                                print_r($company);
//                                print_r($category);
//                                print_r($dealer_img);
                            ?>
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="h5"><?php echo lang('payment-summery-title'); ?></h2>
                                </div>
                                <div class="card-body">
                                    <h3 class="h5 mb-2"><?php echo lang('my-profile'); ?></h3>
                                    <?php
                                        if(file_exists($dealer_img)){
                                    ?>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('your-logo'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><img src="<?php echo base_url().$dealer_img;?>" height="70px" width="100px"></div>
                                    </div>
                                    <?php } ?>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('full_name'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $company['title'].' '.$company['first_name'].' '.$company['last_name']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('company_name'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $plan['companyName']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('street_address'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $plan['StreetAddress']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('postcode'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $plan['postcodeTown']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('city'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $plan['City']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('ust_id_label2'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $plan['Vat']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('phone'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $company['CountryCode'].$company['ph']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('fax'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo (!empty($company['mob']))?$company['CountryFaxCode'].$company['mob']:''; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('website_link'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $company['website']; ?></div>
                                    </div>
                                    <?php $con = confirmationBox();
                                    if($con){
                                        ?>
<!--                                        <div class="row">-->
<!--                                            <div class="col-md col-sm-6 mb-3">--><?php //echo lang('privacy-policy'); ?><!--</div>-->
<!--                                            <div class="col-md col-sm-6 mb-3">--><?php //echo $con->row()->Content; ?><!--</div>-->
<!--                                        </div>-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h6 class="mb-3"><?php echo lang('privacy-policy'); ?></h6>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="checkbox w-100">
                                                    <input type="checkbox" name="policy_read" id="policyread" value="yes">
                                                    <label for="policyread"><?php echo $con->row()->Content; ?></label>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <!--
                                    <hr>
                                    <h3 class="h5 mb-2"><?php echo lang('dealer'); ?></h3>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('company-description'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo $dealer_profile['company_description']; ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('privacy-policy'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><input type="checkbox" <?php ($dealer_profile['policy_read'] == 'yes')?'checked':'' ?> checked disabled></div>
                                    </div>
                                    -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <input type="hidden" name="paymentMethod" id="PaymentType" value="<?php echo $paymentMethod; ?>">
                                    <button type="button" class="btn btn-primary btn-lg submit-payment" style="margin-top: 15px;"><?php echo lang('final-btn'); ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4">
                            <?php
                            $coupon_discount_total = $this->session->userdata('coupon_discount_total');
                            $default_lang = getDefaultLanguage();
                            $item = getValue('packages_text',array('PackageID' => $rec->PackageID, 'SystemLanguageID' => $default_lang->SystemLanguageID));
                            $price = ($v->RegularPrice-($v->RegularPrice/100)*$v->Discount);
                            $amount = $v->RegularPrice*$v->QTY;
                            $total = $price*$v->Duration;
                            ?>
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="h5"><?php echo lang('order-summary'); ?></h2>
                                </div>
                                <div class="card-body">
                                    <h3 class="h5 mb-2"><?php echo $item->Title; ?> / <?php echo $v->QTY; ?></h3>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><strong><?php echo lang('per_advertisement'); ?> / <?php echo lang('month'); ?></strong></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo currency($price); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('equals_to').' '.lang('per_month'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"> <?php echo currency($total); ?></div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><strong><?php echo lang('contract_period'); ?> </strong></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><strong><?php
                                                $created_date = date('Y-m-d');
                                                $ExpiryDate = date('Y-m-d',strtotime("+".$v->Duration." month"));
                                                echo dateformat($created_date,'de'); ?> - <?php echo dateformat($ExpiryDate,'de'); ?></strong></div>
                                    </div>
                                    <?php //$amount = $total / 1.19; ?>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3">Zwischensumme</div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo currency($total); ?></div>
                                    </div>
                                    <?php
                                    $tax = $total  * (19 / 100);
                                    $g_total = $total + $tax;
                                    if($this->session->userdata('coupon_discount_total')){
                                        $g_total = ($g_total-$coupon_discount_total);
                                    }
                                    ?>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3">Zzgl. 19% MwSt.</div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo currency($tax); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('payment-method') ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo ($paymentMethod == 'b')?'Bank':'Paypal'; ?></div>
                                    </div>
                                    <?php
                                    if($this->session->userdata('coupon_discount_total')){
                                    ?>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><strong><?php echo  lang('discount_amount'); ?></strong></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><strong><?php echo currency($coupon_discount_total); ?></strong></div>
                                    </div>
                                    <?php } ?>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3 text-capitalize"><strong><?php echo  lang('total'); ?></strong></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><strong><?php echo currency($g_total); ?></strong></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md text-right mt-3 mb-3">
                        <form action="<?php echo $site_setting->PaypalUrl; ?>" class="paypal-form" method="post">
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="business" value="<?php echo $site_setting->PaypalEmail; ?>">
                            <input type="hidden" name="item_name" value="<?php echo $item->Title; ?>">
                            <input type="hidden" name="item_number" value="<?php echo $rec->PurchaseID; ?>">
                            <input type="hidden" name="currency_code" value="EUR">
                            <input type="hidden" name="amount" value="<?php echo $total+$site_setting->PaypalFee; ?>">
                            <input name="return" type="hidden" value="<?php echo base_url('purchase/PackagePurchase/'.$UserID); ?>">
                            <input type="hidden" name="cancel_return"  value="<?php echo base_url('paypal-cancel').'/'.$rec->PurchaseID; ?>">
                            <input type="hidden" name="notify_url"  value="<?php echo base_url('paypal-ipn').'/'.$rec->PurchaseID; ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(".payment-method").click(function (){
        $("#PaymentType").val($(this).data('payment-type'));
    });

    $(".submit-payment").click(function (){
        if($('#policyread:checkbox:checked').length < 1)
        {
            showError("<?= lang('please_check_checkbox') ?>");
            return false;
        }
        var PaymentType = $("#PaymentType").val();
        if(PaymentType == 'b')
        {
            $(".bank-transfer-form").submit();
        }
        else{
            $(".paypal-form").submit();
        }
    });

</script>
