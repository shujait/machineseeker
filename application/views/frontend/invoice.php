<section class="signup-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo lang('invoice'); ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php if($message != ''){ ?>
                <div class="alert alert-danger">
                    <?php echo $message; ?>
                </div>
            <?php } ?>
                <form action="<?php echo base_url(); ?>zusammenfassung" class="bank-transfer-form" method="post">
                    <div class="row">
                        <div class="col-md-8 mb-4">
                            <div class="card h-100">
                                <div class="card-header">
                                    <h2 class="h5"><?php echo lang('payment-method'); ?></h2>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md">
                                            <div class="row">
                                                <div class="col-md mb-3">
                                                    <div class="radio">
                                                        <input type="radio" name="PaymentMethod" data-payment-type="b" value="b" class="form-control payment-method" id="BankTransfer" checked>
                                                        <label for="BankTransfer"><?php echo lang('bank_label'); ?></label>
                                                    </div>
                                                </div>
                                                <div class="col-md mb-3">
                                                   <img src="<?php echo base_url();?>assets/frontend/images/bank-transfer-img.jpg" alt="" class="img-fluid" width="45">
                                                </div>
                                                <div class="col-md-12">
                                                    <p><?php echo lang('invoice-text'); ?></p>
                                                </div>
                                            </div>
                                            <?php
                                            if($site_setting->PaypalActive == 1)
                                            {
                                            ?>
                                                <div class="row">
                                                    <div class="col-md mb-3">
                                                        <div class="radio">
                                                            <input type="radio" name="PaymentMethod" data-payment-type="p" value="p" class="form-control payment-method" id="Paypal">
                                                            <label for="Paypal"><?php echo lang('paypal'); ?></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md mb-3">
                                                       <img src="<?php echo base_url();?>assets/frontend/images/paypal-img.jpg" alt="" class="img-fluid">
                                                    </div>
                                                </div>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 mb-4">
                            <?php
                             $default_lang = getDefaultLanguage();
                             $item = getValue('packages_text',array('PackageID' => $rec->PackageID, 'SystemLanguageID' => $default_lang->SystemLanguageID));
                             $price = ($v->RegularPrice-($v->RegularPrice/100)*$v->Discount);
                             $amount = $v->RegularPrice*$v->QTY;
                             $total = $price*$v->Duration;
                             ?>
                            <div class="card">
                                <div class="card-header">
                                    <h2 class="h5"><?php echo lang('order-summary'); ?></h2>
                                </div>
                                <div class="card-body">
                                    <h3 class="h5 mb-2"><?php echo $item->Title; ?> / <?php echo $v->QTY; ?></h3>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><strong><?php echo lang('per_advertisement'); ?> / <?php echo lang('month'); ?></strong></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo currency($price); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><?php echo lang('equals_to').' '.lang('per_month'); ?></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"> <?php echo currency($total); ?></div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><strong><?php echo lang('contract_period'); ?> </strong></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><strong><?php
                                            $created_date = date('Y-m-d');
                                            $ExpiryDate = date('Y-m-d',strtotime("+".$v->Duration." month"));
                                            echo dateformat($created_date,'de'); ?> - <?php echo dateformat($ExpiryDate,'de'); ?></strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3">Zwischensumme</div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo currency($total); ?></div>
                                    </div>
                                    <?php
                                        $tax = $total  * (19 / 100);
                                        $g_total = $total + $tax;
                                    ?>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3">Zzgl. 19% MwSt.</div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><?php echo currency($tax); ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md col-sm-6 mb-3"><strong><?php echo  lang('total'); ?></strong></div>
                                        <div class="col-md col-sm-6 mb-3 text-right"><strong class="show_g_total"><?php echo currency($g_total); ?></strong></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-3 mt-2"><strong><?php echo  lang('coupon_code'); ?></strong></div>
                                        <div class="col-md-8 col-sm-9 mb-3 text-right">
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" name="couponCode" id="couponCode" placeholder="Code" aria-label="Coupon Code" aria-describedby="checkCouponCode">
                                                <div class="input-group-append">
                                                    <button type="button" class="btn btn-primary" id="checkCouponCode"><?= lang('coupon_code_label') ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <input type="hidden" id="PaymentType" value="b">
                                    <button type="submit" class="btn btn-primary btn-lg submit-payment" style="margin-top: 15px;"><?php echo lang('next'); ?></button>
                                    <input type="hidden" name="form_type" value="summery">
                                    <input type="hidden" name="user_id" value="<?php echo $UserID; ?>">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>

                <div class="row">
                    <div class="col-md text-right mt-3 mb-3">
                        <form action="<?php echo $site_setting->PaypalUrl; ?>" class="paypal-form" method="post">
                            <?php
                                $paypalTotal = $g_total+$site_setting->PaypalFee;
                            ?>
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="business" value="<?php echo $site_setting->PaypalEmail; ?>">
                            <input type="hidden" name="item_name" value="<?php echo $item->Title; ?>">
                            <input type="hidden" name="item_number" value="<?php echo $rec->PurchaseID; ?>">
                            <input type="hidden" name="currency_code" value="EUR">
                            <input type="hidden" name="amount" id="totalAmount" value="<?php echo $paypalTotal; ?>">
                            <input name="return" type="hidden" value="<?php echo base_url('purchase/PackagePurchase/'.$UserID); ?>">
                            <input type="hidden" name="cancel_return"  value="<?php echo base_url('paypal-cancel').'/'.$rec->PurchaseID; ?>">
                            <input type="hidden" name="notify_url"  value="<?php echo base_url('paypal-ipn').'/'.$rec->PurchaseID; ?>">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('#checkCouponCode').on('click', function (){
        var code = $('#couponCode').val();
        var total = "<?php echo $g_total; ?>";
        $.ajax({
            method: "POST",
            url: base_url + "cms/validate-voucher-code",
            data: { code: code, total:total },
            dataType: 'json'
        }).done(function(result){
            var new_g_total = result.g_total;
            var new_total = result.total;
            $("#totalAmount").val(new_total);
            $(".show_g_total").text('');
            $(".show_g_total").text(new_g_total);
            showSuccess(result.message);
            $('#checkCouponCode').attr("disabled", 'disabled');
        }).fail(function(response) {
            var failMessage = "<?php lang('something_went_wrong'); ?>";
            showError(failMessage);
        });
    });
</script>