    <section class="signup-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo lang('dealer-title'); ?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p><?php echo lang('dealer-heading'); ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo base_url(); ?>frontend/purchase/action" onsubmit="return false;" class="form_data inquiry-form" method="post" enctype="multipart/form-data" data-parsley-validate novalidate>
                        <div class="row form-group">
                            <div class="col-md-12">
                                 <!--
                                <div class="border alert p-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="mb-3">
                                                <?php echo lang('dealer-title'); ?>
                                                <button type="button" class="help btn btn-default" data-container="body" data-toggle="popover" data-trigger="hover click" data-placement="right" data-html="true" data-content="<?php echo lang('dealer-hints'); ?>" data-original-title="" title="">
                                                    ?
                                                </button>
                                            </h5>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="radio w-100">
                                                <input type="radio" name="ShowDealerProfile" value="4" id="ShowDealerProfile" checked>
                                                <label for="ShowDealerProfile"><?php echo lang('show-dealer-profile'); ?></label>
                                            </div>
                                            <div class="radio w-100">
                                                <input type="radio" name="ShowDealerProfile" value="3" id="DNShowDealerProfile">
                                                <label for="DNShowDealerProfile"><?php echo lang('do-not-show-dealer'); ?></label>
                                                <div class="reveal-if-active">
                                                    <div class="badge-light bg-warning shadow">
                                                     <?php echo lang('no-dealer-hints'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 -->
                                 <input type="hidden" name="ShowDealerProfile" value="4" id="ShowDealerProfile">
                                <div class="border alert p-3 showHideForm">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="mb-3">
                                                <?php echo lang('company-description'); ?>
                                                <button type="button" class="help btn btn-default" data-container="body" data-toggle="popover" data-trigger="hover click" data-placement="right" data-html="true" data-content="<?php echo lang('company-description-hints'); ?>" data-original-title="" title="">
                                                    ?
                                                </button>
                                            </h5>
                                        </div>
                                        <div class="col-md-12">
                                            <textarea name="company_description" class="form-control" placeholder="<?php echo lang('company-description-placeholder'); ?>" rows="10" cols="20"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="border alert p-3 showHideForm">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="mb-3"><?php echo lang('your-logo'); ?></h5>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="input-group mb-3">
                                                <div class="custom-file">
                                                    <input type="file" name="file" class="custom-file-input" id="inputGroupFile02" accept="image/x-png,image/jpeg">
                                                    <label class="custom-file-label" for="inputGroupFile02"></label>
                                                </div>
                                            </div>
                                            <p><?php echo lang('logo-file-size'); ?></p>
                                        </div>
                                    </div>
                                </div>

<!--								--><?php //$con = confirmationBox();
//                                    if($con){
//                                ?>
<!--                                <div class="border alert p-3">-->
<!--                                    <div class="row">-->
<!--                                        <div class="col-md-12">-->
<!--                                            <h5 class="mb-3">--><?php //echo lang('privacy-policy'); ?><!--</h5>-->
<!--                                        </div>-->
<!--                                        <div class="col-md-12">-->
<!--											<div class="checkbox w-100">-->
<!--											<input type="checkbox" name="policy_read" id="policyread" value="yes">-->
<!--                                            <label for="policyread">--><?php //echo $con->row()->Content; ?><!--</label>-->
<!--                                          </div>-->
<!--									    </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                --><?php //} ?>
                            </div>
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg w-25"><?php echo lang('next'); ?></button>
								<input type="hidden" name="form_type" value="final-setp">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>