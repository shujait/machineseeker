<section class="signup-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo lang('paypal'); ?></h1>
                </div>
            </div>
           
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo $site_setting->PaypalUrl; ?>" class="inquiry-form" method="post">
                        <div class="row">
                            <div class="col-md-8 mb-4">
                                <div class="card h-100">
                                    <div class="card-header">
                                        <h2 class="h5"><?php echo lang('payment-method'); ?></h2>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md">
                                                <div class="row">
                                                    <div class="col-md mb-3">
                                                        <div class="radio">
                                                            <input type="radio" name="PaymentMethod" class="form-control" id="Paypal" checked>
                                                            <label for="Paypal"><?php echo lang('paypal'); ?></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md mb-3 text-right">
                                                        <img src="<?php echo base_url();?>assets/frontend/images/paypal-img.jpg" alt="" class="img-fluid">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-4">
								<?php
								 $default_lang = getDefaultLanguage();
                                 $item = getValue('packages_text',array('PackageID' => $rec->PackageID, 'SystemLanguageID' => $default_lang->SystemLanguageID));
								 $amount = $rec->Rate*$rec->Qty;
								 $total = $rec->Rate*$rec->Duration;
								 ?>
                                <div class="card">
                                    <div class="card-header">
                                        <h2 class="h5"><?php echo lang('order-summary'); ?></h2>
                                    </div>
                                    <div class="card-body">
                                        <h3 class="h5 mb-2"><?php echo $item->Title; ?> / <?php echo $rec->Qty; ?></h3>
                                        <div class="row">
                                            <div class="col-md col-sm-6 mb-3"><strong><?php echo lang('per_advertisement'); ?> / <?php echo lang('month'); ?></strong></div>
                                            <div class="col-md col-sm-6 mb-3 text-right"><?php echo currency($rec->Rate); ?></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md col-sm-6 mb-3"><?php echo lang('equals_to').' '.lang('per_month'); ?></div>
                                            <div class="col-md col-sm-6 mb-3 text-right"> <?php echo currency($rec->Rate); ?></div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md col-sm-6 mb-3"><strong><?php echo lang('contract_period'); ?> </strong></div>
                                            <div class="col-md col-sm-6 mb-3 text-right"><strong><?php 
											    $created_date = getValue('user_package_purchase',array('PurchaseID' => $rec->PurchaseID));
												echo dateformat($created_date->CreatedAt,'de'); ?> - <?php echo dateformat($rec->ExpiryDate,'de'); ?></strong></div>
                                        </div>
<!--
                                        <div class="row">
                                            <div class="col-md col-sm-6 mb-3">plus 0% VAT</div>
                                            <div class="col-md col-sm-6 mb-3 text-right">&euro; 0.00</div>
                                        </div>
-->
                                        <hr>
                                        <div class="row">
                                            <div class="col-md col-sm-6 mb-3"><strong><?php echo  lang('total'); ?></strong></div>
                                            <div class="col-md col-sm-6 mb-3 text-right"><strong><?php echo currency($total); ?></strong></div>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
									<div class="col-md text-right mt-3 mb-3">
										<input type="hidden" name="cmd" value="_xclick">
										<input type="hidden" name="business" value="<?php echo $site_setting->PaypalEmail; ?>">
										<input type="hidden" name="item_name" value="<?php echo $item->Title; ?>">
										<input type="hidden" name="item_number" value="<?php echo $rec->PurchaseID; ?>">
										<input type="hidden" name="currency_code" value="EUR">
										<input type="hidden" name="amount" value="<?php echo $total; ?>">
										<input name="return" type="hidden" value="<?php echo base_url('paypal-complete').'/'.$rec->PurchaseID; ?>">
										<input type="hidden" name="cancel_return"  value="<?php echo base_url('paypal-cancel').'/'.$rec->PurchaseID; ?>">
										<input type="hidden" name="notify_url"  value="<?php echo base_url('paypal-ipn').'/'.$rec->PurchaseID; ?>">
										<input type="submit" name="submit" class="btn btn-secondary text-uppercase w-25" value="<?php echo lang('submit'); ?>">
									</div>
								</div>
                            </div>
							
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>