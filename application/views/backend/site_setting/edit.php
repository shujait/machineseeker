<link href="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/css/intlTelInput.min.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput-jquery.min.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('EditSiteSettings'); ?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/site_setting/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate="">
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="SiteSettingID" value="<?php echo $SiteSettingID; ?>">


                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SiteName"><?php echo lang('SiteName'); ?>  : <span class="red">*</span></label>
                                        <input type="text" class="form-control" name="SiteName" id="SiteName"  value="<?php echo $result->SiteName; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating is-focused">
                                        <label class="control-label" for="PhoneNumber"><?php echo lang('ph'); ?> : <span class="red">*</span></label>
                                        <input type="text" class="form-control" name="PhoneNumber" id="PhoneNumber" value="<?php echo $result->PhoneNumber; ?>" >
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FromEmail"><?php echo lang('from').' '.lang('email'); ?> : <span class="red">*</span></label>
                                        <input type="email" class="form-control" name="FromEmail" id="FromEmail" value="<?php echo $result->FromEmail; ?>" >
                                    </div>
                                </div>
								<div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FromName"><?php echo lang('from').' '.lang('name'); ?> : <span class="red">*</span></label>
                                        <input type="text" class="form-control" name="FromName" id="FromName" value="<?php echo $result->FromName; ?>" >
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="OnwerName"><?php echo lang('onwer'); ?> : <span class="red">*</span></label>
                                        <input type="text" class="form-control" name="OnwerName" id="OnwerName" value="<?php echo $result->OnwerName; ?>" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating is-focused">
                                        <label class="control-label" for="Whatsapp"><?php echo lang('Whatsapp'); ?> :</label>
                                        <input type="text" class="form-control" name="Whatsapp" id="Whatsapp" value="<?php echo $result->Whatsapp; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Skype"><?php echo lang('Skype'); ?> :</label>
                                        <input type="text" class="form-control" name="Skype" id="Skype" value="<?php echo $result->Skype; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating is-focused">
                                        <label class="control-label" for="Fax"><?php echo lang('Fax'); ?> :</label>
                                        <input type="text" class="form-control" name="Fax" id="Fax" value="<?php echo $result->Fax; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Address"><?php echo lang('street_address'); ?> :</label>
                                        <input type="text" class="form-control" name="Address" id="Address" value="<?php echo $result->Address; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="PostalCode"><?php echo lang('postcode'); ?> :</label>
                                        <input type="text" class="form-control" name="PostalCode" id="PostalCode" value="<?php echo $result->PostalCode; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="City"><?php echo lang('city'); ?> :</label>
                                        <input type="text" class="form-control" name="City" id="City" value="<?php echo $result->City; ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
<!--
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Image"><?php //echo lang('SiteLogo'); ?> :</label>
										<br />
										 <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">
											 
                                    <?php if ($result->SiteImage != '') { ?>
                                        <img src="<?php echo base_url($result->SiteImage); ?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:200px;"/>
                                    <?php } ?>
											 </div>
                                            <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="Image[]" multiple="multiple"  accept="image/jpeg,image/png"/>
                                                    </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
-->
								<div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Image"><?php echo lang('google-map-embed'); ?> :</label>
										<br />
										<br />
										 <textarea name="GoogleMap" class="form-control TinymceEditor"><?php echo $result->GoogleMap; ?></textarea>
                                    </div>
                                </div>
                            </div>

                            <hr/>

                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="FacebookUrl"><?php echo lang('FacebookUrl'); ?> :</label>
                                        <input type="url" class="form-control" name="FacebookUrl" id="FacebookUrl" value="<?php echo $result->FacebookUrl; ?>">
                                    </div>
                                </div>
								 <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TwitterUrl"><?php echo lang('TwitterUrl'); ?> :</label>
                                        <input type="url" class="form-control" name="TwitterUrl" id="TwitterUrl" value="<?php echo $result->TwitterUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="InstagramUrl"><?php echo lang('InstagramUrl'); ?> :</label>
                                        <input type="url" class="form-control" name="InstagramUrl" id="InstagramUrl" value="<?php echo $result->InstagramUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="LinkedInUrl"><?php echo lang('LinkedInUrl'); ?> :</label>
                                        <input type="url" class="form-control" name="LinkedInUrl" id="LinkedInUrl" value="<?php echo $result->LinkedInUrl; ?>">
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BlogUrl"><?php echo lang('BlogUrl'); ?> :</label>
                                        <input type="url" class="form-control" name="BlogUrl" id="BlogUrl" value="<?php echo $result->BlogUrl; ?>">
                                    </div>
                                </div>
                            </div>
							<br />
							<div class="clearfix"></div>
                            <div class="row">
								 <div class="card-header card-header-icon" data-background-color="purple">
										<i class="material-icons">payment</i>
									</div>
                                <h4 class="card-title"><?php echo lang('paypal-settings'); ?></h4>
								<br />
							</div>
							<div class="row">
								 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="paypal-url"><?php echo lang('paypal-url'); ?> <span class="red">*</span></label>
									<input type="text" class="form-control" name="PaypalUrl" id="paypal-url"  value="<?php echo $result->PaypalUrl; ?>">

                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="paypal-email"><?php echo lang('paypal-email'); ?> <span class="red">*</span></label>
									<input type="text" class="form-control" name="PaypalEmail" id="paypal-email"  value="<?php echo $result->PaypalEmail; ?>">

                                    </div>
                                </div>
								 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="paypal-fee"><?php echo lang('paypal-fee'); ?><span class="red">*</span></label>
										 <input type="text" class="form-control" name="PaypalFee" id="paypal-fee"  value="<?php echo $result->PaypalFee; ?>">

                                    </div>
                                </div>
								<div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="paypal-active"><?php echo lang('paypal-active'); ?><span class="red">*</span></label>
										 <div class="radio">
                                                    <label for="paypal-on">
                                                       <input type="radio" name="PaypalActive" id="paypal-on" value="1" <?php echo getChecked(1,$result->PaypalActive);  ?>> <?php echo lang('on'); ?>
                                                    </label>
											          <label for="paypal-off">
                                                      <input type="radio" name="PaypalActive" id="paypal-off" value="0" <?php echo getChecked(0,$result->PaypalActive); ?> > <?php echo lang('off'); ?>
                                                    </label>
										</div>
                                    </div>
                                </div>
                            </div>
							<br />
                            <div class="clearfix"></div>
                            <div class="row">
                                 <div class="card-header card-header-icon" data-background-color="purple">
                                        <i class="material-icons">payment</i>
                                    </div>
                                <h4 class="card-title"><?php echo lang('bank-settings'); ?></h4>
                                <br />
                            </div>
                            <div class="row">
                                 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BankName"><?php echo lang('bank-name'); ?> <span class="red">*</span></label>
                                    <input type="text" class="form-control" name="BankName" id="BankName"  value="<?php echo $result->BankName; ?>">

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="IBAN"><?php echo lang('iban'); ?> <span class="red">*</span></label>
                                    <input type="text" class="form-control" name="IBAN" id="IBAN"  value="<?php echo $result->IBAN; ?>">

                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="BIC"><?php echo lang('bic'); ?><span class="red">*</span></label>
                                         <input type="text" class="form-control" name="BIC" id="BIC"  value="<?php echo $result->BIC; ?>">

                                    </div>
                                </div>
                               <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="USTID"><?php echo lang('ust'); ?><span class="red"></span></label>
                                         <input type="text" class="form-control" name="USTID" id="USTID"  value="<?php echo $result->USTID; ?>">

                                    </div>
                                </div>
                            </div>
                            <br />

							<div class="clearfix"></div>
                            <div class="row">
								 <div class="card-header card-header-icon" data-background-color="purple">
										<i class="material-icons">euro_symbol</i>
									</div>
                                <h4 class="card-title"><?php echo lang('currency-options'); ?></h4>
								<br/>
							</div>
							<div class="row">
								 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Currency"><?php echo lang('currency'); ?> <span class="red">*</span></label>
									<input type="text" class="form-control" name="Currency" id="Currency"  value="<?php echo $result->Currency; ?>">

                                    </div>
                                </div>
								 <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="CurrencyPosition"><?php echo lang('currency-position'); ?><span class="red">*</span></label>
								<select id="CurrencyPosition" class="selectpicker" data-style="select-with-transition"  name="CurrencyPosition">
									<?php 
										$arr = CurrencyPosition();
										if(!empty($arr)){ 
											foreach($arr as $k => $val){ ?>
												<option value="<?php echo $k; ?>" <?php echo  getSelected($k,$result->CurrencyPosition); ?>><?php echo $val; ?> </option>
									<?php } } ?>
								   </select> 
                                    </div>
                                </div>
								 <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ThousandSeparator"><?php echo lang('thousand-separator'); ?><span class="red">*</span></label>
									  <input type="text" class="form-control" name="ThousandSeparator" id="ThousandSeparator"  value="<?php echo $result->ThousandSeparator; ?>">
                                    </div>
                                </div>
								 <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DecimalSeparator"><?php echo lang('decimal-separator'); ?><span class="red">*</span></label>
									<input type="text" class="form-control" name="DecimalSeparator" id="DecimalSeparator" value="<?php echo $result->DecimalSeparator; ?>">

                                    </div>
                                </div>
								 <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="NumberDecimals"><?php echo lang('number-decimals'); ?><span class="red">*</span></label>
									  <input type="number" class="form-control" name="NumberDecimals" id="NumberDecimals"  value="<?php echo $result->NumberDecimals; ?>">
                                    </div>
                                </div>
                            </div>
							
							<br />
							<div class="clearfix"></div>
                            <div class="row">
								 <div class="card-header card-header-icon" data-background-color="purple">
										<i class="material-icons">build</i>
									</div>
                                <h4 class="card-title"><?php echo lang('frontend-settings'); ?></h4>
								<br />
							</div>
							<div class="row">
								 <div class="col-md-3">
                                    <div class="form-group ">
                                        <label class="form-group is-empty"><?php echo lang('advertise-settings'); ?></label>
										<br />
										<div class="radio">
                                            <label for="Advertise">
                                                <input type="radio" name="Advertise" id="Advertise" value="1"  <?php echo ($result->Advertise == 1)?getChecked(1,$result->Advertise):'checked'; ?>> <?php echo lang('on'); ?>
                                            </label>

                                            <label for="Advertise1">
                                                <input type="radio" name="Advertise" id="Advertise1" value="0" <?php echo getChecked(0,$result->Advertise); ?>> <?php echo lang('off'); ?>
                                            </label>
										</div>
                                    </div>
                                </div>
								 <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="form-group is-empty"><?php echo lang('category-count'); ?></label>
											<br />
										<div class="radio">
                                            <label for="CategoryCount">
                                                <input type="radio" name="CategoryCount" id="CategoryCount" value="1" <?php echo ($result->CategoryCount == 1)?getChecked(1,$result->CategoryCount):'checked';  ?> checked> <?php echo lang('on'); ?>
                                            </label>
                                              <label for="CategoryCount1">
                                                <input type="radio" name="CategoryCount" id="CategoryCount1" value="0" <?php echo getChecked(0,$result->CategoryCount); ?>> <?php echo lang('off'); ?>
                                            </label>
										</div>
                                    </div>
                                </div>
                                 <div class="col-md-3">
                                    <div class="form-group">
                                        <label  for="FreePackageRelaxation"><?php echo lang('free-package-relax'); ?><span class="red">*</span></label>
                                      <input type="number" class="form-control" name="FreePackageRelaxation" id="FreePackageRelaxation"  value="<?php echo $result->FreePackageRelaxation; ?>">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label  for="FreePackageRelaxation"><?php echo lang('home_page_category'); ?><span class="red">*</span></label>
                                        <select id="display_categories" minlength="8" name="display_categories[]" class="select2 form-control select2-container" required multiple data-style="select-with-transition">
                                            <?php
                                                $CategoryID_array = array_column($home_categories,'CategoryID');
                                                foreach($categories as $category){
                                                    if(in_array($category['CategoryID'], $CategoryID_array)){
                                                        $selected = "selected";
                                                    }else{
                                                        $selected = "";
                                                    }
                                            ?>
                                                <option value="<?= $category['CategoryID'] ?>" <?= $selected ?>><?= $category['Title'] ?></option>
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <?php /*?><div class="clearfix"></div>
                            <div class="row top-space">
                                 <div class="card-header card-header-icon" data-background-color="purple">
                                        <i class="material-icons">build</i>
                                    </div>
                                <h4 class="card-title">Header Ads <?php echo lang('banner-setting'); ?></h4>
                                <br/>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="GoogleAdCode">Google Ad :</label>
                                        <textarea class="form-control" name="GoogleAdCode" id="GoogleAdCode" rows="5"><?= $result->GoogleAdCode; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12">
                                    <div class="form-group label-floating">
                                         <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">
                                                <img src="<?php echo base_url($result->SingleAdImage) ?>" alt="image" class="img-responsive img-thumbnail" style="width:300px;"/>
                                             </div>
                                            <div>
                                                <span class="btn btn-rose btn-round btn-file">
                                                    <span class="fileinput-new">Select image</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="SingleAdImage[]"/>
                                                </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SingleAdImageURL">URL :</label>
                                        <input type="text" class="form-control" name="SingleAdImageURL" id="SingleAdImageURL"  value="<?php echo $result->SingleAdImageURL; ?>">
                                    </div>
                                </div>
                            </div><?php */?>
                            <!-- <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group ">
                                        <label class="form-group is-empty">Header Ad <?php //echo lang('Type'); ?></label>
										<br />
										<div class="radio">
                                            <label for="HeaderAdType">
                                                <input type="radio" name="HeaderAdType" id="HeaderAdType" value="1"><?= lang('MultipleImages'); ?>
                                            </label>
                                            <label for="HeaderAdType1">
                                                <input type="radio" name="HeaderAdType" id="HeaderAdType1" value="2"><?= lang('SingleImage'); ?> Ad 
                                            </label>
                                            <label for="HeaderAdType2">
                                                <input type="radio" name="HeaderAdType" id="HeaderAdType2" value="3"><?= lang('MultipleImages'); ?> Ad
                                            </label>
										</div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-12" id="HeaderAdTypeHTML">
                                </div>
                            </div> -->
                            
                            <div class="clearfix"></div>
                            <div class="row top-space">
                                 <div class="card-header card-header-icon" data-background-color="purple">
                                        <i class="material-icons">build</i>
                                    </div>
                                <h4 class="card-title"><?php echo lang('banner-setting'); ?></h4>
                                <br/>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
<!--                                        <label class="control-label" for="Image"><?php echo lang('Image'); ?> :</label>-->
                                         <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">
                                                <img src="<?php echo base_url($result->BannerImage) ?>" alt="image" class="img-responsive img-thumbnail" width="200" style="width:200px;"/>
                                             </div>
                                            <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="BannerImage[]"/>
                                                    </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group label-floating">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                        <?php echo lang('submit'); ?>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>


<script>
$(document).ready(function(){
    //$(".MobileNumberMask").inputmask({"mask": "+999999999999"}); //specifying options
    var input2 = document.querySelector("#PhoneNumber");
    //var countryData = 'de';
    var iti2 = intlTelInput(input2, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "on",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "de",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'de', 'en'],
      // placeholderNumberType: "MOBILE",
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "<?php echo base_url();?>assets/backend/plugins/Intl-tel/js/utils.js",
    });

    //var iti2 = intlTelInput(input2);
    iti2.setNumber('<?php echo ($result->CountryCode != '' ? $result->CountryCode : '+49'); ?> <?php echo $result->PhoneNumber; ?>')
    //$("#CountryCode").val('<?php //echo ($result[0]->CountryCode != '' ? $result[0]->CountryCode : '+49');?>');
    var countryData = iti2.getSelectedCountryData();
    //console.log(countryData.iso2);
    input2.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti2.getSelectedCountryData();
      $("#CountryCode").val("+"+changeCountryCode.dialCode);
      //console.log(changeCountryCode.iso2);
    });


    var input3 = document.querySelector("#Whatsapp");
    //var countryData = 'de';
    var iti3 = intlTelInput(input3, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "on",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "de",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'de', 'en'],
      // placeholderNumberType: "MOBILE",
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "<?php echo base_url();?>assets/backend/plugins/Intl-tel/js/utils.js",
    });

    //var iti2 = intlTelInput(input2);
    iti3.setNumber('<?php echo ($result->WhatsAppCountryCode != '' ? $result->WhatsAppCountryCode : '+49'); ?> <?php echo $result->Whatsapp; ?>')
    //$("#MobCountryCode").val('<?php //echo ($result[0]->MobCountryCode != '' ? $result[0]->MobCountryCode : '+49');?>');
    var countryData = iti3.getSelectedCountryData();
    //console.log(countryData.iso2);
    input3.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti3.getSelectedCountryData();
      $("#WhatsAppCountryCode").val("+"+changeCountryCode.dialCode);
      //console.log(changeCountryCode.iso2);
    });

    var input4 = document.querySelector("#Fax");
    //var countryData = 'de';
    var iti4 = intlTelInput(input4, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "on",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "de",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'de', 'en'],
      // placeholderNumberType: "MOBILE",
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "<?php echo base_url();?>assets/backend/plugins/Intl-tel/js/utils.js",
    });

    //var iti2 = intlTelInput(input2);
    iti4.setNumber('<?php echo ($result->FaxCountryCode != '' ? $result->FaxCountryCode : '+49'); ?> <?php echo $result->Fax; ?>')
    //$("#MobCountryCode").val('<?php //echo ($result[0]->MobCountryCode != '' ? $result[0]->MobCountryCode : '+49');?>');
    var countryData = iti4.getSelectedCountryData();
    //console.log(countryData.iso2);
    input4.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti4.getSelectedCountryData();
      $("#FaxCountryCode").val("+"+changeCountryCode.dialCode);
      //console.log(changeCountryCode.iso2);
    });

    

}); 
</script>

<script>
    $("#HeaderAdType").on("click",function(){
        $("#HeaderAdTypeHTML").empty(); 
        var html = `<div class="form-group">
                        <label class="form-group is-empty">Google Ad Url</label><br />
                        <textarea class="form-control" name="GoogleAdUrl" id="GoogleAdUrl"  value="<?= $result->GoogleAdUrl; ?>"></textarea>
                    </div>`;

        $("#HeaderAdTypeHTML").append(html);       
    });
    $("#HeaderAdType1").on("click",function(){
        $("#HeaderAdTypeHTML").empty(); 
        var html = `<div class="form-group">
                        <label class="form-group is-empty"><?= lang('SingleImage'); ?> Ad </label><br />
                        <div class="fileinput-preview thumbnail">
                            <img src="<?= base_url($result->SingleImage); ?>" alt="image" class="img-responsive img-thumbnail" width="200" style="width:200px;"/>
                        </div>
                        <div>
                            <span class="btn btn-rose btn-round btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="SingleImage[]"/>
                            </span>
                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                        </div>
                    </div>`;

        $("#HeaderAdTypeHTML").append(html);       
    });
    $("#HeaderAdType2").on("click",function(){
        $("#HeaderAdTypeHTML").empty(); 
        var html = `<div class="form-group">
                        <label class="form-group is-empty"><?= lang('MultipleImages'); ?> Ad</label><br />
                        <div class="fileinput-preview thumbnail">
                            <img src="<?= base_url($result->MultipleAdImages); ?>" alt="image" class="img-responsive img-thumbnail" width="200" style="width:200px;"/>
                        </div>
                        <div>
                            <span class="btn btn-rose btn-round btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="MultipleAdImages[]" multiple>
                            </span>
                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                        </div>
                    </div>`;

        $("#HeaderAdTypeHTML").append(html);       
    });
</script>