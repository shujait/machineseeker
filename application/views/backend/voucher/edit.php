<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('edit').' '.singular(lang($ControllerName));?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="update">
                            <input type="hidden" name="VouchersID" value="<?= $result['VouchersID'] ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('name'); ?></label>
                                        <input type="text" name="Title" parsley-trigger="change"  class="form-control" id="Title" value="<?= $result['Title'] ?>" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="VoucherCode"><?php echo lang('voucher_code'); ?></label>
                                        <input type="text" name="VoucherCode" parsley-trigger="change" required disabled  class="form-control" id="VoucherCode" value="<?= $result['VoucherCode'] ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="DiscountType"><?php echo lang('type'); ?> *</label>
                                        <select id="DiscountType" class="selectpicker" data-style="select-with-transition" name="DiscountType" required>
                                            <option value=""><?php echo lang('please-select') ?></option>
                                            <option value="percentage" <?= ($result['DiscountType'] == 'percentage')? 'selected':'' ?> ><?php echo lang('percentage') ?></option>
                                            <option value="flat" <?= ($result['DiscountType'] == 'flat')? 'selected':'' ?>><?php echo lang('flat') ?> </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="VoucherValue"><?php echo lang('voucher_value'); ?></label>
                                        <input type="number" name="VoucherValue" parsley-trigger="change" required  class="form-control" id="VoucherValue" value="<?= $result['VoucherValue'] ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ExpiryDate"><?php echo lang('expiry_date'); ?></label>
                                        <input type="text" name="ExpiryDate" required  class="form-control" id="ExpiryDate" value="<?= date('d.m.Y', strtotime($result['ExpiryDate'])) ?>">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" <?= ($result['IsActive'] == 1)?'checked':'' ?>/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function (){
        $( "#ExpiryDate" ).datepicker({
            dateFormat: "dd.mm.yy"
        });
    });
    $("#ExpiryDate").on('change', function (){
        var expireDate = $('#ExpiryDate').val();
        var dmy = expireDate.split(".");
        expireDate = new Date(dmy[2], dmy[1]-1, parseInt(dmy[0])+1).toJSON().slice(0,10).replace(/-/g,'/');
        var todayDate = new Date().toJSON().slice(0,10).replace(/-/g,'/');

        if (Date.parse(expireDate) < Date.parse(todayDate)) {
            showError('Expire Date should be greater than toady');
            $(this).val('');
            return false;
        };
    });
</script>