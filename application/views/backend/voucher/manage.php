
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('voucher_code');?></th>
                                    <th><?php echo lang('type');?></th>
                                    <th><?php echo lang('voucher_value');?></th>
                                    <th><?php echo lang('expiry_date');?></th>
                                    <th><?php echo lang('is_active');?></th>
                                    <?php
                                        if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') ||
                                            checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') ||
                                            checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')) { ?>
                                        <th><?php echo lang('actions');?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if(isset($result) && count($result) > 0){
                                    foreach ($result as $voucher){
                                ?>
                                    <tr>
                                        <td><?= $voucher->Title?></td>
                                        <td><?= $voucher->VoucherCode?></td>
                                        <td><?= $voucher->DiscountType?></td>
                                        <td><?= $voucher->VoucherValue?></td>
                                        <td><?= dateformat($voucher->ExpiryDate, "de")?></td>
                                        <td><?= $voucher->IsActive?></td>
                                        <td>
                                            <?php
                                            if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') ||
                                            checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')) { ?>
                                            <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$voucher->VouchersID);?>" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('edit') .' '.lang($ControllerName); ?>"><i class="material-icons">edit</i><div class="ripple-container"></div></a>
                                            <?php } ?>
                                            <?php
                                            if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete')) { ?>
                                                <a href="#" onclick="deleteRecord('<?php echo $voucher->VouchersID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove" title="<?php echo lang('delete') .' '.lang($ControllerName); ?>"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php
                                    }
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script type="text/javascript">

</script>