<?php $default_lang = getDefaultLanguage(); ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add_machine_ads_entry');?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
							 <input type="hidden" name="SystemLanguageID" value="<?php echo base64_encode($default_lang->SystemLanguageID); ?>">
							 <input type="hidden" name="IsDefault" value="1">
							 <input type="hidden" name="CountryShort" id="country_short">
						     <input type="hidden" name="Lat" id="lat">
                             <input type="hidden" name="Lng" id="lng">
							 <input type="hidden" name="machine_ads_type" id="machine_ads_type" value="1">
							<br />
                            
                            <h4 class="card-title"><?php echo lang('data');?></h4>
                            <div class="row">
                                <div class="col-md-12">                                    
                                      <div class="col-sm-2">
                                        <div class="form-group upflow-02" style="margin: 21px 0 0 0;">
                                        <label><?php echo lang('machine_add_type'); ?></label>
                                    </div>
                                    </div>
                                    <div class="col-sm-10">
                                       <div class="col-sm-4 checkbox-radios">
                                        <div class="form-group label-floating">
                                            <label class="radio01">
                                                <input type="radio" name="ads_type" value="1" checked="true"> <?php echo lang('ads_visiter'); ?>                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="radio01">  
                                                 <input type="radio" name="ads_type" value="2"> <?php echo lang('ads_offering'); ?>                                              <span class="checkmark"></span>
                                            </label>

                                        </div>
                                    </div>                                       
                                   </div>
                                </div>
                            </div>
                            <?php if(!empty($dealers)){?>
                                <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="UserID"><?php echo lang('assignee'); ?> <span class="red"></span></label>
                                        <select class="selectpicker"  data-style="select-with-transition" name="UserID" id="UserID" required>
                                            <option value=""  ><?php echo lang('self'); ?>  </option>
    <?php
                                            
                                            foreach ($dealers as $k => $val) {
                                                echo '<option value="' . $val['UserID']. '">' . $val['UName'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <?php } ?>
                           
                            <div class="row">
								
								 <!-- <div class="col-md-6">
                                    <div class="form-group label-floating">
										<button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"  data-toggle="modal" data-target="#categoryModal" >Select Category</button>
										<input type="hidden" value="" required name="category">
	                                    <span class="cat-title"></span>    
									                                    
									 </div>
                                </div> -->
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                         <select class="select2 form-control getSubCategory genTitleOther" data-type="category"  data-style="select-with-transition" name="category" id="category" required>
                                                <option value=""><?php echo lang('select_main_category'); ?></option>
                                             <?php
                                                $category = array();
                                                $arr = getCategories($language,'categories.ParentID = 0',$limit = false,$start = 0,$categorytype=1);
                                                if($arr){
                                                foreach($arr as $value){
                                                 $arr_child = getCategories($language,'categories.ParentID = '.$value->CategoryID,$limit = false,$start = 0,$categorytype=1);
                                                ?>
                                                        <option value="<?php echo $value->CategoryID; ?>"><?php echo $value->Title; ?></option>
                                                        <?php //echo  MachineCategoryOptionTree($arr_child, $language); ?>
                                                <?php                   
                                                   }    
                                                }
                                                ?>
                                    </select>
                                    </div>
                                </div>
                                <div id="subCategoryDiv">
                                </div>
                                <div id="subSubCategoryDiv">
                                </div>
								 <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="title"><?php echo lang('title').lang('does_not_have_to_be_filled_in'); ?> <span class="red">*</span></label>
									    <input type="text" name="title" required  class="form-control" id="title">
                              
									 </div>
                                     <span class="red"><small><?php echo lang('machine-title-placeholder');?></small></span>
                                </div>
                            </div>
							<!-- <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating non-services">
                                        <label class="control-label " for="manufacturer" ><?php //echo lang('manufacturer'); ?> <span class="red">*</span></label>
                                        <input type="text" name="manufacturer" required  class="form-control genTitle" id="manufacturer">
                                    </div>
                                 <div class="form-group label-floating services" style="display: none;">
                                        <label class="control-label " for="Services"  ><?php //echo lang('Article/Services'); ?> <span class="red">*</span></label>
                                       <input type="text" name="service" required  class="form-control genTitle" id="service">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="machine_type"><?php //echo lang('machine_type'); ?> <span class="red">*</span></label>
                                        <input type="text" name="machine_type" required  class="form-control genTitle" id="machine_type">
                                    </div>
                                </div>
								 
                            </div> -->
							<div class="row">
								
								 <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="codition"><?php echo lang('codition'); ?> <span class="red">*</span></label>
										 <select class="selectpicker"  data-style="select-with-transition" name="codition" id="codition" required>
											 <?php 
											 $condition_list = condition_list(true);
											 foreach($condition_list as $k => $val){
	  													echo '<option value="'.$k.'">'.$val.'</option>';
												} ?>
                                    </select>
                                    </div>
                                </div>
								
                                <!-- <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="model"><?php //echo lang('model'); ?> <span class="red">*</span></label>
                                        <input type="text" name="model" required  class="form-control genTitle" id="model">
                                    </div>
                                </div> -->
                            </div>
							
							
							<br />
							  <h4 class="card-title"><?php echo lang('price_and_location');?></h4>

							<div class="row">
								
								 <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="price"><?php echo lang('price'); ?></label>
                                        <input type="text" name="price" required  class="form-control" id="price">
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="vat"><?php //echo lang('vat'); ?></label>
                                        <input type="text" name="vat" required  class="form-control" id="vat">
                                    </div>
                                </div> -->
							</div>
							<div class="row">
								 <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="location"><?php echo lang('location'); ?></label>
                                        <input type="text" name="location"  class="form-control" id="location" onKeyPress="initMap();" autocomplete="off">
                                    </div>
                                </div>
                            </div>
							<br />
							<h4 class="card-title"><?php echo lang('offer_detail');?></h4>

							<div class="row">

                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="reference_number"><?php echo lang('reference_number'); ?></label>
                                        <input type="text" name="reference_number" required  class="form-control" id="reference_number">
                                    </div>
                                </div>
								 <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="serial_number"><?php echo lang('serial_number'); ?></label>
                                        <input type="text" name="serial_number" required  class="form-control" id="serial_number">
                                    </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
										     <label><?php echo lang('file'); ?></label> <span class="red">*</span><small style="margin-left: 120px; font-weight: bold;"><?php echo lang('upload_image_or_pdf');?></small><br>
                                        <input type="file" name="images"  id="filer_input1" required>
                                    </div>
                                </div>
                            </div>
							
                           <!--  <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
                                             <label><?php echo lang('set_featured_img'); ?></label> <span class="red">*</span><small style="margin-left: 60px; font-weight: bold;"><?php echo lang('feater_image_text');?></small><br>
                                        <input type="file" name="feature_image"  id="filer_input2" required>
                                    </div>
                                </div>
                            </div> -->
							<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating" id="add_url_Video">
										     <label class="control-label" for="video"><?php echo lang('video'); ?></label>
                                        <input type="url" name="video" class="form-control youtube_link" id="video">
										<br />
										<iframe style="display:none;" width="420" height="315" src="" frameborder="0" allowfullscreen></iframe>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating" id="keywords">
                                        <label class="control-label" for="keywords"><?php echo lang('seo_keywords'); ?></label>
                                        <input type="text" name="keywords" class="form-control " id="keywordstxt">

                                    </div>
                                </div>
                            </div>			<br />
						<h4 class="card-title"><?php echo lang('Description');?>                                                  
                                                <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" 
                                                   title="<?php echo lang('you_need_help_filling_out_the_add'); ?>" class="tooltip001">?</a>
</h4>
                                               
							<div class="row">

                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="description"><?php echo lang('Description'); ?> <span class="red">*</span></label>
                                        <textarea name="description" rows="20" required  class="form-control" id="descriptionText"></textarea>
                                    </div>
                                </div>
								
                            </div>
                            <div class="row">
                                <div class="col-sm-12 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="TermsAndCondition">
                                                <input name="TermsAndCondition" value="1" type="checkbox" id="TermsAndCondition"/> <?php echo str_replace('[LINK]', '<a href="'.base_url('allgemeine-geschaeftsbedingungen').'" target="_blank">'.lang('terms_condition_text').'</a>', lang('terms_condition_machine')) ; ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php  if($this->session->userdata['admin']['RoleID']!=4){;?>
                            <!-- <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive"/> <?php echo lang('publish_entry'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- end -->
                             <div class="row">
                                    <div class="col-sm-4 checkbox-radios">
                                        <div class="form-group label-floating">
                                            <label class="radio01">
                                                <input name="IsActive" value="1" type="radio" id="IsActive"/> <?php echo lang('publish_entry'); ?>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="radio01">  
                                                <input checked name="IsActive" value="0" type="radio" /> <?php echo lang('draft_entry'); ?>
                                                <span class="checkmark"></span>
                                            </label>

                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');
                                   if ($this->session->userdata['admin']['RoleID']==4)
                                       echo ' & '.lang('preview');
                                    
                                    ?> 
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'descriptionText' );
</script>