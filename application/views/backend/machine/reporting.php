<?php $default_lang = getDefaultLanguage(); 
$title =  getValue('machines_text',array('MachineID' => $this->uri->segment(4),'SystemLanguageID' => $default_lang->SystemLanguageID))->Title;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo $title.' '.lang($ControllerName); ?></h4>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('machine');?></th>

                                    <th><?php echo lang($type);?></th>

                                     <th><?php echo lang('actions');?></th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){
									$json = json_encode(json_decode($value->InquiryDetail), JSON_PRETTY_PRINT);
									?>
                                        <tr id="<?php echo $value->InquiryID;?>">

                                            <td><?php echo $title; ?></td>

                                            <td>
												<?php echo $json; ?>
											</td>
                                           
                                            <td>
                                                    <a href="#" onclick="deleteInquiry('<?php echo $value->InquiryID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove" title="<?php echo lang('delete') .' '.lang($ControllerName); ?>"><i class="material-icons">close</i><div class="ripple-container"></div>
												</a>
											
                                            </td>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>