<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('dealer_inquiries'); ?></h4>
                        <div class="clearfix"></div>
                        <div class="material-datatables">
                            <table id="datatables-1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?></th>

                                    <th><?php echo lang('name');?></th>

                                    <th><?php echo lang('company_name');?></th>

                                    <th><?php echo lang('PhoneNumber');?></th>

                                    <th><?php echo lang('email');?></th>

                                    <?php

                                    if($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($InquiriesOfMachine){
                                    foreach($InquiriesOfMachine as $value){
                                        $InquiryDetail = json_decode($value->InquiryDetail);
                                        /*echo '<pre>';
                                        print_r($InquiryDetail);*/
                                    $user_info =  get_user_info(array('a.UserID' => $value->UserID));
                                    ?>
                                        <tr id="<?php echo $value->MachineID;?>">

                                            <td><?php echo $value->Title; ?></td>

                                            <td><?php echo $InquiryDetail->name; ?></td>

                                            <td><?php echo $InquiryDetail->company_name; ?></td>

                                            <td><?php echo $InquiryDetail->phone; ?></td>

                                            <td><?php echo $InquiryDetail->email; ?></td>

                                             <?php if($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                            <td>
                                                     <a href="javascript:void(0);" data-toggle="modal" data-target="#OpenInquiryDetail" data-title="<?php echo $value->Title; ?>" data-json-object='<?php echo implode('||', (array)$InquiryDetail);?>' class="btn btn-simple btn-success btn-icon inquiryModal" title="<?php echo lang($ControllerName).' '.lang('sale_inquiry'); ?>"><i class="material-icons">speaker_notes</i><div class="ripple-container"></div></a>


                                                <?php if(checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="#" onclick="deleteRecord('<?php echo $value->MachineID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove" title="<?php echo lang('delete') .' '.lang($ControllerName); ?>"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>

                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                //exit();
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <?php
            $checkUser = ($this->session->userdata['admin']['UserID'] == $value->UserID) ? true : false;
            $can_edit = checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit');
            $can_delete = checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete');
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
						<?php if($check_package_active){
						?>
                        <div class="toolbar col-md-7">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a><a href="<?php echo base_url('cms/'.$ControllerName.'/import');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('import_from_csv'); ?></button>
                            </a>
                        </div>


						<?php

						if(in_array($this->session->userdata['admin']['RoleID'],array(3,4))){
						if($purchase){
						$total_machine =  $purchase->Qty;
						?>
						<div class="col-md-5 purchase_package">

                        <table width="100%">
                        <tr>
                        <td><b><?php echo lang('purchase-package'); ?>:</b></td>
                            <td><?php
                             if($package){
                             echo $package->Title;
                                }
                              ?>

                              </td>
                              </tr>
                              <tr>

                        <td><b><?php echo lang('expiry_date'); ?>:</b></td>
                        <td><?php echo dateformat($purchase->ExpiryDate,'de'); ?></td>

                        </tr>

                        <tr>
                        <td><b><?php echo ucwords(lang('per_month').' '.lang('machines')); ?>:</b></td>
                        <td><?php echo $purchase->Qty; ?></td>
                        </tr>
                        <tr>
                        <td><b><?php echo lang('remaining').' '.lang('machines'); ?>:</b></td>
                        <td><?php echo ($total_machine)-count($results); ?></td>

                        </tr>

                        </table>


						</div>
						<?php } } } ?>
						<div class="clearfix"></div>
                        <div class="material-datatables">
                            <table id="datatables-machine" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>
                                    <th><?php echo lang('featured_img');?></th>
                                    <th><?php echo lang('title');?></th>
                                    <th><?php echo lang('user');?></th>
                                    <th><?php echo 'ID';?></th>
                                    <th><?php echo lang('last_published');?></th>
                                    <th><?php echo lang('created_on');?></th>
                                    <th><?php echo lang('is_active');?></th>
                                    <?php
                                        if(($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')) || (isset($results[0]->UserID) && $this->session->userdata['admin']['UserID'] == $results[0]->UserID)){
                                    ?>
                                    <th><?php echo lang('actions');?></th>
                                    <?php } ?>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<div class="modal fade product-modal" id="OpenInquiryDetail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('dealer_inquiries'); ?></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                  <table class="table table table-striped table-hover m-0">
                      <tbody>
                          <tr><th><?php echo lang('title');?></th><td id="inquiryTitle"></td></tr>

                          <tr><th><?php echo lang('name');?></th><td id="inquiryName"></td></tr>

                          <tr><th><?php echo lang('company_name');?></th><td id="inquiryCompany"></td></tr>

                          <tr><th><?php echo lang('PhoneNumber');?></th><td id="inquiryPhone"></td></tr>

                          <tr><th><?php echo lang('street_address');?></th><td id="inquiryStreet"></td></tr>

                          <tr><th><?php echo lang('email');?></th><td id="inquiryEmail"></td></tr>

                          <tr><th><?php echo lang('postcode');?></th><td id="inquiryPostal"></td></tr>

                          <tr><th><?php echo lang('message');?></th><td id="inquiryMessage"></td></tr>
                      </tbody>
                  </table>

                </div> <!-- table-responsive -->
            </div>
        </div>
    </div>
    <?php
       $sessionUserId = $this->session->userdata['admin']['UserID'];
       $can_edit = checkUserRightAccess(56,$sessionUserId,'CanEdit');
       $can_delete = checkUserRightAccess(56,$sessionUserId,'CanDelete');

    ?>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>

<script type="text/javascript">
    $(".inquiryModal").click(function (){

        var MachineTitle = $(this).attr('data-title');
        var InquiryDetail = $(this).attr('data-json-object');

        var inquiryarr = InquiryDetail.split('||');

        $("#inquiryTitle").html(MachineTitle);
        $("#inquiryName").html(inquiryarr[0]);
        $("#inquiryCompany").html(inquiryarr[1]);
        $("#inquiryPhone").html(inquiryarr[2]);
        $("#inquiryStreet").html(inquiryarr[3]);
        $("#inquiryEmail").html(inquiryarr[4]);
        $("#inquiryPostal").html(inquiryarr[5]);
        $("#inquiryMessage").html(inquiryarr[6]);
    });
    var machinesDataTable;
    $(function() {
        var UserId = "<?php echo $sessionUserId; ?>";
        var IsVisible = false;
        if(UserId === '1')
        {
            IsVisible = true;
        }
        console.log("user id : " +IsVisible);
        machinesDataTable = $('#datatables-machine').DataTable({
            /* Processing indicator */
            "processing": true,
            /* DataTables server-side processing mode */
            "serverSide": true,
            /* Initial no order. */
            "order": [],
            "language": {
                "url": DataTable_Language,
                "decimal": ',',
                "thousands": '.',
            },
            /* Load data from an Ajax source */
            "ajax": {
                "beforeSend": function (){
                    $.blockUI({
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                },
                "url": "<?php echo base_url('cms/machine/getLists/'); ?>",
                "type": "POST",
                complete: function () {
                    $.unblockUI();
                }
            },
            /* Set column definition initialisation properties */
            "columns": [
                {
                    orderable: false,
                    searchable: false,
                    render:function (data, type, full, meta) {
                        return '<img data-src="'+data+'" class="lazyload" draggable="false" style="width: 80px;">';
                    }
                },
                {
                    orderable: true,
                    searchable: true,
                    render:function (data, type, full, meta) {
                        return data;
                    }
                },
                {
                    orderable: true,
                    searchable: true,
                    visible: IsVisible,
                    render:function (data, type, full, meta) {
                        return data;
                    }
                },
                {
                    orderable: true,
                    searchable: true,
                    render:function (data, type, full, meta) {
                        return data;
                    }
                },
                {
                    orderable: true,
                    searchable: true,
                    render:function (data, type, full, meta) {
                        return data;
                    }
                },
                {
                    orderable: true,
                    searchable: true,
                    render:function (data, type, full, meta) {
                        return data;
                    }
                },
                {
                    orderable: true,
                    searchable: true,
                    render:function (data, type, full, meta) {
                        let $msg = 'No';
                        if(data)
                        {
                            $msg = 'Yes';
                        }
                        return $msg;
                    }
                },
                {
                    orderable: false,
                    searchable: false,
                    render:function (data, type, full, meta) {

                        var check_package_active = "<?php echo $check_package_active; ?>";
                        var can_edit = "<?php echo $can_edit; ?>";
                        var can_delete = "<?php echo $can_delete; ?>";
                        var use_id = "<?php echo $sessionUserId ?>";
                        var $html = '';
                        if(check_package_active && can_edit || can_delete || (use_id == full[9])){
                                $html +='<td><a href="'+full[10]+'" class="btn btn-simple   btn-icon" title="<?php echo lang($ControllerName).' '.lang('preview'); ?>"><i class="material-icons">visibility</i><div class="ripple-container"></div></a>';
                            if((check_package_active && can_edit || can_delete)){
                                $html +='<a href="<?php echo base_url('cms/'.$ControllerName.'/inquiry/');?>'+full[7]+'" class="btn btn-simple btn-success btn-icon" title="<?php echo lang($ControllerName).' '.lang('sale_inquiry'); ?>"><i class="material-icons">speaker_notes</i><div class="ripple-container"></div></a>'+
                            '<a href="<?php echo base_url('cms/'.$ControllerName.'/share/');?>'+full[7]+'" class="btn btn-simple btn-rose btn-icon" title="<?php echo lang($ControllerName).' '. lang('share'); ?>"><i class="material-icons">share</i><div class="ripple-container"></div></a>'+
                            '<a href="<?php echo base_url('cms/'.$ControllerName.'/report/');?>'+full[7]+'" class="btn btn-simple btn-twitter btn-icon" title="<?php echo lang($ControllerName) .' '. lang('report'); ?>"><i class="material-icons">insert_chart</i><div class="ripple-container"></div></a>';
                            }
                            if((check_package_active && can_edit )|| (use_id == full[9])){
                                $html +='<a href="<?php echo base_url('cms/'.$ControllerName.'/edit/');?>'+full[7]+'" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('edit') .' '.lang($ControllerName); ?>"><i class="material-icons">edit</i><div class="ripple-container"></div></a>';
                            }
                            if(check_package_active && can_delete ){
                                $html +='<a href="#" data-machine_id="'+full[7]+'" data-url="<?php echo 'cms/'.$ControllerName.'/action'; ?>" class="btn btn-simple btn-danger btn-icon remove machine_remove" title="<?php echo lang('delete') .' '.lang($ControllerName); ?>"><i class="material-icons">close</i><div class="ripple-container"></div></a>';
                            }
                            $html += '</td>';
                        }

                        return $html;
                    }
                }
            ]
        });

        $('#datatables-machine').on('click', 'a.machine_remove', function (e) {
        /* var machineDataTable = $("#datatables").DataTable({ ajax: "<?php //echo base_url('cms/machine/getLists/'); ?>//" }); */
            e.preventDefault();
            var machine_id = $(this).data('machine_id');
            var url = $(this).data('url');
            deleteDatatableRecord(machine_id, url);
            machinesDataTable.ajax.reload();
        } );
    });
</script>