<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('dealer_inquiries'); ?></h4>
                        <div class="clearfix"></div>
                        <div class="material-datatables">
                            <table id="datatables-1" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('title');?></th>

                                    <th><?php echo lang('name');?></th>

                                    <th><?php echo lang('company_name');?></th>

                                    <th><?php echo lang('PhoneNumber');?></th>

                                    <th><?php echo lang('email');?></th>

                                    <?php 
                                    
                                    if($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($InquiriesOfMachine){
                                    foreach($InquiriesOfMachine as $value){
                                        $InquiryDetail = json_decode($value->InquiryDetail);
                                        /*echo '<pre>';
                                        print_r($InquiryDetail);*/
                                    $user_info =  get_user_info(array('a.UserID' => $value->UserID));
                                    ?>
                                        <tr id="<?php echo $value->MachineID;?>">

                                            <td><?php echo $value->Title; ?></td>

                                            <td><?php echo $InquiryDetail->name; ?></td>

                                            <td><?php echo $InquiryDetail->company_name; ?></td>

                                            <td><?php echo $InquiryDetail->phone; ?></td>

                                            <td><?php echo $InquiryDetail->email; ?></td>

                                             <?php if($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?> 
                                            <td>
                                                     <a href="javascript:void(0);" data-toggle="modal" data-target="#OpenInquiryDetail" data-title="<?php echo $value->Title; ?>" data-json-object='<?php echo implode('||', (array)$InquiryDetail);?>' class="btn btn-simple btn-success btn-icon inquiryModal" title="<?php echo lang($ControllerName).' '.lang('sale_inquiry'); ?>"><i class="material-icons">speaker_notes</i><div class="ripple-container"></div></a>
                                                
                                               
                                                <?php if(checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="#" onclick="deleteRecord('<?php echo $value->MachineID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove" title="<?php echo lang('delete') .' '.lang($ControllerName); ?>"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                            
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                //exit();
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <?php if($check_package_active){ 
                        ?>
                       
                         <div class="toolbar col-md-7">
                            <a href="<?php echo base_url('cms/otherads/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_machine_add'); ?></button>
                            </a>
                        </div>
                        <?php 

                        if(in_array($this->session->userdata['admin']['RoleID'],array(3,4))){
                        if($purchase){
                        $total_machine =  $purchase->Qty;
                        ?>
                        <div class="col-md-5 purchase_package">

                        <table width="100%">
                        <tr>
                        <td><b><?php echo lang('purchase-package'); ?>:</b></td>
                            <td><?php 
                             if($package){                
                             echo $package->Title;
                                }
                              ?>
                                  
                              </td> 
                              </tr>
                              <tr>

                        <td><b><?php echo lang('expiry_date'); ?>:</b></td>
                        <td><?php echo dateformat($purchase->ExpiryDate,'de'); ?></td>

                        </tr>

                        <tr>
                        <td><b><?php echo ucwords(lang('per_month').' '.lang('machines')); ?>:</b></td>
                        <td><?php echo $purchase->Qty; ?></td>
                        </tr>
                        <tr>
                        <td><b><?php echo lang('remaining').' '.lang('machines'); ?>:</b></td>
                        <td><?php echo ($total_machine)-count($results); ?></td>

                        </tr>

                        </table>
                            
                            
                        </div>
                        <?php } } } ?>
                        <div class="clearfix"></div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('featured_img');?></th>

                                    <th><?php echo lang('title');?></th>
                                    
                                    <th><?php echo lang('user');?></th>
                                    
                                    <th><?php echo 'ID';?></th>

                                    <th><?php echo lang('is_active');?></th>

                                    <?php 
                            
                                    if(($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')) || (isset($results[0]->UserID) && $this->session->userdata['admin']['UserID'] == $results[0]->UserID)){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){
                                    $user_info =  get_user_info(array('a.UserID' => $value->UserID));
                                    ?>
                                        <tr id="<?php echo $value->MachineID;?>">

                                            <td><img src="<?php echo (file_exists($value->FeaturedImage) ? base_url() . $value->FeaturedImage : base_url('uploads/no_image.png'));?>" draggable="false" style="width: 100px;"></td>

                                            <td><?php echo $value->Title; ?></td>
                                            
                                             <td><?php echo $user_info->UName; ?> <?php echo $user_info->Title; ?></td>

                                            <td><?php echo $value->MachineID; ?></td>


                                            <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>

                                             <?php if(($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')) || $this->session->userdata['admin']['UserID'] == $value->UserID){?> 
                                            <td>
                                            <a href="<?php echo base_url('detail/'.str_replace(" ","-", $value->Title).'-'.$value->MachineID);?>" class="btn btn-simple   btn-icon" title="<?php echo lang($ControllerName).' '.lang('preview'); ?>"><i class="material-icons">visibility</i><div class="ripple-container"></div></a>

                                                <?php if(($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete'))){?> 

                                                <a href="<?php echo base_url('cms/'.$ControllerName.'/inquiry/'.$value->MachineID);?>" class="btn btn-simple btn-success btn-icon" title="<?php echo lang($ControllerName).' '.lang('sale_inquiry'); ?>"><i class="material-icons">speaker_notes</i><div class="ripple-container"></div></a>


                                                <a href="<?php echo base_url('cms/'.$ControllerName.'/share/'.$value->MachineID);?>" class="btn btn-simple btn-rose btn-icon" title="<?php echo lang($ControllerName).' '. lang('share'); ?>"><i class="material-icons">share</i><div class="ripple-container"></div></a>


                                                <a href="<?php echo base_url('cms/'.$ControllerName.'/report/'.$value->MachineID);?>" class="btn btn-simple btn-twitter btn-icon" title="<?php echo lang($ControllerName) .' '. lang('report'); ?>"><i class="material-icons">insert_chart</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                                <?php if(($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanEdit') )|| $this->session->userdata['admin']['UserID'] == $value->UserID){?>
                                                    <a href="<?php echo base_url('cms/otherads/edit/'.$value->MachineID);?>" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('edit') .' '.lang($ControllerName); ?>"><i class="material-icons">edit</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                               
                                                <?php if($check_package_active and checkUserRightAccess(56,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="#" onclick="deleteRecord('<?php echo $value->MachineID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove" title="<?php echo lang('delete') .' '.lang($ControllerName); ?>"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                            
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<div class="modal fade product-modal" id="OpenInquiryDetail">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('dealer_inquiries'); ?></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                  <table class="table table table-striped table-hover m-0">
                      <tbody>
                          <tr><th><?php echo lang('title');?></th><td id="inquiryTitle"></td></tr>

                          <tr><th><?php echo lang('name');?></th><td id="inquiryName"></td></tr>

                          <tr><th><?php echo lang('company_name');?></th><td id="inquiryCompany"></td></tr>

                          <tr><th><?php echo lang('PhoneNumber');?></th><td id="inquiryPhone"></td></tr>

                          <tr><th><?php echo lang('street_address');?></th><td id="inquiryStreet"></td></tr>

                          <tr><th><?php echo lang('email');?></th><td id="inquiryEmail"></td></tr>

                          <tr><th><?php echo lang('postcode');?></th><td id="inquiryPostal"></td></tr>

                          <tr><th><?php echo lang('message');?></th><td id="inquiryMessage"></td></tr>
                      </tbody>
                  </table>

                </div> <!-- table-responsive -->
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>

<script type="text/javascript">
    $(".inquiryModal").click(function (){
        
        var MachineTitle = $(this).attr('data-title');
        var InquiryDetail = $(this).attr('data-json-object');

        var inquiryarr = InquiryDetail.split('||');

        $("#inquiryTitle").html(MachineTitle);
        $("#inquiryName").html(inquiryarr[0]);
        $("#inquiryCompany").html(inquiryarr[1]);
        $("#inquiryPhone").html(inquiryarr[2]);
        $("#inquiryStreet").html(inquiryarr[3]);
        $("#inquiryEmail").html(inquiryarr[4]);
        $("#inquiryPostal").html(inquiryarr[5]);
        $("#inquiryMessage").html(inquiryarr[6]);

    });
</script>