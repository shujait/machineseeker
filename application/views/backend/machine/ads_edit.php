<?php
$language2 = $language;
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
		$common_fields3 = '';
        $common_fields4 =$common_fields5= '';
        if($key == 0){
       // $rec = getCategory(array('b.CategoryTextID' => $result[0]->CategoryID),'row'); 
        $rec = getCategories($language2,'categories.CategoryID = '.$result[0]->CategoryID,$limit = false,$start = 0,$categorytype=1); 
        $rec = $rec[0];
        if($rec->ParentID > 0)
        {
            $checkRec = getCategories($language2,'categories.CategoryID = '.$rec->ParentID,$limit = false,$start = 0,$categorytype=1);
            $checkRec = $checkRec[0];
            if($checkRec->ParentID > 0)
            {
                $common_fields = '<div class="col-md-12">
                                    <div class="form-group label-floating">
                                         <select class="select2 form-control getSubCategory genTitle" data-type="category" data-style="select-with-transition" required>
                                                <option value="">'.lang('select_category').'</option>';
                                             
                                                $category = array();
                                                $arr = getCategories($language2,'categories.ParentID = 0',$limit = false,$start = 0,$categorytype=1);
                                                if($arr){
                                                foreach($arr as $value){
                                                 $arr_child = getCategories($language2,'categories.ParentID = '.$value->CategoryID,$limit = false,$start = 0,$categorytype=1);
                                                        $common_fields .= '<option '.($checkRec->ParentID == $value->CategoryID ? 'selected' : '').' value="'.$value->CategoryID.'">'.$value->Title.'</option>';
                                                        //$common_fields .= MachineCategoryOptionTree($arr_child, $language2, '20', $result[0]->CategoryID);
                                                                   
                                                   }    
                                                }
                                            
                                    $common_fields .= '</select>
                                    </div>
                                </div>';
                $categoryData = getSubCategoryBy(array('a.ParentID'=>$checkRec->ParentID));
                $common_fields .= '<div id="subCategoryDiv">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                             <select class="select2 form-control getSubCategory genTitle" data-type="subcategory" data-style="select-with-transition" required>
                                                    <option value="">'.lang('select_category').'</option>';
                                                 
                                                    $category = array();
                                                    //$arr = getCategories($language2,'categories.ParentID = 0');
                                                    if($categoryData){
                                                    foreach($categoryData as $value){
                                                     $arr_child = getCategories($language2,'categories.ParentID = '.$value->CategoryID,$limit = false,$start = 0,$categorytype=1);
                                                            $common_fields .= '<option '.($rec->ParentID == $value->CategoryID ? 'selected' : '').' value="'.$value->CategoryID.'">'.$value->Title.'</option>';
                                                            //$common_fields .= MachineCategoryOptionTree($arr_child, $language2, '20', $result[0]->CategoryID);
                                                                       
                                                       }    
                                                    }
                                                
                                        $common_fields .= '</select>
                                        </div>
                                    </div>
                                   </div>
                                <div id="subSubCategoryDiv">
                                </div>';
            }
            else
            {
                $common_fields = '<div class="col-md-12">
                                    <div class="form-group label-floating">
                                         <select class="select2 form-control getSubCategory genTitle" data-type="category" data-style="select-with-transition" required>
                                                <option value="">'.lang('select_category').'</option>';
                                             
                                                $category = array();
                                                $arr = getCategories($language2,'categories.ParentID = 0',$limit = false,$start = 0,$categorytype=1);
                                                if($arr){
                                                foreach($arr as $value){
                                                 $arr_child = getCategories($language2,'categories.ParentID = '.$value->CategoryID,$limit = false,$start = 0,$categorytype=1);
                                                        $common_fields .= '<option '.($rec->ParentID == $value->CategoryID ? 'selected' : '').' value="'.$value->CategoryID.'">'.$value->Title.'</option>';
                                                        //$common_fields .= MachineCategoryOptionTree($arr_child, $language2, '20', $result[0]->CategoryID);
                                                                   
                                                   }    
                                                }
                                            
                                    $common_fields .= '</select>
                                    </div>
                                </div>';   
            }
            $categoryData = getSubCategoryBy(array('a.ParentID'=>$rec->ParentID));
            $common_fields .= '<div id="'.($checkRec->ParentID > 0 ? 'subSubCategoryDiv' : 'subCategoryDiv').'">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                         <select class="select2 form-control getSubCategory genTitle" data-type="'.($checkRec->ParentID > 0 ? 'subsubcategory' : 'subcategory').'" data-style="select-with-transition" name="category" id="category" required>
                                                <option value="">'.lang('select_category').'</option>';
                                             
                                                $category = array();
                                                //$arr = getCategories($language2,'categories.ParentID = 0');
                                                if($categoryData){
                                                foreach($categoryData as $value){
                                                 $arr_child = getCategories($language2,'categories.ParentID = '.$value->CategoryID,$limit = false,$start = 0,$categorytype=1);
                                                        $common_fields .= '<option '.($result[0]->CategoryID == $value->CategoryID ? 'selected' : '').' value="'.$value->CategoryID.'">'.$value->Title.'</option>';
                                                        //$common_fields .= MachineCategoryOptionTree($arr_child, $language2, '20', $result[0]->CategoryID);
                                                                   
                                                   }    
                                                }
                                            
                                    $common_fields .= '</select>
                                    </div>
                                </div>
                            </div>
                            <div id="'.($checkRec->ParentID > 0 ? '' : 'subSubCategoryDiv').'">
                                </div>';
        }
        elseif ($rec->ParentID == 0) {

            $common_fields = '<div class="col-md-12">
                                    <div class="form-group label-floating">
                                         <select class="select2 form-control getSubCategory genTitle" data-type="category" data-style="select-with-transition" name="category" id="category" required>
                                                <option value="">'.lang('select_category').'</option>';
                                             
                                                $category = array();
                                                $arr = getCategories($language2,'categories.ParentID = 0',$limit = false,$start = 0,$categorytype=1);
                                                if($arr){
                                                foreach($arr as $value){
                                                 $arr_child = getCategories($language2,'categories.ParentID = '.$value->CategoryID,$limit = false,$start = 0,$categorytype=1);
                                                        $common_fields .= '<option '.($result[0]->CategoryID == $value->CategoryID ? 'selected' : '').' value="'.$value->CategoryID.'">'.$value->Title.'</option>';
                                                        //$common_fields .= MachineCategoryOptionTree($arr_child, $language2, '20', $result[0]->CategoryID);
                                                                   
                                                   }    
                                                }
                                            
                                    $common_fields .= '</select>
                                    </div>
                                </div>
                                <div id="subCategoryDiv">
                                </div>
                                <div id="subSubCategoryDiv">
                                </div>';    
        }
        $common_fields2 = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('publish_entry').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsDraft">
                                                <input value="1" type="checkbox" id="IsDraft" disabled '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? '' : 'checked').'/> '.lang('draft_entry').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';

        $common_fields4 = '<div class="col-sm-12 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="TermsAndCondition">
                                                <input name="TermsAndCondition" value="1" type="checkbox" id="TermsAndCondition" '.((isset($result[$key]->TermsAndCondition) && $result[$key]->TermsAndCondition == 1) ? 'checked' : '').'/> '.str_replace('[LINK]', '<a href="'.base_url('allgemeine-geschaeftsbedingungen').'" target="_blank">'.lang('terms_condition_text').'</a>', lang('terms_condition_machine')).'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
		
			$common_fields3 = '	<br />
							  <h4 class="card-title">'.lang('price_and_location').'</h4>
								<div class="row"><div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="price">'.lang('price').'</label>
                                        <input type="text" name="price" required  class="form-control" id="price" value="'.(isset($result[0]->Price) ? $result[0]->Price : '').'">
                                    </div>
                                </div>
								
								</div>
								<div class="row">
							<div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="location">'.lang('location').'</label>
                                        <input type="text" name="location" class="form-control" id="location" value="'.(isset($result[$key]->Location) ? $result[$key]->Location : '').'"  onKeyPress="initMap();" autocomplete="off">
                                    </div>
                                </div>
                            </div>
								';
                        if (!empty($dealers)) {
                            $common_fields5 = ' 
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label" for="UserID">' . lang('assignee') . '<span class="red"></span></label>
                                                    <select class="selectpicker"  data-style="select-with-transition" name="UserID" id="UserID" required>
                                                        <option value=""  > ' . lang('self') . '  </option>';

                            foreach ($dealers as $k => $val) {
                                $common_fields5 .= '<option value="' . $val['UserID'] . '"'.(($result[$key]->UserID==$val['UserID']) ? 'selected' : '').'>' . $val['UName'] . '</option>';
                            }

                            $common_fields5 .= '</select>  </div>     </div>';
                        }
        }
      
        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $seach_checked="";
        $visiter_checked="";
        if(isset($result[0]->AdsVisitor) &&($result[0]->AdsVisitor==1)){
            $seach_checked='checked="true"';
        }else{
            $visiter_checked='checked="true"';
        }
        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">
													<input type="hidden" name="CountryShort" id="country_short" value="'.$result[0]->CountryShort.'">
													<input type="hidden" name="Lat" id="lat" value="'.$result[0]->Lat.'">
													<input type="hidden" name="Lng" id="lng" value="'.$result[0]->Lng.'">
                                                    <input type="hidden" name="machine_ads_type" id="machine_ads_type" value="1">
													';
												
                                                    $lang_data .= '<br />


                            <h4 class="card-title">'.lang('machine_data').'</h4>
                             <div class="row">
                                <div class="col-md-12">
                                     <div class="col-sm-4">
                                        <div class="form-group upflow-02" style="margin: 21px 0 0 0;">
                                      <label>'.lang('machine_add_type').'</label>
                                      </div>
                                    </div>
                                    <div class="col-sm-8">
                                                 <div class="col-sm-6 checkbox-radios">
                                                <div class="form-group label-floating">
                                                    <label class="radio01">
                                                           
                                                                <input type="radio" name="ads_type" value="1" '.$seach_checked.'>'.lang('ads_visiter').'
                                                            <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                  <div class="form-group">
                                            <label class="radio01"> 
                                                        <input type="radio" name="ads_type" value="2" '.$visiter_checked.'>'. lang('ads_offering').'
                                                           <span class="checkmark"></span>
                                                    </label>
                                         </div>
                                    </div> 
                                                 
                                        </div>
                                </div>
                            </div>
                            <div class="row">
										  '.$common_fields5.$common_fields.'  

								 <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="title">'.lang('title').' <span class="red">*</span></label>
									    <input type="text" name="title" required  class="form-control" id="title" value="'.(isset($result[$key]->Title) ? $result[$key]->Title : '').'">
                              
									 </div>
                                     <span class="red"><small>'.lang('machine-title-placeholder').'</small></span>
                                </div>';
								 if($common_fields != ''){		
								   $lang_data .= '</div>
									<div class="row">';
								 }
                               
							 if($common_fields != ''){										
								  $lang_data .= '<div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="codition">'.lang('codition').' <span class="red">*</span></label>
										 <select class="selectpicker"  data-style="select-with-transition" name="codition" id="codition" required>';
										
											 $condition_list = condition_list(true);
											 foreach($condition_list as $k => $val){
	  										$lang_data .= '<option value="'.$k.'"  '.getSelected((isset($result[0]->Condition) ? $result[0]->Condition : ''),$k).'>'.$val.'</option>';
												}
                                   $lang_data .= '</select>
                                    </div>
                                </div>';
								   $lang_data .= '</div>
									<div class="row">';
								
                                /*$lang_data .= '
								 
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="model">'.lang('model').' <span class="red">*</span></label>
                                        <input type="text" name="model" required  class="form-control genTitle" id="model" value="'.(isset($result[$key]->Model) ? $result[$key]->Model : '').'">
                                    </div>
                                </div>';*/
								   $lang_data .= '</div>
									<div class="row">';
							
                                
									 	 }
							$lang_data .= '</div>
						
								 '.$common_fields3.'
                                ';
						
		                  if($key == 0){
							$lang_data .= '<br />
							<h4 class="card-title">'.lang('offer_detail').'</h4>

							<div class="row">

                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="reference_number">'.lang('reference_number').'</label>
                                        <input type="text" name="reference_number" required  class="form-control" id="reference_number" value="'.(isset($result[0]->ReferenceNumber) ? $result[0]->ReferenceNumber : '').'">
                                    </div>
                                </div>
								 <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="serial_number">'.lang('serial_number').'</label>
                                        <input type="text" name="serial_number" required  class="form-control" id="serial_number" value="'.(isset($result[0]->SerialNumber) ? $result[0]->SerialNumber : '').'">
                                    </div>
                                </div>
                            </div>';
											}
                            //Multiple File Upload
		                      if($key == 0){
							$lang_data .= '
							<div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
										     <label>'.lang('file').' ('.(lang('images_will_save')).')</label> <span class="red">*</span><br>
                                        <input type="file" name="images"  id="filer_input1" required>
                                    </div>';

										$lang_data .= '<div class="form-group clearfix">
										   <div class="col-sm-12 padding-left-0 padding-right-0">
											  <div class="jFiler jFiler-theme-dragdropbox">
												 <div class="jFiler-items jFiler-row">
													<ul class="jFiler-items-list jFiler-items-grid sorting3">
                                                        <input type="hidden"  id="machine-'.$result[0]->MachineID.'" >' ;

                                                if(!empty($result[0]->FeaturedImage))
                                                $lang_data .='<li class="jFiler-item ArticleImage ui-sortable-handle" data-jfiler-index="1" style="" 
                                                        id="item-'.$result[0]->MachineID.'">
                                                        <div class="jFiler-item-container">
                                                             <div class="jFiler-item-inner">
                                                                <div class="jFiler-item-thumb">
                                                                   <div class="jFiler-item-status"></div>
                                                                   <div class="jFiler-item-info"></div>
                                                                   <div class="jFiler-item-thumb-image"> <img src="'. ($result[0]->IsImport? $result[0]->FeaturedImage : base_url() . $result[0]->FeaturedImage).'" draggable="false jpg"></div></div>
                                                                <div class="jFiler-item-assets jFiler-row">
                                                                   <ul class="list-inline pull-left">
                                                                      <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
                                                                   </ul>
                                                                   <ul class="list-inline pull-right">
                                                                      <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-type="f" data-image-id="'.$result[0]->MachineID.'" data-image-path="'
                                                .   ($result[0]->IsImport? $result[0]->FeaturedImage : base_url() . $result[0]->FeaturedImage) . '"></a></li>
                                                                   </ul>
                                                                </div>
                                                             </div>
                                                          </div>
                                                       </li>
                                                ';


                                                if (!empty($site_images)) {
													foreach($site_images as $img){
													   $lang_data .= '<li class="jFiler-item ArticleImage" data-jfiler-index="1" style="" id="item-'.$img['SiteImageID'].'" 
                                                          >
														  <div class="jFiler-item-container">
															 <div class="jFiler-item-inner">
																<div class="jFiler-item-thumb">
																   <div class="jFiler-item-status"></div>
																   <div class="jFiler-item-info"></div>
																   <div class="jFiler-item-thumb-image">';
															 	     $ext = pathinfo($img['ImageName'], PATHINFO_EXTENSION);
															         if(in_array($ext,array('docx','doc','pdf','xls','xlsx'))){
																		if($ext == 'doc' or $ext == 'docx'){
																			$ext = 'doc';
																		}else if($ext == 'xls' or $ext == 'xlsx'){
																			$ext = 'xls';
																		}
																		$lang_data .= ' <img src="' . base_url('uploads/'.$ext.'.png') . '" draggable="false '.$ext.'"></div>'; 
																	 }else{
																   $lang_data .= ' <img src="' . (file_exists($img['ImageName']) ? base_url() . $img['ImageName'] : base_url('uploads/no_image.png')) . '" draggable="false '.$ext.'"></div>';
																	 }
																 $lang_data .= '</div>
																<div class="jFiler-item-assets jFiler-row">
																   <ul class="list-inline pull-left">
																	  <li><span class="jFiler-item-others"><i class="icon-jfi-file-image jfi-file-ext-jpg"></i></span></li>
																   </ul>
																   <ul class="list-inline pull-right">
																	  <li><a class="icon-jfi-trash jFiler-item-trash-action remove_image" data-image-type="o" data-image-id="'. $img['SiteImageID'] .'" data-image-path="'. $img['ImageName'] .'"></a></li>
																   </ul>
																</div>
															 </div>
														  </div>
													   </li>';
														 } 

									$lang_data .= '</ul>
												 </div>
											  </div>
										   </div>
										</div>';


									}
						$lang_data .= '</div>
                            </div>';
							  }

                            //Featured Image
                            if($key == 0){
                            $lang_data .= '
                           
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating" id="add_url_Video">
                                             <label class="control-label" for="video">'.lang('video').'</label>
                                        <input type="url" name="video" class="form-control youtube_link" id="video" value="'.(isset($result[0]->Video) ? $result[0]->Video : '').'">';
                                  if($result[0]->Video){
                                        $lang_data .= '<br />
                                        <iframe  width="420" height="315" src="'.$result[0]->Video.'" frameborder="0" allowfullscreen></iframe>';
                                  }
                                   $lang_data .= ' </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating" id="keywords">
                                        <label class="control-label" for="keywords"> '. lang('seo_keywords') .'</label>
                                        <input type="text" name="keywords" class="form-control " value="'.$result[$key]->keywords.'"id="keywordstxt">

                                    </div>
                                </div>
                            </div>
                            ';
                              }
							$lang_data .= '<br /><h4 class="card-title">'.lang('Description').
                                                       '<a href="'.base_url().$helpingUrl.'"data-toggle="tooltip" data-placement="bottom" 
                                                   title="'.lang('you_need_help_filling_out_the_add').'" class="tooltip001">?</a></h4>

							<div class="row">

                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="description">'.lang('Description').' <span class="red">*</span></label>
										<br />
										<br />
                                        <textarea rows="20" name="description" required  class="form-control" id="descriptionText">'.(isset($result[$key]->Description) ? $result[$key]->Description : '').'</textarea>
                                    </div>
                                </div>
								
                            </div>
                                                    <div class="row">
                                                        '.$common_fields4.'
                                                    </div>
                                                    <div class="row">
                                                         '.$common_fields2.'
                                                    </div>
                                                    
                                                   
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>



<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('edit').' ' .$result[0]->Title.' '.lang($ControllerName);?></h5>
						<h6 class="card-sub-title"><?php echo lang('card_sub_title'); ?></h6>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                                <div class="col-md-8">
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="DeleteArticleImage" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading"><?php echo lang('delete'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign col-md-1"></span> <?php echo lang('are_you_sure'); ?></div>
            </div>
            <div class="modal-footer ">
                <a type="button" class="btn btn-success delete_url" ><span class="glyphicon glyphicon-ok-sign"></span> <?php echo lang('Yes'); ?></a>
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> <?php echo lang('No'); ?></button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>


<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'descriptionText' );
</script>

<script>
    $(document).on('click', '.delete_url', function () {
        var image_id       = $(this).attr('data-modal-image-id');
        var image_path       = $(this).attr('data-modal-image-path');
        var image_type = $(this).attr('data-image-type');
        var $this = $(this);
        $.ajax({
                type: "POST",
                url: '<?php echo base_url() . 'cms/' . $ControllerName . '/action'; ?>',
                data: {
                    image_path: image_path,
                    image_id: image_id,
                    image_type: image_type,
					form_type: 'deleteimage'
                },
                success: function (result) {
                    $.unblockUI;
                    if (result.error != false) {
                        $("#img-"+image_id).remove();
                    }



                },
                complete: function () {
                    $('#DeleteArticleImage').modal('hide');
                    $.unblockUI();
                }
            });
    });
</script>