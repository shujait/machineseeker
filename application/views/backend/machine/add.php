<?php $default_lang = getDefaultLanguage(); ?>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add_entry');?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data1" id="form_data1" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
                            <input type="hidden" name="SystemLanguageID" value="<?php echo base64_encode($default_lang->SystemLanguageID); ?>">
                            <input type="hidden" name="IsDefault" value="1">
                            <input type="hidden" name="CountryShort" id="country_short">
                            <input type="hidden" name="Lat" id="lat">
                            <input type="hidden" name="Lng" id="lng">
                            <input type="hidden" name="machine_ads_type" id="machine_ads_type" value="0">
                            <br />
                            <h4 class="card-title"><?php echo lang('data');?> </h4>
                            <?php if(!empty($dealers)){?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group label-floating">
                                            <label class="card-title" for="UserID">
                                                <?php echo lang('assignee'); ?> <span class="red"></span>
                                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_assignee'); ?>" class="tooltip001">?</a>
                                            </label>
                                            <select class="selectpicker"  data-style="select-with-transition" name="UserID" id="UserID" required>
                                                <option value=""  ><?php echo lang('self'); ?>  </option>
                                                <?php
                                                foreach ($dealers as $k => $val) {
                                                    echo '<option value="' . $val['UserID']. '">' . $val['UName'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="row">
                                <!-- <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"  data-toggle="modal" data-target="#categoryModal" >Select Category</button>
                                            <input type="hidden" value="" required name="category">
                                            <span class="cat-title"></span>
                                        </div>
                                    </div> -->
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <select class="select2 form-control getSubCategory genTitle" data-type="category"  data-style="select-with-transition" name="category" id="category" required>
                                            <option value=""><?php echo lang('select_main_category'); ?></option>
                                            <?php
                                            $category = array();
                                            $arr = getCategories($language,'categories.ParentID = 0',$limit = false,$start = 0,$categorytype=-1);
                                            if($arr){
                                                foreach($arr as $value){
                                                    $arr_child = getCategories($language,'categories.ParentID = '.$value->CategoryID,$limit = false,$start = 0,$categorytype=-1);
                                                    ?>
                                                    <option value="<?php echo $value->CategoryID; ?>"><?php echo $value->Title; ?></option>
                                                    <?php //echo  MachineCategoryOptionTree($arr_child, $language); ?>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="subCategoryDiv">
                                </div>
                                <div id="subSubCategoryDiv">
                                </div>
                                <!-- <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="title"><?php //echo lang('title').lang('does_not_have_to_be_filled_in'); ?> <span class="red">*</span></label>
                                        <input type="text" name="title" required  class="form-control" id="title">
                                     </div>
                                     <span class="red"><small><?php //echo lang('machine-title-placeholder');?></small></span>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating non-services">
                                        <label class="control-label " for="manufacturer" ><?php echo lang('manufacturer'); ?> <span class="red">*</span>
                                            <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_manufacturer'); ?>" class="tooltip001">?</a></label>
                                        <input type="text" name="manufacturer" required  class="form-control genTitle" id="manufacturer">
                                    </div>
                                    <div class="form-group label-floating services" style="display: none;">
                                        <label class="control-label " for="Services"  ><?php echo lang('Article/Services'); ?> <span class="red">*</span> <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_services'); ?>" class="tooltip001">?</a></label>
                                        <input type="text" name="service" required  class="form-control genTitle" id="service">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="machine_type"><?php echo lang('machine_type'); ?> <span class="red">*</span> <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_machine_type'); ?>" class="tooltip001">?</a></label>
                                        <input type="text" name="machine_type" required  class="form-control genTitle" id="machine_type">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="codition"><?php echo lang('codition'); ?> <span class="red">*</span> <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_condition'); ?>" class="tooltip001">?</a></label>
                                        <select class="selectpicker"  data-style="select-with-transition" name="codition" id="codition" required>
                                            <?php
                                            $condition_list = condition_list(true);
                                            foreach($condition_list as $k => $val){
                                                echo '<option value="'.$k.'">'.$val.'</option>';
                                            } ?>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="model"><?php //echo lang('model'); ?> <span class="red">*</span></label>
                                        <input type="text" name="model" required  class="form-control genTitle" id="model">
                                    </div>
                                </div> -->
                            </div>

                            <div class="row non-services">
                                <div class="col-md-12">
                                    <div class="form-group label-floating is-focused">
                                        <label class="control-label" for="manufacturer_year">
                                            <?= lang('manufacturer_year'); ?>
                                            <span class="red">*</span> <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_manufacturer_year'); ?>" class="tooltip001">?</a>
                                        </label>
                                        <select id="manufacturer_year" class="select2 form-control" data-style="select-with-transition" required name="manufacturer_year">
                                            <option value=""><?= lang('SelectAYear'); ?></option>
                                            <?php
                                            $arr = YearList();
                                            if(!empty($arr)){
                                                foreach($arr as $year){ ?>
                                                    <option value="<?php echo $year; ?>"><?php echo $year; ?> </option>
                                                <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <h4 class="card-title"><?php echo lang('price_and_location');?></h4>

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="price"><?php echo lang('price'); ?>  <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_price'); ?>" class="tooltip001">?</a></label>
                                        <input type="text" name="price" required  class="form-control" id="price">
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="vat"><?php //echo lang('vat'); ?></label>
                                        <input type="text" name="vat" required  class="form-control" id="vat">
                                    </div>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="location"><?php echo lang('location'); ?> <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_location'); ?>" class="tooltip001">?</a></label>
                                        <input type="text" name="location"  class="form-control" id="location" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <br />
                            <h4 class="card-title"><?php echo lang('offer_detail');?></h4>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="reference_number"><?php echo lang('reference_number'); ?> <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_reference_number'); ?>" class="tooltip001">?</a></label>
                                        <input type="text" name="reference_number" required  class="form-control" id="reference_number">
                                    </div>
                                </div>
                                <!-- <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="serial_number"><?php echo lang('serial_number'); ?></label>
                                        <input type="text" name="serial_number" required  class="form-control" id="serial_number">
                                    </div>
                                </div> -->
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating sortable_row">
                                        <label><?= lang('file'); ?> (<?= lang('upload_image_or_pdf');?>) <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_file'); ?>" class="tooltip001">?</a></label>

                                        <br>
                                        <!--                                        <input type="file" name="images"  id="filer_input1" required>-->
                                        <div class="dropzone dz-clickable" id="myDrop">
                                            <div class="dz-default dz-message" data-dz-message="">
                                                <span>Drop files here to upload</span>
                                            </div>
                                        </div>
                                        <input type="hidden" id="sortArray" name="sortArray">
                                    </div>
                                </div>
                            </div>

                            <!--  <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
                                             <label><?php //echo lang('set_featured_img'); ?></label> <span class="red">*</span><small style="margin-left: 60px; font-weight: bold;"><?php //echo lang('feater_image_text');?></small><br>
                                        <input type="file" name="feature_image"  id="filer_input2" required>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating" id="add_url_Video">
                                        <label class="control-label" for="video"><?php echo lang('video'); ?> <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_video'); ?>" class="tooltip001">?</a></label>
                                        <input type="url" name="video" class="form-control youtube_link" id="video">
                                        <br />
                                        <iframe style="display:none;" width="420" height="315" src="" frameborder="0" allowfullscreen></iframe>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating" id="keywords">
                                        <label class="control-label" for="keywords"><?php echo lang('seo_keywords'); ?> <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('info_keywords'); ?>" class="tooltip001">?</a></label>
                                        <input type="text" name="keywords" class="form-control " id="keywordstxt">

                                    </div>
                                </div>
                            </div>          <br />
                            <h4 class="card-title">
                                <?php echo lang('Description');?>
                                <a href="<?php echo base_url();?><?php echo $helpingUrl;?>" data-toggle="tooltip" data-placement="bottom" title="<?php echo lang('you_need_help_filling_out_the_add'); ?>" class="tooltip001">?</a>
                            </h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="control-label" for="description">
                                        <?php echo lang('Description'); ?>
                                    </label>
                                    <div class="form-group label-floating">
                                        <!--                                        <textarea rows="20" name="description" required  class="form-control TinymceEditor" id="description"></textarea>-->
                                        <textarea rows="20" name="description" class="form-control" id="descriptionText" required ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="">
                                            <label for="TermsAndCondition">
                                                <input name="TermsAndCondition" value="1" type="checkbox" id="TermsAndCondition"/> <?php echo str_replace('[LINK]', '<a href="'.base_url('allgemeine-geschaeftsbedingungen').'" target="_blank">'.lang('terms_condition_text').'</a>', lang('terms_condition_machine')) ; ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php  if($this->session->userdata['admin']['RoleID']!=4){;?>
                                <div class="row">
                                    <div class="col-sm-4 checkbox-radios">
                                        <div class="form-group label-floating">
                                            <label class="radio01">
                                                <input name="IsActive" value="1" type="radio" id="IsActive"/> <?php echo lang('publish_entry'); ?>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="radio01">
                                                <input checked name="IsActive" value="0" type="radio" /> <?php echo lang('draft_entry'); ?>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');
                                    if ($this->session->userdata['admin']['RoleID']==4)
                                        echo ' & '.lang('preview');
                                    ?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<!--<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>-->
<script src="<?php echo base_url(); ?>assets/backend/js/ckeditor/ckeditor.js" type="text/javascript"></script>
<script>
    CKEDITOR.replace( 'descriptionText' );
    
    Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone("div#myDrop",{
        addRemoveLinks: true,
        maxFiles: 20,
        uploadMultiple: true,
        resizeQuality: 0.6,
        autoProcessQueue: true,
        acceptedFiles: 'image/jpeg,image/jpg,image/png,.jpeg,.jpg,.png',
        url: "<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action",
        init: function() {
            this.on("maxfilesexceeded", function(file) {
                showError("<?= lang('file_upload_max') ?>");
                this.removeFile(file);
            });
            this.on("addedfile", function(file, xhr, formData) {
                if ($.inArray(file.type, ['image/jpeg', 'image/jpg', 'image/png', 'image/gif']) == -1) {
                    showError("<?= lang('wrong_file_upload') ?>");
                    this.removeFile(file);
                }
            });
        }
    });

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, {type: contentType});
        return blob;
    }

    $('.form_data1').on('submit', function (e){
        e.preventDefault();
        var Images = [];
        var form = document.getElementById("form_data1");
        /* Create a FormData and append the file with "image" as parameter name */
        var formDataToUpload = new FormData(form);

        myDropzone.getAcceptedFiles().forEach(function(entry) {
            Images.push(entry.dataURL);
            var ImageURL = entry.dataURL;
            /* Split the base64 string in data and contentType */
            var block = ImageURL.split(";");
            /* Get the content type of the image */
            var contentType = block[0].split(":")[1]; /* In this case "image/gif" */
            var extensionParse = contentType.split("/");
            /* get the real base64 content of the file */
            var realData = block[1].split(",")[1]; /* In this case "R0lGODlhPQBEAPeoAJosM...." */

            /* Convert it to a blob to upload */
            var blob = '';
            blob = b64toBlob(realData, contentType);
            console.log("realData : " +realData);
            console.log("conent type : " +contentType);
            console.log("blob : " + blob);
            formDataToUpload.append("images[]", blob, Date.now()+'.'+extensionParse[1]);
        });

        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: formDataToUpload,
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            /* async:false, */
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }

                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
            }
        });

        $('#images').val(Images);
    });
</script>