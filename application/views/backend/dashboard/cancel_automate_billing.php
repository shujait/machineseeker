<div class="col-md-12">
    <div class="billing-o">
            <?= lang('cancel_payment_text'); ?>
        <div>
            <button type="button" class="btn btn-primary cancelPayment" data-is_auto_payment="<?= $current_package->IsAutoPayment ?>" <?= ($current_package->IsAutoPayment == 0)?'disabled':'' ?>><?= lang('cancel_payment_label');?></button>
        </div>
    </div>
</div>

<script>
    $('.cancelPayment').on('click', function (){
        var is_auto_payment = $(this).data('is_auto_payment');
        $.ajax({
            method:"post",
            url: base_url + "cms/update-automate-billing",
            dataType:"json",
            data:{is_auto_payment:is_auto_payment},
            success: function (response){
                if(response.status){
                    showSuccess(response.message);
                    $(".cancelPayment").prop("disabled", true);
                }else{
                    showError(response.message);
                }
            }
        })
    });
</script>