<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="col-lg-4 col-md-6 col-sm-6 pull-right">
        <div class="pull-right">
            <a href="<?php echo base_url().'cms/tarifwechsel' ?>" class="btn btn-sm btn-success"><?php echo lang('upgrade_package'); ?></a>
            <a href="<?php echo base_url().'cms/kuendigung' ?>" class="btn btn-sm btn-success"><?php echo lang('terminate_auto_billing'); ?></a>
        </div>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="wig-card">
			<h4><?php echo lang('current_package') ?></h4>
			<div class="wig-body">
                <div><b><?php echo lang('current_package') ?></b></div>
                <div><?php echo $planDetails->Title ?></div>
                <div class="head-small-o"><b><?php echo lang('activation_date') ?></b></div>
                <div><?php echo date('d.m.Y', strtotime($purchase->CreatedAt)) ?></div>
            </div>
		</div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="wig-card">
            <h4><?php echo lang('ads') ?></h4>
            <div class="wig-body">
                <div><b><?php echo lang('total_advertisements') ?></b></div>
                <div><?php echo $purchase->Qty ?></div>
                <div class="head-small-o"><b><?php echo lang('ads_left') ?></b></div>
                <div><?php echo ($purchase->Qty - $addedMachinesCount) ?></div>
            </div>
        </div>
    </div>

	<div class="col-lg-3 col-md-6 col-sm-6">
		<div class="wig-card">
			<h4><?php echo lang('base_amount') ?></h4>
			<div class="wig-body">
			<div><?php echo lang('base_amount_without_discount') ?></div>
			<div class="num-o"><?php echo currency(abs($purchase->Rate*$purchase->Duration)) ?></div>
			</div>
		</div>
	</div>
    <?php
        if($purchase->IsAutoPayment == 1){
    ?>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="wig-card">
            <h4><?php echo lang('next_invoice') ?></h4>
            <div class="wig-body">
                <div><?php echo lang('date_of_next_invoice') ?></div>
                <div class="num-o text-danger"><?php echo date('d.m.Y', strtotime($purchase->ExpiryDate)) ?></div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>


<!--<div class="col-lg-12 col-md-12 col-sm-12 p-30">-->
<!--	-->
<!--	<div class="tab-head-o">-->
<!--		<h4>My Current Machine Listing</h4>-->
<!---->
<!--		<div class="table-data-o">-->
<!--			Data table here-->
<!--		</div>-->
<!--	</div>-->
<!---->
<!--</div>-->