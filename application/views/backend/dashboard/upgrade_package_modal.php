<div class="modal" tabindex="-1" role="dialog" id="upgrade_package_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('payment-method'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php
                $package = $this->session->userdata('packageUpgradeDetails');
                ?>
                <form>
                    <input type="hidden" name="new_package" value="<?=  $params['packageId'] ?>">
                    <input type="hidden" name="current_package" value="<?= $params['current'] ?>">
                    <div class="form-group">
                        <span><?php echo lang('new_expiry_date').' '.$package['expiryDate']; ?></span>
                    </div>
                    <div class="form-group">
                        <label for="paymentType"><?php echo lang('payment-method'); ?></label>
                        <select class="form-control" id="paymentType" name="paymentType">
                            <option value=""><?php echo lang('please-select'); ?></option>
                            <option value="paypal">Paypal</option>
                            <option value="bank">Bank</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary updatePackage"><?= lang('upgrade-package'); ?></button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><?= lang('close') ?></button>
            </div>
        </div>
    </div>
</div>
<script>
    $('.updatePackage').on('click', function (){
        var paymentType = $('#paymentType').val();
        if(paymentType == '')
        {
            showError("<?php echo lang('select_payment_method'); ?>");
            return false;
        }
        $.ajax({
            method: "post",
            url: "<?= base_url("cms/tarifaktualisierung") ?>",
            dataType: "json",
            data: {paymentType:paymentType},
            beforeSend: function (){
                $('.updatePackage').prop("disabled", true);
            },
            success: function (response){
                if(response.status)
                {
                    showSuccess(response.message);
                }else{
                    showError(response.message);
                }
                location.reload();
                // console.log(" response : "+response);
            }
        });
    });
</script>