<!-- Bootstrap CSS -->
<div class="row">
  <div class="col-md-12">
    <div class="carousel carousel-showsixmoveone slide" id="carousel123" data-interval="false">
      <div class="carousel-inner">

        <?php  
            foreach ($plans as $key => $plan){
        ?>
        <div class="item <?php echo ($key == 0)?'active':'' ?>">

          <div class="all_packages col-xs-12 col-sm-4 col-md-4 package_<?php echo $key; ?>" data-key=<?php echo $key ?> data-sort_id=<?php echo $plan['SortOrder']; ?> data-current_sort_id=<?php echo $package_details->SortOrder; ?>>
              <div class="overlay_<?php echo $key ?>"></div>
            <div class="package-o">
             <div class="pack-header">
                <h3><?= $plan['Title']; ?></h3>
             </div>
            <?php
                $ar = json_decode($plan['MetaData']);
                $price = $ar[0]->RegularPrice - ($ar[0]->RegularPrice / 100) * $ar[0]->Discount;
                $durationPrice = $price * $ar[0]->Duration;
            ?>
             <div class="pricing-pack-o">
                <h2 class="price_<?= $plan['PackageID'] ?>"><?php echo currency($price)?></h2>
                <p>für alle Inserate / Monat
                   (komplette Laufzeit <strong class="totalPrice_<?= $plan['PackageID'] ?>"><?php echo currency($durationPrice)?> </strong>)
                </p>
             </div>
             <div class="pack-body">
                <table class="table-package">
                   <tr>
                      <td><?php echo lang('machine')?></td>
                      <td><?php echo $ar[0]->QTY; ?></td>
                   </tr>
                   <tr>
                      <td>Vertragsdauer <?= $plan['Type'] ?></td>
                      <td>
                         <div class="form-group p-0">
                           <?php
                           if((int)$plan['Type'] == 1){
                             ?>
                            <select class="changePrice" id="changePrice_<?= $plan['PackageID'] ?>">
                               <?php foreach ($ar as $metaData){?>
                                <option value="<?= $metaData->RegularPrice; ?>" data-duration="<?= $metaData->Duration; ?>" data-packageId="<?= $plan['PackageID']; ?>" data-discount="<?= $metaData->Discount; ?>" data-qty="<?= $metaData->QTY; ?>">
                                    <?php echo $metaData->Duration.' '.lang('month') ?>
                                </option>
                               <?php } ?>
                            </select>
                             <?php }else{ ?>
                                <span data-duration="<?= $ar[0]->Duration; ?>" data-packageId="<?= $plan['PackageID']; ?>" data-discount="<?= $ar[0]->Discount; ?>" data-qty="<?= $metaData->QTY; ?>"><?php echo $ar[0]->Duration.' '.lang('month') ?></span>
                             <?php }?>
                         </div>
                      </td>
                   </tr>
                   <?php foreach ($plan['features'] as $feature){ ?>
                       <tr>
                          <td><?php echo $feature->Title; ?></td>
                          <td><i class="fa <?php echo ($feature->IsActive == 1)?'fa-check-circle':'fa-times-circle' ?>" aria-hidden="true"></i></td>
                       </tr>
                    <?php } ?>
                   <tr>
                      <td colspan="2">
                         <button type="button" data-packageid="<?= $plan['PackageID']; ?>"
                            data-current="<?= $current_package->PackageID; ?>"
                            class="btn upgradePackage <?php echo ($current_package->PackageID == $plan['PackageID'])?'btn-block':'btn-primary'; ?> btn-block"
                             <?php echo ($current_package->PackageID == $plan['PackageID'])?'disabled':''; ?>
                             <?php echo ($plan['SortOrder'] <= $package_details->SortOrder)?'disabled':''; ?>
                         >Upgrade</button>
                      </td>
                   </tr>
                </table>
             </div>
             <div class="pack-footer"></div>
            </div>
          </div>
        </div>
        <?php } ?>


      </div>
      <div class="arrow-slide">
      <a class="left" href="#carousel123" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
      <a class="right" href="#carousel123" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
  	 </div>
    </div>
  </div>
</div>
<!-- jQuery library -->

<!-- Initialize Bootstrap functionality -->
<script>
(function() {
  // setup your carousels as you normally would using JS
  // or via data attributes according to the documentation
  // http://getbootstrap.com/javascript/#carousel
  // $('#carousel123').carousel({
  //   interval: 2000
  // });
  // $('#carouselABC').carousel({
  //   interval: 3600
  // });
}());

(function() {
  $('.carousel-showsixmoveone .item').each(function() {
    var itemToClone = $(this);

    for (var i = 1; i < 3; i++) {
      itemToClone = itemToClone.next();

      /* wrap around if at end of item collection */
      if (!itemToClone.length) {
        itemToClone = $(this).siblings(':first');
      }

      /* grab item, clone, add marker class, add to collection */
      itemToClone.children(':first-child').clone()
        .addClass("cloneditem-" + (i))
        .appendTo($(this));
    }
  });
}());

// $price = $ar[0]->RegularPrice - ($ar[0]->RegularPrice / 100) * $ar[0]->Discount;
// $durationPrice = $price * $ar[0]->Duration;

    $('.upgradePackage').on('click', function (){
        var packageId = $(this).data("packageid");
        var current = $(this).data("current");
        var RegularPrice = $('#changePrice_'+packageId).val();
        var Duration = $('#changePrice_'+packageId).find(':selected').data("duration");
        var Discount = $('#changePrice_'+packageId).find(':selected').data("discount");
        var qty = $('#changePrice_'+packageId).find(':selected').data("qty");

        $.ajax({
            method: "get",
            url : "<?= base_url("cms/upgradePackageModal") ?>",
            data : {packageId:packageId, current:current, RegularPrice:RegularPrice, Duration:Duration, Qty:qty, Discount:Discount},
            dataType : 'html',
            success: function (html) {
                $('#upgradeModal').html(html);
                $('#upgrade_package_modal').modal();
            }
        });
    });

    $('.changePrice').on('change', function (){

        var RegularPrice = $(this).val();
        var Duration = $(this).find(':selected').data("duration");
        var Discount = $(this).find(':selected').data("discount");
        var packageId = $(this).find(':selected').data("packageid");
        var price = RegularPrice - (RegularPrice / 100) * Discount;
        var durationPrice = price * Duration;
        $('.price_'+packageId).text(price + ",00 €");
        $('.totalPrice_'+packageId).text(durationPrice + ",00 €");
    });
    $('.all_packages').on('mouseenter', function(){
      let current = $(this).find('button').data('current');
      let packageid = $(this).find('button').data('packageid');
      if(current == packageid)
      {
        var html = '<div class="overlay-hover"><h4><?php echo lang('actual_package')?></h4></div>';
      }
      else
      {
        var html = '<div class="overlay-hover"><h4><?php echo lang('cant_downgrade')?></h4></div>';  
      }
      let key = $(this).data('key');
      let sortId = $(this).data('sort_id');
      let currentSortId = $(this).data('current_sort_id');
      console.log('SortID: '+sortId);
      console.log('currentSortId: '+currentSortId);
      if(sortId <= currentSortId)
      {
         $('.overlay_'+key).append(html);
         $('.package_'+key).addClass('disable-package');
      }

    });
    $('.all_packages').on('mouseleave', function(){
      let key = $(this).data('key');
      $('.overlay_'+key).html('');
      $('.package_'+key).removeClass('disable-package');
    });


</script>