<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
		$common_fields3 = '';
		$common_fields4 = '';
        if($key == 0){
        $common_fields = '<div class="col-md-2">
                <div class="form-group label-floating">
                        <label class="control-label" for="expiry_date'.$key.'">'.lang('expiry_date').'  <span class="red">*</span></label>
                        <input type="text" name="expiry_date" required  class="form-control datepicker" id="expiry_date'.$key.'"  value="'.((isset($result[0]->ExpiryDate)) ? dateformat($result[0]->ExpiryDate,'de') : '').'">
                    </div>
                </div>
                <div class="col-md-3">
                <div class="form-group label-floating">
                    <label class="control-label" for="feature">'.lang('feature').'  <span class="red">*</span></label>
                    <select name="feature[]" class="selectpicker" data-style="select-with-transition" multiple required title="'.lang('feature').'" data-size="7">';
                $fea_arr = isset($result[0]->FeatureID)?explode(',',$result[0]->FeatureID):array();
                foreach($feature as $val){
                        $common_fields .= '<option value="'.$val->FeatureID.'" '.getArraySelected($fea_arr,$val->FeatureID).'>'.$val->Title.' </option>';
                        }
                        $common_fields .= '</select>
                </div>
            </div>
            <div class="col-md-1">
          <a href="#" ng-click="add()">
          <i class="material-icons">add_circle</i>
              </a>
            </div>';
			
			
        $common_fields2 = '
        <div class="col-sm-6">
            <div class="form-group label-floating">
                <label class="control-label" for="type"> '.lang('type').' <span class="red">*</span></label>
                <select name="type" id="type" class="selectpicker" data-style="select-with-transition" required title="'.lang('type').'" data-size="7">
                   <option value="">'.lang('select').'</option>
                   <option value="0" '.((!is_null($result[$key]->Type) && $result[$key]->Type == 0) ? 'selected' : '').'> '.lang('one_time_seller').' </option>
                   <option value="1" '.((!is_null($result[$key]->Type) && $result[$key]->Type == 1) ? 'selected' : '').'> '.lang('dealer').'</option>
                </select>
            </div>
        </div>
        <div class="col-sm-6 checkbox-radios">
            <div class="form-group label-floating">
                <div class="checkbox">
                    <label for="IsActive">
                        <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                    </label>
                </div>
            </div>
        </div>';
        
		$common_fields3 = '<br /> <h4 class="card-title">'.lang('machine').' '. lang($ControllerName).'</h4><div class="row">
											</div>';
										     $arr = isset($result[0]->MetaData)?json_decode($result[0]->MetaData):'';
										 	if(!empty($arr) and $result[$key]->SystemLanguageID == $language->SystemLanguageID){

											foreach($arr as $k => $rr){
											$common_fields3 .= '
											<div class="row"  id="row-'.$k.'">
											<div class="col-md-5">
												<div class="form-group label-floating">
													<label class="control-label" for="machine_package">'.lang('machine').' <span class="red">*</span></label>
													<input type="text" name="machine_package[]"  class="form-control number" required id="machine_package" value="'.$rr->QTY.'">
												</div>
											</div>
											<div class="col-md-2">
														<div class="form-group label-floating">
															<label class="control-label" for="duration'.$key.'">'.lang('duration').'  <span class="red">*</span></label>
															<select name="duration[]" id="duration" class="selectpicker" data-style="select-with-transition" required title="'.lang('month').'" data-size="7">';
															$months = MonthList(); 
															foreach($months as $month){
																
														 $common_fields3 .='<option value="'.$month.'" '.getSelected($month,((isset($rr->Duration)) ? $rr->Duration : '')).'>'.$month. ' '.lang('month').' </option>';
															 }  
																	 $common_fields3 .='</select>';	
														 $common_fields3 .='</div>
													</div>	
											 <div class="col-md-2">
												<div class="form-group label-floating">
													<label class="control-label" for="regular_price">'.lang('regular_price').' <span class="red">*</span></label>
													<input type="text" name="regular_price[]"  class="form-control number" required id="regular_price"  value="'.$rr->RegularPrice.'">
												</div>
											</div>	
											 <div class="col-md-2">
												<div class="form-group label-floating">
													<label class="control-label" for="discount">'.lang('discount').' <span class="red">*</span></label>
													<input type="text" name="discount[]" class="form-control number" id="discount" required value="'.$rr->Discount.'">
												</div>
											</div>	
											 <div class="col-md-1">
											  <a href="#" ng-click="del_row('.$k.')">
											  <i class="material-icons">remove_circle</i>
												  </a>
												</div>
										</div>';
											}
											}
											$common_fields3 .= '<div class="row" ng-repeat=\'item in items\' ng-cloak>
											<div class="col-md-5">
												<div class="form-group label-floating is-empty">
													<label class="control-label" for="machine_package{{$index}}">'.lang('machine').'</label>
													<input type="text" name="machine_package[]"  class="form-control number" required id="machine_package{{$index}}">
												</div>
											</div>	
												<div class="col-md-2">
														<div class="form-group label-floating">
															<label class="control-label" for="duration'.$key.'">'.lang('duration').'  <span class="red">*</span></label>
															<select name="duration[]" id="duration" class="selectpicker" data-style="select-with-transition" required title="'.lang('month').'" data-size="7">';
															$months = MonthList(); 
															foreach($months as $month){
																
														 $common_fields3 .='<option value="'.$month.'">'.$month. ' '.lang('month').' </option>';
															 }  
															 $common_fields3 .='</select>';	
														 $common_fields3 .='</div>
													</div>	
											 <div class="col-md-2">
												<div class="form-group label-floating is-empty">
													<label class="control-label" for="regular_price{{$index}}">'.lang('regular_price').'</label>
													<input type="text" name="regular_price[]"  class="form-control number" required id="regular_price{{$index}}">
												</div>
											</div>	
											 <div class="col-md-2">
												<div class="form-group label-floating is-empty">
													<label class="control-label" for="discount{{$index}}">'.lang('discount').'</label>
													<input type="text" name="discount[]" class="form-control number" required id="discount{{$index}}">
												</div>
											</div>
											  <div class="col-md-1">
											  <a href="#" ng-click="del($index)">
											  <i class="material-icons">remove_circle</i>
												  </a>
												</div>
                    						</div>
											';
		}
        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                   
                                                    <div class="row">
                                                        
                                                        <div class="col-md-3">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'  <span class="red">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
												'.$common_fields.'
												</div> '.$common_fields3.'';
		                                		
												$lang_data .= '<div class="row">
                                                        '.$common_fields2.'
													</div>
                                                   
                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>



<div class="content" ng-controller="CustomController">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h5>
						<h6 class="card-sub-title"><?php echo lang('card_sub_title'); ?></h6>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                             <?php if(count($languages) > 1){?>
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                            <div class="col-md-8">
                              <?php }else{ ?>
                                <div class="col-md-10">
                                  <?php } ?>
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>