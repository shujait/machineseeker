<?php $default_lang = getDefaultLanguage(); ?>
<div class="content" ng-controller="CustomController">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">
 							<input type="hidden" name="SystemLanguageID" value="<?php echo base64_encode($default_lang->SystemLanguageID); ?>">

                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?>  <span class="red">*</span></label>
                                        <input type="text" name="Title" required  class="form-control" id="Title">
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="expiry_date"><?php echo lang('expiry_date'); ?>  <span class="red">*</span></label>
                                        <input type="text" name="expiry_date" required  class="form-control datepicker" value="<?php echo date('d.m.Y') ?>" id="expiry_date">
                                    </div>
                                </div>
								 
								 <div class="col-md-4">
                                    <div class="form-group label-floating">
										<label class="control-label" for="feature"><?php echo lang('feature'); ?>  <span class="red">*</span></label>

										<select name="feature[]"  class="selectpicker" id="feature" data-style="select-with-transition" multiple required title="<?php echo lang('feature'); ?>" data-size="7">
											<?php foreach($feature as $val){ ?>
                                                        <option value="<?php echo $val->FeatureID; ?>"><?php echo $val->Title; ?> </option>
											<?php }  ?>
                                                    </select>
                                    </div>
                                </div>
                            </div>
							<br />
							<h4 class="card-title"><?php echo lang('machine') .' '. lang($ControllerName);?></h4>
							<div class="row">
								 <div class="col-md-5">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="machine_package"><?php echo lang('machine'); ?> <span class="red">*</span></label>
										<input type="text" name="machine_package[]"  class="form-control number" required id="machine_package">
                                    </div>
                                </div>
								<div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="duration"><?php echo lang('duration'); ?>  <span class="red">*</span></label>
										<select name="duration[]" id="duration" class="selectpicker" data-style="select-with-transition" required title="<?php echo lang('month'); ?>" data-size="7">
											<?php 
											$months = MonthList(); 
											foreach($months as $month){ ?>
                                                        <option value="<?php echo $month; ?>"><?php echo $month. ' '.lang('month'); ?> </option>
											<?php }  ?>
                                                    </select>										
										                                     
                                    </div>
                                </div>
								 <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="regular_price"><?php echo lang('regular_price'); ?> <span class="red">*</span></label>
                                        <input type="text" name="regular_price[]"  class="form-control number" required id="regular_price">
                                    </div>
                                </div>	
								 <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="discount"><?php echo lang('discount'); ?> <span class="red">*</span></label>
                                        <input type="text" name="discount[]" class="form-control number" required id="discount">
                                    </div>
                                </div>	
								 <div class="col-md-1">
								  <a href="#" ng-click="add()">
								  <i class="material-icons">add_circle</i>
									  </a>
									</div>
							</div>
							<div class="row" ng-repeat='item in items' ng-cloak>
								<div class="col-md-5">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label" for="machine_package{{$index}}"><?php echo lang('machine'); ?> <span class="red">*</span></label>
										<input type="text" name="machine_package[]"  class="form-control number" required id="machine_package{{$index}}">
                                    </div>
                                </div>
								<div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="duration"><?php echo lang('duration'); ?>  <span class="red">*</span></label>
										<select name="duration[]" id="duration" class="selectpicker" data-style="select-with-transition" required title="<?php echo lang('month'); ?>" data-size="7">
											<?php 
											$months = MonthList(); 
											foreach($months as $month){ ?>
                                                        <option value="<?php echo $month; ?>"><?php echo $month. ' '.lang('month'); ?> </option>
											<?php }  ?>
                                                    </select>										
										                                     
                                    </div>
                                </div>
								 <div class="col-md-2">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label" for="regular_price{{$index}}"><?php echo lang('regular_price'); ?> <span class="red">*</span></label>
                                        <input type="text" name="regular_price[]"  class="form-control number" required id="regular_price{{$index}}">
                                    </div>
                                </div>	
								 <div class="col-md-2">
                                    <div class="form-group label-floating is-empty">
                                        <label class="control-label" for="discount{{$index}}"><?php echo lang('discount'); ?> <span class="red">*</span></label>
                                        <input type="text" name="discount[]" class="form-control number" required id="discount{{$index}}">
                                    </div>
                                </div>
								  <div class="col-md-1">
								  <a href="#" ng-click="del($index)">
								  <i class="material-icons">remove_circle</i>
									  </a>
									</div>
                    </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="duration"><?php echo lang('type'); ?>  <span class="red">*</span></label>
                                        <select name="type" id="type" class="selectpicker" data-style="select-with-transition" required title="<?php echo lang('type'); ?>" data-size="7">
                                           <option value=""><?php echo lang('select'); ?></option>
                                           <option value="0"><?php echo lang('one_time_seller'); ?></option>
                                           <option value="1"><?php echo lang('dealer'); ?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>