<!--<div class="sidebar" data-active-color="rose" data-background-color="black" data-image="<?php /*echo base_url();*/ ?>assets/backend/img/sidebar-1.jpg">-->

<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
        </div>
    </div>
</nav>
<div class="sidebar eas">
    <!--
Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
Tip 2: you can also add an image using data-image tag
Tip 3: you can change the color of the sidebar with data-background-color="white | black"
-->
    <div class="logo">
        <a href="<?php echo base_url('cms/dashboard'); ?>" class="simple-text">
            <?php echo $site_setting->SiteName; ?>
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="<?php echo base_url('cms/dashboard'); ?>" class="simple-text">
            <?php echo $site_setting->SiteName; ?>
        </a>
    </div>
    <div class="sidebar-wrapper scrollbar" id="style-1">
        <div class="force-overflow">
            <div class="user">
                <div class="photo">
                    <?php 
                $rec = get_user_info(array('a.UserID' => $this->session->userdata['admin']['UserID'],'a.IsActive' => 1));
                if ($rec->CoverImage != '' && file_exists(FCPATH.$rec->CoverImage)) {
                    $image = base_url($rec->CoverImage);
                } else {
                    $image = base_url('uploads/user_dummy.jpg');
                }
                ?>
                    <img src="<?php echo $image; ?>" />
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                        <?php echo $this->session->userdata['admin']['Title']; ?>
                        <b class="caret"></b>
                    </a>
                    <div class="collapse" id="collapseExample">
                        <ul class="nav">
                            <li class="<?php echo($this->uri->segment(3) == 'profile' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('cms/user/profile'); ?>"><?php echo lang('my-profile'); ?></a>
                            </li>
                            <li class="<?php echo($this->uri->segment(3) == 'change_password' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('cms/user/change_password'); ?>"><?php echo lang('change-password'); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('logout'); ?>">
                                    <p><?php echo lang('logout'); ?></p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php $url_module = $this->uri->segment(2); ?>
            <ul class="nav">
                <!-- <li class="<?php echo($url_module == 'dashboard' ? 'active' : ''); ?>">
                    <a href="<?php echo base_url('cms/dashboard'); ?>">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </a>
                </li> -->

                <?php

            $modules = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,'modules.ParentID = 0 AND modules.IsActive = 1');
            //$modules = getActiveUserModule($this->session->userdata['admin']['UserID'], $language, 'modules.ParentID = 0 AND modules.IsActive = 1');

            if (!empty($modules)) {
                foreach ($modules as $value) {
                    // $getParentUserAccess = getActiveUserModule($this->session->userdata['admin']['UserID'],$language,'modules.ParentID = 0 AND modules.IsActive = 1 AND modules.ModuleID = '.$value->ModuleID);

                    if ($value['CanView'] == 1 || $value['CanAdd'] == 1 || $value['CanEdit'] == 1 || $value['CanDelete'] == 1) {

                        $child_modules = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,'modules.ParentID = '.$value['ModuleID'].' AND modules.IsActive = 1');

                        //$child_modules = getActiveUserModule($this->session->userdata['admin']['UserID'], $language, 'modules.ParentID = ' . $value['ModuleID'] . ' AND modules.IsActive = 1');
                        if (!empty($child_modules)) {

                            $sub = array_column($child_modules, 'Slug');
                            // print_rm($sub);
                            // $getParent = getAllActiveModules($this->session->userdata['admin']['RoleID'],$language,"modules.Slug = '".$this->uri->segment(2)."' AND modules.IsActive = 1");
                            if (in_array($url_module, $sub)) {
                                echo '<script>$( document ).ready(function() {
                                         setTimeout(function(){  $("#parent' . $value['ModuleID'] . '").click(); }, 500);
  
});</script>';
                            }

                            ?>

                <li>
                    <a data-toggle="collapse" href="#<?php echo $value['ModuleID']; ?>" id="parent<?php echo $value['ModuleID']; ?>">
                        <i class="material-icons"><?php echo $value['IconClass']; ?></i>
                        <p><?php echo $value['ModuleTitle']; ?>
                            <b class="caret"></b>
                        </p>
                    </a>


                    <?php


                                if (!empty($child_modules)) { ?>

                    <div class="collapse" id="<?php echo $value['ModuleID']; ?>">
                        <ul class="nav">


                            <?php
                                            foreach ($child_modules as $child_value) {
                                                //$getChildUserAccess = getActiveUserModule($this->session->userdata['admin']['UserID'],$language,'modules.ParentID = '.$value->ModuleID.' AND modules.IsActive = 1 AND modules.ModuleID = '.$child_value->ModuleID);
                                                if (($child_value['CanView'] == 1 || $child_value['CanAdd'] == 1 || $child_value['CanEdit'] == 1 || $child_value['CanDelete'] == 1)) { ?>


                            <li class="<?php echo($url_module == $child_value['Slug'] ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('cms/' . $child_value['Slug']); ?>"><?php echo $child_value['ModuleTitle']; ?></a>
                            </li>
                            <?php }
                            } ?>
                        </ul>
                    </div>

                    <?php } ?>
                </li>
                <?php } else { ?>

                <li class="<?php echo($url_module == $value['Slug'] ? 'active' : ''); ?>">
                    <a href="<?php echo base_url('cms/' . $value['Slug']); ?>">
                        <i class="material-icons"><?php echo $value['IconClass']; ?></i>
                        <p><?php 
                                    echo $value['ModuleTitle'];?></p>
                    </a>
                </li>
                <?php } ?>

                <?php
                    }
                }
            }
            ?>


                <li>
                    <a href="<?php echo base_url('logout'); ?>">
                        <i class="material-icons">lock</i>
                        <p><?php echo lang('logout'); ?></p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="main-panel">
    <?php $this->load->view('frontend/layouts/menu'); ?>
