</div>
<?php $this->load->view('frontend/layouts/footer-menu'); ?>
	 	<div class="modal fade product-modal" id="categoryModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('all-categories'); ?></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
			<br />
            <div class="modal-body" style="height: 300px; overflow-y: auto;">
			<ul class="tree-menu">
								<?php
									$category = 'categories.ParentID = 0';
									if($this->session->userdata['admin']['RoleID'] == 4){
				                    $purchase  = getValue('user_package_purchase',array('UserID' => $this->session->userdata['admin']['UserID']));
									 $quote = "'";
									 $category = $quote. str_replace(",", "','", $purchase->Category) . $quote;	
									 $category = "categories_text.CategoryTextID IN (".$category.")";
									}

									$arr = getCategories($language,false);
									if($arr){
										if($this->session->userdata['admin']['RoleID'] != 4){
                                    foreach($arr as $value){
                                    	$arr_child = getCategories($language,'categories.ParentID = '.$value->CategoryID);
									 ?>
								<li>
									<input type="radio" value="<?php echo $value->CategoryID;  ?>" data-title="<?php echo $value->Title; ?>" name="choose-category" class="category">
									<input type="checkbox" id="menu<?php echo $value->CategoryID; ?>"/><label for="menu<?php echo $value->CategoryID; ?>"><?php echo $value->Title; ?></label>
									<?php echo  MachineCategoryTree($arr_child, $language); ?>
								</li>
									<?php					
										
									} }else {
									   foreach($arr as $value){
									   	$arr_child = getCategories($language,'categories.ParentID = '.$value->CategoryID);
										?>
										<li>
									<input type="radio" value="<?php echo $value->CategoryID;  ?>" data-title="<?php echo $value->Title; ?>" name="choose-category" class="category">
									<input type="checkbox" id="menu<?php echo $value->CategoryID; ?>"/><label for="menu<?php echo $value->CategoryID; ?>"><?php echo $value->Title; ?></label>
									<?php echo  MachineCategoryTree($arr_child, $language); ?>
								</li>
				
										
									<?php } } } ?>

				</ul>
				
				 </div>
			<div class="pull-right">
			       <input type="submit" class="btn btn-primary waves-effect waves-light add-category" value="<?php echo lang('add'); ?>">
			</div>
        </div>
    </div>
</div>
</div>
</body>
<div id="upgradeModal"></div>
 <script src="<?php echo base_url(); ?>assets/backend/plugins/tinymce/tinymce.min.js"></script>
<!-- <script src="<?php //echo base_url(); ?>assets/backend/plugins/tinymce/tinymce.min.js" referrerpolicy="origin"></script> -->
<!-- <script src="https://cdn.tiny.cloud/1/6pveniurwtti4m1eff6lgtwv6w8hlrete0guiwnb3dqk7bj4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> -->
<!--<script src="https://cdn.tiny.cloud/1/772xu2l5fowawsyn6577ndlup1dwefscqh3yg7jw4uy4wy8l/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>-->
<!-- Forms Validations Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.validate.min.js"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo base_url(); ?>assets/backend/js/moment.min.js"></script>
<!--  Charts Plugin -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/chartist.min.js"></script>-->
<!--  Plugin for the Wizard -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/jquery.bootstrap-wizard.js"></script>-->
<!--  Notifications Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-notify.js"></script>
<!--   Sharrre Library    -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/jquery.sharrre.js"></script>-->
<!-- DateTimePicker Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/bootstrap-datetimepicker.js"></script>
<!-- Vector Map plugin -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/jquery-jvectormap.js"></script>-->
<!-- Sliders Plugin -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/nouislider.min.js"></script>-->

<script src="<?php echo base_url(); ?>assets/backend/plugins/select2/js/select2.min.js"></script>
<!--  Google Maps Plugin    -->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!-- Select Plugin -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.select-bootstrap.js"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.datatables.js"></script>
<!-- Sweet Alert 2 plugin -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/sweetalert2.js"></script>-->
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/jasny-bootstrap.min.js"></script>-->
<!--  Full Calendar Plugin    -->
<script src="<?php echo base_url(); ?>assets/backend/js/fullcalendar.min.js"></script>
<!-- TagsInput Plugin -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/jquery.tagsinput.js"></script>-->
<!-- Material Dashboard javascript methods -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/material-dashboard.js"></script>-->
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/demo.js"></script>-->
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/dropzone/file-uploader.js" type="text/javascript"></script>-->
<script src="<?php echo base_url(); ?>assets/backend/js/script.js?<?php echo  CSS_VERSION_NUMBER;?>"></script>
<script src="<?php echo base_url(); ?>assets/backend/js/jquery.blockUI.js"></script>
<!--<script src="--><?php //echo base_url(); ?><!--assets/backend/js/custom-angular.js" type="text/javascript"></script>-->

<script type="text/javascript">

$(document).ready(function () {
	$(".select2").select2();

    $("#display_categories").select2({
        maximumSelectionLength: 8
    });

    $("#change_password").validate({
        errorPlacement: function(error, element) {
            $(element).parent('div').addClass('has-error');
        }
    });
    /*sidebar height*/
    var documentHeight = $(document).height();
    var footerHeight = $('.footer').height();
    var sidebarHeight = $('.sidebar').height();
    var SidebarOuterHeight = documentHeight - footerHeight;
    var SidebarinnerHeight = SidebarOuterHeight - 100;
    $(window).on("load resize scroll",function(){
        $('.sidebar').css("height", SidebarOuterHeight);
    });
});

/*tinymce.init({
    selector: '.machine_description',  // change this value according to your HTML
    language: SystemLanguage,
    menubar: 'file edit view format'
});*/

tinymce.init({
      selector: 'textarea.TinymceEditor',
      plugins: 'casechange formatpainter linkchecker autolink lists powerpaste',
      toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
      toolbar2: "print preview | forecolor backcolor emoticons,shortcode | fontsizeselect | fontselect",
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
      language: '<?php echo $language;?>'
   });

</script>
<?php if ($this->session->flashdata('message')) { ?>
    <script>
        $(document).ready(function () {
            showError('<?php echo $this->session->flashdata('message'); ?>');
        });
    </script>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.custom01').each(function(i, obj) {
            /*if($(obj).css("display") == "block"){
                setTimeout(function(){
                    $(obj).attr('style', 'display: none !important');
                }, 5000);
            }*/
        });
    });
    $("#CountryCode").on("click", function(){
        var PhoneNumber = $(".MobileNumber").val();
        if(PhoneNumber == "+34" || PhoneNumber == "34"){
            $(".MobileNumber").val("+");
        } else {
            $(".MobileNumber").val("+49");
        }
    });

    $('.Number').keypress(function(e){
        var txt = String.fromCharCode(e.which);
        if(!txt.match(/[+0-9]/)){ return false; }
    });
</script>
</html>
