<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/backend/img/favicon2.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url(); ?>assets/backend/img/favicon2.png" type="image/x-icon">
    <!-- App title -->
    <title><?php echo $site_setting->SiteName; ?></title>
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.min.css" rel="stylesheet"/>
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo base_url(); ?>assets/backend/css/material-dashboard.css" rel="stylesheet"/>
    <!--  CSS for Demo Purpose, don't include it in your project     -->
<!--    <link href="--><?php //echo base_url(); ?><!--assets/backend/css/demo.css" rel="stylesheet"/>-->
	<link rel="preload" as="style" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />

    <!--     Fonts and icons     -->
    <link href="<?php echo base_url(); ?>assets/backend/css/font-awesome.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/google-roboto-300-700.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/nestable.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/jquery.filer.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/file-uploader/file-uploader.css" rel="stylesheet"/>
    <link href="<?php echo base_url(); ?>assets/backend/css/ltr.css?<?php echo  CSS_VERSION_NUMBER;?>" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url(); ?>assets/backend/css/responsive.css?<?php echo  CSS_VERSION_NUMBER;?>" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url(); ?>assets/backend/css/tree-menu.css" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url(); ?>assets/backend/plugins/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url(); ?>assets/backend/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url(); ?>assets/backend/css/custom.css" rel="stylesheet" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/jquery-ui/jquery-ui.min.css">


    <!--   Core JS Files   -->
    <script src="<?php echo base_url(); ?>assets/backend/js/jquery-2.2.1.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/material.min.js" type="text/javascript"></script>
<!--    <script src="<?php echo base_url(); ?>assets/backend/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>-->
<!--    <script src="--><?php //echo base_url(); ?><!--assets/backend/js/angular.min.js" type="text/javascript"></script>-->
    <script src="<?php echo base_url(); ?>assets/backend/js/jquery.nestable.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/js/jquery.filer.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/backend/plugins/dropzone/dist/dropzone.js" type="text/javascript"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPrjbKrBDh7lAXE4Mch0Yb7roXu8TkY1w&libraries=places"></script>
<!--    <script id="Cookiebot" src="--><?php //echo base_url(); ?><!--assets/backend/js/uc.min.js" data-cbid="1de5fbf4-2de4-4b30-975b-bd5594131b8a" data-blockingmode="auto" type="text/javascript"></script>-->
    <script src="<?php echo base_url();?>assets/plugin/lazyloading/lazyloading.min.js" type="text/javascript"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PJDK2TZ');</script>
    <!-- End Google Tag Manager -->

</head>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var delete_msg = '<?php echo lang('are_you_sure');?>';
	var duplicate_page = '<?php echo lang('duplicate-page-already-added-menu'); ?>';
	var mail_short_code = '<?php echo json_encode(mail_tpl_shortcode()); ?>';
    var SystemLanguage = '<?= $this->session->userdata('lang'); ?>';
    <?php 
        if($this->session->userdata('lang') == 'de')
        {
    ?>
            var DataTable_Language = "../German.json";
    <?php
        }
        else
        {
    ?>
            var DataTable_Language = "";
    <?php
        } 
    ?>
</script>
<style>
    .validate_error {
        border: 1px solid red;
    }

    #validation-msg {
        visibility: hidden;
        min-width: 250px;
        margin-left: -125px;
        text-align: center;
        border-radius: 2px;
        padding: 16px;
        position: fixed;
        z-index: 9999;
        left: 50%;
        top: 80px;
        font-size: 17px;
    }

    #validation-msg.show {
        visibility: visible;
        -webkit-animation: fadein 0.5s, fadeout 0.5s 4.5s;
        animation: fadein 0.5s, fadeout 0.5s 4.5s;
    }
    
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    /* Firefox */
    .MobileNumber {
        -moz-appearance: textfield;
    }
</style>
<body ng-App="App">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PJDK2TZ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!--<div class="alert <?php /*if($this->session->flashdata('message')){ echo 'alert-danger show'; } */ ?>" id="validation-msg">
                        <?php /*if($this->session->flashdata('message')){

                            echo $this->session->flashdata('message');


                        } 
                        */ ?>
     <?php /*if($this->session->flashdata('message')){ */ ?>
        <script>
        
        $( document ).ready(function() {
                 setTimeout(function() {
                            $('#validation-msg').removeClass('alert-danger show alert-success');
                        }, 4000);
        });
        
        </script>
        <?php /*} */ ?>
    </div>-->
<div class="wrapper">
        
  
  