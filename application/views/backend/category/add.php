<div class="content" style="overflow: initial;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                        <input type="hidden" name="form_type" value="save">
                            <div class="col-md-5">
						    <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                            <div class="row">
  								<div class="col-md-12">
                                 <div class="form-group label-floating select-relative">
								<label class="control-label" for="ParentID"><?php echo lang('choose_parent_category'); ?>  <span class="red">*</span></label>
								<select id="ParentID" class="selectpicker" data-style="select-with-transition" required name="ParentID">
									<option value="0"><?php echo lang('parent_category'); ?></option>
									<?php 
										$arr = getCategories($language,$where = false,$limit = false,$start = 0,$categorytype=-1);
										if(!empty($arr)){ 
											foreach($arr as $result){
													?>
												<option value="<?php echo $result->CategoryID; ?>"><?php echo $result->Title; ?> </option>
												<?php 
													}
												}
											?>
								</select>
                                   </div>
                                </div>
                            </div>
							<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Title"><?php echo lang('title'); ?>  <span class="red">*</span></label>
                                        <input type="text" name="Title" required class="form-control" id="Title">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ShortTitle"><?= lang('ShortTitle'); ?></label>
                                        <input type="text" name="ShortTitle" class="form-control" id="ShortTitle">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <input type="text" name="slug" required class="form-control" id="slug" value="{{title | spaceless | lowercase}}" placeholder="<?php echo lang('slug'); ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <!-- ng-model="title" -->
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
                                       <label class="control-label" for="Image"><?php echo lang('CategoryImage'); ?> :</label>
										 <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">
											 </div>
                                            <div>
                                                <span class="btn btn-rose btn-round btn-file">
                                                    <span class="fileinput-new">Select image</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="Image[]"/>
                                                </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <div class="form-group label-floating">
                                       <!-- <label class="control-label" for="Image"> <?php echo lang('Image'); ?> :</label> -->
										 <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">
											 </div>
                                            <div>
                                                <span class="btn btn-rose btn-round btn-file">
                                                    <span class="fileinput-new">Select image</span>
                                                    <span class="fileinput-exists">Change</span>
                                                    <input type="file" name="AdImage[]"/>
                                                </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                        <small><?= lang('Categoryadfieldinfo'); ?></small>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="AdImageURL">URL :</label>
                                        <input type="text" class="form-control" name="AdImageURL" id="AdImageURL"  value="">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="GoogleAdCode">Google Ad</label>
										<br />
										<br />
										<textarea class="form-control" id="GoogleAdCode" name="GoogleAdCode" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsShow">
                                                <input name="IsShow" value="1" type="checkbox" id="IsShow"/> <?php echo lang('is_show'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/> <?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
							</div>							
							<div class="col-md-5">
								  <h4 class="card-title"><?php echo lang('seo_setting');?></h4>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="seo_title"><?php echo lang('seo_title'); ?></label>
                                        <input type="text" name="seo_title" class="form-control" id="seo_title">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="keywords"><?php echo lang('keywords'); ?></label>
                                        <input type="text" name="keywords" class="form-control" id="keywords">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="meta_desc"><?php echo lang('meta_desc'); ?></label>
                                        <input type="text" name="meta_desc" class="form-control" id="meta_desc">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="crawl_after_day"><?php echo lang('crawl_after_day'); ?></label>
                                        <input type="text" name="crawl_after_day" class="form-control" id="crawl_after_day">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
									  <label class="col-sm-2 margin-top-10 padding-left-0"><?php echo lang('follow'); ?></label>
									<div class="col-sm-10">
										<div class="radio">
                                                    <label>
                                                        <input type="radio" name="follow" value="follow" checked="true"> <?php echo lang('yes'); ?>
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="follow" value="nofollow"> <?php echo lang('no'); ?>
                                                    </label>
                                                </div> 
										</div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                        <label class="col-sm-2 margin-top-10 padding-left-0"><?php echo lang('index'); ?></label>
									<div class="col-sm-10">
										<div class="radio">
                                                    <label>
                                                        <input type="radio" name="index" value="index" checked="true"> <?php echo lang('yes'); ?>
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="index" value="noindex"> <?php echo lang('no'); ?>
                                                    </label>
                                                </div> 
										</div>
                                </div>
                            </div>
								</div>
						    <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="content"><?php echo lang('content'); ?></label>
										<br />
										<br />
										<textarea class="form-control" id="descriptionText" name="content"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'descriptionText' );

    $("#Title").on("change",function(){

        var title = $("#Title").val();
            title = title.toLowerCase();
        var slug = title.split(' ').join('-'); //.replace(" ","-");
        $("#slug").val("");
        $("#slug").val(slug);
    });

</script>