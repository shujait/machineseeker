<?php

$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
		$seo = isset($result[$key]->Seo)?json_decode($result[$key]->Seo):'';
        $common_fields = '';
        $common_fields2 = '';
		 $common_fields3 = '';
		$title = ((isset($result[0]->Title)) ? 'ng-init="title=\''.$result[0]->Title.'\'"' : '');
        if($key == 0){
         
        $common_fields2 = '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsShow">
                                                <input name="IsShow" value="1" type="checkbox" id="IsShow" '.((isset($result[$key]->IsShow) && $result[$key]->IsShow == 1) ? 'checked' : '').'/>'.lang("is_show").'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
		  $common_fields  = '<div class="row">
                                                        <div class="col-md-12">
														 <div class="form-group label-floating">
														<label class="control-label" for="ParentID">'.lang('choose_parent_category').'  <span class="red">*</span></label>
														<select id="ParentID" class="selectpicker" data-style="select-with-transition" required name="ParentID">
															<option value="0">'.lang('parent_category').'</option>';
										
										$arr = getCategories($language2);
										foreach($arr as $rr){
                                            if($rr->CategoryID != $result[0]->CategoryID){
										$common_fields .= '<option value="'.$rr->CategoryID.'" '.($result[0]->ParentID == $rr->CategoryID ? 'selected' : '').'>'.$rr->Title.'</option>';
												} }
													$common_fields .= '</select>
														   </div>
														</div>
														</div>';
				 $common_fields3	  = '<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <input type="text" name="slug" required class="form-control" id="slug" value="'.((isset($result[$key]->Slug)) ? $result[$key]->Slug : '{{title | spaceless | lowercase}}').'" placeholder="'.lang('slug').' " readonly>
                                    </div>
                                </div>
                            </div>									
								';	
        }
        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';


        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                          <div class="col-md-5">
                        <h4 class="card-title">'.lang('add').' '.lang($ControllerName).'</h4>

                                                    '.$common_fields.'
                                                    
														<div class="row" '.$title.'>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('title').'  <span class="red">*</span></label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">                         
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="ShortTitle'.$key.'">'.lang("ShortTitle").'</label>
                                                                <input type="text" name="ShortTitle" class="form-control" id="ShortTitle'.$key.'" value="'.((isset($result[$key]->ShortTitle)) ? $result[$key]->ShortTitle : '').'">
                                                            </div>
                                                        </div>
														</div>
														'.$common_fields3.'
														';
                                                      // '.(($key == 0) ? 'ng-model="title"': '').'
		                                             if($key == 0){
													$lang_data .= '<div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
                                    <label class="control-label" for="Image">'.lang('CategoryImage').' :</label>
										 <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">';
											 
                                        if (!empty($result[$key]->Image) and !empty($result[$key]->Image)) {
                                        $lang_data .= '<img src="'.base_url(((isset($result[$key]->Image)) ? $result[$key]->Image : '')).'" alt="image" class="img-responsive img-thumbnail" width="200" style="width:200px;"/>';
										  }
											$lang_data .= '</div>
                                            <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="Image[]"/>
                                                    </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>'; 
													 }
                                                      
                                                     
                                                     if($key == 0){
                                                        $lang_data .= '<div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="form-group label-floating">
                                             <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                                <div class="fileinput-preview thumbnail">';
                                                 
                                            if (!empty($result[$key]->AdImage) and !empty($result[$key]->AdImage)) {
                                            $lang_data .= '<img src="'.base_url(((isset($result[$key]->AdImage)) ? $result[$key]->AdImage : '')).'" alt="image" class="img-responsive img-thumbnail" width="200" style="width:200px;"/>';
                                              }
                                                $lang_data .= '</div>
                                                <div>
                                                        <span class="btn btn-rose btn-round btn-file">
                                                            <span class="fileinput-new">Select image</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="file" name="AdImage[]"/>
                                                        </span>
                                                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <small>'.lang('Categoryadfieldinfo').'</small>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label" for="AdImageURL">URL :</label>
                                            <input type="text" class="form-control" name="AdImageURL" id="AdImageURL"  value="'.$result->AdImageURL.'">
                                        </div>
                                    </div>
                                </div>'; 
                                                         }

                    $lang_data .= '<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="GoogleAdCode">Google Ad</label>
										<br />
										<br />
										<textarea class="form-control" id="GoogleAdCode" name="GoogleAdCode" rows="5">'.$result->GoogleAdCode.'</textarea>
                                    </div>
                                </div>
                            </div>';


													$lang_data .= '<div class="row">
                                                         '.$common_fields2.'
                                                    </div>
													</div>';
		 				$lang_data .= '<div class="col-md-5">
								  <h4 class="card-title">'.lang('seo_setting').'</h4>
								
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="seo_title">'.lang('seo_title').'</label>
                                        <input type="text" name="seo_title" class="form-control" id="seo_title'.$key.'" value="'.((!empty($seo->seo_title))?$seo->seo_title:'').'">
                                    </div>
                                </div>
                            </div>
								
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="keywords">'.lang('keywords').'</label>
                                        <input type="text" name="keywords" class="form-control" id="keywords'.$key.'" value="'.((!empty($seo->keywords))?$seo->keywords:'').'">
                                    </div>
                                </div>
                            </div>
								
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="meta_desc">'.lang('meta_desc').'</label>
                                        <input type="text" name="meta_desc" class="form-control" id="meta_desc'.$key.'" value="'.((!empty($seo->meta_desc))?$seo->meta_desc:'').'">
                                    </div>
                                </div>
                            </div>
								<div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="crawl_after_day">'.lang('crawl_after_day').'</label>
                                        <input type="text" name="crawl_after_day" class="form-control" id="crawl_after_day'.$key.'" value="'.((!empty($seo->crawl_after_day))?$seo->crawl_after_day:'').'">
                                    </div>
                                </div>
                            </div>
								
								<div class="row">
                                <div class="col-md-12">
									  <label class="col-sm-2 margin-top-10 padding-left-0">'.lang('follow').'</label>
									<div class="col-sm-10">
										<div class="radio">
                                                    <label>
                                                        <input type="radio" name="follow" value="follow" '.getChecked('follow',!empty($seo->follow)?$seo->follow:'follow').'> '.lang('yes').'
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="follow" value="nofollow" '.getChecked('nofollow',!empty($seo->follow)?$seo->follow:'').'> '.lang('no').'
                                                    </label>
                                                </div> 
										</div>
                                </div>
                            </div>
								
										<div class="row">
										<div class="col-md-12">
												<label class="col-sm-2 margin-top-10 padding-left-0">'.lang('index').'</label>
														<div class="col-sm-10">
															<div class="radio">
																		<label>
																			<input type="radio" name="index" value="index" '.getChecked('index',!empty($seo->index)?$seo->index:'index').'>'.lang('yes').'
																		</label>
																		<label>
																			<input type="radio" name="index" value="noindex" '.getChecked('noindex',!empty($seo->index)?$seo->index:'').'>'.lang('no').'
																		</label>
																	</div> 
															</div>
													</div>
												</div>
													</div>

												<div class="row">
													<div class="col-md-12">
														<div class="form-group label-floating is-empty">
															<label class="control-label" for="content">'.lang('content').'</label>
															<br />
															<br />
															<textarea class="form-control" id="descriptionText'.$key.'" name="content">'.((isset($result[$key]->Content)) ? cleanOut( $result[$key]->Content) : '').'</textarea>
														</div>
													</div>

												</div>

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>

                        </div>';
        
        
        
        
        
    }
}


?>



<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h5>
						<h6 class="card-sub-title"><?php echo lang('card_sub_title'); ?></h6>
                    </div>
                    
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                               
                            </div>
                            <div class="col-md-10">
                                    <div class="tab-content">
                                        <?php echo $lang_data; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'descriptionText0' );
    CKEDITOR.replace( 'descriptionText1' );

$("#Title0").on("change",function(){
    var title = $("#Title0").val();
        title = title.toLowerCase();
    var slug = title.split(' ').join('-'); //.replace(" ","-");
    $("#slug").val("");
    $("#slug").val(slug);
});

</script>