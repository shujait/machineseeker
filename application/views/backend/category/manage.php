<style>
    .ShowHideCategory{
        margin-top: 0px !important;
    }
    .btn .material-icons{
        font-size:20px !important;
    }
</style>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
								<ul class="tree-menu sorting">
								<?php
									if($results){
										$n = 0;
                                        foreach($results as $value){
										    $n++;
									
									    $child_categories = getCategories($language,'categories.ParentID = '.$value->CategoryID);
                                ?>
								    <li id='item-<?php echo $value->CategoryID;?>'>
                                        <input type="checkbox" id="menu<?php echo $n; ?>"/>
                                        <label class="<?= (empty($child_categories)?:"eChild"); ?>"   for="menu<?php echo $n; ?>">
                                            <span class="arrow11"></span> 
                                            <?php echo $value->Title; ?>
										    <?php if($child_categories){ ?> <?php } ?>
									    </label>
                                        <?php if(!empty($child_categories)){?>
                                        <span class="badge badge-primary badge-pill">
                                            <?= count($child_categories) ; ?>
                                        </span>
                                        <?php } ?>
									    <span class="pull-right">
                                            <?php if($value->IsShow == 1){?>
                                                <a href="<?= base_url('cms/'.$ControllerName.'/isshow/'.$value->CategoryID.'/0');?>" class="btn btn-simple btn-success btn-icon ShowHideCategory" title="<?=lang('not_is_show')?>"><i class="material-icons">check_circle_outline</i></a>
                                            <?php }else{ ?>
                                                <a href="<?= base_url('cms/'.$ControllerName.'/isshow/'.$value->CategoryID.'/1');?>" class="btn btn-simple btn-secondary btn-icon ShowHideCategory" title="<?=lang('is_show')?>"><i class="material-icons">highlight_off</i></a>
                                            <?php } ?>
                                            <?php if(checkUserRightAccess(50,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                            <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->CategoryID);?>" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('edit') .' '.lang($ControllerName); ?>"  style="padding:0px; margin-top:0px;"><i class="material-icons">edit</i></a>
									        <?php } ?>
                                            <?php if(checkUserRightAccess(50,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                <a href="#" onclick="deleteRecord('<?= $value->CategoryID;?>','cms/<?= $ControllerName; ?>/delete','')" class="btn btn-simple btn-danger btn-icon remove" title="<?php echo lang('delete') .' '.lang($ControllerName); ?>"  style="padding:0px; margin-top:0px;"><i class="material-icons">close</i></a>
                                            <?php } ?>
										</span>
									<?php echo  buildTree($child_categories,$language); ?>
								</li>
									<?php  } } ?>

							</ul>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>