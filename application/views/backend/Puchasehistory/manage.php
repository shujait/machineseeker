<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <div class="toolbar">
                            
                        </div><br>
                        <ul class="nav nav-pills nav-pills-warning">
                            <li class="active">
                                <a href="#pill1" data-toggle="tab" aria-expanded="true"><?php echo lang('bank_label');?></a>
                            </li>
                            <li class="" style="display: none;">
                                <a href="#pill2" data-toggle="tab" aria-expanded="false"><?php echo lang('paypal'); ?> </a>
                            </li>
                            
                        </ul>
                        <div class="tab-content">
                            <div class="material-datatables tab-pane active table-responsive" id="pill1">
                                <table id="purchaseTable" class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('invoice');?></th>
                                    
                                         <th><?php echo ucfirst(strtolower(lang('user').lang('name')));?></th>

                                    <th><?php echo lang('package');?></th>
                                    
                                    <th><?php echo lang('duration');?></th>
                                    
                                    <th><?php echo lang('expiry_date');?></th>
                                    
                                    <th><?php echo lang('payment-status');?></th>


                                    <?php if(checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){
                                    if($value->Status == 4 || $value->Status == 5){


                                    $package = getPackage($value->PackageID);
                                    $user_info = get_user_info(array('a.UserID' => $value->UserID));    
                                    ?>
                                        <tr id="<?php echo $value->PurchaseID;?>">
                                            
                                            <td><?php echo $value->PurchaseID; ?></td>
                                            <?php 
                                            if($this->session->userdata['admin']['RoleID'] == 1)
                                            {
                                            ?>
                                                <td><a href="<?= base_url('cms/user/view/'.base64_encode($value->UserID));?>"><?php echo $user_info->Title; ?></a></td>
                                            <?php
                                            }else
                                            {
                                            ?>
                                                <td><?php echo $user_info->Title; ?></td>
                                            <?php 
                                            }
                                            ?>
                                            <td>
                                                <?php
                                                 if($package){
                                                echo $package->Title; 
                                                 }
                                                ?>
                                            </td>
                                            
                                            <td><?php echo $value->Duration .' '.lang('month'); ?></td>
                                            
                                            <td><?php echo dateformat($value->ExpiryDate,'de'); ?></td>
                                            
                                            <td id="status<?php echo $value->PurchaseID;?>"><?php echo payment_status($value->Status); ?></td>


                                             <?php if(checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanEdit') ){?> 
                                            <td>
                                                <?php if(checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="javascript:void(0);" onclick="deleteRecord('<?php echo $value->PurchaseID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                                <?php if(checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                    <a href="javascript:void(0);" id="anchar<?php echo $value->PurchaseID; ?>" data-status = "<?php echo $value->Status; ?>" data-id="<?php echo $value->PurchaseID; ?>" class="btn btn-simple btn-danger btn-icon approve"><?php echo ($value->Status == 4 ? lang('click_to_approved') : lang('click_to_reset'));?><div class="ripple-container"></div></a>
                                                <?php } ?>
                                                <?php //if($value->IsVerified == 1){
                                                    if(!empty($value->filePath)){   
                                                           $path=$value->filePath; 
                                                    }else
                                                    {
                                                       $path='R-'.$value->PurchaseID.'.pdf'; 
                                                    }
                                                    $path = rtrim($path, '.pdf');
                                                    ?>
                                                    <a href="<?php echo base_url('invoice_pdf/'.$path);?>"  target="_blank"  class="btn btn-simple btn-danger btn-icon"><?php echo lang('download-invoice');?><div class="ripple-container"></div></a>
                                                <?php //} ?>

                                                
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                }
                                ?>

                                </tbody>
                            </table>
                            </div>
                            <div class="material-datatables tab-pane" id="pill2">
                                <table id="" class="table datatables table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                <tr>

                                    <th><?php echo lang('invoice');?></th>
                                    
                                     <th><?php echo ucfirst(strtolower(lang('user').lang('name')));?></th>

                                    <th><?php echo lang('package');?></th>
                                    
                                    <th><?php echo lang('duration');?></th>
                                    
                                    <th><?php echo lang('expiry_date');?></th>
                                    
                                    <th><?php echo lang('payment-status');?></th>

                                    <?php if(checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanDelete')){?>  
                                            <th><?php echo lang('actions');?></th>
                                     <?php } ?>

                                </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                        
                                    foreach($results as $value){
                                    if($value->Status != 4 && $value->Status != 5){

                                    $package = getPackage($value->PackageID);
                                    $user_info = get_user_info(array('a.UserID' => $value->UserID));    
                                    ?>
                                        <tr id="<?php echo $value->PurchaseID;?>">
                                            
                                            <td><?php echo $value->PurchaseID; ?></td>
                                            
                                            <td><a href="<?= base_url('cms/user/edit/'.$value->UserID);?>" ><?php echo $user_info->Title; ?></a></td>

                                            <td>
                                                <?php
                                                 if($package){
                                                echo $package->Title; 
                                                 }
                                                ?>
                                            </td>
                                            
                                            <td><?php echo $value->Duration .' '.lang('month'); ?></td>
                                            
                                            <td><?php echo dateformat($value->ExpiryDate,'de'); ?></td>
                                            
                                            <td><?php echo payment_status($value->Status); ?></td>


                                             <?php if(checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanDelete')){?> 
                                            <td>
                                                <?php if(checkUserRightAccess(57,$this->session->userdata['admin']['UserID'],'CanDelete')){?>
                                                    <a href="#" onclick="deleteRecord('<?php echo $value->PurchaseID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove"><i class="material-icons" title="Delete">close</i><div class="ripple-container"></div></a>
                                                <?php } ?>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                }
                                ?>

                                </tbody>
                            </table>
                            </div>
                            
                            
                        </div><!-- tab-content -->
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script>
    $(document).ready(function () {
        $('table.datatables').DataTable({
            "ordering": false
        });

        var update_msg = '<?php echo lang('are_you_sure_you_want_to_update_status');?>'

        //$(".approve").on('click',function(){
        $("#purchaseTable").on("click", ".approve", function(){
            var id = $(this).attr('data-id');
            var status = $(this).attr('data-status');
            if (confirm(update_msg)) {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });

                $.ajax({
                    type: "POST",
                    url: base_url + 'cms/puchasehistory/update_status',
                    data: {
                        'id': id,
                        'status': status
                    },
                    dataType: "json",
                    cache: false,
                    //async:false,
                    success: function (result) {
                        if(status == 4){
                            var update_status = '<?php echo payment_status(5); ?>';
                            $('#anchar' + id).attr('data-status',5);
                            $('#anchar' + id).html('<?php echo lang('click_to_reset'); ?>');
                        }else if(status == 5){
                            var update_status = '<?php echo payment_status(4); ?>';
                             $('#anchar' + id).attr('data-status',4);
                              $('#anchar' + id).html('<?php echo lang('click_to_approved'); ?>');
                        }
                        if (result.error != false) {
                            showError(result.error);
                        } else {
                            showSuccess(result.success);
                            $('#status' + id).html(update_status);
                            
                        }


                       

                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
                return true;
            } else {
                return false;
            }
        });
    });
</script>
<script src="<?php echo base_url(); ?>assets/backend/js/datatable.js"></script>
