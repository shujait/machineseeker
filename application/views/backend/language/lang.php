<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('edit').' '.$langText['SystemLanguageTitle'].' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <div class="col-md-12" style="overflow-y: scroll; height: 500px;">
                            <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                                <input type="hidden" name="form_type" value="langupdate">
                                 <input type="hidden" name="SystemLanguageID" value="<?php echo $SystemLanguageID;?>">
                                 <input type="hidden" name="ShortCode" value="<?php echo $langText['ShortCode'];?>">
                                <?php if($this->session->userdata['admin']['RoleID'] == 2 && $langText['IsDefault'] == 1){ ?>
                                    <div class="row m-b-20 text-right">
                                        <div class="col-md-12">
                                            <label>&nbsp;</label>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#addNewLang" class="btn btn-success"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                        </div>
                                    </div>
                                <?php }?>
                                <?php 
                                $i=0;
    							if($languageTexts){
                                foreach($languageTexts as $key=>$value){ ?>
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?php //if($i == 0){ ?>
                                                    <label for="LanguageIndex<?php echo $i;?>"><?php echo 'Index'; ?><span class="text-danger">*</span></label>
                                                <?php // } ?>
                                                <input type="text" parsley-trigger="change" required  class="form-control" id="LanguageIndex<?php echo $i;?>" disabled value="<?php echo $key;?>">
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <?php //if($i == 0){ ?>
                                                    <label for="LanguageValue<?php echo $i;?>"><?php echo 'Value'; ?><span class="text-danger">*</span></label>
                                                <?php //}   ?>
                                                <!-- <textarea name="LanguageValue[<?php //echo $key; ?>]" parsley-trigger="change" required  class="form-control" id="LanguageValue<?php //echo $i;?>" <?php //echo ($langText['Direction'] == 'RTL' ? 'style="text-align: right;"' : ''); ?>><?php //echo $value;?></textarea> -->
                                                <input type="text" name="LanguageValue[<?php echo $key; ?>]" parsley-trigger="change" required  class="form-control" id="LanguageValue<?php echo $i;?>" value="<?php echo $value;?>" <?php echo ($langText['Direction'] == 'RTL' ? 'style="text-align: right;"' : ''); ?>>
                                            </div>
                                        </div>
                                        
                                    </div>
                                <?php 
                                    $i++;
                                  }
    							}
    							?>
                                
                                <?php if($this->session->userdata['admin']['RoleID'] == 2 && $langText['IsDefault'] == 1){ ?>
                                    <div class="row m-b-20 text-right">
                                        <div class="col-md-12">
                                            <label>&nbsp;</label>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#addNewLang" class="btn btn-success"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                        </div>
                                    </div>
                                <?php }?>


                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        <?php echo lang('submit');?>
                                    </button>
                                    <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                            <?php echo lang('back');?>
                                        </button>
                                    </a>
                                </div>

                            </form>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<?php if($this->session->userdata['admin']['RoleID'] == 2 && $langText['IsDefault'] == 1){ ?>        
    <!-- modal -->
    <div id="addNewLang" class="modal fade fade-scale" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_new_lang_translation'); ?>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></h4>

                </div>
                <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                    <input type="hidden" name="form_type" value="langindexadd">
                    <input type="hidden" name="SystemLanguageID" value="<?php echo $SystemLanguageID;?>">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label class="control-label" for="LanguageIndex"><?php echo 'Index'; ?><span class="text-danger">*</span></label>
                                    <input type="text" parsley-trigger="change" required name="LanguageIndex"  class="form-control langIndex" id="LanguageIndex" value="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">                            
                                    <label class="control-label" for="LanguageValue"><?php echo 'Value'; ?><span class="text-danger">*</span></label>
                                    <input type="text" name="LanguageValue" parsley-trigger="change" required  class="form-control" id="LanguageValue" value="" <?php echo ($langText['Direction'] == 'RTL' ? 'style="text-align: right;"' : ''); ?>>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary waves-effect waves-light" type="submit"><?php echo lang('submit');?></button>
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal"><?php echo lang('cancel'); ?></button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php 
}
?>

<script type="text/javascript">
    $('.langIndex').keypress(function(event) {
        //alert(event.which);
        if ((event.which < 97 || event.which > 122 || event.which == 32) && (event.which != 95)) {
            event.preventDefault();
        }
    });
           
</script>