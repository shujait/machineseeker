<?php
$option = '';

$languages = getSystemLanguages();
//echo $this->db->last_query();exit;
//print_rm($languages);
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
       
       
        if($key == 0){
        $common_fields = '<div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="ShortCode" class="control-label">'.lang('short_code').'<span class="red">*</span></label>
                                        <input type="text" parsley-trigger="change" name="ShortCode" required  class="form-control" id="ShortCode" value="'.$result[0]['ShortCode'].'" readonly>
                                    </div>
                                </div>';   
        $common_fields2 = '<div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="IconClass" class="control-label">'.lang('icon_class').'</label>
                                        <input type="text" name="IconClass" parsley-trigger="change" required  class="form-control" id="IconClass" value="'.$result[0]['IconClass'].'">
                                    </div>
                                </div>

                                <div class="col-md-6 checkbox-radios">
                                    <div class="form-group">
                                        <label class="control-label">'.lang('text_direction').'</label>

                                        <div class="radio">
                                            <label for="DirectionLTR">
                                                <input type="radio" name="Direction" id="DirectionLTR" value="ltr" data-parsley-multiple="Direction" '.($result[0]['Direction'] == 'ltr' ? 'checked' : '').'>
                                                '.lang('left_to_right').'
                                            </label>
                                        </div>
                                        <div class="radio" style="margin-top: 10px;">
                                            <label class="control-label" for="DirectionRTL"> 
                                                <input type="radio" name="Direction" id="DirectionRTL" value="rtl" data-parsley-multiple="Direction" '.($result[0]['Direction'] == 'rtl' ? 'checked' : '').'>
                                                '.lang('right_to_left').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                 <div class="col-md-6 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label  for="Hide">
                                                <input name="Hide" value="1" type="checkbox" id="Hide" '.($result[0]['Hide'] == 1 ? 'checked' : '').'/> '.lang('hide').'
                                            </label>
                                        </div>
                                    </div>
                                </div></div>';                         

                                 
       
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="LanguageID" value="'.base64_encode($result[0]['LanguageID']).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                     
                                                    <div class="row">
                                                   
                                                        <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="SystemLanguageTitle'.$key.'">'.lang('title').' <span class="red">*</span></label>
                                                                <input type="text" name="SystemLanguageTitle" parsley-trigger="change" required  class="form-control" id="SystemLanguageTitle'.$key.'" value="'.((isset($result[$key]['SystemLanguageTitle'])) ? $result[$key]['SystemLanguageTitle'] : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        '.$common_fields.'
                                                       
                                                    </div>
                                                       '.$common_fields2.' 
                                                    ';

                                                    if($key == 0){
                                                    $lang_data .= '<div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="form-group label-floating">
                                         <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">';
                                             
                                          
                                        $lang_data .= '<img src="'.base_url(((isset($result[$key]['Logo'])) ? $result[$key]['Logo'] : '')).'" alt="image" class="img-responsive img-thumbnail" width="200" style="width:200px;"/>';
                                           
                                            $lang_data .= '</div>
                                            <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="logo[]"/>
                                                    </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>'; 
                                                     }
                                                   
                                                    
                                                    

                                                  $lang_data .= '<div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                   <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                         <h5 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h5>

                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
