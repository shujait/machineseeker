<?php

$languages = getSystemLanguages();

?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="SystemLanguageTitle"><?php echo lang('title'); ?> <span class="red"> * </span></label>
                                        <input type="text" name="SystemLanguageTitle" parsley-trigger="change" required  class="form-control" id="SystemLanguageTitle">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="ShortCode"><?php echo lang('short_code'); ?> <span class="red"> * </span></label>
                                        <input type="text" name="ShortCode" parsley-trigger="change" required  class="form-control" id="ShortCode">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="IconClass"><?php echo lang('icon_class'); ?></label>
                                        <input type="text" name="IconClass" parsley-trigger="change" required  class="form-control" id="IconClass">
                                    </div>
                                </div>
                                
                                <div class="col-md-6 checkbox-radios">
                                    <div class="form-group">
                                        <label><?php echo lang('text_direction'); ?></label>

                                        <div class="radio">
                                            <label for="DirectionLTR">
                                                <input type="radio" name="Direction" id="DirectionLTR" value="ltr" data-parsley-multiple="Direction" checked="checked">
                                                <?php echo lang('left_to_right');?>
                                            </label>
                                        </div>
                                        <div class="radio" style="margin-top: 10px;">
                                            <label for="DirectionRTL"> 
                                                <input type="radio" name="Direction" id="DirectionRTL" value="rtl" data-parsley-multiple="Direction">
                                                <?php echo lang('right_to_left');?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
								
								 <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Image"><?php echo lang('SiteLogo'); ?> :</label>
										<br />
										 <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">
											 </div>
                                            <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new"><?php echo lang('SiteLogo'); ?></span>
                                                        <span class="fileinput-exists"><?php echo lang('change'); ?></span>
                                                        <input type="file" name="logo[]"  accept="image/jpeg,image/png"/>
                                                    </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> <?php echo lang('delete'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
<!-- 
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label class="control-label" for="IsDefault">
                                                <input name="IsDefault" value="1" type="checkbox" id="IsDefault" checked/> <?php //echo lang('is_default'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="col-sm-6 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="Hide">
                                                <input name="Hide" value="1" type="checkbox" id="Hide"/> <?php echo lang('hide'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>



                            </div>


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
