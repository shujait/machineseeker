<?php
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        $common_fields4 = '';
        if ($key == 0) {
            $common_fields = '
			  <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TplList">'.lang('email_tpl_list').' <span class="red"> * </span></label>
								<select id="TplList" class="selectpicker" data-style="select-with-transition" required name="TplList">';
									$arr = TplList();
									if(!empty($arr)){ 
									foreach($arr as $k => $va){ 
								    $common_fields .= '<option value="'.$k.'" '.getSelected($k,$result[$key]->TplList).'>'.$va.' </option>';
									  }
									} 
								 $common_fields .= '</select>                                     
									  </div>
                                </div>';
		$common_fields2 .='<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_active').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
         $common_fields3 .= '<div class="row tp_files" '.($result[$key]->TplList != 15 ? 'style="display: none;' : '').'">';
            $product_images = getImage(array('FileID' => $result[$key]->Email_templateID,'ImageType' => 'EmailFile'), false);
           // print_rm($product_images);
            if ($product_images) {
                foreach ($product_images as $product_image) {
                    $common_fields3 .= '<div class="col-md-2 col-sm-2 col-xs-2"><i class="fa fa-trash delete_image" data-image-id="'.$product_image->SiteImageID.'" aria-hidden="true"></i><a href="'.base_url($product_image->ImageName).'" target="_blank"><img src="' . base_url('assets/frontend/pdf.png'). '" style="height:100px;width:100px;"></a></div>';
                }
            }
        $common_fields3 .= '<div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Attach Files </label>
                                    <div class="form-group">
                                        <input type="file" name="Image[]" multiple="multiple" style="opacity: 1;    position: relative;">
                                        
                                    </div>
                                </div>
                            </div>';
        $common_fields4 = '<div class="row tp_notification_days" '.($result[$key]->TplList != 16 ? 'style="display: none;' : '').'">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="NotificationBeforeDays">'.lang('send_mail_before_days').' <span class="red"> * </span></label>
                                        <input type="text" value="' . ((isset($result[$key]->NotificationBeforeDays)) ? $result[$key]->NotificationBeforeDays : '') . '" name="NotificationBeforeDays" parsley-trigger="change" required  class="form-control" id="NotificationBeforeDays">
                                    </div>
                                </div>
                            </div>';                                            
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                      <form action="' . base_url() . 'cms/' . $ControllerName . '/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="' . base64_encode($language->SystemLanguageID) . '">
                                                    <input type="hidden" name="' . $TableKey . '" value="' . base64_encode($result[0]->$TableKey) . '">
                                                    <input type="hidden" name="IsDefault" value="' . $language->IsDefault . '">

                                                    
                                                    <div class="row">
													   ' . $common_fields . '
                                                        <div class="col-md-4">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Subject' . $key . '">' . lang('subject') . ' <span class="red"> * </span></label>
                                                                <input type="text" name="Subject" parsley-trigger="change" required  class="form-control" id="Subject' . $key . '" value="' . ((isset($result[$key]->Subject)) ? $result[$key]->Subject : '') . '">
                                                                <input type="hidden" name="OldTitle" value="' . ((isset($result[$key]->Subject)) ? $result[$key]->Subject : '') . '">
                                                            </div>
                                                        </div>
                                                     
                                                        
                                                        
                                                    </div>
                                                    '. $common_fields2 .'
                                                     '.$common_fields4.'
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Description">'. lang('Description') .' <span class="red"> * </span></label><br>
                                                                <textarea class="form-control TinymceEditor" id="Description" name="Description" value="">' . ((isset($result[$key]->Description)) ? $result[$key]->Description : '') . '</textarea>
															 <input type="file" id="upload" class="hidden" onchange="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    '.$common_fields3.'

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            ' . lang('submit') . '
                                                        </button>
                                                        <a href="' . base_url() . 'cms/' . $ControllerName . '">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         ' . lang('back') . '
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
    }
}
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
					  <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
						 <h5 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h5>

                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $("#TplList").on('change',function(){
            
            if($("#TplList").val() == 15){
                $(".tp_files").show();
            }else{
                 $(".tp_files").hide();
            }
            if($("#TplList").val() == 16){
                $(".tp_notification_days").show();
            }else{
                 $(".tp_notification_days").hide();
            }
        });



        $(".delete_image").on('click',function(){
            var id=$(this).attr('data-image-id');

            url = "cms/email_template/deleteImage2";
            reload = "<?php echo base_url();?>cms/email_template/edit/<?php echo $result[0]->Email_templateID; ?>";
            deleteRecord(id,url,reload);

        });
    });

</script>