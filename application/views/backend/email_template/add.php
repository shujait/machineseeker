<?php
$languages = getSystemLanguages();
?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('add').' '.lang($ControllerName);?></h4>
                        <div class="toolbar">
                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                        </div>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
                            <input type="hidden" name="form_type" value="save">


                            <div class="row">
								  <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="TplList"><?php echo lang('email_tpl_list'); ?> <span class="red"> * </span></label>
								<select id="TplList" class="selectpicker" data-style="select-with-transition" required name="TplList">
									<?php 
									$arr = TplList();
									if(!empty($arr)){ 
											foreach($arr as $key => $va){ ?>
												<option value="<?php echo $key; ?>"><?php echo $va; ?> </option>
									<?php } } ?>
								</select>                                     
									  </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Subject"><?php echo lang('subject'); ?> <span class="red"> * </span></label>
                                        <input type="text" name="Subject" parsley-trigger="change" required  class="form-control" id="Subject">
                                    </div>
                                </div>
								<div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" checked/><?php echo lang('is_active'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="Description"><?php echo lang('Description'); ?>  <span class="red"> * </span></label><br><br>
                                        <textarea class="form-control TinymceEditor" id="Description" name="Description" required></textarea>
										 <input type="file" id="upload" class="hidden" onchange="">
                                    </div>
                                </div>
                            </div>
                            <div class="row tp_notification_days" style="display: none;">
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label" for="NotificationBeforeDays"><?php echo lang('send_mail_before_days'); ?> <span class="red"> * </span></label>
                                        <input type="text" name="NotificationBeforeDays" parsley-trigger="change" required  class="form-control" id="NotificationBeforeDays">
                                    </div>
                                </div>
                            </div>
                            <div class="row tp_files" style="display: none;">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Attach Files </label>
                                    <div class="form-group">
                                        <input type="file" name="Image[]" multiple="multiple" style="opacity: 1;    position: relative;">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit'); ?>
                                </button>
                                <a href="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back'); ?>
                                    </button>
                                </a>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#TplList").on('change',function(){
            
            if($("#TplList").val() == 15){
                $(".tp_files").show();
            }else{
                 $(".tp_files").hide();
            }
            if($("#TplList").val() == 16){
                $(".tp_notification_days").show();
            }else{
                 $(".tp_notification_days").hide();
            }
        });
    });

</script>