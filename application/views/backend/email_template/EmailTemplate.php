<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- If you delete this tag, the sky will fall on your head -->
        <meta name="viewport" content="width=device-width" />

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo $site_setting->SiteName; ?></title>
    </head>
    <body bgcolor="#FFFFFF">
		<div  style="width: 50%;margin: 0 auto;">
                <?php
                $Description = '';
                $DefaultLang = getDefaultLanguage();
                if ($DefaultLang) {
                    $lang_id = $DefaultLang->SystemLanguageID;
                } else {
                    $lang_id = '1';
                }
                if ($result) {
                    foreach ($result as $value) {
                        if ($value->SystemLanguageID == $lang_id) {
                            $Description = $value->Description;
                        }
                    }
                }
                echo $Description; ?>
			</div>
                
    </body>
</html>