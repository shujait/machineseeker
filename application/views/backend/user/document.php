<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('upload_document'); ?></h4>
                        <form action="<?php echo base_url(); ?>cms/<?php echo $ControllerName; ?>/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate id="change_password" >
                            <input type="hidden" name="form_type" value="document">

                            <div class="card-content">
                            <div class="row">
                                <div class="col-md-6 col-xs-6" style="display: <?php echo !empty($document['businessRegistration'])?'none':'';?>">
                                    <div class="form-group label-floating">
                                        <label><?php echo lang('business_registration'); ?></label> <span class="red">*</span><small style="margin-left: 120px; font-weight: bold;"><?php echo lang('upload_image_or_pdf_or_doc');?></small><br>
                                        <input type="file" name="businessRegistration" id="filer_input_single" required>
                                     </div>
                                </div>
                                <div class="col-md-6 col-xs-6" style="display: <?php echo !empty($document['taxCertificate'])?'none':''; ?>">
                                    <div class="form-group label-floating">
                                        <label><?php echo lang('tax_certificate'); ?></label> <span class="red">*</span><small style="margin-left: 120px; font-weight: bold;"><?php echo lang('upload_image_or_pdf_or_doc');?></small><br>
                                        <input type="file" name="taxCertificate"  id="filer_input_single1" required>
                                    </div>
                                </div>
                            </div>

                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        <?php echo lang('submit'); ?>
                                    </button>
                                    <a href="<?php echo base_url(); ?>cms/<?php 
                                   
                                    echo $ControllerName; ?>">
                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                            <?php echo lang('back'); ?>
                                        </button>
                                    </a>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>