
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">assignment</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang($ControllerName.'s'); ?></h4>
                        <div class="toolbar">
                            <a href="<?php echo base_url('cms/'.$ControllerName.'/add');?>">
                                <button type="button" class="btn btn-primary waves-effect w-md waves-light m-b-5"><?php echo lang('add_'.$ControllerName); ?></button>
                            </a>
                        </div>
                        <div class="material-datatables">
                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                <thead>
                                    <tr>
                                        <th><?php echo lang('title');?></th>
                                        <th><?php echo lang('email');?></th>
                                        <th><?php echo lang('vat');?></th>
                                        <th><?php echo lang('is_company');?></th>
                                        <th><?php echo lang('role');?></th>
                                        <th><?php echo lang('is_active');?></th>
                                        <th><?php echo lang('created_on');?></th>
                                        <th><?php echo lang('LastLogin');?></th>
                                        <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                            <th><?php echo lang('actions');?></th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if($results){
                                    foreach($results as $value){ 
									$role = UserRole(array('b.RoleID' => $value->RoleID),'row_array');
									?>
                                        <tr id="<?php echo $value->UserID;?>">
                                            <td><?php echo $value->Title; ?></td>
                                            <td><?php echo $value->Email; ?></td>
											<td><?php echo $value->Vat; ?></td>
                                            <td><?php echo ($value->IsCompany == 1 ? lang('yes') : lang('no')); ?></td>
											<td><?php echo ($role)?$role['Title']:''; ?></td>
                                            <td><?php echo ($value->IsActive ? lang('yes') : lang('no')); ?></td>
                                            <td data-sort="<?php echo $value->CreatedDate;?>"><?php echo dateformat($value->CreatedDate,'de'); ?></td>
                                            <td data-sort="<?php echo (!empty($value->LastLogin) && $value->LastLogin!="0000-00-00" ? $value->LastLogin : ''); ?>"><?php echo (!empty($value->LastLogin) && $value->LastLogin!="0000-00-00" ? dateformat($value->LastLogin,'de') : lang("NotLoggedIn")); ?></td>
                                            <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete') || checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){?>
                                                <td>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/edit/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('edit') .' '.lang($ControllerName); ?>"><i class="material-icons">edit</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanDelete')){ ?>
                                                        <a href="#" onclick="deleteRecord('<?php echo $value->UserID;?>','cms/<?php echo $ControllerName; ?>/action','')" class="btn btn-simple btn-danger btn-icon remove" title="<?php echo lang('delete') .' '.lang($ControllerName); ?>"><i class="material-icons">close</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
													 <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanView')){ ?>
                                                        <a href="<?php echo base_url('cms/'.$ControllerName.'/view/'.base64_encode($value->UserID));?>" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('view') .' '.lang($ControllerName); ?>"><i class="material-icons">visibility</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                    <?php /* if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                        <a href="<?php echo base_url('cms/user/rights/'.$value->UserID);?>" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('edit') .' '.lang('user_rights'); ?>"><i class="material-icons">vpn_key</i><div class="ripple-container"></div></a>
                                                    <?php } */?>
                                                    <?php if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit')){?>
                                                        <a href="#" onclick="send_information_email()" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('send_information_email'); ?>"><i class="material-icons send_information_email" data-UserID="<?php echo $value->UserID;?>" data-msg="<?php echo lang('are_you_sure_you_want_to_send_email');?>">email</i><div class="ripple-container"></div></a>
                                                    <?php } ?>
                                                     <?php 
                                                    
                                                     if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') && !empty($value->document['businessRegistration']->path) ){?>
                                                        <a href="<?php echo base_url($value->document['businessRegistration']->path);?>" target="_blank" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('business_registration'); ?>"><i class="  fa fa-download " data-UserID="<?php echo $value->UserID;?>"> </i><div class="ripple-container"></div></a>
                                                    <?php } ?>    
                                                     <?php 
                                                     if(checkUserRightAccess(23,$this->session->userdata['admin']['UserID'],'CanEdit') && !empty($value->document['taxCertificate']->path) ){?>
                                                        <a href="<?php echo base_url($value->document['taxCertificate']->path);?>" target="_blank" class="btn btn-simple btn-warning btn-icon edit" title="<?php echo lang('tax_certificate'); ?>"><i class="  fa fa-download " data-UserID="<?php echo $value->UserID;?>"> </i><div class="ripple-container"></div></a>
                                                    <?php } ?>    
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <?php
                                    }

                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>
<script src="<?php echo base_url();?>assets/backend/js/datatable.js"></script>
<script type="text/javascript">
   // $( document ).ready(function() {
    function send_information_email(){
       // $('.send_information_email').on('click',function(){

            var UserID = $(".send_information_email").data('UserID'); 
            var msg    = $(".send_information_email").data('msg');


            if (confirm(msg)) {
                $.blockUI({
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });

        $.ajax({
            type: "POST",
            url: base_url + 'cms/user/action',
            data: {
                'UserID': UserID,
                'form_type': 'send_information_email'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                 $.unblockUI();
                if (!result.error) {
                    alert('<?php echo lang('email_sent');?>');

                } 


            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

      //  });

        
    }
   // });

</script>