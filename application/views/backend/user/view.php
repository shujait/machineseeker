<?php
$option = '';
if (!empty($roles)) {
    foreach ($roles as $role) {
        $option .= '<option value="' . $role->RoleID . '" ' . ((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '') . '>' . $role->Title . ' </option>';
    }
}
$option2 = '';
if (!empty($activities)) {
    foreach ($activities as $activity) {
        $option2 .= '<option value="' . $activity->ActivityID . '" ' . ((isset($result[0]->ActivityID) && $result[0]->ActivityID == $activity->ActivityID) ? 'selected' : '') . '>' . $activity->Title . ' </option>';
    }
}
$languages = getSystemLanguages();
  
											 
											 
											 
$lang_tabs = '';
$lang_data = '';
if (!empty($languages)) {
    foreach ($languages as $key => $language) {
     $arr = CountryList();
     foreach($arr as $val){
         if($val->CountryID == $result[$key]->Country) 
         $countryName=$val->Title;
        }
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
       
        if ($key == 0) {
            $common_fields = '<div class="row"><div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="RoleID' . $key . '">' . lang('choose_user_role') . '</label>
                                                                <select id="RoleID' . $key . '" class="form-control" required="" disabled>
                                                                    ' . $option . '
                                                                </select>
                                                            </div>
                                                        </div><div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Email">' . lang('email') . '<span class="text-danger"></span></label>
                                                                <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="Email" value="' . ((isset($result[$key]->Email)) ? $result[$key]->Email : '') . '">
                                                            </div>
                                                        </div>
                                </div>';
            $common_fields2 = '<div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company' . $key . '">' . lang('company_name') . '</label>
                                                <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="company' . $key . '" value="' . ((isset($result[$key]->CompanyName)) ? $result[$key]->CompanyName : '') . '">

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="website">' . lang('website') . '<span class="text-danger"></span></label>
                                                https://<input type="text" disabled  parsley-trigger="change" required  class="form-control" id="website" value="' . ((isset($result[$key]->Website)) ? $result[$key]->Website : '') . '">
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="PhoneNumber' . $key . '">' . lang('ph') . '</label>
                                                    <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="PhoneNumber' . $key . '" value="' . ((isset($result[$key]->Phone)) ? $result[$key]->Phone : '') . '">
                                                </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="website">' . lang('mob') . '<span class="text-danger"></span></label>
                                                <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="website" value="' . ((isset($result[$key]->Mobile)) ? $result[$key]->Mobile : '') . '">
                                            </div>
                                        </div>
                                    </div> 
                                <div class="row">
                                        <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="country' . $key . '">' . lang('country') . '</label>
                                                    <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="country' . $key . '" value="' . ((isset($countryName)) ? $countryName : '') . '">
                                                </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="StreetAddress">' . lang('street_address') . '<span class="text-danger"></span></label>
                                                <input type="text" disabled  parsley-trigger="change" required  class="form-control" id="StreetAddress' . $key . '" value="' . ((isset($result[$key]->StreetAddress)) ? $result[$key]->StreetAddress : '') . '">
                                            </div>
                                        </div>
                                </div>';
        $common_fields3 = ' <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title' . $key . '">' . lang('vat') . '<span class="text-danger"></span></label>
                                                                <input type="text" disabled parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Vat)) ? $result[$key]->Vat : '') . '">
                                                            </div>
                                                        </div>';
        }

        $lang_tabs .= '<li class="' . ($key == 0 ? 'active' : '') . '">
                                        <a href="#' . $language->SystemLanguageTitle . '" data-toggle="tab">
                                            ' . $language->SystemLanguageTitle . '
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane ' . ($key == 0 ? 'active' : '') . '" id="' . $language->SystemLanguageTitle . '">
                                                     ' . $common_fields . '
                                                      <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="Title' . $key . '">' . lang('name') . '<span class="text-danger"></span></label>
                                                                <input type="text" disabled parsley-trigger="change" required  class="form-control" id="Title' . $key . '" value="' . ((isset($result[$key]->Title)) ? $result[$key]->Title : '') . '">
                                                            </div>
                                                        </div>
                                                      ' . $common_fields3 . '  
                                                    </div>
                                                     ' . $common_fields2 . '
                        </div>';
    }
}
?>


<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>

                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('view') . ' ' . lang($ControllerName); ?></h5>
                        <h6 class="card-sub-title"><?php echo lang('card_sub_title'); ?></h6>

                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
<?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
