<link href="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/css/intlTelInput.min.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/Intl-tel/js/intlTelInput-jquery.min.js"></script>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title"><?php echo lang('edit').' '.lang('my-profile');?></h4>
                        <form action="<?php echo base_url();?>cms/<?php echo $ControllerName; ?>/profile" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate>
							<div class="col-md-2">
					     	<div class="row">
								<div class="col-md-1">
									<div class="form-group label-floating">
										 <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail">
                                    <?php if (!empty($result[0]->CoverImage)) { ?>
                                        <img src="<?php echo base_url($result[0]->CoverImage); ?>" alt="image" class="img-responsive img-thumbnail" width="200" style="height:150px;"/>
                                    <?php }else{
                                    	?>
                                    	<img src="<?php echo base_url(); ?>uploads/user_dummy.jpg" alt="image" class="img-responsive img-thumbnail" width="200" style="height:150px;"/>
                                   	<?php
                                    } ?>
											 </div>
                                           <div><?php echo lang('size'); ?>: 150px x 150px (jpeg,png) </div> <div>
                                                    <span class="btn btn-rose btn-round btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="Image[]"/>
                                                    </span>
                                                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                                            </div>
                                        </div>
									</div>
								</div>
							</div>
							</div>
							<div class="col-md-10">
							<!-- <div class="row">
								<div class="col-md-2">
									<input type="text" class="form-control" readonly value="K-<?php echo str_pad($result[0]->UserID, 7, '0', STR_PAD_LEFT); ?>">
								</div>
							</div> -->
							<div class="row">
								<div class="col-md-1">
									<div class="form-group label-floating">
										<label class="control-label" for="uname"><?php echo lang('title'); ?> <span class="red">*</span></label>
										<select name="uname" id="uname" required class="form-control" >
											<?php $arr = getGenderTitle();
											foreach($arr as $val){
											?>
											<option value="<?php echo $val; ?>" <?php echo getSelected($val,$result[0]->UName); ?>><?php echo $val; ?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group label-floating">
										<label class="control-label" for="title"><?php echo lang('name'); ?><span class="red">*</span></label>
										<input type="text" name="title" class="form-control" required id="title" value="<?php echo $result[0]->Title; ?>">
									</div>
								</div>
								 <div class="col-md-4">
									 	<div class="form-group label-floating">
								<label class="control-label" for="Email"><?php echo lang('email'); ?></label>
								<input type="email" id="Email"  class="form-control" value="<?php echo $result[0]->Email; ?>" autocomplete="off" readonly>
											</div>
								  </div>
								<div class="col-md-4">
									<div class="form-group label-floating">
							 <label  class="control-label" for="companyName"><?php echo lang('company_name'); ?> <span class="red">*</span></label>
                             <input type="text" name="companyName" required id="companyName" value="<?php echo $result[0]->CompanyName; ?>" class="form-control" autocomplete="off">
									</div>
								</div>
								</div>
							<div class="row">
								
								<div class="col-md-4">
									<div class="form-group label-floating">
								   <label class="control-label" for="website"><?php echo lang('website'); ?></label>
                                    <input type="url" name="website" id="website" class="form-control" value="<?php echo $result[0]->Website; ?>" autocomplete="off">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group label-floating is-focused">
										<label class="control-label" for="ph"><?php echo lang('ph'); ?></label>
		                                <input type="text" name="ph" required id="ph" value="<?php echo $result[0]->Phone; ?>" class="form-control Number" autocomplete="off">
                                    	<input type="hidden" name="CountryCode" id="CountryCode" value="<?php echo $result[0]->CountryCode; ?>">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group label-floating is-focused">
										 <label class="control-label" for="mob"><?php echo lang('mob'); ?></label>
                                        <input type="text" name="mob" id="mob" value="<?php echo $result[0]->Mobile; ?>" class="form-control Number" autocomplete="off">
                                    	<input type="hidden" name="MobCountryCode" id="MobCountryCode" value="<?php echo $result[0]->MobCountryCode; ?>">
									</div>
								</div>
							</div>
							<div class="row">
								
								<div class="col-md-4">
									<div class="form-group label-floating is-focused">
								<label class="control-label" for="fax"><?php echo lang('fax'); ?></label>
                                        <input type="text" name="fax" id="fax" value="<?php echo $result[0]->Fax; ?>" class="form-control Number" autocomplete="off">
                                      <input type="hidden" name="CountryFaxCode" id="CountryFaxCode" value="<?php echo $result[0]->CountryFaxCode; ?>">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group label-floating">
									<label class="control-label" for="country"><?php echo lang('country'); ?></label>
										<select name="country"  id="country" class="form-control" required>
											<?php $arr = CountryList();
											foreach($arr as $val){
											?>
											<option value="<?php echo $val->CountryID; ?>" <?php echo getSelected($val->CountryID,$result[0]->Country); ?>><?php echo $val->Title; ?></option>
											<?php } ?>
											</select>  
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group label-floating">
									<label class="control-label" for="StreetAddress"><?php echo lang('street_address'); ?></label>
									<input type="text" name="StreetAddress" required id="StreetAddress" value="<?php echo $result[0]->StreetAddress; ?>" class="form-control" autocomplete="off">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group label-floating">
								  <label class="control-label"  for="postcodeTown"><?php echo lang('postcode'); ?></label>
                                  <input type="text" name="postcodeTown" required  value="<?php echo $result[0]->PostalCode; ?>" id="postcodeTown" class="form-control" autocomplete="off">
									</div>
								</div>
								<div class="col-md-4">
                            <div class="form-group  label-floating">
								 <label  class="control-label" for="lang"><?php echo lang('default').' '.lang('language'); ?></label>
								<select name="lang"  id="lang" class="form-control" required>
											<?php $arr = getSystemLanguagesBySession();
											foreach($arr as $val){
											?>
											<option value="<?php echo $val->ShortCode; ?>"  <?php echo getSelected($val->ShortCode,$result[0]->Lang); ?>><?php echo $val->SystemLanguageTitle; ?></option>
											<?php } ?>
											</select> 
                           </div>
                        </div>
                        	<?php 
                        		if($this->session->userdata['admin']['RoleID'] == 4)
                        		{
                        	?>
                        		<div class="col-md-4">
									<div class="form-group label-floating">
								  <label class="control-label"  for="postcodeTown"><?php echo lang('vat'); ?></label>
                                  <input type="text" required disabled="disabled"  value="<?php echo $result[0]->Vat; ?>" id="Vat" class="form-control" autocomplete="off">
									</div>
								</div>
							<?php
								}
							?>
							</div>
							<div class="row">
                                <!-- <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="is_company">
                                                <input type="checkbox" id="is_company" <?php echo ((isset($result[0]->IsCompany) && $result[0]->IsCompany == 1) ? 'checked' : '');?> disabled="disabled" /> <?php echo lang('is_company'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="is_certified_trader">
                                                <input type="checkbox" id="is_certified_trader" <?php echo ((isset($result[0]->IsCertifiedTrader) && $result[0]->IsCertifiedTrader == 1) ? 'checked' : '');?> disabled="disabled" /> <?php echo lang('is_certified_trader'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>-->
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label class="control-label"  for="city"><?php echo lang('city'); ?></label>
                                        <input type="text" name="City" required  value="<?php echo $result[0]->City; ?>" id="city" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="is_paid">
                                                <input type="checkbox" id="is_paid" <?php echo ((isset($result[0]->IsPaid) && $result[0]->IsPaid == 1) ? 'checked' : '');?> disabled="disabled" /> <?php echo lang('is_paid'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="tax_status_confirmed">
                                                <input type="checkbox" id="tax_status_confirmed" <?php //echo ((isset($result[0]->TaxStatusConfirmed) && $result[0]->TaxStatusConfirmed == 1) ? 'checked' : '');?> disabled="disabled" /> <?php //echo lang('tax_status_confirmed'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="vat_confirmed">
                                                <input type="checkbox" id="vat_confirmed" <?php //echo ((isset($result[0]->VATConfirmed) && $result[0]->VATConfirmed == 1) ? 'checked' : '');?> disabled="disabled" /> <?php //echo lang('vat_confirmed'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label><?php echo lang('company-description'); ?></label>
										<div class="form-group label-floating">
                                            <textarea name="company_description" class="form-control" id="descriptionText" placeholder="" rows="10" cols="20"><?php echo $result[0]->CompanyProfile; ?></textarea>
										</div>
									</div>
								</div>
							</div>
							
                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('submit');?>
                                </button>
                                <a href="<?php echo base_url();?>cms/<?php echo $ControllerName;?>">
                                    <button type="button" class="btn btn-default waves-effect m-l-5">
                                        <?php echo lang('back');?>
                                    </button>
                                </a>
                            </div>
								</div>

                        </form>
                    </div>
                    <!-- end content-->
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>

<script>
CKEDITOR.replace( 'descriptionText' );

$(document).ready(function(){
    //$(".MobileNumberMask").inputmask({"mask": "+999999999999"}); //specifying options
    var input2 = document.querySelector("#ph");
    //var countryData = 'de';
    var iti2 = intlTelInput(input2, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "on",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "de",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'de', 'en'],
      // placeholderNumberType: "MOBILE",
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "<?php echo base_url();?>assets/backend/plugins/Intl-tel/js/utils.js",
    });

    //var iti2 = intlTelInput(input2);
    iti2.setNumber('<?php echo ($result[0]->CountryCode != '' ? $result[0]->CountryCode : '+49'); ?> <?php echo $result[0]->Phone; ?>')
    //$("#CountryCode").val('<?php //echo ($result[0]->CountryCode != '' ? $result[0]->CountryCode : '+49');?>');
    var countryData = iti2.getSelectedCountryData();
    //console.log(countryData.iso2);
    input2.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti2.getSelectedCountryData();
      $("#CountryCode").val("+"+changeCountryCode.dialCode);
      //console.log(changeCountryCode.iso2);
    });


    var input3 = document.querySelector("#mob");
    //var countryData = 'de';
    var iti3 = intlTelInput(input3, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "on",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "de",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'de', 'en'],
      // placeholderNumberType: "MOBILE",
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "<?php echo base_url();?>assets/backend/plugins/Intl-tel/js/utils.js",
    });

    //var iti2 = intlTelInput(input2);
    iti3.setNumber('<?php echo ($result[0]->MobCountryCode != '' ? $result[0]->MobCountryCode : '+49'); ?> <?php echo $result[0]->Mobile; ?>')
    //$("#MobCountryCode").val('<?php //echo ($result[0]->MobCountryCode != '' ? $result[0]->MobCountryCode : '+49');?>');
    var countryData = iti3.getSelectedCountryData();
    //console.log(countryData.iso2);
    input3.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti3.getSelectedCountryData();
      $("#MobCountryCode").val("+"+changeCountryCode.dialCode);
      //console.log(changeCountryCode.iso2);
    });

    var input4 = document.querySelector("#fax");
    //var countryData = 'de';
    var iti4 = intlTelInput(input4, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "on",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "de",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'de', 'en'],
      // placeholderNumberType: "MOBILE",
      preferredCountries: ['de'],
      separateDialCode: true,
      utilsScript: "<?php echo base_url();?>assets/backend/plugins/Intl-tel/js/utils.js",
    });

    //var iti2 = intlTelInput(input2);
    iti4.setNumber('<?php echo ($result[0]->CountryFaxCode != '' ? $result[0]->CountryFaxCode : '+49'); ?> <?php echo $result[0]->Fax; ?>')
    //$("#MobCountryCode").val('<?php //echo ($result[0]->MobCountryCode != '' ? $result[0]->MobCountryCode : '+49');?>');
    var countryData = iti4.getSelectedCountryData();
    //console.log(countryData.iso2);
    input4.addEventListener("countrychange", function() {
      var changeCountryCode = '';
      changeCountryCode = iti4.getSelectedCountryData();
      $("#CountryFaxCode").val("+"+changeCountryCode.dialCode);
      //console.log(changeCountryCode.iso2);
    });

    

}); 
</script>