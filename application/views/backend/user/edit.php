<?php
$option = '';
if(!empty($roles)){ 
    foreach($roles as $role){ 
        $option .= '<option value="'.$role->RoleID.'" '.((isset($result[0]->RoleID) && $result[0]->RoleID == $role->RoleID) ? 'selected' : '').'>'.$role->Title.' </option>';
    } }
$option2 = '';
if(!empty($centers)){ 
    foreach($centers as $center){ 
        $option2 .= '<option value="'.$center->CenterID.'" '.((isset($result[0]->CenterID) && $result[0]->CenterID == $center->CenterID) ? 'selected' : '').'>'.$center->Title.' </option>';
    } }     
$languages = getSystemLanguages();
$lang_tabs = '';
$lang_data = '';
if(!empty($languages)){
    foreach($languages as $key => $language){
        $common_fields = '';
        $common_fields2 = '';
        $common_fields3 = '';
        if($key == 0){
         $common_fields = '<div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="RoleID'.$key.'">'.lang('choose_user_role').'</label>
                                                                <select id="RoleID'.$key.'" class="selectpicker" data-style="select-with-transition" required name="RoleID">
                                                                    '.$option.'
                                                                </select>
                                                            </div>
                                                        </div>';

        if($result[0]->RoleID == 4 && ($loggedInUser['RoleID'] == 1 || $loggedInUser['RoleID'] == 2)){
            $common_fields2 = ' <div class="col-md-6">
                                                                <div class="form-group label-floating">
                                                                    <label class="control-label" for="Vat">'.lang('vat_id').'</label>
                                                                    <input type="text" name="Vat" parsley-trigger="change" required  class="form-control" id="Vat" value="'.((isset($result[0]->Vat)) ? $result[0]->Vat : '').'">
                                                                </div>
                                                            </div>'; 
        }
        $common_fields2 .= ' <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Email">'.lang('email').'</label>
                                                                <input type="text" disabled name="Email" parsley-trigger="change" required  class="form-control" id="Email" value="'.((isset($result[0]->Email)) ? $result[0]->Email : '').'">
                                                            </div>
                                                        </div>'; 
        $common_fields3 = '<div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsActive">
                                                <input name="IsActive" value="1" type="checkbox" id="IsActive" '.((isset($result[$key]->IsActive) && $result[$key]->IsActive == 1) ? 'checked' : '').'/> '.lang('is_verified').'
                                            </label>
                                        </div>
                                    </div>
                                </div><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsCompany">
                                                <input name="IsCompany" value="1" type="checkbox" id="IsCompany" '.((isset($result[$key]->IsCompany) && $result[$key]->IsCompany == 1) ? 'checked' : '').'/> '.lang('is_company').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
			if($result[0]->RoleID == 4 && ($loggedInUser['RoleID'] == 1 || $loggedInUser['RoleID'] == 2)){
			 $common_fields3 .= '<div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="is_certified_trader">
                                                <input name="IsCertifiedTrader" value="1" type="checkbox" id="is_certified_trader" '.((isset($result[0]->IsCertifiedTrader) && $result[0]->IsCertifiedTrader == 1) ? 'checked' : '').'/> '.lang('is_certified_trader').'
                                            </label>
                                        </div>
                                    </div>
                                </div>';
				}
				 $common_fields3 .= '
                            </div>';
                            if($result[0]->RoleID == 4 && ($loggedInUser['RoleID'] == 1 || $loggedInUser['RoleID'] == 2)){
                            $common_fields3 .= '<div class="row">
                                <div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="IsPaid">
                                                <input name="IsPaid" value="1" type="checkbox" id="IsPaid" '.((isset($result[0]->IsPaid) && $result[0]->IsPaid == 1) ? 'checked' : '').'/> '.lang('is_paid').'
                                            </label>
                                        </div>
                                    </div>
                                </div><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="TaxStatusConfirmed">
                                                <input name="TaxStatusConfirmed" value="1" type="checkbox" id="TaxStatusConfirmed" '.((isset($result[0]->TaxStatusConfirmed) && $result[0]->TaxStatusConfirmed == 1) ? 'checked' : '').'/> '.lang('tax_status_confirmed').'
                                            </label>
                                        </div>
                                    </div>
                                </div><div class="col-sm-4 checkbox-radios">
                                    <div class="form-group label-floating">
                                        <div class="checkbox">
                                            <label for="VATConfirmed">
                                                <input name="VATConfirmed" value="1" type="checkbox" id="VATConfirmed" '.((isset($result[0]->VATConfirmed) && $result[0]->VATConfirmed == 1) ? 'checked' : '').'/> '.lang('vat_confirmed').'
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                </div>';
			                 }
                        if($result[0]->RoleID == 4 && ($loggedInUser['RoleID'] == 1 || $loggedInUser['RoleID'] == 2)){
                            $common_fields3 .= '<div class="row">
                                                    <div class="col-sm-4 checkbox-radios">
                                                        <div class="form-group label-floating">
                                                            <div class="checkbox">
                                                                <label for="IsTillActivated">
                                                                    <input name="IsTillActivated" value="1" type="checkbox" id="IsTillActivated" '.((isset($result[0]->IsTillActivated) && $result[0]->IsTillActivated == 1) ? 'checked' : '').'/> '.lang('IsTillActivated').'
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div> 
                                                    <div class="col-md-4">
                                                        <div class="form-group label-floating" >
                                                            <label class="control-label" for="TillActivated">'.lang('TillActivated').'</label>
                                                            <input type="text" name="TillActivated"  class="form-control datepicker" id="TillActivated" value="'.((isset($result[0]->TillActivated)) ? dateformat($result[0]->TillActivated,'de') : '').'">
                                                        </div>
                                                    </div>
                                                </div>';
                             }
                             
                            if($loggedInUser['RoleID'] == 1){
                                $common_fields3 .= '<div class="row">
                                                        <div class="col-sm-4 checkbox-radios">
                                                            <div class="form-group label-floating">
                                                                <div class="checkbox">
                                                                    <label for="IsManualExtended">
                                                                        <input name="IsManualExtended" value="1" type="checkbox" id="IsManualExtended" '.((isset($result[0]->IsManualExtended) && $result[0]->IsManualExtended == 1) ? 'checked' : '').'/> '.lang('IsManualExtended').'
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group label-floating" >
                                                                <label class="control-label" for="ManualExtendedTo">'.lang('ManualExtendedTo').'</label>
                                                                <input type="text" name="ManualExtendedTo"  class="form-control datepicker" id="ManualExtendedTo" value="'.((isset($result[0]->ManualExtendedTo)) ? dateformat($result[0]->ManualExtendedTo,'de') : '').'">
                                                            </div>
                                                        </div>  
                                                    </div>';
                            }
        }

        $lang_tabs .= '<li class="'.($key == 0 ? 'active' : '').'">
                                        <a href="#'.$language->SystemLanguageTitle.'" data-toggle="tab">
                                            '.$language->SystemLanguageTitle.'
                                        </a>
                                  </li>';

        $lang_data .= '<div class="tab-pane '.($key == 0 ? 'active' : '').'" id="'.$language->SystemLanguageTitle.'">
                      <form action="'.base_url().'cms/'.$ControllerName.'/action" method="post" onsubmit="return false;" class="form_data" enctype="multipart/form-data" data-parsley-validate novalidate> 
                                                    <input type="hidden" name="form_type" value="update">
                                                    <input type="hidden" name="SystemLanguageID" value="'.base64_encode($language->SystemLanguageID).'">
                                                    <input type="hidden" name="'.$TableKey.'" value="'.base64_encode($result[0]->$TableKey).'">
                                                    <input type="hidden" name="IsDefault" value="'.$language->IsDefault.'">

                                                    
                                                      <div class="row">
                                                     '.$common_fields.'
                                                     <div class="col-md-6">
                                                            <div class="form-group label-floating">
                                                                <label class="control-label" for="Title'.$key.'">'.lang('name').'</label>
                                                                <input type="text" name="Title" parsley-trigger="change" required  class="form-control" id="Title'.$key.'" value="'.((isset($result[$key]->Title)) ? $result[$key]->Title : '').'">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="row">
                                                        
                                                        
                                                         '.$common_fields2.'
                                                    </div>
                                                    
                                                   '.$common_fields3.'
                                                    

                                                    <div class="form-group text-right m-b-0">
                                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                            '.lang('submit').'
                                                        </button>
                                                        <a href="'.base_url().'cms/'.$ControllerName.'">
                                                        <button type="button" class="btn btn-default waves-effect m-l-5">
                                                         '.lang('back').'
                                                        </button>
                                                        </a>
                                                    </div>

                                                </form>


                        </div>';
        
        
        
        
        
    }
}


?>

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                   <div class="card-header card-header-icon" data-background-color="purple">
                        <i class="material-icons">person</i>
                    </div>
                    
                    <div class="card-header">
                        <h5 class="card-title"><?php echo lang('edit').' '.lang($ControllerName);?></h5>
						<h6 class="card-sub-title"><?php echo lang('card_sub_title'); ?></h6>

                    </div>
                    <div class="card-content">
                        <div class="row">
                            <div class="col-md-2">
                                <ul class="nav nav-pills nav-pills-rose nav-stacked">
                                    <?php echo $lang_tabs; ?>
                                </ul>
                            </div>
                            <div class="col-md-10">
                                <div class="tab-content">
                                    <?php echo $lang_data; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>