<?php
/**
 * This class represents a generic error.
 */
class GeoIP2_Exception_OutOfQueriesException extends GeoIP2_Exception_GeoIp2Exception
{
}
