<?php
/**
 * This class represents a generic error.
 */
class GeoIP2_Exception_AddressNotFoundException extends GeoIP2_Exception_GeoIp2Exception
{
}
