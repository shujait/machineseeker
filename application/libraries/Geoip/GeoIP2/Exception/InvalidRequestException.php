<?php
/**
 * This class represents an error returned by MaxMind's GeoIP2
 * web service.
 */
class GeoIP2_Exception_InvalidRequestException extends GeoIP2_Exception_HttpException
{
    /**
     * The code returned by the MaxMind web service
     */
    public $error;

    public function __construct(
        $message,
        $error,
        $httpStatus,
        $uri,
        Exception $previous = null
    ) {
        $this->error = $error;
        parent::__construct($message, $httpStatus, $uri, $previous);
    }
}
