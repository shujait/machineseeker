<?php
/**
 * This class represents a generic error.
 */
class GeoIP2_Exception_AuthenticationException extends GeoIP2_Exception_GeoIp2Exception
{
}
