<?php
/**
 * This class should be thrown when unexpected data is found in the database.
 */
class GeoIP2_MaxMind_Db_Reader_InvalidDatabaseException extends Exception
{
}
