<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @package       CodeIgniter
 * @subpackage    Libraries
 * @category      CI_GeoIp2
 * @author        cappssolution
 *
 */
require_once(dirname(__FILE__).'/vendor/autoload.php');
use GeoIp2\Database\Reader;

class Geoip {

    protected $record;
    protected $ip;
    protected $database_path = APPPATH.'libraries/Geoip/GeoIP2/GeoLite2-Country.mmdb'; //default
    protected $ci;
    public function __construct() {

        //by default
        $this->ci =& get_instance();
        log_message('debug', "Codeigniter GeoIP Class Initialized");

    }

    /**
     * Setting Custom IP
     * @param $ip
     */
    public function setIp($ip = null){
        $reader = new Reader($this->database_path);

        //get server's ip instead
       $this->record = $reader->country($ip);

    }

  
	
    /**
     * getCountryName
     * @return CountryName
     */

    public function getCountry(){
        return $this->record->country->isoCode;
    }

}

?>