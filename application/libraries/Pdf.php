<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once dirname(__FILE__) . '/tcpdf/tcpdfl.php';

class Pdf extends TCPDFL {

    function __construct() {
        parent::__construct();
    }

    public function CreatePDF($data,$pdftitle = 'Invoice') {
        
        $this->print_html($data,$pdftitle);
    }

     private function print_html($html, $pdftitle = 'Invoice') {

//      $LOGOPATH = ''; 
//          $CompanyLogo =  $LOGOPATH. 'assets/images/logo.jpg';

        $pdf = new TCPDFL(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle($pdftitle);

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
        //$pdf->SetHeaderData($CompanyLogo, PDF_HEADER_LOGO_WIDTH, '', '');
        // set header and footer fonts
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(15, PDF_MARGIN_TOP, 15);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 0);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set font
        $pdf->SetFont('helvetica', '', 10, '', true);

        // add a page
        $pdf->AddPage();
        //$pdf->Image(base_url('assets/logo.png'),10, 5, 150, 35);

        // Image example with resizing
        //$pdf->Image(file path, X, Y, w, h);
//      $pdf->Image($CompanyLogo, 30, 5, 150, 35);
//      $pdf->setY(50);
        // create some HTML content
        //require_once(__DIR__ . '/templates/print_registration_slip.php');

        // output the HTML content
        $pdf->writeHTML($html, true, 0, true, 0);

        // reset pointer to the last page
        $pdf->lastPage();
        $filelocation = FCPATH;
        $filename = str_replace(' ', '', $pdftitle) . '.pdf';
        if (in_array($_SERVER['HTTP_HOST'], ['localhost','localhost:8888','localhost:8080', 'projects.local'])) {
            $fileNL = $filelocation."assets\\invoice_pdf\\".$filename;//Windows
        }else{
            $fileNL = $filelocation."assets/invoice_pdf/".$filename;//linux
        }
       
        
        $pdf->Output($fileNL, 'F');

        // ---------------------------------------------------------
        //Close and output PDF document
       // $pdf->IncludeJS("print();");
        //$pdf->Output(str_replace(' ', '', $pdftitle) . '.pdf', 'I');
    } 
    
    

   /* private function print_html($html) {

//		$LOGOPATH = ''; 
//       	$CompanyLogo =  $LOGOPATH. 'assets/images/logo.jpg';

        $pdf = new TCPDFL(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Invoice');

        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
        //$pdf->SetHeaderData($CompanyLogo, PDF_HEADER_LOGO_WIDTH, '', '');
        // set header and footer fonts
        //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        //$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(15, PDF_MARGIN_TOP, 15);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 0);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set font
        $pdf->SetFont('helvetica', '', 10, '', true);

        // add a page
        $pdf->AddPage();

        // Image example with resizing
        //$pdf->Image(file path, X, Y, w, h);
//		$pdf->Image($CompanyLogo, 30, 5, 150, 35);
//		$pdf->setY(50);
        // create some HTML content
        //require_once(__DIR__ . '/templates/print_registration_slip.php');

        // output the HTML content
        $pdf->writeHTML($html, true, 0, true, 0);

        // reset pointer to the last page
        $pdf->lastPage();
        echo substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT']));exit;
        // ---------------------------------------------------------
        //Close and output PDF document
        $pdf->Output(lang('DeliveryHistory') . '.pdf', 'I');
    }
*/


    
    

}
