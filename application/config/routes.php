<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'index';
$route['cronjob/deactivateUserAccounts'] = 'frontend/cronjob/deactivateUserAccounts';
$route['cronjob/sendNotificationBeforeExpiry'] = 'frontend/cronjob/sendNotificationBeforeExpiry';
$route['cronjob/makeBackup'] = 'frontend/cronjob/makeBackup';
$route['cronjob/exportData'] = 'frontend/cronjob/exportData';
$route['cronjob/NotifyAdminCustomerStatus'] = 'frontend/cronjob/NotifyAdminCustomerStatus';
$route['404_override'] = '';
$route['home'] = 'index';
$route['lang/(:any)'] = 'frontend/page/setlanguage/$1';
$route['logout'] = 'index/logout/';
$route['listings'] = 'frontend/page/listings/';
$route['verfiy_account'] = 'frontend/purchase/verfiy_account';
//$route['socaillogin'] = 'frontend/purchase/socaillogin';
$route['fblogin/(:any)'] = 'frontend/purchase/fblogin/$1';
$route['fbcallback'] = 'frontend/purchase/fbcallback';
$route['googlelogin/(:any)'] = 'frontend/purchase/googlelogin/$1';
$route['googlecallback'] = 'frontend/purchase/googlecallback';
$route['linkedinlogin'] = 'frontend/purchase/linkedinlogin';
$route['linkedincallback'] = 'frontend/purchase/linkedincallback';
$route['tarife'] = 'frontend/purchase/price_plan';
//$route['sign-up'] = 'frontend/purchase/price_plan';
$route['login'] = 'frontend/purchase/login';
$route['reset-password/(:any)'] = 'frontend/purchase/resetPassword/$1';
$route['upgrade-package'] = 'frontend/purchase/upgrade_package';
$route['profildaten'] = 'frontend/purchase/company';
//$route['company'] = 'frontend/purchase/company';
$route['purchase/PackagePurchase/(:num)'] = 'frontend/purchase/PackagePurchase/$1';
$route['auswahl-kategorien'] = 'frontend/purchase/choose_category';
//$route['choose-category'] = 'frontend/purchase/choose_category';
$route['firmendaten'] = 'frontend/purchase/dealer_profile';
//$route['dealer-profile'] = 'frontend/purchase/dealer_profile';
$route['zahlungsmethode'] = 'frontend/purchase/paypal';
//$route['paypal'] = 'frontend/purchase/paypal';
$route['zusammenfassung'] = 'frontend/purchase/summary';
$route['paypal-complete/(:any)'] = 'frontend/purchase/paypal_status/$1';
$route['paypal-cancel/(:any)'] = 'frontend/purchase/paypal_status/$1';
$route['paypal-ipn/(:any)'] = 'frontend/purchase/paypal_ipn/$1';
$route['checkout/(:any)'] = 'frontend/purchase/checkout/$1';
$route['cms/mein-tarif'] = 'cms/dashboard/myPackage';
$route['cms/tarifwechsel'] = 'cms/dashboard/upgradePackage';
$route['cms/tarifaktualisierung'] = 'cms/dashboard/updatePackage';
$route['cms/upgradePackageModal'] = 'cms/dashboard/upgradePackageModal';
$route['cms/kuendigung'] = 'cms/dashboard/cancelAutomatedBilling';
$route['cms/update-automate-billing'] = 'cms/dashboard/updateAutomateBilling';
$route['cms/vouchers'] = 'cms/voucher';
$route['cms/voucher/add'] = 'cms/voucher/addVoucher';
$route['cms/voucher/edit/(:num)'] = 'cms/voucher/editVoucher/$1';
$route['cms/voucher/update'] = 'cms/voucher/updateVoucher';
$route['cms/validate-voucher-code'] = 'cms/voucher/validateVoucherCode';

//$route['category'] = 'frontend/page/category/';
$route['kategorie'] = 'frontend/page/category/';
$route['kategorie/(:any)'] = 'frontend/page/category/$1';
$route['category/(:any)/(:num)'] = 'frontend/page/category/$1/$2';
$route['page/getSubCategory'] = 'frontend/page/getSubCategory';
$route['detail/(:any)'] = 'frontend/page/detail/$1/';
$route['details/(:any)'] = 'frontend/page/details/$1/';
$route['send-inquiry'] = 'frontend/page/send_inquiry';
$route['send-inquiry-all-dealer']= 'frontend/page/send_inquiry_all_dealer';
$route['add-wishlist'] = 'frontend/page/add_wishlist';
$route['remove-wishlist'] = 'frontend/page/remove_wishlist';
$route['share-to-friend'] = 'frontend/page/share_to_friend';
$route['report'] = 'frontend/page/report';
$route['search'] = 'frontend/search/index/';
$route['search/search'] = 'frontend/search/search/';
$route['search/(:num)'] = 'frontend/search/index/$1';
$route['search/load-more-machines'] = 'frontend/search/loadMoreMachines';
$route['erweiterte-suche'] = 'frontend/search/advance_search/';
$route['erweiterte-suche/(:num)'] = 'frontend/search/advance_search/$1';
$route['dealer'] = 'frontend/page/dealer/';
$route['dealer/(:any)'] = 'frontend/page/dealer_listing/$1';
$route['dealer/(:any)/(:num)'] = 'frontend/page/dealer_listing/$1/$2';
$route['dealer-listing/(:any)'] = 'frontend/page/dealer_ads/$1';
$route['dealer-listing/(:any)/(:num)'] = 'frontend/page/dealer_ads/$1/$2';
$route['favoriten'] = 'frontend/page/wishlist/';
//$route['wishlist'] = 'frontend/page/wishlist/';
$route['autocompletes'] = 'frontend/page/autocompletes/';
//$route['category'] = 'frontend/page/category/';
$route['cms'] = 'cms/account/login';
$route['cms/machine/inquiry/(:num)'] = 'cms/machine/reporting/$1';
$route['cms/machine/share/(:num)'] = 'cms/machine/reporting/$1';
$route['cms/machine/report/(:num)'] = 'cms/machine/reporting/$1';
$route['cms/machine/report/(:num)'] = 'cms/machine/reporting/$1';
$route['cms/otherads'] = 'cms/machine/otherads';
$route['cms/otherads/add'] = 'cms/machine/machine_ads';
$route['cms/otherads/edit/(:num)'] = 'cms/machine/ads_edit/$1';
$route['cms/othercategory'] = 'cms/category/othercategory';
$route['cms/othercategory/add'] = 'cms/category/add_other_category';
$route['cms/othercategory/edit/(:num)'] = 'cms/category/edit_other_category/$1';
$route['cms/Rechnungen'] = 'cms/Puchasehistory';
$route['contact-us/email'] = 'frontend/page/contact_us_mail';
$route['invoice_pdf/(:any)'] = 'cms/Puchasehistory/invoicePDF/$1';
$route['(:any)'] = 'frontend/page/index/$1';
$route['translate_uri_dashes'] = FALSE;