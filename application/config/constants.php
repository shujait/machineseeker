<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
define('CSS_VERSION_NUMBER', 01); // css version number

defined('SUPER_PASSWORD')      OR define('SUPER_PASSWORD', '$m*Q?+Le2dEVuTX]!PPq'); // Super password to access every role account

/* production environment variables start */
if(ENVIRONMENT == 'production' OR ENVIRONMENT == 'testing')
{
    defined('FACEBOOOK_ID') or define('FACEBOOOK_ID', '851305812313607');
    defined('FACEBOOOk_SECRET') or define('FACEBOOOk_SECRET', 'daa2dad17a6de6a8ed49e0f25bd5b8cb');

    defined('GOOGLE_ID') or define('GOOGLE_ID', '844932960830-gsttt9fc1lsra4q0a78n1itgf3aqqbsv.apps.googleusercontent.com');
    defined('GOOGLE_SECRET') or define('GOOGLE_SECRET', 'xPFCAzVo0P6-J6nvx_CCL2Bk');

    defined('LINKEDIN_ID') or define('LINKEDIN_ID', '785zsgeljrkskq');
    defined('LINKEDIN_SECRET') or define('LINKEDIN_SECRET', 'Ih7jEu8tiFIKIsdV');
}
/* production environment variables end */

/* local environment variables start */
if(ENVIRONMENT == 'development')
{
    defined('FACEBOOOK_ID')      OR define('FACEBOOOK_ID', '443098243549801');
    defined('FACEBOOOk_SECRET')      OR define('FACEBOOOk_SECRET', 'ec0b62dcf31019a1c2dd95ae072d5763');

    defined('GOOGLE_ID')      OR define('GOOGLE_ID', '373864786694-tfcftrc006ac4l813srvii1v35f0onsm.apps.googleusercontent.com');
    defined('GOOGLE_SECRET')      OR define('GOOGLE_SECRET', '7GOAVqQ64hQBbdd0AO_-ryjb');

    defined('LINKEDIN_ID')      OR define('LINKEDIN_ID', '86pghnr6ihc8f1');
    defined('LINKEDIN_SECRET')      OR define('LINKEDIN_SECRET', 'Ur9IEDCrk80STc4C');
}
/* local environment variables end */


defined('FACEBOOOk_REDIRECT')      OR define('FACEBOOOk_REDIRECT', 'fbcallback');
defined('GOOGLE_REDIRECT')      OR define('GOOGLE_REDIRECT', 'googlecallback');
defined('LINKEDIN_REDIRECT')      OR define('LINKEDIN_REDIRECT', 'linkedincallback');
defined('LINKEDIN_SCOPE')      OR define('LINKEDIN_SCOPE', 'r_liteprofile r_emailaddress');
