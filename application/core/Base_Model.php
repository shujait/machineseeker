<?php

class Base_Model extends CI_Model
{

    public $table;

    public function __construct($table = "")
    {
        parent::__construct();

        if (!empty ($table)) {
            $this->table = $table;

            $fields = $this->db->list_fields($table);

            foreach ($fields as $field) {
                $this->$field = NULL;
            }
        }
        $this->db->query("SET sql_mode = ''");

    }

    /**
     * Inserts a row into the table and returns the row id
     * @param data Array
     * @return insert_id int
     */
    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function insert_batch($data)
    {
            return $this->db->insert_batch($this->table, $data);
    }

    public function update_batch($data,$update_by)
    {
             return $this->db->update_batch($this->table, $data,$update_by);

    }

    /**
     *
     * @param array $data
     * @param array $search
     */
    public function update($data, $search)
    {
        $this->db->update($this->table, $data, $search);
        $this->db->last_query();
    }

    /**
     * Get an instance of the model and initialize the properties with table row for supplied ID.
     * @param unknown $id
     * @return Base_Model|boolean
     */
    public function get($id, $as_array = false, $field_name = false)
    {
        if ($field_name) {
            $result = $this->db->get_where($this->table, array($field_name => $id));

        } else {
            $result = $this->db->get_where($this->table, array('id' => $id));

        }
        if ($result->num_rows() > 0) {
            $row = $result->row_array();

            if ($as_array) {
                return $row;
            }

            foreach ($row as $col => $val) {
                $this->$col = $val;
            }

            return $this;
        } else {
            return false;
        }
    }
    
    
    
    public function getAllJoinedData($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'SortOrder',$title_key = 'Title',$limit = false,$start = 0,$other_sort_field = false,$MachinAdsTypes=false,$CategoryType=false,$active_category=false)// please for now not usable function in text model 
    {
            $first_field = $this->table.'.'.$join_field;
            $second_field = $this->table.'_text.'.$join_field;
            $this->db->select($this->table.'.*, ' . $this->table . '_text.*,'.$this->table.'.CreatedAt as CreatedDate');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field);
            $this->db->join('system_languages','system_languages.SystemLanguageID = '.$this->table.'_text.SystemLanguageID','left' );
            
            if($this->table == 'machines' && !in_array($this->session->userdata['admin']['RoleID'],array(1,2))){
                $this->db->Join('users','users.UserID = machines.UserID AND users.IsActive = 1');
                $this->db->group_by('machines.MachineID');
            }            
            
            if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
            } else {
                    $this->db->where('system_languages.IsDefault','1');
            }
            if(!empty($MachinAdsTypes))
            {
                if($MachinAdsTypes==-1){
                    $MachinAdsTypes=0;
                }
                $this->db->where('machines.MachinAdsTypes',$MachinAdsTypes);
            }
            if(!empty($CategoryType))
            {
                if($CategoryType==-1){
                    $CategoryType=0;
                }
                $this->db->where('categories.CategoryType',$CategoryType);
            }
            if($where)
            {
                $this->db->where($where);
            }
            if($active_category)
            {
                $this->db->where($active_category);
            }

            $this->db->where($this->table.'.Hide','0');
            if ($limit) {
                $this->db->limit($limit, $start);
            }

            if($this->table == 'countries'){
               $this->db->order_by('countries.SortOrder','ASC');
            }

            if($other_sort_field){
                $this->db->order_by($sort_field,$sort);
            }else{
                $this->db->order_by($this->table.'.'.$sort_field,$sort);
            }
            
            $result = $this->db->get($this->table);
             
            //echo $this->db->last_query();
            if($as_array){
                $data =  $result->result_array();
            }else{
                $data = $result->result();
            }

            foreach ($data as $row) {
                if($as_array) {
                    if($row[$title_key] == ''){

                        $row[$title_key] = $this->GetDefaultLangData($join_field , $row[$join_field],$title_key);
                    }
                } else {

                    if($row->$title_key == ''){
                        $row->$title_key = $this->GetDefaultLangData($join_field , $row->$join_field,$title_key);
                    }
                }
            
            }


           //print_rm($data);

            return $data;
            
    }
    
    
     public function getJoinedData($as_array= false,$join_field,$where = false,$sort = 'ASC',$sort_field = 'SortOrder')// please for now not usable function in text model 
    {

            $first_field = $this->table.'.'.$join_field;
            $second_field = $this->table.'_text.'.$join_field;
            $this->db->select($this->table.'.*, ' . $this->table . '_text.*');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field );
            $this->db->join('system_languages','system_languages.SystemLanguageID = '.$this->table.'_text.SystemLanguageID','Left' );
           
            if($where)
            {
                    $this->db->where($where);
            }
            $this->db->where('system_languages.Hide',0);
            $this->db->order_by('system_languages.IsDefault','DESC');
        
            $result = $this->db->get($this->table);
           
            if($as_array)
            {
                    //return $result->result_array();
                    $data =  $result->result_array();
            }else{
                $data = $result->result();
            }

            

            /*echo "<pre>";
            print_r($data);
            echo "</pre>";*/

           /* foreach ($data as $row) {
                if($as_array) {
                    if($row['Title'] == ''){
                        $row['Title'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
                    }
                } else {
                    if($row->Title == ''){
                        $row->Title = $this->GetDefaultLangData($join_field , $row->$join_field);
                    }
                }
            
            }
*/
          

            return $data;
            //return $result->result();
    }

    public function getJoinedDataForSystemLanguage($as_array= false,$join_field,$where = false,$sort = 'ASC',$sort_field = 'SortOrder')// please for now not usable function in text model 
    {

             $first_field = $this->table.'.SystemLanguageID';
            $second_field = $this->table.'_text.LanguageID';
            $this->db->select('system_languages.*,system_languages_text.SystemLanguageTitle,system_languages_text.LanguageID');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field );
            
           
            if($where)
            {
                    $this->db->where($where);
            }
           
        
            $result = $this->db->get($this->table);
           
            if($as_array)
            {
                    //return $result->result_array();
                    $data =  $result->result_array();
            }else{
                $data = $result->result();
            }

        
          

            return $data;
            //return $result->result();
    }
    
    public function getMultipleRows($fields, $as_array = false, $idOrderBy = 'asc', $limit = false, $start = 0)
    {


        if ($idOrderBy == 'desc')
            $this->db->order_by('id', 'desc');

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        $result = $this->db->get_where($this->table, $fields);

        //$this->db->last_query(); exit();

        if ($result->num_rows() > 0) {


            if ($as_array) {
                return $result->result_array();
            }

            return $result->result();
        } else {
            return false;
        }
    }

    public function getWithMultipleFields($fields, $as_array = false)
    {


        $result = $this->db->get_where($this->table, $fields);

        if ($result->num_rows() > 0) {
            $row = $result->row_array();

            if ($as_array) {
                return $row;
            }

            foreach ($row as $col => $val) {
                $this->$col = $val;
            }


            return $this;
        } else {
            return false;
        }

    }

    /**
     * Gets all the records from the table.
     */
    public function getAll($as_array = false)
    {
        $result = $this->db->get($this->table);

        if ($as_array) {
            return $result->result_array();
        }

        return $result->result();
    }

    public function deleteIn($key, $valuesArr)
    {

        $this->db->where_in($key, $valuesArr);
        $this->db->delete($this->table);

    }

    public function delete($search)
    {
        $this->db->delete($this->table, $search);

    }


    public function getFields()
    {
        $fields = $this->db->list_fields($this->table);
        return $fields;
    }

    
    
    public function getLastRow($primery_key='id')
	{
            return $this->db->select('SortOrder')->order_by($primery_key,"desc")->limit(1)->get($this->table)->row_array();
	}
        
    public function getJoinedDataWithOtherTable($as_array=false,$join_field,$second_table,$where = false)// please for now not usable function in text model
    {

        $first_field = $this->table.'.'.$join_field;
        $second_field = $second_table.'.'.$join_field;
        $this->db->select($this->table.'.*, ' . $second_table . '.*');
        $this->db->join($second_table,$first_field.' = '.$second_field );

        if($where)
        {
            $this->db->where($where);
        }

        //$this->db->order_by($this->table.'_cod.Code','asc');
        $result = $this->db->get($this->table);
        //echo $this->db->last_query();exit();
        if($as_array)
        {
            // return $result->result_array();
             $data =  $result->result_array();
        }else{
            $data = $result->result();
        }

        

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

       /* foreach ($data as $row) {
            if($as_array) {
                if($row['Title'] == ''){
                    $row['Title'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
                }
            } else {
                if($row->Code == ''){
                    $row->Title = $this->GetDefaultLangData($join_field , $row->$join_field);
                }
            }

        }
*/
        return $data;
        //return $result->result();
    }

    public function getLeftJoinedDataWithOtherTable($as_array=false,$join_field,$second_table,$where = false)// please for now not usable function in text model
    {

        $first_field = $this->table.'.'.$join_field;
        $second_field = $second_table.'.'.$join_field;
        $this->db->select($second_table . '.*, ' . $this->table.'.*');
        $this->db->join($second_table,$first_field.' = '.$second_field,'left');

        if($where)
        {
            $this->db->where($where);
        }

        //$this->db->order_by($this->table.'_cod.Code','asc');
        $result = $this->db->get($this->table);
        //echo $this->db->last_query();exit();
        if($as_array)
        {
            // return $result->result_array();
             $data =  $result->result_array();
        }else{
             $data = $result->result();
        }

        

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";*/

       /* foreach ($data as $row) {
            if($as_array) {
                if($row['Title'] == ''){
                    $row['Title'] = $this->GetDefaultLangData($join_field , $row[$join_field]);
                }
            } else {
                if($row->Title == ''){
                    $row->Title = $this->GetDefaultLangData($join_field , $row->$join_field);
                }
            }

        }*/

        return $data;
        //return $result->result();
    }
	   public function getActiveAllJoinedData($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'SortOrder',$rec = 'result',array $limit = [],$title_key = 'Title',$test = false,$is_active=true)// please for now not usable function in text model 
    {

            $first_field = $this->table.'.'.$join_field;
            $second_field = $this->table.'_text.'.$join_field;
            $this->db->select($this->table.'.*, ' . $this->table . '_text.*');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field );
            $this->db->join('system_languages','system_languages.SystemLanguageID = '.$this->table.'_text.SystemLanguageID' );

            if($this->table == 'machines'){
                $this->db->Join('users','users.UserID = machines.UserID AND users.IsActive = 1');
                $this->db->group_by('machines.MachineID');
            } 
           
           
            if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
            }else
            {
                    $this->db->where('system_languages.IsDefault','1');
            }
            if($where)
            {
                    $this->db->where($where);
            }
            $this->db->where($this->table.'.Hide','0');
            if($is_active)
            $this->db->where($this->table.'.IsActive','1');
            $this->db->order_by($this->table.'.'.$sort_field,$sort);
		   if($limit){
		    $this->db->limit( $limit[0], $limit[1] );
		   }
			$result = $this->db->get($this->table);
            // echo $this->db->last_query();exit();
            if($as_array)
            {
                   
                $data =  $result->result_array();
            }else{
                $data = $result->$rec();
            }

            //print_rm($data);exit;
            if($rec != 'row'){

                foreach ($data as $row) {

                if($as_array) {

                    if($row[$title_key] == ''){

                        $row[$title_key] = $this->GetDefaultLangData($join_field , $row[$join_field],$title_key);
                    }
                } else {
                    
                    if($row->$title_key == ''){

                        $row->$title_key = $this->GetDefaultLangData($join_field , $row->$join_field,$title_key);

                    }
                }
            
            }
            }else if($rec == 'row' && $data->$title_key == ''){

				$data->$title_key  = $this->GetDefaultLangData($join_field, $data->$join_field,$title_key);

			}
          


            return $data;
            
    }


    public function getMachineActiveAllJoinedData($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'SortOrder',$rec = 'result',array $limit = [],$title_key = 'Title',$test = false)// please for now not usable function in text model 
    {
            $first_field = $this->table.'.'.$join_field;
            $second_field = $this->table.'_text.'.$join_field;
            $this->db->select($this->table.'.*, ' . $this->table . '_text.*');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field );
            $this->db->join('system_languages','system_languages.SystemLanguageID = '.$this->table.'_text.SystemLanguageID' );
            $this->db->Join('users','users.UserID = machines.UserID AND users.IsActive = 1');
            $this->db->Join('categories','categories.CategoryID = machines.CategoryID', 'LEFT');
            $this->db->Join('categories_text','categories_text.CategoryID = categories.CategoryID', 'LEFT');

            $this->db->group_by('machines.MachineID');
           
            if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
            }else
            {
                    $this->db->where('system_languages.IsDefault','1');
            }
            if($where)
            {
                    $this->db->where($where);
            }
            $this->db->where($this->table.'.Hide','0');
            $this->db->where($this->table.'.IsActive','1');
            $this->db->order_by($this->table.'.'.$sort_field,$sort);
           if($limit){
            $this->db->limit( $limit[0], $limit[1] );
           }
            $result = $this->db->get($this->table);
            //echo $this->db->last_query();exit();
            if($as_array){
                $data =  $result->result_array();
            }else{
                $data = $result->$rec();
            }

            //print_rm($data);exit;
            if($rec != 'row'){

                foreach ($data as $row) {

                if($as_array) {

                    if($row[$title_key] == ''){

                        $row[$title_key] = $this->GetDefaultLangData($join_field , $row[$join_field],$title_key);
                    }
                } else {
                    
                    if($row->$title_key == ''){

                        $row->$title_key = $this->GetDefaultLangData($join_field , $row->$join_field,$title_key);

                    }
                }
            
            }
            }else if($rec == 'row' && $data->$title_key == ''){

                $data->$title_key  = $this->GetDefaultLangData($join_field, $data->$join_field,$title_key);

            }
          


            return $data;
            
    }

    public function GetDefaultLangData($join_field , $CurrntID,$title_key)
    {
        $this->db->select($title_key);

        $this->db->join('system_languages','system_languages.SystemLanguageID = '.$this->table.'_text.SystemLanguageID');
        $result = $this->db->get_where($this->table.'_text', array($this->table.'_text.'.$join_field => $CurrntID , 'system_languages.IsDefault' => 1 ));
        $data = $result->row_array();
        return $data[$title_key];
    }

    public function getActiveAllJoinedDataForSystemLanguage($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'SortOrder',$rec = 'result',array $limit = [],$title_key = 'SystemLanguageTitle',$test = false)// please for now not usable function in text model 
    {

            $first_field = $this->table.'.SystemLanguageID';
            $second_field = $this->table.'_text.LanguageID';
            $this->db->select('system_languages.*,system_languages_text.SystemLanguageTitle,system_languages_text.LanguageID');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field );
            
            if($where)
            {
                    $this->db->where($where);
            }
            //$this->db->where('system_languages.ShortCode', $system_language_code);
            //$this->db->where($this->table.'.Hide','0');
           // $this->db->where($this->table.'.IsActive','1');
            $this->db->order_by($this->table.'.'.$sort_field,$sort);
            //$this->db->group_by($this->table.'_text.LanguageID');
           if($limit){
            $this->db->limit( $limit[0], $limit[1] );
           }

            $result = $this->db->get($this->table);
           // echo $this->db->last_query();exit();
            if($as_array)
            {
                   
                $data =  $result->result_array();
            }else{
                $data = $result->$rec();
            }

            //print_rm($data);exit;
            if($rec != 'row'){

                foreach ($data as $row) {

                if($as_array) {

                    if($row[$title_key] == ''){

                        $row[$title_key] = $this->GetDefaultLangData($join_field , $row[$join_field],$title_key);
                    }
                } else {
                    
                    if($row->$title_key == ''){

                        $row->$title_key = $this->GetDefaultLangData($join_field , $row->$join_field,$title_key);

                    }
                }
            
            }
            }
          


            return $data;
            
    }

}