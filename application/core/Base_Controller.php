<?php

/*use Ajaxray\PHPWatermark\Watermark;

include 'application/libraries/PHPWatermark/vendor/autoload.php';*/

use Intervention\Image\ImageManagerStatic as Image;

class Base_Controller extends CI_Controller {

    protected $language;

    public function __construct() {

        parent::__construct();
		$this->load->Model(['System_language_model']);
        if ($this->session->userdata('lang')) {
            $this->language = $this->session->userdata('lang');
            // $this->language = $this->session->userdata('languageID');
        } else {
            $result = getDefaultLanguage();
            if ($result) {
                $this->language = $result->ShortCode;
            } else {
                $this->language = 'de';
            }
        }
        $this->data['site_setting'] = $this->getSiteSetting();
        $this->data['loggedInUser'] = $this->session->userdata('admin');
        $this->data['language'] = $this->language;
        
        $this->load->library('image_lib');
    }

    public function changeLanguage($language){
        
        $fetch_by['ShortCode'] = $language;
        $result = $this->System_language_model->getWithMultipleFields($fetch_by);
        $languageID = $result->SystemLanguageID;
        if (!$result) {

            $default_lang = getDefaultLanguage();
            $language = $default_lang->ShortCode;
            $languageID = $default_lang->SystemLanguageID;
        }
        $this->session->set_userdata('lang', $language);
        //$this->session->set_userdata('languageID',$languageID);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function getSiteSetting()
    {
        $this->load->model('Site_setting_model');
        return $this->Site_setting_model->get(1, false, 'SiteSettingID');
    }

    public function uploadImage($file_key, $path, $id = false, $type = false, $multiple = false) {
        $data = array();
        $extension = array("jpeg","JPEG","jpg","webp","JPG","png","PNG","pdf","PDF","doc","DOC","docx","DOCX","xls","XLS","xlsx","XLSX");

            if (!is_dir($path."/")) {
                mkdir($path . "/", 0777, true);
            }
            foreach ($_FILES[$file_key]["tmp_name"] as $key => $tmp_name) {
//                $file_name = rand(9999, 999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
                $file_name = rand(9999, 999999999) . date('Ymdhsi') .'.webp';
                $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
                $ext = pathinfo($file_name, PATHINFO_EXTENSION);

                if (in_array($ext, $extension) && !empty($ext)) {

                    move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"][$key], $path . $file_name);

                    if (!$multiple) {
                        return $path . $file_name;
                    } else {

                        $this->load->model('Site_images_model');
                        $data['FileID'] = $id;
                        $data['ImageType'] = $type;
                        $data['ImageName'] = $path . $file_name;
                        $this->Site_images_model->save($data);
                        $imgExt = array("jpeg","JPEG","jpg","JPG","png","PNG");
                        if($type == 'MachineFile' && in_array($ext, $imgExt))
                        {
                            $this->watermarkimage($path . $file_name);
                        }
                    }
                }else{
                    return false;
                }
            }
        return true;
    }

    public function uploadfeature_image($file_key, $path,$key=0, $id = false, $type = false, $multiple = false) {
        $data = array();
        $extension = array("jpeg","JPEG","jpg","JPG","png","PNG","pdf","PDF","doc","DOC","docx","DOCX","xls","XLS","xlsx","XLSX");


        $file_name = rand(9999, 999999999) . date('Ymdhsi') . str_replace(' ', '_', $_FILES[$file_key]['name'][$key]);
        $file_tmp = $_FILES[$file_key]["tmp_name"][$key];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {
            if(!is_dir($path."/")) {
                mkdir($path."/", 0777, true);
            }
                move_uploaded_file($file_tmp, $path . $file_name);
                if (!$multiple) {
                    return $path . $file_name;
                }
            }else{
                return false;
            }
        return true;
    }

    public function uploadImageforDocs($file_key, $path, $id = false, $type = false, $multiple = false) {
        $path=$path.$id.'/';
        $data = array();
        $extension = array("jpeg","JPEG","jpg","JPG","png","PNG","pdf","PDF","doc","DOC","docx","DOCX","xls","XLS","xlsx","XLSX");
        if (!file_exists($path )) {
          mkdir($path ); 
        }
        $file_name =    str_replace(' ', '_', $_FILES[$file_key]['name']);
            $file_tmp = $_FILES[$file_key]["tmp_name"];
            $ext = pathinfo($file_name, PATHINFO_EXTENSION);
            if (in_array($ext, $extension)) {
                move_uploaded_file($file_tmp = $_FILES[$file_key]["tmp_name"], $path . $file_name);
                $this->load->model('Site_documents_model');
                $data['UserID'] = $id;
                $data['DocumentType'] = $type;
                $data['path'] = $path . $file_name;
                $this->Site_documents_model->save($data);
            } else {
                return false;
            }
        
        return $data['path'];
    }

    public function DeleteImage() {
        $deleted_by = array();
        $ImagePath = $this->input->post('image_path');
        $deleted_by['SiteImageID'] = $this->input->post('image_id');
        if (file_exists($ImagePath)) {
            unlink($ImagePath);
        }
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;
    }



    public function addAllLanguageData($model,$table_key,$insert_id,$extra_val = false){

        $languages = $this->System_language_model->getAll();
        $save_child_data = array();
        if($languages){
            foreach ($languages as $key => $value) {
                if($value->IsDefault == 0){
                   // $save_child_data['Title']                        = '';
                    $save_child_data[$table_key]                     = $insert_id;
                    $save_child_data['SystemLanguageID']             = $value->SystemLanguageID;
                    if($extra_val){
                        foreach ($extra_val as $ext_key => $extra) {
                            $save_child_data[$ext_key]                        = $extra;
                        }
                    }
                    $save_child_data['CreatedAt']                    = $save_child_data['UpdatedAt']    = date('Y-m-d H:i:s');
                    if($this->session->userdata('admin')){
                        $save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = $this->session->userdata['admin']['UserID'];
                    }else{
                        $save_child_data['CreatedBy']                    = $save_child_data['UpdatedBy']    = 0;
                    }
                    $this->$model->save($save_child_data);
                }
            }
            return true;
        }else{
            return false;
        }



    }



    public function insert_data(){
        $parent                             = $this->data['Parent_model'];
        $child                              = $this->data['Child_model'];



        $this->load->model('System_language_model');
        $tab_key = $this->data['TableKey'];
        $languages = $this->System_language_model->getAll();
        $res = $this->$parent->getAll();
        foreach ($languages as $key => $language) 
        {
                foreach ($res as $key => $value) 
                {

                        
                        $fetch_by = array();
                        $fetch_by[$this->data['TableKey']] = $value->$tab_key;
                        $fetch_by['SystemLanguageID'] = $language->SystemLanguageID;
                        $check_if_alread = $this->$child->getWithMultipleFields($fetch_by);
                        if(!$check_if_alread){

                            $save_child_data = array();
                            $save_child_data[$this->data['TableKey']] = $value->$tab_key;
                            $save_child_data['SystemLanguageID'] = $language->SystemLanguageID;
                            $save_child_data['CreatedAt'] = $save_child_data['UpdatedAt'] = date('Y-m-d H:i:s');
                            $save_child_data['CreatedBy'] = $save_child_data['UpdatedBy'] = $this->session->userdata['admin']['UserID'];
                            
                            $this->$child->save($save_child_data);

                        }

                           
                }

        }
    }
    public function watermarkimage($DestinationPath)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $DestinationPath;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = 'assets/frontend/images/Maschinenpilot-grey.png';
        //the overlay image
        $config['wm_opacity'] = 20;
        /*$config['width'] = '50';
        $config['height'] = '50';*/
        $config['wm_padding'] = 0;
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        /*$config['wm_vrt_offset'] = '130';                
        $config['wm_hor_offset'] = '50';*/
        $this->image_lib->initialize($config);
        if (!$this->image_lib->watermark()) {
            return false;
        } else {
            return true;
        }
    }
    public function watermarktext($DestinationPath)
    {
        $imgConfig = array();
                        
        $imgConfig['image_library']   = 'GD2';
                                
        $imgConfig['source_image']    = $DestinationPath;
                                
        $imgConfig['wm_text']         = 'Maschinenpilot';
                                
        $imgConfig['wm_type']         = 'text';
                                
        $imgConfig['wm_font_size']    = '50';

        $imgConfig['wm_vrt_alignment'] = 'bottom';

        $imgConfig['wm_hor_alignment'] = 'right';

        $imgConfig['wm_opacity'] = 20;
        
        /*$imgConfig['wm_vrt_offset'] = '130';                
        $imgConfig['wm_hor_offset'] = '300';*/
        /*$config['image_library'] = 'GD2';
        $config['source_image'] = $DestinationPath;
        $config['wm_type'] = 'text';
        $config['wm_text'] = 'Maschinenpilot';
                                
        $config['wm_font_size'] = '16';
        //the overlay image
        $config['wm_opacity'] = 20;
        //$config['width'] = '50';
        //$config['height'] = '50';
        $config['wm_padding'] = 0;
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'right';
        $config['wm_vrt_offset'] = '130';                
        $config['wm_hor_offset'] = '50';*/

        //print_rm($imgConfig);
        $this->image_lib->initialize($imgConfig);
        if (!$this->image_lib->watermark()) {
            return false;
        } else {
            return true;
        }
    }

    /*public function watermarktextnew($DestinationPath)
    {

        $watermark = new Watermark($DestinationPath);

        $watermark->setFont('Arial')
            ->setFontSize(36)
            ->setOpacity(.4)
            ->setRotate(330)
            ->setOffset(-80, 200)
            ->setPosition(Watermark::POSITION_RIGHT);

        $text = "Maschinenpilot";
        $watermark->withText($text, $DestinationPath);
    }*/
    public function deleteImage2(){
        
        $this->load->Model('Site_images_model');
        

        $deleted_by = array();
        $deleted_by['SiteImageID'] = $this->input->post('id');
        
        $this->Site_images_model->delete($deleted_by);


        $success['error'] = false;
        $success['success'] = lang('deleted_successfully');

        echo json_encode($success);
        exit;

    }

    public function ColumnNameByIndex($Col=-1, $Row=-1)
    {
      if($Col!=-1)
          $ColName = PHPExcel_Cell::stringFromColumnIndex($Col);

      if($Row == -1)
          $Row = '';

          return $ColName.$Row;
    }

    public function uploadWithThumbnail($path) {
            $config['upload_path'] = './'.$path;
            $config['allowed_types'] = 'gif|jpg|png';
            $config['remove_spaces'] = TRUE;
            //Loading Library - File Uploading
            print_r($config);
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $imagename = 'no-img.jpg';
            if (!$this->upload->do_upload('imageURL')) {
                $error = array('error' => $this->upload->display_errors());
                echo $this->upload->display_errors();
                exit;
            } else {
                $data = $this->upload->data();
                $fixed_name = $data['file_name'];
                $raw_name = $data['raw_name']. time();
                $ext = $data['file_ext'];
                $imagename = $raw_name.$ext;   
            }
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
        
             $paths = array();
             $j=0;
            $image_sizes = array('_large' => array(1020, 720), '_thumb' => array(300, 200));
            // load library
            $this->load->library('image_lib');
            $i = TRUE;
            foreach ($image_sizes as $key=>$resize) {               
                if($i){

                    $i = FALSE;
                }
                else{
                     $raw_name = $data['raw_name']. time().'_thumb';
                     $ext = $data['file_ext'];
                     $imagename = $raw_name.$ext;
                }

                $paths[$j] = $imagename;
                $j++;

                $config = array(
                    'encrypt_name' => true,
                    'source_image' => $data['full_path'],
                    'new_image' => $path.$imagename,
                    'maintain_ratio' => TRUE,
                    'width' => $resize[0],
                    'height' => $resize[1],
                    'quality' =>70,
                );

                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();
              
            } 

          unlink($path.$fixed_name);
          return $paths;
    }
    
}
