<?php
Class Purchase_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct('user_package_purchase');

    }
    
    public function getPurchaseData(array $where,$res = 'result'){
        $this->db->select('a.*,b.*,c.Lang,c.IsVerified,c.IsPaid');
        $this->db->from('user_package_purchase AS a');
        $this->db->join('user_payment AS b','a.PurchaseID = b.PurchaseID'); 
        $this->db->join('users AS c','c.UserID = a.UserID'); 
		if(is_array($where)){
        foreach($where as $key => $val){
		$this->db->where($key,$val);
		 }
		}
		$this->db->order_by('a.PurchaseID','DESC');

        return $this->db->get()->$res();
    }
	
	 public function delete($where)
    {
        $this->db->delete('user_package_purchase', $where);

    }
	
	public function update_payment_status($array,$where)
    {
        $this->db->update('user_payment', $array,$where);

    }

}
?>