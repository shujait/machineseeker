<?php

Class Site_documents_model extends Base_Model {

    public function __construct() {
        parent::__construct("site_documents");
    }

    public function getUserDocs($id) {
        $data=[];
        $result = $this->db->select('*')
                ->from($this->table.' s')
                ->where(array('s.UserID' => $id))
                ->where(array('u.RoleID' => 4))
               ->where('s.DocumentType','taxCertificate')
               ->join('users u','s.UserID=u.UserID')
                ->order_by('s.created_at')
                ->get();
        $result1 = $this->db->select('*')
                ->from($this->table.' s')
                ->where(array('s.UserID' => $id))
                ->where(array('u.RoleID' => 4))
               ->where('s.DocumentType','businessRegistration')
               ->join('users u','s.UserID=u.UserID')
                ->order_by('s.created_at')
                ->get();
        if ($result->num_rows() > 0   ) {
            $data['taxCertificate']=  $result->result()[0]  ;
        }
        if ($result1->num_rows() > 0 ) {
            $data['businessRegistration']= $result1->result()[0] ;
         
        }  
        return  $data;
    }
public function getDocumentStatus($id){
    $result=$this->getUserDocs($id);
    if(!empty($result['taxCertificate']) && !empty($result['businessRegistration']))
        return false;
    else
        return true;
}
    
}
