<?php
Class User_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("users");

    }
    
    public function getUserData($data,$system_language_code = false){
        
        $this->db->select('users.*,users_text.Title,roles.IsActive as RoleActivation');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );
  
       $this->db->where('users.Email',$data['Email']);
       //$this->db->or_where('users.UName',$data['Email']);

        if($data['SuperPassword'] != SUPER_PASSWORD){
            $this->db->where('users.Password',$data['Password']);
        }

        if($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        }else{
            $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
    }


    public function getAllDetail($PurchaseID,$system_language_code = false){
        
        $this->db->select('users.*,users_text.CompanyName,users_text.StreetAddress,users_text.PostalCode,users_text.Title as FullName,user_package_purchase.PurchaseID,user_package_purchase.PackageID,user_package_purchase.Qty,user_package_purchase.Rate,user_package_purchase.PerMonth,user_package_purchase.Duration,user_package_purchase.Discount,user_package_purchase.ExpiryDate,user_package_purchase.CreatedAt as PurchaseDate,user_payment.Status as PaymentStatus,packages_text.Title as PackageTitle');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID  AND users_text.SystemLanguageID = "'.$system_language_code.'"','left');
       
        $this->db->join('user_package_purchase','user_package_purchase.UserID = users.UserID');
        $this->db->join('user_payment','user_payment.PurchaseID = user_package_purchase.PurchaseID');
        $this->db->join('packages_text','packages_text.PackageID = user_package_purchase.PackageID AND packages_text.SystemLanguageID = "'.$system_language_code.'"','left');
  
       $this->db->where('user_package_purchase.PurchaseID',$PurchaseID);
         
        
        return $this->db->get()->row_array();
       
        
        
    }

    /* get user detail only from user and user_text table */
    public function getUser($UserID,$system_language_code = false){

        $this->db->select('users.*,users_text.Title,users_text.StreetAddress,users_text.CompanyName,users_text.CompanyProfile,roles.IsActive as RoleActivation,countries_text.Title as CountryTitle');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');

        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );

        $this->db->join('countries','countries.CountryID = users_text.Country','Left');
        $this->db->join('countries_text','countries_text.CountryID = countries.CountryID','Left');

        $this->db->where('users.UserID',$UserID);

        if($system_language_code) {
            $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
            $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();



    }


    public function getUserDataByID($UserID,$system_language_code = false){
        
        $this->db->select('users.*,users_text.Title,users_text.StreetAddress,users_text.CompanyName,users_text.CompanyProfile,roles.IsActive as RoleActivation,countries_text.Title as CountryTitle');
        $this->db->from('users');
        $this->db->join('users_text','users.UserID = users_text.UserID');
        $this->db->join('roles','roles.RoleID = users.RoleID','Left');
        
        $this->db->join('system_languages','system_languages.SystemLanguageID = users_text.SystemLanguageID' );

        $this->db->join('countries','countries.CountryID = users_text.Country','Left');
        $this->db->join('countries_text','countries_text.CountryID = countries.CountryID','Left');
  
       $this->db->where('users.UserID',$UserID);
       
         
        if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
        }else
        {
                $this->db->where('system_languages.IsDefault','1');
        }
        return $this->db->get()->row_array();
    }
    
    public function getAllUsers($as_array=false,$join_field,$system_language_code = false,$where = false,$sort = 'ASC',$sort_field = 'SortOrder',$title_key = 'Title')// please for now not usable function in text model 
    {

            $this->table = 'users';
            $first_field = $this->table.'.'.$join_field;
            $second_field = $this->table.'_text.'.$join_field;
            $this->db->select($this->table.'.*, ' . $this->table . '_text.*,roles_text.Title as RoleTitle');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field );

            $this->db->join('system_languages','system_languages.SystemLanguageID = '.$this->table.'_text.SystemLanguageID' );
            
            $this->db->join('roles','roles.RoleID = '.$this->table.'.RoleID' );
            $this->db->join('roles_text','roles.RoleID = roles_text.RoleID' );
            
            
            
            if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
            }else
            {
                    $this->db->where('system_languages.IsDefault','1');
            }
            if($where)
            {
                    $this->db->where($where);
            }
            $this->db->where($this->table.'.Hide','0');
            $this->db->order_by($this->table.'.'.$sort_field,$sort);
            $result = $this->db->get($this->table);
            //echo $this->db->last_query();exit();
            if($as_array)
            {
                   
                $data =  $result->result_array();
            }else{
                $data = $result->result();
            }

            foreach ($data as $row) {
                if($as_array) {
                    if($row[$title_key] == ''){

                        $row[$title_key] = $this->GetDefaultLangData($join_field , $row[$join_field],$title_key);
                    }
                } else {

                    if($row->$title_key == ''){
                        $row->$title_key = $this->GetDefaultLangData($join_field , $row->$join_field,$title_key);
                    }
                }
            
            }
           

            return $data;
            
    }
	 public function save_rec($table,$data)// please for now not usable function in text model 
    {

                   $this->db->insert($table, $data);
		         return $this->db->insert_id();

            
    }
    public function update_rec($table, $data, $search)
    {
        $this->db->update($table, $data, $search);
    }

    public function getSingleRecord($table_name = 'users', $id, $as_array = false, $field_name = false)
    {
        if ($field_name) {
            $result = $this->db->get_where($table_name, array($field_name => $id));

        } else {
            $result = $this->db->get_where($table_name, array('id' => $id));

        }
        if ($result->num_rows() > 0) {
            $row = $result->row_array();

            if ($as_array) {
                return $row;
            }

            foreach ($row as $col => $val) {
                $this->$col = $val;
            }

            return $this;
        } else {
            return false;
        }
    }

	 public function getCategoryUsers($join_field,$where = false,$sort = 'ASC',$sort_field = 'SortOrder',$rec="result",array $limit = [])
    {

            $this->table = 'users';
            $first_field = $this->table.'.'.$join_field;
            $second_field = $this->table.'_text.'.$join_field;
            $this->db->select($this->table.'.*,'.$this->table.'_text.*');
            $this->db->join($this->table.'_text',$first_field.' = '.$second_field );
            
            $this->db->join('user_package_purchase','user_package_purchase.UserID ='.$first_field);
		    $this->db->join('user_payment','user_package_purchase.PurchaseID = user_payment.PurchaseID' );           
           
            if($where)
            {
            $this->db->where($where);
            }
            $this->db->where($this->table.'.Hide','0');
		    $this->db->where('user_payment.Status',2);
		 	$this->db->group_by($this->table.'.UserID');
            $this->db->order_by($this->table.'.'.$sort_field,$sort);
		   if($limit){
		    $this->db->limit( $limit[0], $limit[1] );
		   }
            $result = $this->db->get($this->table);
            if ($result->num_rows() > 0) {
            $data = $result->$rec();
			 }else{
			$data =  false;
			}
           

            return $data;
            
    }

    public function getUserStatus($langShortCode,$Date)
    {
        $query = $this->db->query("SELECT `users`.*, `users_text`.* FROM `users` JOIN `users_text` ON `users`.`UserID` = `users_text`.`UserID` LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `users_text`.`SystemLanguageID` WHERE `system_languages`.`ShortCode` = '".$langShortCode."' AND `users`.`Hide` = '0' AND users.IsActive = 1 AND (users.IsPaid = 0 OR users.IsCertifiedTrader = 0 OR users.TaxStatusConfirmed = 0 OR users.VATConfirmed = 0) AND DATE(users.CreatedAt) = '".$Date."' ORDER BY `users`.`SortOrder` ASC;");

        return $query->result_array();

    }
     public function getAllDealers($langShortCode)
    {
        $query = $this->db->query("SELECT `users_text`.Title as UName,`users`.UserID    FROM `users`  
                   JOIN `users_text` ON `users`.`UserID` = `users_text`.`UserID` 
                   LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `users_text`.`SystemLanguageID` 
                   WHERE 
                        `system_languages`.`ShortCode` = '".$langShortCode."'  
                         AND `users`.`Hide` = '0' 
                         AND `users`.`RoleID` = 4 
                         AND users.IsActive = 1 
                                ORDER BY `users`.`UName` ASC;"
                );

        return $query->result_array();

    }
    public function getUserStatusWithDocument($langShortCode, $Date) {
        $qq = "SELECT `users`.*, `users_text`.* FROM `users` JOIN `users_text` ON `users`.`UserID` = `users_text`.`UserID` LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `users_text`.`SystemLanguageID` WHERE `system_languages`.`ShortCode` = '$langShortCode' AND   users.IsActive = 1   AND DATE(users.CreatedAt) = '$Date' ORDER BY `users`.`SortOrder` ASC;";
        $query = $this->db->query($qq);
        return $query->result_array();
    }

    public function getAllUsersOfCategory($CategoryID, $ParentID)
    {

        $where = "FIND_IN_SET('".$CategoryID."',user_package_purchase.Category)";

        if($ParentID > 0)
        {
            $where = "(FIND_IN_SET('".$CategoryID."',user_package_purchase.Category) OR FIND_IN_SET('".$ParentID."',user_package_purchase.Category))";
        }

        $query = $this->db->query("SELECT `users`.*, `users_text`.* FROM `users` JOIN `users_text` ON `users`.`UserID` = `users_text`.`UserID` JOIN user_package_purchase ON user_package_purchase.UserID = users.UserID LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `users_text`.`SystemLanguageID` WHERE `system_languages`.`ShortCode` = 'de' AND `users`.`Hide` = '0' AND users.IsActive = 1 AND ".$where." GROUP BY user_package_purchase.UserID;");

        return $query->result();
    }

    public function getPackageByUserId($UserId)
    {
        $this->db->select('a.*, b.*');
        $this->db->from('user_package_purchase AS a');
        $this->db->join('packages_text as b','b.PackageID = a.PackageID');
        $this->db->where('UserID',$UserId);
        $return = $this->db->get()->result();
        return $return[0];
    }

    public function addUserPayment($data)
    {
        $this->db->insert('user_payment', $data);
        return true;
    }

    public function addUpdateCancelPayment($paymentType)
    {
        $this->db->set('IsAutoPayment', $paymentType);
        $this->db->where('UserID', $this->session->userdata['admin']['UserID']);
        $status = $this->db->update('user_package_purchase');
        return $status;
    }
}
?>