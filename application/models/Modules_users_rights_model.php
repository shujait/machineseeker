<?php
Class Modules_users_rights_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("modules_users_rights");

    }
    
    
    
    
    public function getModulesWithRights($user_id,$system_language_code,$where = false){
        
            
            $this->db->select('modules.ModuleID,modules.Slug,modules.IconClass,modules.ParentID,modules_text.Title as ModuleTitle,modules_users_rights.*');
            $this->db->from('modules');
            $this->db->join('modules_text','modules_text.ModuleID = modules.ModuleID');
            
            $this->db->join('modules_users_rights','modules.ModuleID = modules_users_rights.ModuleID');
		    $this->db->join('system_languages','system_languages.SystemLanguageID = modules_text.SystemLanguageID' );
            if($system_language_code) {
                    $this->db->where('system_languages.ShortCode', $system_language_code);
            }else
            {
                    $this->db->where('system_languages.IsDefault','1');
            }
            $this->db->where('modules.Hide','0');
            $this->db->where('modules_users_rights.UserID',$user_id);
            
            
            if($where){
                $this->db->where($where);
            }
            
            $this->db->group_by('modules.ModuleID');
            
            $this->db->order_by('modules.SortOrder','ASC');
            $result = $this->db->get();
           //echo $this->db->last_query();exit;
            return $result->result_array();
    }

    
}