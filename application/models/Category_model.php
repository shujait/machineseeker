<?php
Class Category_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("categories");

    }

    public function SaveHomeCategory($CategoryID){
        $result = $this->db->insert('home_categories', array('CategoryID' => $CategoryID));
        return $result;
    }

    public function getall($system_language_code){
        //and categories.ParentID = 0
        //$data = $this->db->query('SELECT * FROM categories inner join categories_text on categories_text.CategoryID = categories.CategoryID where categories.IsActive = 1 and categories.ParentID = 0 group by categories.CategoryID order by categories_text.Title')->result_array();

        $data = $this->db->query('Select * from system_languages inner join categories_text on system_languages.SystemLanguageID = categories_text.SystemLanguageID inner join categories on categories_text.CategoryID = categories.CategoryID where categories.IsActive = 1 and categories.ParentID = 0 AND system_languages.ShortCode ="'.$system_language_code.'" group by categories.CategoryID order by categories_text.Title')->result_array();
        return $data;
    }

    public function gethomecategory($system_language_code){
        //$data = $this->db->query('SELECT * FROM home_categories inner join categories on home_categories.CategoryID = categories.CategoryID inner join categories_text on categories_text.CategoryID = categories.CategoryID where categories.IsActive = 1 group by categories.CategoryID order by categories_text.Title')->result_array();

        $data = $this->db->query('SELECT * from system_languages inner join categories_text on system_languages.SystemLanguageID = categories_text.SystemLanguageID inner join categories on categories_text.CategoryID = categories.CategoryID inner join home_categories on home_categories.CategoryID = categories.CategoryID where categories.IsActive = 1 AND system_languages.ShortCode = "'.$system_language_code.'" group by categories.CategoryID order by categories_text.Title')->result_array();
        return $data;
    }

    public function selectedhomecategory(){
        $data = $this->db->query('SELECT * FROM home_categories')->result_array();
        return $data;
    }

    //Update Home Categories to display in home screen
    public function DisplayHomeCategories($CategoryIDs){

        $deleted = $this->db->truncate('home_categories'); //$this->db->delete('home_categories');

        if($deleted){
            foreach($CategoryIDs as $CategoryID){
                //if(isset($CategoryID) && !empty($CategoryID)){
                //$update = $this->db->set('Display', 1)->where('CategoryID', $CategoryID)->update('home_categories');
                //}
                $data = array(
                    'CategoryID' => $CategoryID,
                    'Display' => 1
                );
                $inserted = $this->db->insert('home_categories', $data);
            }
        }
        if($inserted){
            return true;
        }else{
            return false;
        }
    }
    //Display off the category who is not active
    public function UpdateHomeCategories($CategoryID){
        $update = $this->db->set('Display', 0)->where('CategoryID', $CategoryID)->update('home_categories');
        return true;
    }

    public function DeleteHomeCategory($CategoryID){
        $deleted = $this->db->where('CategoryID', $CategoryID)->delete('home_categories');
        return $deleted;
    }
}