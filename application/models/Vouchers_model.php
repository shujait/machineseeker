<?php
Class Vouchers_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("voucherses");

    }

    public function getVoucherById($VouchersID,$system_language_code = false)
    {
        $this->db->select('voucherses.*');
        $this->db->from('voucherses');
        $this->db->where('voucherses.VouchersID',$VouchersID);
        return $this->db->get()->row_array();
    }

    public function updateVoucher($data)
    {
        $this->db->set('Title', $data['Title']);
        $this->db->set('IsActive', $data['IsActive']);
        $this->db->set('DiscountType', $data['DiscountType']);
        $this->db->set('VoucherValue', $data['VoucherValue']);
        $this->db->set('ExpiryDate', $data['ExpiryDate']);
        $this->db->set('UpdatedBy', $data['UpdatedBy']);
        $this->db->set('UpdatedAt', date('Y-m-d H:i:s'));
        $this->db->where('VouchersID', $data['VouchersID']);
        return $this->db->update('voucherses');
    }

    public function getVoucherCodeDetail($VouchersCode)
    {
        $this->db->select('voucherses.*');
        $this->db->from('voucherses');
        $this->db->where('voucherses.VoucherCode',$VouchersCode);
        return $this->db->get()->row_array();
    }


}