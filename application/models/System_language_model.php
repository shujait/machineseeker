<?php
Class System_language_model extends Base_Model
{
    
   
    public function __construct()
    {
        parent::__construct("system_languages");
        
    }
    
    
    
    
    public function getAllLanguages($system_language_id = 1){
        
        $this->db->select('system_languages.*,system_languages_text.SystemLanguageTitle');
        $this->db->from($this->table);
        $this->db->join('system_languages_text','system_languages_text.LanguageID = system_languages.SystemLanguageID');
        $this->db->where('system_languages_text.SystemLanguageID',$system_language_id);//1 for english
        $this->db->where('Hide',0);
        $this->db->order_by('IsDefault','DESC');

        
        return $this->db->get()->result();
        
    } 
    
}

    