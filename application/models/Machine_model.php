<?php
    Class Machine_model extends Base_Model
    {
        public function __construct()
        {
            parent::__construct("machines");
            // Set table name
            $this->table = 'machines';
            // Set orderable column fields

            $this->column_order = array(null, 'machines_text.Title','UName','MachineID','PublishDate','CreatedAt','IsActive', 'UsersTextTitle');
            // Set searchable column fields
            $this->column_search = array('machines_text.Title','users.UName','machines.MachineID','machines.PublishDate','machines.CreatedAt','machines.IsActive');
            // Set default order
            $this->order = array('machines_text.Title' => 'asc');

        }


        public function getInquiriesOfMachine($UserID)
        {
        	$query = $this->db->query("SELECT `machines`.*, `machines_text`.*, `inquiries`.* FROM `machines` JOIN `machines_text` ON `machines`.`MachineID` = `machines_text`.`MachineID` JOIN `inquiries` ON `inquiries`.`MachineID`= `machines`.`MachineID` LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `machines_text`.`SystemLanguageID` JOIN `users` ON `users`.`UserID` = `machines`.`UserID` AND `users`.`IsActive` = 1 WHERE `system_languages`.`ShortCode` = 'de' AND `machines`.`UserID` = ".$UserID." AND `machines`.`Hide` = '0' GROUP BY `inquiries`.`InquiryID` ORDER BY `machines`.`SortOrder` ASC");

        	return $query->result();
        }
        public function getMachineWithWhere($id)
        {
   
           if($id['machine'][0]!=$id['item'][0])
            {
            $this->db->select('*');
            $this->db->from('machines');
            $this->db->where('MachineID',$id['machine'][0]);
                  $query = $this->db->get();
            $result= $query->result_array();
            $machineImage= $result[0]['FeaturedImage'];
            if(!empty( $result)){
                $this->db->select('ImageName');
                $this->db->from('site_images');
                $this->db->where('SiteImageID',$id['item'][0]);
                $query = $this->db->get();
                $result= $query->result_array();
                $siteImage= $result[0]['ImageName'];
                if(!empty($siteImage)){
                    $this->db->set('ImageName', $machineImage);
                    $this->db->where('SiteImageID', $id['item'][0]);
                    $this->db->update('site_images');

                    $this->db->set('FeaturedImage', $siteImage);
                    $this->db->where('MachineID', $id['machine'][0]);
                    $this->db->update('machines');
                }
            }
           
        }
            return true;
    }
         public function updateMachine($array)
        {
            $this->db->select('*');
            $this->db->from('machines');
            $this->db->where($array);
            $query = $this->db->get();
        }
        
    public function updateMachinebyId($table, $data, $where) {
       
        $this->db->where($where); 
        return $this->db->update($table, $data);
      }

    public function getCategoryBytext($text)
        {  
           $str="SELECT c.CategoryID FROM categories c JOIN `categories_text` ct ON `c`.`CategoryID` = `ct`.`CategoryID` 
           WHERE ct.Title LIKE '%$text%' and ( ct.SystemLanguageID=2 or ct.SystemLanguageID=1) and ParentID!=0 limit 1;";
          $query = $this->db->query($str);
           return $query->result_array();
        } 
    public function getMachineIdByTitle($text) {
        $val = trim($text['Title_de']);
        $str = 'SELECT
                `machines`.`MachineID`
            FROM
                `machines`
            JOIN `machines_text` ON `machines`.`MachineID` = `machines_text`.`MachineID`
            WHERE
                `machines_text`.`SystemLanguageID` =1 AND 
                    `machines_text`.`Title` = "' . $val . '"
                 AND `machines`.`Hide` = "0" AND `machines`.`IsActive` = "1"
           ';
        $query = $this->db->query($str);
        if ($query->num_rows() > 0 ){
            return $query->row_array();
        }
        return false;
    }

    public function addCategoryBytext($text) {
        $str = "SELECT c.CategoryID FROM categories c JOIN `categories_text` ct ON `c`.`CategoryID` = `ct`.`CategoryID` 
           WHERE ct.Title LIKE '%$text%' and ( ct.SystemLanguageID=2 or ct.SystemLanguageID=1) and ParentID=0 limit 1;";
        $query = $this->db->query($str);
        $res = $query->result_array();
        $id = $res[0]['CategoryID'];
        if (!empty($id)) {
            $data = array(
                'ParentID' => $id,
                'CategoryType' => '0',
                'CreatedBy' => $this->session->userdata['admin']['UserID'],
                'UpdatedBy' => $this->session->userdata['admin']['UserID'],
                'Slug' => $text,
                'SortOrder' => 100,
                'Hide' => 0,
                'IsActive' => 1,
            );
            $this->db->insert('categories', $data);
            $categoryId = $this->db->insert_id();
            $data = array(
                'CategoryID' => $categoryId,
                'Title' => $text,
                'Content' => $text,
                'SystemLanguageID' => 1,
                'CreatedBy' => $this->session->userdata['admin']['UserID'],
                'UpdatedBy' => $this->session->userdata['admin']['UserID']
            );
            $this->db->insert('categories_text', $data);
            $data = array(
                'CategoryID' => $categoryId,
                'Title' => '',
                'Content' => '',
                'SystemLanguageID' => 2,
                'CreatedBy' => $this->session->userdata['admin']['UserID'],
                'UpdatedBy' => $this->session->userdata['admin']['UserID']
            );
            $this->db->insert('categories_text', $data);
            return $categoryId;
        } else
            return 0;
    }

    public function getMachineTitle($val){
            
            $str='SELECT
                `machines_text`.`Title` as mTitle 
            FROM
                `machines`
            JOIN `machines_text` ON `machines`.`MachineID` = `machines_text`.`MachineID`
            JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `machines_text`.`SystemLanguageID`
            JOIN `users` ON `users`.`UserID` = `machines`.`UserID` AND `users`.`IsActive` = 1
            LEFT JOIN `categories` ON `categories`.`CategoryID` = `machines`.`CategoryID`
            LEFT JOIN `categories_text` ON `categories_text`.`CategoryID` = `categories`.`CategoryID`
            WHERE
                `system_languages`.`ShortCode` ="de" AND 
                (`machines_text`.`Title` LIKE "%'.$val.'%" OR
                `machines`.`ManufacturerYear` LIKE "%'.$val.'%" OR
                `machines`.`Price` LIKE "%'.$val.'%" OR 
                `machines`.`ReferenceNumber` LIKE "%'.$val.'%" OR 
                `machines`.`SerialNumber` LIKE "%'.$val.'%" OR
                `machines_text`.`keywords` LIKE "%'.$val.'%" OR
                `machines_text`.`Description` LIKE "%'.$val.'%") AND 
                `machines`.`Hide` = "0" AND 
                `machines`.`IsActive` = 1
            GROUP BY
                `machines`.`MachineID`
            ORDER BY
                `machines`.`MachineID`
            DESC
            LIMIT 10';
            $query = $this->db->query($str);

        	return $query->result_array();
        }

         public function getcategoriesTitle($val)
        {
            // $str='SELECT
            //     `categories_text`.`Title` as cTitle 
            // FROM
            //     `machines`
            // JOIN `machines_text` ON `machines`.`MachineID` = `machines_text`.`MachineID`
            // JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `machines_text`.`SystemLanguageID`
            // JOIN `users` ON `users`.`UserID` = `machines`.`UserID` AND `users`.`IsActive` = 1
            // LEFT JOIN `categories` ON `categories`.`CategoryID` = `machines`.`CategoryID`
            // LEFT JOIN `categories_text` ON `categories_text`.`CategoryID` = `categories`.`CategoryID`
            // WHERE
            //     `system_languages`.`ShortCode` ="de" AND 
            //         `categories_text`.`Title` LIKE "%'.$val.'%" 
            //      AND `machines`.`Hide` = "0" AND `machines`.`IsActive` = "1"
            // GROUP BY
            //     `machines`.`MachineID`
            // ORDER BY
            //     `machines`.`MachineID`
            // DESC
            // LIMIT 10';

            $str = 'SELECT
            `categories_text`.`Title` AS cTitle
        FROM
            `categories_text`
        LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `categories_text`.`SystemLanguageID`
        WHERE
            `system_languages`.`ShortCode` = "de" AND `categories_text`.`Title` LIKE "%'.$val.'%"';
            $query = $this->db->query($str);

        	return $query->result_array();
        }
         public function getManufacturer($val)
        {
            $str='SELECT
                `machines`.`Manufacturer` as Manufacturer 
            FROM
                `machines`
            JOIN `machines_text` ON `machines`.`MachineID` = `machines_text`.`MachineID`
            JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `machines_text`.`SystemLanguageID`
            JOIN `users` ON `users`.`UserID` = `machines`.`UserID` AND `users`.`IsActive` = 1
            LEFT JOIN `categories` ON `categories`.`CategoryID` = `machines`.`CategoryID`
            LEFT JOIN `categories_text` ON `categories_text`.`CategoryID` = `categories`.`CategoryID`
            WHERE
                `system_languages`.`ShortCode` ="de" AND 
                    `machines`.`Manufacturer` LIKE "%'.$val.'%" 
                 AND `machines`.`Hide` = "0" AND `machines`.`IsActive` = "1"
            GROUP BY
                `machines`.`MachineID`
            ORDER BY
                `machines`.`MachineID`
            DESC
            LIMIT 10';
            $query = $this->db->query($str);

        	return $query->result_array();
        }


        public function getMachines($val)
        {

            $str='SELECT machines.*, machines_text.*, categories.*, categories_text.Title as categoriesTextTitle,
        categories_text.ShortTitle, categories_text.CategoryTextID, categories_text.Content, categories_text.Seo
            FROM
                `machines`
            JOIN `machines_text` ON `machines`.`MachineID` = `machines_text`.`MachineID`
            LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `machines_text`.`SystemLanguageID`
            LEFT JOIN `categories` ON `categories`.`CategoryID` = `machines`.`CategoryID`
            LEFT JOIN `categories_text` ON `categories_text`.`CategoryID` = `categories`.`CategoryID`
            WHERE
                `system_languages`.`ShortCode` ="de" AND 
                (`machines_text`.`Title` LIKE "%'.$val.'%" OR
                `machines`.`ManufacturerYear` LIKE "%'.$val.'%" OR
                `machines`.`Price` LIKE "%'.$val.'%" OR 
                `machines`.`ReferenceNumber` LIKE "%'.$val.'%" OR 
                `machines`.`SerialNumber` LIKE "%'.$val.'%" OR
                `machines_text`.`keywords` LIKE "%'.$val.'%" OR
                `machines_text`.`Description` LIKE "%'.$val.'%" OR 
                `categories_text`.`Title` LIKE "%'.$val.'%" OR 
                `categories_text`.`ShortTitle` LIKE "%'.$val.'%" OR 
                `categories_text`.`Content` LIKE "%'.$val.'%") AND 
                `machines`.`Hide` = "0" AND 
                `machines`.`IsActive` = 1
            GROUP BY
                `machines`.`MachineID`
            ORDER BY
                `machines`.`MachineID`
            DESC
            LIMIT 5';
            $query = $this->db->query($str);

        	return $query->result_array();
        }
    /*
     * Perform the SQL queries needed for an server-side processing requested
     * @param $_POST filter data based on the posted parameters
     */
        private function _get_datatables_query($postData, $where = false, $system_language_code = false)
        {

            $this->db->select('machines.*, machines_text.Title, users.UName, users_text.Title as UsersTextTitle');
            $this->db->from('machines');

            $i = 0;
            // loop searchable columns
            foreach($this->column_search as $item){
                // if datatable send POST for search
                if($postData['search']['value']){
                    // first loop
                    if($i===0){
                        // open bracket
                        $this->db->group_start();
                        $this->db->like($item, $postData['search']['value']);
                    }else{
                        $this->db->or_like($item, $postData['search']['value']);
                    }

                    // last loop
                    if(count($this->column_search) - 1 == $i){
                        // close bracket
                        $this->db->group_end();
                    }
                }
                $i++;
            }
            $this->db->join('machines_text', 'machines_text.MachineID = machines.MachineID');
            $this->db->join('users','users.UserID = machines.UserID');
            $this->db->join('users_text', 'users_text.UserID = machines.UserID');
            $this->db->join('system_languages','system_languages.SystemLanguageID = machines_text.SystemLanguageID','left');
            $this->db->join('site_images', 'site_images.FileID = machines.MachineID', 'left');
            if($where)
            {
                $this->db->where($where);
            }
            if($system_language_code) {
                $this->db->where('system_languages.ShortCode', $system_language_code);
            } else {
                $this->db->where('system_languages.IsDefault','1');
            }
            $this->db->where('users.IsActive = 1');

            if(isset($postData['order'])){
                $this->db->order_by($this->column_order[$postData['order']['0']['column']], $postData['order']['0']['dir']);
            }else if(isset($this->order)){
                $order = $this->order;
                $this->db->order_by(key($order), $order[key($order)]);
            }
            $userId = $this->session->userdata['admin']['UserID'];
            if($userId !== '1')
            {
                $this->db->where('machines.UserID', $userId);
            }
            $this->db->where($this->table.'.Hide','0');
            $this->db->group_by('machines.MachineID');
        }

        /*
     * Fetch members data from the database
     * @param $_POST filter data based on the posted parameters
     */
        public function getRows($postData, $where, $language =false){
            $this->_get_datatables_query($postData, $where, $language);
            if($postData['length'] != -1){
                $this->db->limit($postData['length'], $postData['start']);
            }
            $query = $this->db->get();
            return $query->result();
        }

        /*
         * Count all records
         */
        public function countAll(){
            $this->db->from($this->table);
            return $this->db->count_all_results();
        }

        /*
         * Count records based on the filter params
         * @param $_POST filter data based on the posted parameters
         */
        public function countFiltered($postData){
            $this->_get_datatables_query($postData);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function getMachinesByUserId($Uid)
        {
            $this->db->select('machines.*');
            $this->db->where('machines.UserID', $Uid);
            $query = $this->db->get('machines'); // Produces: SELECT MIN(age) as age FROM members
            return $query->num_rows();
        }


        public function loadMoreMachines($page, $limit = 5)
        {
            $offset = ($page - 1) * $limit;

            $str='SELECT machines.*, machines_text.*, categories.*, categories_text.Title as categoriesTextTitle,
        categories_text.ShortTitle, categories_text.CategoryTextID, categories_text.Content, categories_text.Seo
            FROM
                `machines`
            JOIN `machines_text` ON `machines`.`MachineID` = `machines_text`.`MachineID`
            LEFT JOIN `system_languages` ON `system_languages`.`SystemLanguageID` = `machines_text`.`SystemLanguageID`
            LEFT JOIN `categories` ON `categories`.`CategoryID` = `machines`.`CategoryID`
            LEFT JOIN `categories_text` ON `categories_text`.`CategoryID` = `categories`.`CategoryID`
            WHERE
                `system_languages`.`ShortCode` ="de" AND 
                (`machines_text`.`Title` LIKE "%'.$val.'%" OR
                `machines`.`ManufacturerYear` LIKE "%'.$val.'%" OR
                `machines`.`Price` LIKE "%'.$val.'%" OR 
                `machines`.`ReferenceNumber` LIKE "%'.$val.'%" OR 
                `machines`.`SerialNumber` LIKE "%'.$val.'%" OR
                `machines_text`.`keywords` LIKE "%'.$val.'%" OR
                `machines_text`.`Description` LIKE "%'.$val.'%" OR 
                `categories_text`.`Title` LIKE "%'.$val.'%" OR 
                `categories_text`.`ShortTitle` LIKE "%'.$val.'%" OR 
                `categories_text`.`Content` LIKE "%'.$val.'%") AND 
                `machines`.`Hide` = "0" AND 
                `machines`.`IsActive` = 1
            GROUP BY
                `machines`.`MachineID`
            ORDER BY
                `machines`.`MachineID`
            DESC
            LIMIT '.$offset.', 5';

            $query = $this->db->query($str);

            return $query->result_array();
        }

    }