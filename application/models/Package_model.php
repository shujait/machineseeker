<?php
Class Package_model extends Base_Model
{
    public function __construct()
    {
        parent::__construct("packages");

    }

    public function getPackage($packageId)
    {
        $this->db->select('a.*, b.*');
        $this->db->from('packages AS a');
        $this->db->join('packages_text as b','b.PackageID = a.PackageID');
        $this->db->where('a.PackageID',$packageId);
        $return = $this->db->get()->result();
        return $return[0];
    }

    public function upgradePackage($data, $PurchaseID)
    {
        $this->db->set('PackageID', $data['newPackage']);
        $this->db->set('Qty', $data['Qty']);
        $this->db->set('Rate', $data['Rate']);
        $this->db->set('PerMonth', $data['PerMonth']);
        $this->db->set('Duration', $data['Duration']);
        $this->db->set('Discount', $data['Discount']);
        $this->db->set('ExpiryDate', $data['ExpiryDate']);
        $this->db->set('PaymentMethod', $data['PaymentMethod']);
        $this->db->set('IsExpiryEmailSent', $data['IsExpiryEmailSent']);
        $this->db->where('PurchaseID', $PurchaseID);
        $this->db->update('user_package_purchase');
        return true;
    }

    public function addPackagePurchaseHistory($data)
    {
        $this->db->insert('user_package_purchase_history', $data);
        return true;
    }




}