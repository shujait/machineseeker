var url = window.location;
var host = url.host;
var page = url.pathname.split('/');
var pagename = page_action =  image = filename = '';
if(host == "localhost"){
	pagename  = page[4];
	page_action  = page[5];
}else{
	pagename = page[4];
	page_action = page[5];
}
function get_mail_shortcode_list() {
			var arr = [];
	        var n = 0;

       $.each(JSON.parse(mail_short_code), function(k,v) {
		  	n++;
		   if(pagename == 'page' && n >= 26){
				arr.push({
						text: k, 
						value: v
					});
			}else if(pagename == 'email_template' && n < 26){

				arr.push({
						text: k, 
						value: v
					});
	   			}
		   });
        return arr;
}
String.prototype.htmlProtect = function() {
  var replace_map;

  replace_map = {
    'ä': 'ae',
    'ü': 'oe',
    'ö': 'ue',
	'ß': 'ss' 
  };
	
  return this.replace(/[äüöß]/g, function(match) { // be sure to add every char in the pattern
    return replace_map[match];
  });
}
$(document).ready(function (){
	$('.number').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (!regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        return false;
        }
    });			  
	if(page_action == 'add' || page_action == 'edit'){
$(window).on("beforeunload", function() {
			return "";

		});
	}
 tinymce.init({
    selector: "textarea:not(#GoogleAdCode)",
    language: SystemLanguage,
    theme: "modern",
    paste_data_images: true,
	relative_urls : false,
	 remove_script_host : false,
	 convert_urls : true, 
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview | forecolor backcolor emoticons,shortcode",
	image_advtab: true,
    setup: function (editor) {
	editor.on('change', function () {
	    tinymce.triggerSave();
     });	

	if(pagename == 'email_template' || pagename == 'page'){
    editor.addButton('shortcode', {
      type: 'listbox',
      text: 'Insert shortcode',
      icon: '',
	  tooltip: "Shortcode for Email",	
      onselect: function (e) {
        editor.insertContent(this.value());
      },
      'values': get_mail_shortcode_list(),
    });
	 }
    },
    file_picker_callback: function(callback, value, meta) {
		   show_popup();
		   $(document).on('click','.insert_image' ,function() {
           image = $(".mlib-selected-thumb").attr('mlib-url');
		   filename = $(".mlib-selected-thumb").attr('mlib-filename');	   
           callback(image, { alt: filename });
 		   $('#mlib-lightbox').hide();
        });
      }
		
		

});

 //tinymce.destroy('textarea#GoogleAdCode');
 //tinymce.execCommand('mceRemoveControl', true, 'GoogleAdCode');
 $(".add-category").click(function (){
		
		$(".cat-title").text($(".category:checked").data('title'));
		$("input[name='category']").val($(".category:checked").val());
		$(".close").click();

	});
 $(document).on('click', '.remove_image', function () {
        var image_id       = $(this).attr('data-image-id');
        var image_path       = $(this).attr('data-image-path');
        var image_type       = $(this).attr('data-image-type');
        $(".delete_url").attr('data-modal-image-id', image_id);
        $(".delete_url").attr('data-modal-image-path', image_path);
        $(".delete_url").attr('data-image-type', image_type);
        $('#DeleteArticleImage').modal('show');

    });	
 $("#filer_input1,#filer_input2").filer({
	  limit: null,
	 addMore: true,
        maxSize: null,
        extensions:  ['jpg', 'jpeg', 'png', 'pdf','doc','docx','xls','xlsx'],
        changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list sorting2 jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li>{{fi-progressBar}}</li>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        }
	 
 });
 $("#filer_input1,#filer_input2").change(function () {
      $('.sorting2').sortable(
      {
        stop: function (event, ui) {
              idsInOrder = $(".sorting2").sortable("toArray",{attribute: 'data-jfiler-index'});
    $('#sortArray').val(idsInOrder);
      }}
     );
});
 $("#filer_input_single,#filer_input_single1").filer({
	  limit: 1,
        maxSize: null,
        extensions:  ['jpg', 'jpeg', 'png', 'pdf','doc','docx','xls','xlsx'],
        changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><a class="jFiler-input-choose-btn blue">Browse Files</a></div></div>',
        showThumbs: true,
        theme: "dragdropbox",
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li>{{fi-progressBar}}</li>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            itemAppend: '<li class="jFiler-item">\
                            <div class="jFiler-item-container">\
                                <div class="jFiler-item-inner">\
                                    <div class="jFiler-item-thumb">\
                                        <div class="jFiler-item-status"></div>\
                                        <div class="jFiler-item-info">\
                                            <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                            <span class="jFiler-item-others">{{fi-size2}}</span>\
                                        </div>\
                                        {{fi-image}}\
                                    </div>\
                                    <div class="jFiler-item-assets jFiler-row">\
                                        <ul class="list-inline pull-left">\
                                            <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                        </ul>\
                                        <ul class="list-inline pull-right">\
                                            <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                        </ul>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: false,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        }
	 
 });	
 $(window).bind("load", function() {
	
		$(".menu-list li > .menu-remove,.menu-list li >  .menu-type").show();    
	  var  page = window.location.pathname.split('/')[4];
		if(page != 'add'){
			updateOutput();
		}
		});

	$(document).on("click",".draggable",function(){
		var id = $(this).data('id');
		var type = $(this).data('menu-type');
		var link = $(this).data('menu-link');
		
		if($(".menu-list").find('[data-id="'+id+'"][data-menu-type="'+type+'"]').length == 0){
		$(".menu-list").append($(this)[0].outerHTML);
			$(".menu-list li").removeClass("draggable").addClass('dd-item');
			$(".menu-list li > div").addClass('dd-handle');
			$(".menu-list li > .menu-remove,.menu-list li > .menu-type").show();
			updateOutput();
		   }else{
			showWarning(duplicate_page);
		   }
	});
	
    $(document).on("click",".menu-remove",function(){
	console.log('remove');	
        $($(this).parent()).remove();
		 updateOutput();
	});
    var updateOutput = function(e)

    {
		 var data = [];

		jQuery('.dd-item').each(function(){
	console.log('add');	

			var id 		= jQuery(this).attr('data-id');
			
			var menu_type = jQuery(this).attr('data-menu-type');
			var link = jQuery(this).attr('data-link');
			var stitle = jQuery(this).attr('data-subtitle');

			var parent  = jQuery(this).parent().parent().attr('data-id');

			if(typeof parent == 'undefined')
				parent = 0;
                        if(typeof link == 'undefined')
				link = '';
                        if (typeof stitle == 'undefined')
                                    stitle = '';
            var menu = {'id':id,'parent':parent,'menutype':menu_type,'link':link,'subtitle':stitle};

            data.push(menu);

		}); 
          if(data.length == 0){
			  data = "";
		  }else{
			  data = JSON.stringify(data);
		  }
        jQuery('.top_menu').val(data);
    };

	

    // activate Nestable for list 1

    jQuery('#nestable').nestable({

        group: 1

    }).on('change', updateOutput);

    $(".form_data").submit(function (e) {
		
        e.preventDefault();
		$.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
        $form = $(this);

        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                   showSuccess(result.success);
                }
                if (result.reset) {
                    $form[0].reset();
                }
                if (result.reload) {
                    setTimeout(function () {
                        window.location.reload();
                    }, 1000);
                }
                if (result.redirect) {
                    setTimeout(function () {
                        window.location.href = base_url + result.url;
                    }, 1000);
                }
				$(window).off("beforeunload");

            },
            complete: function () {
                $.unblockUI();
            }
        });
    });

    if ($('.jFiler-input-text').length > 0) {

        $('.jFiler-input-text').hide();
    }

			
$(".youtube_link").change(function (){
				 $("#add_url_Video iframe").hide();
				if($(this).val() != ""){
				   var a = ($(this).val().split('=')[1] || '').split('?')[0];
					a = 'https://www.youtube.com/embed/'+a;
					$(this).val(a);
				   $("#add_url_Video iframe").attr("src",a).show();  
				}
			});

$(".genTitle").change(function (){
    var title = $("#title").val();
    var manufacturer = $("#manufacturer").val();
    var machine_type = $("#machine_type").val();
    var category = $("#category option:selected").text();

    if(machine_type != '' && manufacturer != '' && category != '' && title == '')
    {
        $("#title").val(category+' '+manufacturer+' '+machine_type);
        $("#title").parents('.label-floating').removeClass('is-empty');
    }
});

$(".genTitleOther").change(function (){
    var title = $("#title").val();
    //var manufacturer = $("#manufacturer").val();
    //var machine_type = $("#machine_type").val();
    var category = $("#category option:selected").text();
    if(category != '')
    {
        $("#title").val(category);
        $("#title").parents('.label-floating').removeClass('is-empty');
    }
});

	$('[data-toggle="tooltip"]').tooltip();   
        
        
        
        
     
});
    function addCategories() {
    var sublink = $('#subLink').val();
    var subtitle = $('#subTitle').val();
    var subhtml = ' ';
    if ((!subtitle )) {
       $('#subTitle').focus(); return 0;
    }
    if (!(  sublink)) {
      $('#subLink').focus(); return 0;
    }
        if ((subtitle && sublink)) {
        subhtml =
                '<li class="dd-item" data-menu-type="4" data-id="0" data-link="' + sublink + '" data-subtitle="' + subtitle + '">' +
                '<span class="menu-remove pull-right" style="display: inline;">x</span>' +
                '<span class="menu-type pull-right" style="display: inline;">' +
                'ExternalLink ' + '</span><div class="dd-handle">' + subtitle + '</div></li>';
        $('.dd-list').append(subhtml);

        var data = [];

        jQuery('.dd-item').each(function () {
            console.log('add');

            var id = jQuery(this).attr('data-id');

            var menu_type = jQuery(this).attr('data-menu-type');
            var link = jQuery(this).attr('data-link');
            var stitle = jQuery(this).attr('data-subtitle');

            var parent = jQuery(this).parent().parent().attr('data-id');

            if (typeof parent == 'undefined')
                parent = 0;
            if (typeof link == 'undefined')
                link = '';
            if (typeof stitle == 'undefined')
                stitle = '';

            var menu = {'id': id, 'parent': parent, 'menutype': menu_type, 'link': link,'subtitle': stitle};

            data.push(menu);

        });
        if (data.length == 0) {
            data = "";
        } else {
            data = JSON.stringify(data);
        }
        jQuery('.top_menu').val(data);
        $('#subLink').val('');
     $('#subTitle').val('');
    } 
}
function showSuccess(message_text) {
    type = ['','info','success','warning','danger','rose','primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">'+message_text+'</span>'

    },{
        type: type[2], // 2nd index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function showError(message_text) {
    type = ['','info','success','warning','danger','rose','primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">'+message_text+'</span>'

    },{
        type: type[4],// 4th index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function showWarning(message_text) {
    type = ['','info','success','warning','danger','rose','primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">'+message_text+'</span>'

    },{
        type: type[3],// 4th index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function deleteDatatableRecord(id, actionUrl, reloadUrl) {
    //id can contain comma separated ids too.

    if (confirm(delete_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {
                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                }
            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }
}

function deleteRecord(id, actionUrl, reloadUrl) {
    //id can contain comma separated ids too.

    if (confirm(delete_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                    $('#' + id).remove();
                    setTimeout(function () {
                    document.location.reload();
                }, 1000);

                    
                }


                if (reloadUrl != "") setTimeout(function () {
                    document.location.href = reloadUrl;
                }, 1000);

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}
function publishRecord(id, actionUrl, reloadUrl) {

        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'publish'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                    $('#' + id).remove();
                    setTimeout(function () {
                    document.location.reload();
                }, 1000);

                    
                }


                if (reloadUrl != "") setTimeout(function () {
                    document.location.href = reloadUrl;
                }, 1000);

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
     

}

function deleteInquiry(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm(delete_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'inquiry_delete'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                    $('#' + id).remove();
                }


                if (reloadUrl != "") setTimeout(function () {
                    document.location.href = reloadUrl;
                }, 1000);

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}

function deleteImage(id, actionUrl) {

    //id can contain comma separated ids too.

    if (confirm("Are you sure you want to delete?")) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete_image'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {


                if (result.error != 'false') {
                    alert('There is something went wrong');

                } else {
                    $('#img-' + id).remove();
                    alert('Deleted Successfully');
                }


            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}

function redirect(redirect_to) {
    redirect_to = base_url + redirect_to;
    window.location.href = redirect_to;
}

 function initMap() {
        var input = document.getElementById('location');
      
        var autocomplete = new google.maps.places.Autocomplete(input);
        var place = autocomplete.getPlace();
		   
        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
		google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
			var short_name = place.address_components[place.address_components.length-1].short_name;
			$("#country_short").val(short_name);
			$("#lat").val(place.geometry.location.lat());
			$("#lng").val(place.geometry.location.lng());
        });   
		   
      }


// Sub Category structure

$(document).on('change', '.getSubCategory', function () {
    var curr = $(this);
    var CategoryID = curr.val();
    var CategoryType = curr.attr('data-type');
    $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
    });

    $.ajax({
        type: "POST",
        url: base_url + 'cms/category/getSubCategory',
        data: {
            'CategoryID': CategoryID,
            'CategoryType': CategoryType
        },
        dataType: "json",
        cache: false,
        //async:false,
        success: function (result) {


            if (result.success != 'false') {
                //alert('There is something went wrong');
                if(CategoryType == 'category')
                {
                    $('#subCategoryDiv').html('');             
                    $('#subSubCategoryDiv').html('');
                    curr.removeAttr('id');
                    curr.removeAttr('name');
                    $('#subCategoryDiv').html(result.option);
                    $('#subCategoryDiv').find('select#category').addClass('getSubCategory');
                    if(result.option == '')
                    {
                        curr.attr('id','category');
                        curr.attr('name','category');
                    }
                    $('.select2').select2();   
                                            
                    if(CategoryID=='111')
                    {
                         $('.non-services').hide();
                         $('.services').show();
                    }else{
                        $('.services').hide();
                         $('.non-services').show();
                    }
                }
                else if(CategoryType == 'subcategory')
                {          
                    $('#subSubCategoryDiv').html('');
                    curr.removeAttr('id');
                    curr.removeAttr('name');
                    $('#subSubCategoryDiv').html(result.option); 
                    if(result.option == '')
                    {
                        curr.attr('id','category');
                        curr.attr('name','category');
                    }
                    $('.select2').select2();
                }

            }


        },
        complete: function () {
            $.unblockUI();
        }
    });
  
});
    $(".closeBtn").click(function(){
        var keyss=jQuery(this).attr("id");
        $.ajax({
            url: base_url +'cms/user/destroySession',
            type: "POST",
            dataType: 'json',
            data: {
                key : keyss
                },
            success: function(result){

            }
        });
  });
  $('.sorting').sortable({
        axis: 'y',
        stop: function (event, ui) {
	        var data = $(this).sortable('serialize');
             $.ajax({
                    data: data,
                type: 'POST',
                url:  base_url +'cms/category/sorting'
            }); 
	}
    });
  $('.sorting3').sortable({
        axis: 'x',
        stop: function (event, ui) {
            var data =  $(this).sortable('serialize');
             $.ajax({
                    data: data,
                type: 'POST',
                url:  base_url +'cms/machine/imgSorting'

            }); 
    }
    });