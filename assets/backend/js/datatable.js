 $(document).ready(function() {
        //alert(DataTable_German_Language);
        $('#datatables,#datatables-1').DataTable({
            "pagingType": "full_numbers",
            "lengthMenu": [
                [10, 25, 50, -1],
                [10, 25, 50, "All"]
            ],
            responsive: true,
             language: {
                "url": DataTable_Language,
                "decimal": ',',
                "thousands": '.',
            },

        });


        var table = $('#datatables').DataTable();

        

        $('.card .material-datatables label').addClass('form-group');
    });