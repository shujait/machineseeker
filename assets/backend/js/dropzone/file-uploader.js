var media_library_url = base_url+'cms/page/media_library';
$(document).ready(function(){	
// closing the box
$('body').on('click', '.mlib-close', function(){
$('#mlib-lightbox').remove();
});

$('body').on('click', '.mlib-thumbs', function (e){
$(".mlib-thumbs").removeClass('mlib-selected-thumb');	
$(this).addClass("mlib-selected-thumb");
$('.mlib-how-many').html('<div style="left:427px; position: relative; bottom: 9px;"></div><div class="mlib-how-many-text"><div style="float: right; margin: 10px;" class="mlib-button insert_image">Insert into the page</div><div class="loading" style="right:340px;"></div></div>');

var mhtml = '<div class="mlib-single-edit">\
<img src=' + $(this).attr('mlib-url') + ' style="max-width: 50%" /><br >\
<span><strong>'+$(this).attr('mlib-filename')+'</strong></span>\
<span>'+$(this).attr('mlib-time')+'</span>\
<span>'+$(this).attr('mlib-size')+'</span>\
<span>'+$(this).attr('mlib-width')+'px x '+$(this).attr('mlib-height')+'px</span>\
<span>URL</span>\
<input type="text" readonly value="'+$(this).attr('mlib-url')+'"><br />\
<div  class="mlib-delete-all" onclick="img_delete();">Delete image</div>\
</div>';

$('.mlib-item-properties').html(mhtml);
if($(".mlib-gallery-li").hasClass("mlib-li-active") == true){
		
		$(".mlib-delete-all").hide();
	}else{
		$(".mlib-delete-all").show();
	}
});
});

//show popup
function show_popup(){
 	$('#mlib-lightbox,.dz-hidden-input').remove();	
	var html = '<div id="mlib-lightbox"><div class="mlib-bg"></div>\
<div class="mlib-main">\
<ul class="mlib-left">';
html += "<li class=\"mlib-upload-li mlib-li-active\" onclick=\"show_popup();\">Upload Files</li>";
html +='<li class="mlib-media-li" onclick="load_gallery_data_advanced();">Media Library</li>\
</ul>\
<div class="mlib-right">\
<div class="mlib-contents">\
<div class="mlib-data mlib-upload-tab" style="display:block;">\
<div class="mlib-top"><div class="mlib-head">Add Media</div><div class="mlib-close">X</div></div>\
<div class="fallback"><div class="mlib_fallback_iframe"><div class="upload_file">\
<div id="msg"></div>\
<!--<div class="img_size">Max size: 2MB</div>-->\
<form  action="'+media_library_url+'">\
<h2 class="drop-instructions">Drop files anywhere to upload</h2>\
<p class="drop-instructions">or</p>\
<button type="button" class="dropzone">Select Files</button>\
</form>\
</div>\
</div></div>\
</div>\
<div class="mlib-data mlib-media-tab">\
<div class="mlib-top"><div class="mlib-head">Media Library</div><div class="mlib-close">X</div></div>\
<div class="mlib-bottom"><div class="mlib-how-many"></div></div>\
<div class="mlib-display-canvas"><div class="loading"></div></div>\
<div class="mlib-item-properties"></div>\
</div>\
</div>\
</div>\
</div>';
	
	$("body").append(html);
	
	var ele  = false;
    var myDropzone = new Dropzone(".dropzone",{
	dictDefaultMessage:"",  url: media_library_url,maxFilesize:2,dictMaxFilesExceeded:"2 Mb",uploadMultiple:true,
	acceptedFiles: "image/jpeg,image/png,image/jpg",
	 init: function() {
        this.on("success", function(file, rec) {
		 if(rec  == 1){
		alert('The file type you are uploading is not supported, Please only upload Jpeg and png file types.');	
		$(".dz-preview").fadeOut(2000);
		ele =  false;
		
		}else{
		 ele =  true;
		}
			
        });
	}
	}
	
	);



myDropzone.on("queuecomplete", function(file) {
	if(ele == true){  
    if ( myDropzone.files[0].status == Dropzone.SUCCESS ) {
		 load_gallery_data_advanced();	
			} 
	     }
		 $(".dz-error-mark").click(function(){
			myDropzone.removeFile(myDropzone.files[0]);
			});
        });
}
//show all image
function load_gallery_data_advanced(){
$(".imgareaselect-outer").remove();
	$(".imgareaselect-border1").parent().remove();
	$('.mlib-close').trigger('click');	
    show_popup();
    $('.mlib-data').hide();
	$('.mlib-left li').removeClass('mlib-li-active');
	$(".mlib-media-li").addClass('mlib-li-active');
    $('.mlib-media-tab,.loading').show();
$.post( media_library_url, 
{func:'load_thumbs'},
function(data){
var xdata = jQuery.parseJSON(data);

var xstr = '';
for(var i=0;i<parseInt(xdata.total);++i){
	
xstr = xstr+'<div mlib-id="'+xdata[i].id+'" mlib-size="'+xdata[i].size+'" mlib-time="'+xdata[i].created_date+'" mlib-type="'+xdata[i].type+'" mlib-url="'+xdata[i].url+'"  mlib-width="'+xdata[i].w+'" mlib-height="'+xdata[i].h+'"  mlib-filename="'+xdata[i].filename+'" mlib-filepath="'+xdata[i].filepath+'" class="mlib-thumbs" style="background-image:url(\''+xdata[i].url+'\');background-size: contain;">\
<input type="checkbox" name="img['+xdata[i].url+']">\
<div class="mlib-checkbox"></div></div>';

}
if(xstr==''){
xstr = '<p style="padding:0 10px;"><b>Sorry!</b> There are no images to show.</p>';
}

$( ".mlib-display-canvas" ).html( xstr);
});
}

//delete images
function img_delete (){
 $(".loading").show();
var mid = $(".mlib-selected-thumb").attr('mlib-filepath');
$.post( media_library_url, {
func:'del_thumbs',image_path:mid,image_id:$(".mlib-selected-thumb").attr('mlib-id')
},function( data ) {
	var xdata = jQuery.parseJSON(data);
if(xdata['error'] == false){
$(".mlib-selected-thumb").remove();
  $(".loading").hide();
}
});
}