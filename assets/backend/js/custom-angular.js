 var getUrl = window.location;
if(getUrl.host == "localhost"){
 var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+'/';
}else{
 var baseUrl = getUrl .protocol + "//" + getUrl.host + "/";
}
var app = angular.module('App', []);
	app.filter('spaceless',function() {
    return function(input) {
        if (input) {
			input = input.htmlProtect();
//			var ingredients = ['ä' ,'ü','ö', 'ß'];
//
//			ingredients = ingredients.map(ingredient => {
//			  return ingredient.replace('Avengers', 'Revengers');
//			});
//
//			console.log(ingredients);
			
            return input.replace(/ /g, '-');    
			}
		}
	});
app.controller('CustomController', function ($scope, $log,$http,$filter,$timeout) {

	 $scope.date = $filter('date')(new Date(), 'dd-MM-yyyy');
	 $scope.items = [];
	 $scope.add = function() {
        $scope.items.push($scope.items.length+1);
		  $timeout(function () {
    	angular.element(".selectpicker").selectpicker();
		$('.number').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (!regex.test(str)) {
            return true;
        }
        else
        {
        e.preventDefault();
        return false;
        }
    });			  
    },5);
    };
	 $scope.del = function(i){
    $scope.items.splice(i,1);
  	};
	 $scope.del_row = function(i){
	angular.element( document.querySelector( "#row-"+i ) ).remove();
  	};
});