var map;

geocoder = new google.maps.Geocoder();

var lan = new google.maps.LatLng(52.520008, 13.404954);

function codeAddress() {

    var mapOptions = {

      zoom: 8,

      center: lan,

      mapTypeId: google.maps.MapTypeId.ROADMAP

    };

	  map = new google.maps.Map(document.getElementById('address-map-canvas'), mapOptions);

	    marker = new google.maps.Marker({

                    map: map,

                    position: lan,

                    draggable: true 

                });   
     
    var address = document.getElementById("address").innerHTML;

	  if(address != ""){

    geocoder.geocode( { 'address': address}, function(results, status) {

      if (status == google.maps.GeocoderStatus.OK) {

        var fromAddressLat = results[0].geometry.location.lat();

        var fromAddressLng = results[0].geometry.location.lng();

        var lan = new google.maps.LatLng(fromAddressLat, fromAddressLng);

		 

        map.panTo(lan);

		   marker = new google.maps.Marker({

                    map: map,

                    position: lan,

                    draggable: true 

                }); 

		  google.maps.event.addListener(marker, 'dragend', function() {



				geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {

				if (status == google.maps.GeocoderStatus.OK) {

				if (results[0]) {

				$('#address').val(results[0].formatted_address);

						   }

						}

					});

				});  

      } else {

        console.log("Geocode was not successful for the following reason: " + status);

      }

    });

	  }
  } 

google.maps.event.addDomListener(window, 'load', codeAddress);