$(document).ready(function () {

    $(".nettoinfo2").hide();
    $('#tarif-details').on('show.bs.collapse', function () {
        $('html, body').animate({
            scrollTop: $("#tarif-details-scroll").offset().top
        }, 'fast');
        $('.show-tarif-details').removeAttr('data-toggle');
    });

    $(".button-tarifdetails").on("click", function (event) {
        event.preventDefault();
        $(".pane-tarifdetails").fadeOut(400, function () {
            $("#tarifdetails").slideDown(400);
        });
    });

    $('#noch-fragen').on('click', function () {
        $('#telefon-call').slideDown();
        $('#noch-fragen').addClass('disabled');
    });

    $("#ButtonGeldZurueck").mouseover(function () {
        setTimeoutConst = setTimeout(function () {
            $("#GeldZurueck").modal('show');
        }, 200);
    });

    $(".trustoverlay").mouseover(function () {
        setTimeoutConst = setTimeout(function () {
            $("#TrustOverlay2").modal('show');
        }, 200);
    });

    $("#ButtonGeldZurueck, #ButtonTrustOverlay2").mouseleave(function () {
        clearTimeout(setTimeoutConst);
    });


    popoverInitialisieren();

    if ($.cookie("gesperrt") != null) {
        $("#Neuwahl").modal("show");
    }


    //Desktop Version Tarifseite
    //Vorauswahl
    var url = window.location.href;
    if (url.indexOf("#Tarif1") >= 0) {
        checkTarif("checkbox");
    } else if (url.indexOf("#Tarif2") >= 0) {
        checkTarif("checkbox2");
    } else if (url.indexOf("#Tarif3") >= 0) {
        checkTarif("checkbox3");
    } else if (url.indexOf("#Tarif4") >= 0) {
        checkTarif("checkbox4");
    } else {
        checkTarif("checkbox2");
    }
    $(".checkit").click(function (event) {
        var name = $(this).find("input").attr("id");
        if ($("#" + name).is(':checked')) {
        } else {
            checkTarif(name);
        }
        event.preventDefault();
        return false;
    });


    //Mobile Version Tarifseite. Checkboxen
    //Vorauswahl
    $("#checkboxTarifAuswahl1").prop('checked', true);
    $(".bx-wrapper").show();
    $(".tabelleeinmal").hide();

    $(".checkit2").click(function (event) {
        $(".tarifumblaettern").prop('checked', false);
        var name = $(this).find("input").attr("id");
        if ($("#" + name).is(':checked')) {
            $("#" + name).prop('checked', false);
        } else {
            $("#" + name).prop('checked', true);
        }
        event.preventDefault();
        return false;
    });


    $("#CheckboxHaendler").click(function () {
        $("#PreislisteMobilContainer .bx-wrapper").show();
        $(".tabelleeinmal").hide();
    });
    $("#CheckboxNormal").click(function () {
        $("#PreislisteMobilContainer .bx-wrapper").hide();
        $(".tabelleeinmal").show();
    });


    var previousAbverkauf;
    $(".mediumAbverkaufSelect").on('focus', function () {
        // Store the current value on focus and on change
        previousAbverkauf = this.value;
    }).change(function () {
        // Do something with the previous value after the change
        $('.mediumAbverkauf'+previousAbverkauf).attr("hidden","");
        $('.mediumAbverkauf'+this.value).removeAttr("hidden");

        $('.rabattabverkauf'+previousAbverkauf).attr("hidden","");
        $('.rabattabverkauf'+this.value).removeAttr("hidden");

        var action = $("#w3").attr('action');
        if(action != null) {
            action = action.replace(/\d+$/, this.value);
            $("#w7").attr('action', action);
            $("#w3").attr('action', action);
        }

        $(".max-ads-Tarif4").val(this.value);
        $(".mediumAbverkaufSelect").val(this.value);
        $(".max-ads-abverkauf-reactivate").val(this.value);



        // Make sure the previous value is updated
        previousAbverkauf = this.value;
    });

    var previousPro;
    $(".proTarifSelect").on('focus', function () {
        // Store the current value on focus and on change
        previousPro = this.value;
    }).change(function () {
        // Do something with the previous value after the change
        $('.proTarif'+previousPro).attr("hidden","");
        $('.proTarif'+this.value).removeAttr("hidden");

        $('.rabattpro'+previousPro).attr("hidden","");
        $('.rabattpro'+this.value).removeAttr("hidden");

        var action = $("#w0").attr('action');
        if(action != null) {
            action = action.replace(/\d+$/, this.value);
            $("#w0").attr('action', action);
            $("#w5").attr('action', action);
        }
        $(".max-ads-Tarif1").val(this.value);
        $(".proTarifSelect").val(this.value);
        $(".max-ads-pro-reactivate").val(this.value);
        console.log(this.value);

        // Make sure the previous value is updated
        previousPro = this.value;
    });



});

$(window).on('load', function() {
    $(".checkthis").click();
});

function popoverInitialisieren() {
    $.each(
        $('[data-toggle="popover"]'),
        function (index, value) {
            $(this).popover(
                {
                    html: true,
                    content: ladePopoverInhalt($(this)),
                    container: 'body',
                    title: '<button type="button" id="close" class="close" onclick="$(&quot;#example&quot;).popover(&quot;hide&quot;);">&times;</button>',
                }
            )
        }
    );
}

function ladePopoverInhalt(caller) {
    var idContainer = caller.data('inhalt-id');

    if (!idContainer)
        return;

    return $('#' + idContainer).html();
}


function checkTarif(name) {

    $(".checkit").find("input").prop('checked', false);
    $(".notcheckedin").hide();

    $("#" + name).prop('checked', true);
    $("#form" + name).show();
    if(name==="checkbox4"){
        $(".nettoinfo1").hide();
        $(".nettoinfo2").show();
    } else {
        $(".nettoinfo1").show();
        $(".nettoinfo2").hide();
    }

    $(".letzterow").find("td:eq(1)").css({
        'border-radius': '0px 0px 0px 10px',
        'background-color': '#DCDFE6',
        'color': '#337ab7'
    });
    $(".letzterow").find("td:eq(2)").css({
        'border-radius': '0px 0px 0px 0px',
        'background-color': '#DCDFE6',
        'color': '#337ab7'
    });
    $(".letzterow").find("td:eq(3)").css({
        'border-radius': '0px 0px 10px 0px',
        'background-color': '#DCDFE6',
        'color': '#337ab7'
    });
    $(".letzterow").find("td:eq(5)").css({
        'border-radius': '0px 0px 10px 10px',
        'background-color': '#DCDFE6',
        'color': '#337ab7'
    });

    $(".letzterow").find("td:eq(1) span").css({
        'color': '#337ab7'
    });
    $(".letzterow").find("td:eq(2) span").css({
        'color': '#337ab7'
    });
    $(".letzterow").find("td:eq(3) span").css({
        'color': '#337ab7'
    });
    $(".letzterow").find("td:eq(5) span").css({
        'color': '#337ab7'
    });


    if (name === "checkbox2") {
        $(".close-column.pro").css({
            'background-color': '#3F81BB',
            'border-color': 'white'
        });
    } else {
        $(".close-column").css({
            'background-color': 'white',
            'border-color': 'white'
        });
    }

    $(".movecheck").hide();
    $("#form" + name).parent().find(".movecheck").show();


    $("#" + name).closest("td").css({
        'border-radius': '0px 0px 0px 0px',
        'background-color': '#5CB85C'
    });
    $("#" + name).parent().find("span").css({
        'color': 'white'
    });
}

