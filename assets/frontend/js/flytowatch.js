$(document).ready(function () {
    /*$('a.merken').click(function (event) {
        var link = $(this);
        event.stopPropagation();
        event.preventDefault();
        var url = link.attr('href');
        $.get(url).always(function (response) {
            if (response.status == 302) {
                merkenUmschalten(link);
            }
        });
        return false;
    });*/
});

function merkenUmschalten (link) {
  if ( link.hasClass('gemerkt') ) {
    $(link).removeClass('gemerkt');
    merklisteAddieren (-1)
  } else {
    $(link).addClass('gemerkt');
    flyingStarAnimation(link);
  }
  merkenTooltipUmschalten(link);
}

function merkenTooltipUmschalten(link) {
  var a = link.attr("data-original-title");
  var b = link.attr("data-alt-title");
  link.attr("data-original-title", b);
  link.attr("data-alt-title", a);
}

function flyingStarAnimation (link) {
  var offsetZiel = $("#gemerkte").offset();
  var offsetStart = link.offset();
  var relX =  offsetZiel.left - offsetStart.left;
  var relY =  offsetZiel.top - offsetStart.top;
  $('body').append('<div id="flying-star">');
  $("#flying-star").offset({ top: offsetStart.top, left: offsetStart.left});

  $("#flying-star" ).animate({
      left: "+=" + relX,
      top: "+=" + relY,
      opacity: ".2"
  }, 700, function() {
       $("#flying-star" ).remove();
       merklisteAddieren (+1);
  }).always;
}

function merklisteAddieren (wert) {
  var tag = $("#gemerkte");
  var count = parseInt( tag.text() );
  wert = parseInt(wert);
  tag.text(String(count + wert));  
}