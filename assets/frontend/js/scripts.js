(function ($) {
    'use strict'

    $(document).ready(function () {
        var autocomplete_str = ['MachineType', 'Manufacturer', 'Model', 'SerialNumber', 'ReferenceNumber', 'Title'];
        $(window)
        var options = {
            title: cookie_heading,
            message: cookie_description,
            delay: 600,
            expires: 1,
            link: base_url + 'datenschutz',
            onAccept: function () {
                var myPreferences = $.fn.ihavecookies.cookie();
            },
            uncheckBoxes: true,
            acceptBtnLabel: cookie_accpet_btn,
            moreInfoLabel: read_more,
            fixedCookieTypeLabel: 'Essential',
            fixedCookieTypeDesc: 'These are essential for the website to work correctly.'
        }

        $(document).ready(function () {
            $('body').ihavecookies(options);

            $('#ihavecookiesBtn').on('click', function () {
                $('body').ihavecookies(options, 'reinit');
            });

            $(document).on('contextmenu', 'img', function () {
                return false;
            })


        });



        $(".add-category").click(function () {
            $(".cat-title").text($(".category:checked").data('title'));
            $("input[name='category']").val($(".category:checked").val());
            $(".closed").click();
        });

        $(".mark-all").on("click", function () {
            $('#WatchProList input:checkbox').prop('checked', true);
            $(this).val('check all');
        });
        $(".unmark-all").on("click", function () {
            $('#WatchProList input:checkbox').prop('checked', false);
            $(this).val('check all');
        });



        $("#Dealers").change(function () {
            $(".dealers").show();
            if ($(this).val() == 0) {
                $(".dealers").hide();
            }
        });

        $(".inquiry").on("click", function () {
            var id = $(this).data("id");
            $("input[name='machine_id']").val(id);
        });
        $(".close-similar").on("click", function () {
            $("#modal-similarMachines,#similarMachines").prop('checked', false);
        });
        $(".selectAll").on("click", function () {
            var id = $(this).attr("id");
            $('#categoryBlock-' + id + ' input:checkbox').prop('checked', true);
            $(this).val('check all');
        });
        $(".deselectAll").on("click", function () {
            var id = $(this).attr("id");
            $('#categoryBlock-' + id + ' input:checkbox').prop('checked', false);
            $(this).val('check all');
        });

        /**/
        $("#DNShowDealerProfile").click(function () {
            if ($(this).attr('checked', 'checked')) {
                $(".showHideForm").hide();
            }
        });
        $("#ShowDealerProfile").click(function () {
            if ($(this).attr('checked', 'checked')) {
                $(".showHideForm").show();
            }
        });
        $('.popover-dismiss').popover({
            trigger: 'focus'
        });
        // On load

        $('#langSwitcher').change(function () { // on change event
            $('#langForm').submit();
        });

        $(document).on('click', '.wishlist', function () {
            blockUi();
            $.ajax({
                type: "POST",
                url: base_url + "add-wishlist",
                data: {
                    machine_id: jQuery(this).data("id")
                },
                dataType: "json",
                success: function (result) {
                    if (result.error != false) {
                        showError(result.error);
                    } else {
                        showSuccess(result.success);
                    }
                    if (result.reload) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    if (result.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + result.url;
                        }, 1000);
                    }
                },
                complete: function () {
                    $.unblockUI();
                }
            });

        });

        $(document).on('click', '.remove-all-wishlist', function () {
            //blockUi();
            $('#WatchProList input:checkbox').prop('checked', true);
            var ids = [];
            $('#WatchProList input[type=checkbox]:checked').each(function () {
                console.log(this.value);
                ids.push(this.value);
            });

            $.ajax({
                type: "POST",
                url: base_url + "remove-wishlist",
                data: {
                    machine_id: ids
                },
                dataType: "json",
                success: function (result) {
                    if (result.error != false) {
                        showError(result.error);
                    } else {
                        showSuccess(result.success);
                    }
                    if (result.reload) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    if (result.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + result.url;
                        }, 1000);
                    }
                },
                complete: function () {
                    $.unblockUI();
                }
            });

        });

        $('ul.category-list').each(function () {
            var LiN = $(this).find('li').length;
            var limit = 20;
            if (LiN > limit) {
                $('li', this).eq(limit).nextAll().addClass('toggleable hide');
            }
        });

        $('.more-listings').on('click', function () {
            if ($('li').hasClass('hide')) {
                $(".more-listings .text").text(show_less);
                $('li.toggleable').removeClass('hide');
            } else {
                $(".more-listings .text").text(show_all);
                $('li.toggleable').addClass('hide');
            }

        });

        $(window).bind("load", function () {
            $(".gallery div:first-child").removeClass('d-none').addClass('img-thumbnail post-img');
            // $(".plan").trigger("change");
            jQuery.each(autocomplete_str, (index, item) => {
                $("#" + item).autocomplete({
                    source: function (request, response) {
                        jQuery.getJSON(base_url + "autocompletes", {
                                type: item,
                                term: jQuery("#" + item).val()
                            },
                            response);
                    }
                });
            });

        });
        $(".plan").change(function () {
            var id = $(this).val();
            var discount = $(this).find(':selected').data('discount');
            var duration = $(this).find(':selected').data('duration');
            var price = $(this).find(':selected').data('price');
            var qty = $(this).find(':selected').data('qty');
            var newprice = price - (price / 100) * discount;
            var discount_incl = $(this).find(':selected').data('discount_incl');

            var discount_date = $(this).find(':selected').data('discount_date');
            // $("#duration_" + id).text(duration);


            if (discount != 0) {

                $("#discount_" + id).text(discount + '%');
                $("#discount_next_" + id).html(discount_incl);
                $("#discount_date_" + id).html(discount_date);
                $("#oldprice_" + id).text(formatMoney(price, NumberDecimals, DecimalSeparator, ThousandSeparator, Currency, CurrencyPosition));

            } else {
                $("#discount_" + id).text('');
                $("#discount_next_" + id).text('');
                $("#discount_date_" + id).text('');
                $("#oldprice_" + id).text('');

            }

            $("#newprice_" + id).text(formatMoney(newprice, NumberDecimals, DecimalSeparator, ThousandSeparator, Currency, CurrencyPosition));
            $("#total_" + id).text(formatMoney(duration * newprice, NumberDecimals, DecimalSeparator, ThousandSeparator, Currency, CurrencyPosition));
            if (user_id == "") {
                $("input[name='plan_id']").val(id);
                $("input[name='plan_row']").val($(this).find(':selected').data('row'));
            } else {
                $("#package-" + id + " input[name='plan_id']").val(id);
                $("#package-" + id + " input[name='plan_row']").val($(this).find(':selected').data('row'));
            }


        });

        $(".select_plan").on('click', function () {
            var id = $(this).data('id');
            $("input[name='plan_id']").val(id);
            var row = 0;
            if(typeof $("[data-plan='" + id + "']").find(':selected').data('row') !== "undefined")
            {
                row = $("[data-plan='" + id + "']").find(':selected').data('row');
            }
            $("input[name='plan_row']").val(row);
        });
        $(".form_data").submit(function (e) {
            e.preventDefault();
            blockUi();

            var form = $(this);
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: new FormData(this),
                dataType: "json",
                cache: false,
                contentType: false,
                processData: false,
                //async:false,
                success: function (result) {
                    if (result.error != false) {
                        showError(result.error);
                    } else {
                        showSuccess(result.success);
                    }
                    if (result.reset) {
                        form[0].reset();
                    }
                    if (result.reload) {
                        setTimeout(function () {
                            window.location.reload();
                        }, 1000);
                    }
                    if (result.redirect) {
                        setTimeout(function () {
                            window.location.href = base_url + result.url;
                        }, 1000);
                    }
                },
                complete: function () {
                    $.unblockUI();
                }
            });
        });
        $(".toggle-btn-1").on("click", function () {
            if ($(".header .header-row-3").height() <= 68) {
                $(".header .header-row-3").animate({
                    height: "400px"
                });
            } else if ($(".header .header-row-3").height() >= 250) {
                $(".header .header-row-3").animate({
                    height: "60px"
                });
            }
        });
        $(".toggle-btn-2").on("click", function () {
            if ($(".header .header-row-4").height() <= 68) {
                $(".header .header-row-4").animate({
                    height: "400px"
                });
            } else if ($(".header .header-row-4").height() >= 250) {
                $(".header .header-row-4").animate({
                    height: "60px"
                });
            }
        });

        /**/
        $(".clients-carousel .owl-carousel").owlCarousel({
            loop: true,
            margin: 12,
            nav: true,
            pagination: false,
            navText: ['<i class="fa fa-caret-left" aria-hidden="true"></i>', '<i class="fa fa-caret-right" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 5
                }
            }
        });
        $(".pro-slider .owl-carousel").owlCarousel({
            loop: true,
            margin: 12,
            nav: true,
            pagination: false,
            navText: ['<i class="fa fa-caret-left" aria-hidden="true"></i>', '<i class="fa fa-caret-right" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });

        /**/
        /**/
        /*$('#zoom_01').elevateZoom();*/
        /**/

        // For each image with an SVG class, execute the following function.
        $("img.svg").each(function () {
            // Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
            var $img = jQuery(this);
            // Get all the attributes.
            var attributes = $img.prop("attributes");
            // Get the image's URL.
            var imgURL = $img.attr("src");
            // Fire an AJAX GET request to the URL.
            $.get(imgURL, function (data) {
                // The data you get includes the document type definition, which we don't need.
                // We are only interested in the <svg> tag inside that.
                var $svg = $(data).find('svg');
                // Remove any invalid XML tags as per http://validator.w3.org
                $svg = $svg.removeAttr('xmlns:a');
                // Loop through original image's attributes and apply on SVG
                $.each(attributes, function () {
                    $svg.attr(this.name, this.value);
                });
                // Replace image with new SVG
                $img.replaceWith($svg);
            });
        });
        /**/
        /**/
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 500,
            values: [75, 300],
            slide: function (event, ui) {
                $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        $("#amount").val("$" + $("#slider-range").slider("values", 0) +
            " - $" + $("#slider-range").slider("values", 1));


        $("#slider-range-1").slider({
            range: true,
            min: 0,
            max: 500,
            values: [75, 300],
            slide: function (event, ui) {
                $("#amount-1").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });
        $("#amount-1").val("$" + $("#slider-range-1").slider("values", 0) +
            " - $" + $("#slider-range-1").slider("values", 1));

        /**/

        /**/
        $('#qty_input').prop('disabled', true);
        $('#plus-btn').click(function () {
            $('#qty_input').val(parseInt($('#qty_input').val()) + 1);
        });
        $('#minus-btn').click(function () {
            $('#qty_input').val(parseInt($('#qty_input').val()) - 1);
            if ($('#qty_input').val() == 0) {
                $('#qty_input').val(1);
            }

        });
        /**/
        /**/

        // Configure/customize these variables.
        var showChar = 100; // How many characters are shown by default
        var ellipsestext = "...";
        var moretext = "Show more";
        var lesstext = "Show less";


        $('.more').each(function () {
            var content = $(this).html();

            if (content.length > showChar) {

                var c = content.substr(0, showChar);
                var h = content.substr(showChar, content.length - showChar);

                var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                $(this).html(html);
            }

        });

        $(".morelink").click(function () {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
            }
            $(this).parent().prev().toggle();
            $(this).prev().toggle();
            return false;
        });

        /**/
        /*$('.imgToCSS').jfImgToCSS({
            bgRepeat: 'no-repeat',
            bgSize: 'cover',
            bgPosition: 'top center',
            bgOrigin: 'padding-box',
            bgClip: 'border-box',
            bgAttachment: 'scroll',
            bgColor: 'transparent'
        });*/
        /**/

        /**/
        $('.cat-posts-sec .pro-rm-btn a').click(function () {
            $(this).find('i').toggleClass('fa-angle-up fa-angle-down');
        });
        /**/
        /**/
        $('#showMap').click(function () {
            $('.post-map').addClass('MapShow');
        });
        /**/

        /**/
        var FormStuff = {

            init: function () {
                this.applyConditionalRequired();
                this.bindUIActions();
            },

            bindUIActions: function () {
                $("input[type='radio'], input[type='checkbox']").on("change", this.applyConditionalRequired);
            },

            applyConditionalRequired: function () {

                $(".require-if-active").each(function () {
                    var el = $(this);
                    if ($(el.data("require-pair")).is(":checked")) {
                        el.prop("required", true);
                    } else {
                        el.prop("required", false);
                    }
                });

            }

        };

        FormStuff.init();
        /**/
        /**/
        $('.infoShowBlock').hide();
        $(document).on('click', '.dealer-info a', function (e) {
            e.preventDefault();
            $(this).next('.infoShowBlock').toggle();
        });
        /**/
        /**/
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
        /**/

        /*not working*/
        $("#MoneyBackModalBtn, #MoneyBackModalBtnMob").mouseover(function () {
            setTimeoutConst = setTimeout(function () {
                $("#MoneyBackModal").modal('show');
            }, 200);
        });
        /**/

        /**/
        /*$('.items').flyto({
            item      : '.merken',
            target    : '#watch-count',
            button    : '.merken'
        });*/

        /**/


        $(".print_page").click(function () {

            $(".print-hide").hide();

            var css = '<link href=' + base_url + 'assets/frontend/css/owl.carousel.min.css';

            css += ' rel="stylesheet" />';

            css += '<link href=' + base_url + 'assets/frontend/css/jquery-ui.css';

            css += ' rel="stylesheet" />';

            css += '<link href=' + base_url + 'assets/frontend/css/jcarousel.connected-carousels.css';

            css += ' rel="stylesheet" />';

            css += '<link href=' + base_url + 'assets/frontend/css/rrssb.css';

            css += ' rel="stylesheet" />';

            css += '<link href=' + base_url + 'assets/frontend/css/ltr.css?12';

            css += ' rel="stylesheet" />';

            css += '<link href=' + base_url + 'assets/frontend/css/responsive.css';

            css += ' rel="stylesheet" />';

            css += '<link href=' + base_url + 'assets/backend/css/google-roboto-300-700.css';

            css += ' rel="stylesheet" />';


            css += '<script src=' + base_url + 'assets/frontend/js/jquery-3.3.1.js type="text/javascript"></script>';

            css += '<script src=' + base_url + 'assets/frontend/js/popper.min.js type="text/javascript"></script>';


            css += '<script src=' + base_url + 'assets/frontend/js/bootstrap.min.js type="text/javascript"></script>';

            css += '<script src=' + base_url + 'assets/frontend/js/jquery-ui.js type="text/javascript"></script>';
            css += '<script src=' + base_url + 'assets/frontend/js/owl.carousel.min.js type="text/javascript"></script>';
            css += '<script src=' + base_url + 'assets/frontend/js/jquery.jcarousel.min.js type="text/javascript"></script>';
            css += '<script src=' + base_url + 'assets/frontend/js/jcarousel.connected-carousels.js type="text/javascript"></script>';
            css += '<script src=' + base_url + 'assets/frontend/js/rrssb.js type="text/javascript"></script>';
            css += '<script src=\'https://maps.google.com/maps/api/js?key=AIzaSyAucmpktHJd_g2JIAFsrgpwbYrl-PAKVdc\' type="text/javascript"></script>';
            css += '<script src=' + base_url + 'assets/frontend/js/map.js type="text/javascript"></script>';

            var title = $(".b-detail__head-title").text();

            var contents = $(".pd-section").html();

            var frame1 = $('<iframe />');

            frame1[0].name = "frame1";

            frame1.css({
                "position": "absolute",
                "top": "-1000000px"
            });

            $("body").append(frame1);

            var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;

            frameDoc.document.open();

            //Create a new HTML document.

            frameDoc.document.write('<html><head><title>' + title + '</title>' + css);

            frameDoc.document.write('</head><body><main class="main"><section class="pd-section">');

            //Append the DIV contents.

            frameDoc.document.write(contents);

            frameDoc.document.write('</scetion></main></body></html>');

            console.log('<html><head><title>' + title + '</title>' + css + '</head><body><main class="main"><section class="pd-section">' + contents + '</scetion></main></body></html>');
            frameDoc.document.close();

            setTimeout(function () {

                window.frames["frame1"].focus();

                window.frames["frame1"].print();

                frame1.remove();

                $(".print-hide").show();

            }, 500);

        });

    });
}(jQuery));

// Sub Category structure

$(document).on('change', '.getSubCategory', function () {
    var curr = $(this);
    var CategoryID = curr.val();
    var CategoryType = curr.attr('data-type');
    var PageType = curr.attr('data-page-type');
    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

    $.ajax({
        type: "POST",
        url: base_url + 'page/getSubCategory',
        data: {
            'CategoryID': CategoryID,
            'CategoryType': CategoryType,
            'PageType': PageType
        },
        dataType: "json",
        cache: false,
        //async:false,
        success: function (result) {


            if (result.success != 'false') {
                //alert('There is something went wrong');
                if (CategoryType == 'category') {
                    $('#subCategoryDiv').html('');
                    $('#subSubCategoryDiv').html('');
                    curr.removeAttr('id');
                    curr.removeAttr('name');
                    $('#subCategoryDiv').html(result.option);
                    $('#subCategoryDiv').find('select#category').addClass('getSubCategory');
                    if (result.option == '') {
                        curr.attr('id', 'category');
                        curr.attr('name', 'CategoryID');
                    }
                    $('.select2').select2();
                } else if (CategoryType == 'subcategory') {
                    $('#subSubCategoryDiv').html('');
                    curr.removeAttr('id');
                    curr.removeAttr('name');
                    $('#subSubCategoryDiv').html(result.option);
                    if (result.option == '') {
                        curr.attr('id', 'category');
                        curr.attr('name', 'CategoryID');
                    }
                    $('.select2').select2();
                }

            }


        },
        complete: function () {
            $.unblockUI();
        }
    });
});

function blockUi() {

    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });
}

function showSuccess(message_text) {
    type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">' + message_text + '</span>'

    }, {
        type: type[2], // 2nd index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function showError(message_text) {
    type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];
    $.notify({
        icon: "notifications",
        message: '<span style="font-size: 17px;">' + message_text + '</span>'

    }, {
        type: type[4], // 4th index from type array above
        timer: 3000,
        placement: {
            from: 'bottom',
            align: 'right'
        }
    });
}

function deleteRecord(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    if (confirm(delete_msg)) {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });

        $.ajax({
            type: "POST",
            url: base_url + '' + actionUrl,
            data: {
                'id': id,
                'form_type': 'delete'
            },
            dataType: "json",
            cache: false,
            //async:false,
            success: function (result) {

                if (result.error != false) {
                    showError(result.error);
                } else {
                    showSuccess(result.success);
                    $('#' + id).remove();
                }


                if (reloadUrl != "") setTimeout(function () {
                    document.location.href = reloadUrl;
                }, 1000);

            },
            complete: function () {
                $.unblockUI();
            }
        });
        return true;
    } else {
        return false;
    }

}

function publishRecord(id, actionUrl, reloadUrl) {

    //id can contain comma separated ids too.

    $.blockUI({
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        }
    });

    $.ajax({
        type: "POST",
        url: base_url + '' + actionUrl,
        data: {
            'id': id,
            'form_type': 'publish'
        },
        dataType: "json",
        cache: false,
        //async:false,
        success: function (result) {
            if (result.error != false) {
                showError(result.error);
            } else {
                showSuccess(result.success);
                document.location.href = base_url + '' + result.url;
            }

        },
        complete: function () {
            $.unblockUI();
        }
    });
    return true;


}

function formatMoney(n, c, d, t, cur, pos) {
    var left = right = '';
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;

    if (pos == 1) {
        left = cur;
    } else if (pos == 3) {
        left = cur + ' ';
    }
    if (pos == 2) {
        right = cur;
    } else if (pos == 4) {
        right = ' ' + cur;
    }
    return left + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + right;
};

//  function initMap() {
//         var input = document.getElementById('StreetAddress');

//         var autocomplete = new google.maps.places.Autocomplete(input);

//         // Set the data fields to return when the user selects a place.
//        autocomplete.setFields(
//             ['address_components', 'geometry', 'icon', 'name']);
// 		google.maps.event.addListener(autocomplete, 'place_changed', function () {
//             var place = autocomplete.getPlace();
// 			var address_components = place.address_components;
// 			for (var i = 0; i < address_components.length; i++) {
// 			var addressType = address_components[i].types[0];
// 			  if(addressType=='postal_code')
// 			  {
// 				  $("#postcodeTown").val(address_components[i]['long_name']);
// 			  }

// 			  }
// 			var lat = place.geometry.location.lat();
// 			var lng = place.geometry.location.lng();
//         });   

//       }



var searchRequest = null;
$(function () {
    var minlength = 3;
    $('#sTitle').bind('keyup change', function () {

        if (this.value.length > 0) {
            $('#dropdownhtml').show();
        } else {
            $('#dropdownhtml').hide();
        }

        var that = this,
            value = $(this).val();
        if (value.length >= minlength) {
            if (searchRequest != null)
                searchRequest.abort();
            searchRequest = $.ajax({
                type: "POST",
                url: base_url + 'search/search',
                data: {
                    'search': value
                },
                dataType: "json",
                success: function (msg) {
                    console.log(msg);
                    if (msg.code == '01') {
                        if (msg.html != '') {
                            $('#dropdownhtml').html(msg.html);
                        } else {
                            $('#dropdownhtml').html('');
                        }
                        $(".liClick").click(function () {
                            $("#sTitle").val($(this).html());
                            $("#sform").submit();
                        });
                    } else {

                    }
                }
            });
        }
    });
});



$('.dropdown-menu a.dropdown-o').on('click', function (e) {
    if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
    }
    var $subMenu = $(this).next('.dropdown-menu');
    $subMenu.toggleClass('show');


    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
        $('.dropdown-submenu .show').removeClass('show');
    });

    return false;
});
